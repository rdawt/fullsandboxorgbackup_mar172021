/*  
    Author: Wilson Ng
    Date: 7/02/12
    Desc: Opportunity Consultant Trigger Test class for creating new Partner record when new Opportunity_Consultant__c records are inserted or updated.
*/
@isTest
private class OpportunityConsultantTriggerTest {

    static testMethod void myUnitTest() {
        // create dummy data
        Account[] acctList = new list<Account>();
        acctList.add(new Account(Name='test acct 1', channel__c = 'Institutional', BillingCountry = 'USA'));
        acctList.add(new Account(Name='test acct 2', channel__c = 'Institutional', BillingCountry = 'USA'));
        acctList.add(new Account(Name='test acct 3', channel__c = 'Institutional', BillingCountry = 'USA'));
        insert acctList;
        Opportunity[] opptyList = new list<Opportunity>();
        opptyList.add(new Opportunity(Name='test acct 1 - oppty 1', StageName='Prospecting', CloseDate=(system.today()+1), AccountId=acctList[0].Id));
        opptyList.add(new Opportunity(Name='test acct 1 - oppty 2', StageName='Prospecting', CloseDate=(system.today()+1), AccountId=acctList[0].Id));
        opptyList.add(new Opportunity(Name='test acct 2 - oppty 1', StageName='Prospecting', CloseDate=(system.today()+1), AccountId=acctList[1].Id));
        insert opptyList;
        
        // start tests
        Test.startTest();
        
        // insert new opportunity consultant record
        Opportunity_Consultant__c[] ocList = new list<Opportunity_Consultant__c>();
        ocList.add(new Opportunity_Consultant__c(Firm_Name__c=acctList[2].Id, Opportunity__c=opptyList[0].Id));
        ocList.add(new Opportunity_Consultant__c(Firm_Name__c=acctList[2].Id, Opportunity__c=opptyList[1].Id));
        ocList.add(new Opportunity_Consultant__c(Firm_Name__c=acctList[2].Id, Opportunity__c=opptyList[2].Id));
        insert ocList;
        // assert results
        list<Partner> pList = [select AccountFromId, AccountToId from Partner where AccountToId=:acctList[2].Id];
        system.assertEquals(2, pList.size(), 'Error: Partner records was properly not inserted during opportunity_consultant inserts.');
        
        Test.stopTest();
        // stop tests
    }
}