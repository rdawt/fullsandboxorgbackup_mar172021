@isTest
public with sharing class PortfolioChannelRollupController_Test {

    @TestSetup
    public static void createTestData()
    {
        Fund__c moo = new Fund__c(Name='Agribusiness ETF (MOO)',BU_Region__c = 'USA' ,Fund_Vehicle_Type__c = 'ETF' , Fund_Type__c='FI-ETF',Fund_Ticker_Symbol__c='MOO');
        insert moo; 
        Fund__c angl = new Fund__c(Name='Fallen Angel High Yield Bond ETF (ANGL)',BU_Region__c = 'USA' ,Fund_Vehicle_Type__c = 'ETF',Fund_Type__c='FI-ETF',Fund_Ticker_Symbol__c='ANGL');
        insert angl; 

        Fund__c eme = new Fund__c(Name='VanEck Emerging Markets Fund (EME)',BU_Region__c = 'USA' ,Fund_Vehicle_Type__c = 'Mutual Fund' , Fund_Type__c='MF',Fund_Ticker_Symbol__c='EME');
        insert eme; 
        Fund__c iig = new Fund__c(Name='VanEck International Investors Gold (IIG)',BU_Region__c = 'USA' ,Fund_Vehicle_Type__c = 'Mutual Fund',Fund_Type__c='MF',Fund_Ticker_Symbol__c='IIG');
        insert iig; 


        List<Channel__c> lstChannels = new List<Channel__c>();
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='VE - Financial Advisor', Channel_Description__c='Financial Advisor', Status__c = 'Active',Channel_Type__c = 'SFDC'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='VE - Asset Manager', Channel_Description__c='Asset Manager', Status__c = 'Active',Channel_Type__c = 'SFDC'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='VE - Institutional', Channel_Description__c='Institutional', Status__c = 'Active',Channel_Type__c = 'SFDC'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='VE - RIA', Channel_Description__c='RIA', Status__c = 'Active',Channel_Type__c = 'SFDC'), false));

        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='FINANCIAL ADVISOR', Channel_Description__c='FINANCIAL ADVISOR', Status__c = 'Active',Channel_Type__c = 'Broadridge'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='ASSET MANAGER', Channel_Description__c='ASSET MANAGER', Status__c = 'Active',Channel_Type__c = 'Broadridge'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='INSTITUTIONAL', Channel_Description__c='INSTITUTIONAL', Status__c = 'Active',Channel_Type__c = 'Broadridge'), false));
        lstChannels.add((Channel__c)SL_TestDataFactory.createSObject(
            new Channel__c(Channel_Name__c='RIA', Channel_Description__c='RIA', Status__c = 'Active',Channel_Type__c = 'Broadridge'), false));

        insert lstChannels;
        
        Territory__c ter1 = new Territory__c(Channel__c=lstChannels[5].Id,Territory_Name__c = 'NORTHWEST' ,Status__c = 'Active' );
        insert ter1; 
        Territory__c ter2 = new Territory__c(Channel__c=lstChannels[5].Id,Territory_Name__c = 'MIDATLANTIC' ,Status__c = 'Active');
        insert ter2; 
        // Territory__c ter3 = new Territory__c(Channel__c=lstChannels[3].Id,Territory_Name__c = 'INSTITUTIONAL' ,Status__c = 'Active');
        // insert ter3; 

        List<Firm_Focus_Fund_Channel_Territory__c> lstFirmFF = new List<Firm_Focus_Fund_Channel_Territory__c>();
        lstFirmFF.add((Firm_Focus_Fund_Channel_Territory__c)SL_TestDataFactory.createSObject(
            new Firm_Focus_Fund_Channel_Territory__c(Net_Flows_YTD__c = 125000000,FundAssetBalance__c =250000000,Net_Flows_Last_Month__c=120000000, Fund__c = moo.Id, Asset_As_Of_Date__c = Date.newInstance(2020, 12, 31), Line_of_Business_Description__c='Financial Advisor',OfficeType__c='actual', country__c='united states',territory__c = 'NORTHWEST', DataView__c = 'Salesforce' ,Name = 'MOO'), false));
        lstFirmFF.add((Firm_Focus_Fund_Channel_Territory__c)SL_TestDataFactory.createSObject(
            new Firm_Focus_Fund_Channel_Territory__c(Net_Flows_YTD__c = 125000000,FundAssetBalance__c =250000000,Net_Flows_Last_Month__c=120000000, Fund__c = moo.Id, Asset_As_Of_Date__c = Date.newInstance(2020, 07, 30), Channel__c = 'INSTITUTIONAL' , Territory__c ='INSTITUTIONAL', DataView__c = 'Salesforce' ,Name = 'MOO'), false));

        lstFirmFF.add((Firm_Focus_Fund_Channel_Territory__c)SL_TestDataFactory.createSObject(
            new Firm_Focus_Fund_Channel_Territory__c(Net_Flows_YTD__c = 125000000,FundAssetBalance__c =250000000,Net_Flows_Last_Month__c=120000000, Fund__c = angl.Id,Channel__c = 'ASSET MANAGER' , Asset_As_Of_Date__c = Date.newInstance(2020, 05, 31),   DataView__c = 'Salesforce' ,Name = 'ANGL',Territory__c='MIDATLANTIC'), false));
        lstFirmFF.add((Firm_Focus_Fund_Channel_Territory__c)SL_TestDataFactory.createSObject(
            new Firm_Focus_Fund_Channel_Territory__c(Net_Flows_YTD__c = 125000000,FundAssetBalance__c =250000000,Net_Flows_Last_Month__c=120000000, Fund__c = angl.Id,Channel__c = 'RIA' , Territory__c = 'RIA West' , Asset_As_Of_Date__c = Date.newInstance(2020, 01, 31),   DataView__c = 'Salesforce' ,Name = 'ANGL'), false));

        insert lstFirmFF;

        List<Net_Sales_Data_For_Focus_Funds__c> lstNetSales = new List<Net_Sales_Data_For_Focus_Funds__c>();
        lstNetSales.add((Net_Sales_Data_For_Focus_Funds__c)SL_TestDataFactory.createSObject(
            new Net_Sales_Data_For_Focus_Funds__c(Net_Flows_YTD__c = 125000000,Net_Flow_Last_Month_Fidelity__c =250000000,Net_Flows_Last_Month__c=120000000, Channel__c = 'INSTITUTIONAL' , Asset_As_Of_Date__c = Date.newInstance(2020, 12, 31), Name = 'ANGL'), false));

        insert lstNetSales;
        
        List<Portfolio_Channel_Rollup_Quarterly__c> lstRollups = new List<Portfolio_Channel_Rollup_Quarterly__c>();
        lstRollups.add((Portfolio_Channel_Rollup_Quarterly__c)SL_TestDataFactory.createSObject(
            new Portfolio_Channel_Rollup_Quarterly__c(Fund_Vehicle_Type__c = 'ETF',Year__c ='2020',Channel__c=lstChannels[0].Id,Fund__c = angl.Id,
            Q1_AUM_BR__c = 1000000,Q1_AUM__c = 1100000,Q1_NetSales_BR__c = 1200000,Q1_NetSales_Last_Month_BR__c = 1300000,Q1_NetSales_Last_Month__c = 1100000,Q1_NetSales__c = 1500000,
            Q2_AUM_BR__c = 2000000,Q2_AUM__c = 2200000,Q2_NetSales_BR__c = 2200000,Q2_NetSales_Last_Month_BR__c = 2300000,Q2_NetSales_Last_Month__c = 2200000,Q2_NetSales__c = 2500000,
            Q3_AUM_BR__c = 3000000,Q3_AUM__c = 3300000,Q3_NetSales_BR__c = 3300000,Q3_NetSales_Last_Month_BR__c = 3300000,Q3_NetSales_Last_Month__c = 3300000,Q3_NetSales__c = 3500000,
            Q4_AUM_BR__c = 4000000,Q4_AUM__c = 4100000,Q4_NetSales_BR__c = 4200000,Q4_NetSales_Last_Month_BR__c = 4300000,Q4_NetSales_Last_Month__c = 4400000,Q4_NetSales__c = 4500000), false));
        lstRollups.add((Portfolio_Channel_Rollup_Quarterly__c)SL_TestDataFactory.createSObject(
            new Portfolio_Channel_Rollup_Quarterly__c(Fund_Vehicle_Type__c = 'ETF',Year__c ='2020',Channel__c=lstChannels[0].Id,Fund__c = moo.Id,
            Q1_AUM_BR__c = 1000000,Q1_AUM__c = 1100000,Q1_NetSales_BR__c = 1200000,Q1_NetSales_Last_Month_BR__c = 1300000,Q1_NetSales_Last_Month__c = 1100000,Q1_NetSales__c = 1500000,
            Q2_AUM_BR__c = 2000000,Q2_AUM__c = 2200000,Q2_NetSales_BR__c = 2200000,Q2_NetSales_Last_Month_BR__c = 2300000,Q2_NetSales_Last_Month__c = 2200000,Q2_NetSales__c = 2500000,
            Q3_AUM_BR__c = 3000000,Q3_AUM__c = 3300000,Q3_NetSales_BR__c = 3300000,Q3_NetSales_Last_Month_BR__c = 3300000,Q3_NetSales_Last_Month__c = 3300000,Q3_NetSales__c = 3500000,
            Q4_AUM_BR__c = 4000000,Q4_AUM__c = 4100000,Q4_NetSales_BR__c = 4200000,Q4_NetSales_Last_Month_BR__c = 4300000,Q4_NetSales_Last_Month__c = 4400000,Q4_NetSales__c = 4500000), false));

        lstRollups.add((Portfolio_Channel_Rollup_Quarterly__c)SL_TestDataFactory.createSObject(
            new Portfolio_Channel_Rollup_Quarterly__c(Fund_Vehicle_Type__c = 'Mutual Fund',Year__c ='2020',Channel__c=lstChannels[0].Id,Fund__c = eme.Id,
            Q1_AUM_BR__c = 1000000,Q1_AUM__c = 1100000,Q1_NetSales_BR__c = 1200000,Q1_NetSales_Last_Month_BR__c = 1300000,Q1_NetSales_Last_Month__c = 1100000,Q1_NetSales__c = 1500000,
            Q2_AUM_BR__c = 2000000,Q2_AUM__c = 2200000,Q2_NetSales_BR__c = 2200000,Q2_NetSales_Last_Month_BR__c = 2300000,Q2_NetSales_Last_Month__c = 2200000,Q2_NetSales__c = 2500000,
            Q3_AUM_BR__c = 3000000,Q3_AUM__c = 3300000,Q3_NetSales_BR__c = 3300000,Q3_NetSales_Last_Month_BR__c = 3300000,Q3_NetSales_Last_Month__c = 3300000,Q3_NetSales__c = 3500000,
            Q4_AUM_BR__c = 4000000,Q4_AUM__c = 4100000,Q4_NetSales_BR__c = 4200000,Q4_NetSales_Last_Month_BR__c = 4300000,Q4_NetSales_Last_Month__c = 4400000,Q4_NetSales__c = 4500000), false));
        lstRollups.add((Portfolio_Channel_Rollup_Quarterly__c)SL_TestDataFactory.createSObject(
            new Portfolio_Channel_Rollup_Quarterly__c(Fund_Vehicle_Type__c = 'Mutual Fund',Year__c ='2020',Channel__c=lstChannels[0].Id,Fund__c = iig.Id,
            Q1_AUM_BR__c = 1000000,Q1_AUM__c = 1100000,Q1_NetSales_BR__c = 1200000,Q1_NetSales_Last_Month_BR__c = 1300000,Q1_NetSales_Last_Month__c = 1100000,Q1_NetSales__c = 1500000,
            Q2_AUM_BR__c = 2000000,Q2_AUM__c = 2200000,Q2_NetSales_BR__c = 2200000,Q2_NetSales_Last_Month_BR__c = 2300000,Q2_NetSales_Last_Month__c = 2200000,Q2_NetSales__c = 2500000,
            Q3_AUM_BR__c = 3000000,Q3_AUM__c = 3300000,Q3_NetSales_BR__c = 3300000,Q3_NetSales_Last_Month_BR__c = 3300000,Q3_NetSales_Last_Month__c = 3300000,Q3_NetSales__c = 3500000,
            Q4_AUM_BR__c = 4000000,Q4_AUM__c = 4100000,Q4_NetSales_BR__c = 4200000,Q4_NetSales_Last_Month_BR__c = 4300000,Q4_NetSales_Last_Month__c = 4400000,Q4_NetSales__c = 4500000), false));

        insert lstRollups;
    }

   

    @isTest
    public static void  testGetUSSalesGoalsByPortfolio1() {
        Channel__c financialAdvisor = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Financial Advisor' limit 1];
        Channel__c assetManager = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Asset Manager' limit 1];
        Channel__c institutional = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Institutional' limit 1];
        Channel__c ria = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - RIA' limit 1];
        List<Id> lstFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'ETF'])
        {
            lstFundIds.add(fund.Id);
        }
        List<Id> lstMFFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'Mutual Fund'])
        {
            lstMFFundIds.add(fund.Id);
        }
        Test.startTest();
        PortfolioChannelRollupController.getFundTickers('ETF', false, '2020' , 1 , financialAdvisor.Id);
        PortfolioChannelRollupController.getFundTickers('ETF', true, '2020' , 1 , financialAdvisor.Id);
        PortfolioChannelRollupController.getFundTickers('ETF', true, '2020' , 2 , financialAdvisor.Id);
        PortfolioChannelRollupController.getFundTickers('ETF', true, '2020' , 3 , financialAdvisor.Id);
        PortfolioChannelRollupController.getFundTickers('ETF', true, '2020' , 4 , financialAdvisor.Id);
        PortfolioChannelRollupController.getFundTickers('Mutual Fund', true, '2020' , 4 , financialAdvisor.Id);
        PortfolioChannelRollupController.getChannels();

       
        Test.stopTest();

    }

    @isTest
    public static void  testGetUSSalesGoalsByPortfolio2() {
        Channel__c financialAdvisor = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Financial Advisor' limit 1];
        Channel__c assetManager = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Asset Manager' limit 1];
        Channel__c institutional = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Institutional' limit 1];
        Channel__c ria = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - RIA' limit 1];

        Channel__c financialAdvisorBR = [SELECT Id FROM Channel__c WHERE Channel_Name__c='FINANCIAL ADVISOR' limit 1];
        Channel__c assetManagerBR = [SELECT Id FROM Channel__c WHERE Channel_Name__c='ASSET MANAGER' limit 1];
        Channel__c institutionalBR = [SELECT Id FROM Channel__c WHERE Channel_Name__c='INSTITUTIONAL' limit 1];
        Channel__c riaBR = [SELECT Id FROM Channel__c WHERE Channel_Name__c='RIA' limit 1];


        List<Id> lstFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'ETF'])
        {
            lstFundIds.add(fund.Id);
        }
        List<Id> lstMFFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'Mutual Fund'])
        {
            lstMFFundIds.add(fund.Id);
        }
        Test.startTest();
        

        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,financialAdvisor.Id,'2020',4,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,institutional.Id,'2020',3,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,assetManager.Id,'2020',2,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,ria.Id,'2020',1,'ETF');

        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,financialAdvisorBR.Id,'2020',4,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,institutionalBR.Id,'2020',3,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,assetManagerBR.Id,'2020',2,'ETF');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,riaBR.Id,'2020',1,'ETF');


        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstMFFundIds,financialAdvisor.Id,'2020',4,'Mutual Fund');
        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstMFFundIds,institutional.Id,'2020',3,'Mutual Fund');

        PortfolioChannelRollupController.calculatePortfolioChannelRollup(lstFundIds,institutional.Id,'2020',4,'ETF');

              Test.stopTest();

    }

    @isTest
    public static void  testGetUSSalesGoalsByPortfolio() {
        Channel__c financialAdvisor = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Financial Advisor' limit 1];
        Channel__c assetManager = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Asset Manager' limit 1];
        Channel__c institutional = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - Institutional' limit 1];
        Channel__c ria = [SELECT Id FROM Channel__c WHERE Channel_Name__c='VE - RIA' limit 1];
        List<Id> lstFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'ETF'])
        {
            lstFundIds.add(fund.Id);
        }
        List<Id> lstMFFundIds = new List<Id>();
        for(Fund__c  fund : [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c = 'Mutual Fund'])
        {
            lstMFFundIds.add(fund.Id);
        }
        Test.startTest();

        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',1,'Mutual Fund');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',2,'Mutual Fund');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',3,'Mutual Fund');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',4,'Mutual Fund');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',1,'ETF');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',2,'ETF');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',3,'ETF');
        PortfolioChannelRollupController.getCalculatedDataFundWise(financialAdvisor.Id,'2020',4,'ETF');
        PortfolioChannelRollupController.getMFFirmwiseDrillDown(institutional.Id,'2020',4,'Mutual Fund',lstFundIds[0]);
        PortfolioChannelRollupController.getMFFirmwiseDrillDown(institutional.Id,'2021',4,'Mutual Fund',lstFundIds[0]);
        PortfolioChannelRollupController.getMFFirmwiseDrillDown(institutional.Id,'2020',4,'ETF',lstFundIds[0]);
        PortfolioChannelRollupController.getMFFirmwiseDrillDown(institutional.Id,'2021',4,'ETF',lstFundIds[0]);


        Test.stopTest();

    }
}