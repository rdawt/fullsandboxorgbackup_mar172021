global class BatchScheduleUpdateWithMaps4CSM  implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        contactBatchUpdateWithMaps4CSM b = new contactBatchUpdateWithMaps4CSM ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
   
}