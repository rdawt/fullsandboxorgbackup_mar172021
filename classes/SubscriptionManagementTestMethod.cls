/**
      Author: Suresh Anugu
      Description: Test Class for SubscriptionCount Trigger

      Modified By: Vidhya Krishnan
      Description: Edited for more coverage
 */
@isTest
global class SubscriptionManagementTestMethod{
    static testMethod void runInsert() 
    {
       Account acc = new Account(Name = 'abc123', Channel__c = 'RIA', BillingCountry = 'US'); insert acc; 
       
       Subscription__c subs = new Subscription__c(Name = 'abcd', Subscription_Display_Name__c = 'ABC', IsActive__c = true); insert subs;       
       Subscription__c sub1 = new Subscription__c(Name = 'xyz', Subscription_Display_Name__c = 'XYZ', IsActive__c = true); insert sub1;
       
       Subscription_Type__c subType = new Subscription_Type__c(Name = 'abcd'); insert subType;
       Subscription_Group__c subGrp = new Subscription_Group__c(Name = 'test', IsActive__c = true); insert subGrp;
       
       Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = subs.Id ); insert SGS ;
       Subscription_Group_Subscription__c SGS1 = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = sub1.Id ); insert SGS1 ;
       
       Contact con = new Contact(FirstName = 'ABC', LastName = 'ABC', Email = 'abc@gmail.com', Subscription_Group__c = subGrp.Id, AccountId = acc.Id, MailingCountry = 'US');
       insert con; ApexPages.currentPage().getParameters().put('cId',con.Id);
       
       Subscription_Member__c submem = new subscription_member__c(Subscription__c = subs.id, Contact__c = con.Id, subscribed__c = true);
       insert submem;
       
       list<SubscriptionManagement.Recordset> lstrec = new list<SubscriptionManagement.Recordset>();
       SubscriptionManagement subMgmt = new SubscriptionManagement();
       subMgmt.subGrId = subGrp.Id;

       SubscriptionManagement.Recordset rec1 = new SubscriptionManagement.Recordset();
       subMgmt.GetSubscriptionsInt(); rec1.SubId = subs.Id;
       rec1.Subscribed = false; rec1.subUnsubDate = system.now();
       lstrec.add(rec1); subMgmt.lstRSet = lstrec;
       subMgmt.setSubscribtion(); // update
       
       SubscriptionManagement.Recordset rec2 = new SubscriptionManagement.Recordset();
       subMgmt.GetSubscriptionsInt(); rec1.SubId = subs.Id; 
       rec2.Subscribed = true; rec1.subUnsubDate = null;
       lstrec.add(rec2); subMgmt.lstRSet = lstrec;
       subMgmt.setSubscribtion(); // update

       SubscriptionManagement.Recordset rec3 = new SubscriptionManagement.Recordset();
       subMgmt.GetSubscriptionsInt(); 
       rec3.SubId = sub1.Id; rec3.Subscribed = true;
       lstrec.add(rec3); subMgmt.lstRSet = lstrec;
       subMgmt.setSubscribtion(); // insert

//       subMgmt.UpdateContactPrivacy(con.Id);
       subMgmt.cancelRecord();     
    }
}