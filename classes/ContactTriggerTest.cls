/**
      Author: Vidhya Krishnan
      Date Created: 12/08/2011
      Description: Test Class for Contact Trigger that manages all the sub triggers on Contact
 */
@isTest
private class ContactTriggerTest {
	
    @isTest
    static void testContactTrigger() {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            SL_TestDataFactory.createTestDataForCustomSetting();
            // To check if the No. of contact field in Account is updated
            // To check if the line break in Mailing Address is removed before storing in custome field
            ContactTriggerHandler cth = new ContactTriggerHandler();
    //    	DynamicEligibility de = new DynamicEligibility();
            DMLException e = null;
            Set<ID> accIds = new Set<ID>();
            List<Contact> conlist = new List<Contact>();
             Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            
            Account acc = new Account(Name = 'qwerpoiuadsf', BillingCountry='USA');
            sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
            
            Account acc1 = new Account(Name = 'lkjhgfda 987896', BillingCountry='Canada');
            sObject acc1Inserted = SL_TestDataFactory.createSObject(acc1,true);
            
            Contact con = new Contact(LastName='xyz',FirstName='test',AccountId=acc.Id,MailingStreet='abc', MailingCountry='US');
            sObject conInserted = SL_TestDataFactory.createSObject(con,true);
            
            Contact con1 = new Contact(LastName='xyz11',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz', MailingCountry='US', Subscription_Group__c = objSG.Id);
            sObject con1Inserted = SL_TestDataFactory.createSObject(con1,true);
            
            Contact con2 = new Contact(LastName='xyz21',FirstName='test2',AccountId=acc1.Id,MailingStreet='xyz1', MailingCountry='US', Subscription_Group__c = objSG.Id);
            sObject con2Inserted = SL_TestDataFactory.createSObject(con2,true);
            
            Contact con3 = new Contact(LastName='xyz1',FirstName='test1',AccountId=acc.Id,MailingStreet='', MailingCountry='US',
                ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC', Subscription_Group__c = objSG.Id);
            sObject con3Inserted = SL_TestDataFactory.createSObject(con3,true);
            
            Contact con4 = new Contact(LastName='xyz41',AccountId=acc.Id, Email='test@abc.com', MailingCountry='US', Subscription_Group__c = objSG.Id);
            sObject con4Inserted = SL_TestDataFactory.createSObject(con4,true);
            
            accIds.add(con.AccountId);
            accIds.add(con1.AccountId);
            accIds.add(con2.AccountId);
            
            Subscription__c sub = new Subscription__c(Name='Test Sub1', Subscription_Display_Name__c='Test Sub1');
            sObject subInserted = SL_TestDataFactory.createSObject(sub,true);
            
            Subscription_Member__c sm1 = new Subscription_Member__c(Subscription__c=sub.Id, Contact__c=con4.Id, Email__c='test1@abc.com', Subscribed__c=true);
            sObject sm1Inserted = SL_TestDataFactory.createSObject(sm1,true);
            
            con.MailingStreet = 'abc1';
            con.OR__c='NA';
            update con;
            con3.AccountId = acc1.Id;
            update con3;
            conlist.add(con);
            conlist.add(con1);
            conlist.add(con2);
            conlist.add(con3);
            delete con4;
            ContactTriggerHandler.updateContactCounts(accIds);
            cth.contactRemoveLineBreak(conlist);
            cth.sequoiaCalcualtions(conlist);
    //	        de.updateEligibility(conList);
            System.assert(e==null);
            Test.stopTest();
        }
    }
}