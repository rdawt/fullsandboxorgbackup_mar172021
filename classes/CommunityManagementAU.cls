/*    
    Author  :       Vidhya Krishnan
    Date Created:   10/10/12
    Description:    Community Registration Management Base Class for Australia Market Vectors
*/
global class CommunityManagementAU {
    public enum emailStatus {AVAILABLE, NOT_AVAILABLE, NOT_ACTIVATED}
    public enum memberType {CONTACT, LEAD, REGISTRANT}
    public AUMemberRecord mRec = new AUMemberRecord();
    public string errorPrefix = 'SF_ERROR:';
    public string warningPrefix = 'SF_WARNING:';
    public string errMsg;
    public string subReq{get;set;}
    public SendEmailNotification se = new SendEmailNotification('Error: Community Management AU Class failed');

    public CommunityManagementAU(){
        errMsg = null;
    }
    
    /*  This method is to add new Community Registrant, Community Member Record & Manage Contact mapping    */
    public boolean addMemberRecord(AUMemberRecord memRec)
    {
    	system.debug('Member Record passed.....'+memRec);
        list<Community__c> c = new list<Community__c>();
        list<Community_Registrant__c> crList = new list<Community_Registrant__c>();
        Community_Registrant__c cReg;
        
        if(memRec.communityId == null || memRec.cUserName == null || memRec.firstName == null || memRec.lastName == null || memRec.address1 == null || memRec.companyName == null ||
                memRec.city == null || memRec.zipcode == null || memRec.email == null || memRec.passWord == null){  // All Mandatory fields are not provided
                errMsg = getErrorPrefix()+' One of the Mandatory Fields are missing....'+memRec;
                return false;
            }
            
        c = [select Id, Name, Description__c, Domain_Name__c, Is_Active__c from Community__c where Id =: memRec.communityId limit 1];
        if(c.size() == 0){ // Community doesnt exist
            errMsg = getErrorPrefix()+' Given Community Does not exist....'+memRec.communityId;
            return false;
        }
        if(c[0].Is_Active__c == false){ // Community is not active
            errMsg = getErrorPrefix()+' Given Community is not Active....'+memRec.communityId;
            return false;
        }
        
        crList = [select id from Community_Registrant__c where User_Name__c = : memRec.cUserName limit 1];
        system.debug('Registrant Already Exist.....'+crList);
        if(crList.isEmpty())
        	cReg = new Community_Registrant__c();
        else
        	cReg = new Community_Registrant__c(Id = crList[0].Id);
        	
        if(memRec.cUserName != null) cReg.User_Name__c = memRec.cUserName; // to avoid run time null pointer exception, null is double checked
        if(memRec.firstName != null) cReg.First_Name__c = memRec.firstName;
        if(memRec.lastName != null) cReg.Last_Name__c = memRec.lastName;
        if(memRec.email != null) cReg.Email__c = memRec.email;
        if(memRec.passWord != null) cReg.Password__c = memRec.passWord;
        if(memRec.companyName != null) cReg.Company__c = memRec.companyName;
        if(memRec.jobTitle != null) cReg.Title__c = memRec.jobTitle;
        if(memRec.address1 != null) cReg.Address1__c = memRec.address1;
        if(memRec.address2 != null) cReg.Address2__c = memRec.address2;
        if(memRec.city != null) cReg.City__c = memRec.city;
        if(memRec.state == null || (memRec.state.equals('(Select)'))) cReg.state__c = null; 
            else cReg.State__c = memRec.state;
        if(memRec.country != null) cReg.Country__c = memRec.country;
        if(memRec.zipcode != null) cReg.Zip_Code__c = memRec.zipcode;
        if(memRec.phone != null) cReg.Phone__c = memRec.phone;
        system.debug('Hardcoded Sub Req.....'+subReq);
        if(memRec.subscriptionRequested != null) cReg.Subscription_Options__c = memRec.subscriptionRequested;
        else cReg.Subscription_Options__c = subReq;
        if(memRec.investorType == null || (memRec.investorType.equals('(Select)'))) cReg.Investor_Type__c = null; 
           else cReg.Investor_Type__c = memRec.investorType;
        if(memRec.aum == null || memRec.aum == '(Select)') cReg.AUM__c = null;
            else cReg.AUM__c = memRec.aum;
        if(memRec.sourceId == null) {
            cReg.External_System_Id__c = memRec.communityId;
            string s = c[0].Name + ' (' + c[0].Domain_Name__c + ')';
            cReg.DB_Source__c = s;
        }
        else cReg.External_System_Id__c = memRec.sourceId;
        
        // value is always passed as null - has to be fixed   
        if((memRec.isFP == null)||((memRec.isFP).equalsIgnoreCase('false'))) cReg.isFP__c = false;
        else cReg.isFP__c = Boolean.valueOf(memRec.isFP);
        // value is always passed as null - has to be fixed   
        DateTime ppAck = DateTime.valueOf(memRec.privacyAckDate); 
        if(ppAck==null) cReg.Privacy_Policy_Web_Ack_Date__c = system.now();
          else if(ppAck != null) cReg.Privacy_Policy_Web_Ack_Date__c = ppAck;
        // value is always passed as null - has to be fixed   
        if(memRec.isActive == null || memRec.isActive == false) cReg.isActive__c = false;
            else if(memRec.isActive == true) cReg.isActive__c = memRec.isActive;
        DateTime lLogin = DateTime.valueOf(memRec.lastLogin);
        if(lLogin==null) cReg.Last_Login_Date__c = null;
            else cReg.Last_Login_Date__c = lLogin;
        Date dbCreate = Date.valueOf(memRec.dbSourceCreateDate);
        if(dbCreate == null) cReg.Create_Date_From_External_System__c = null;
            else cReg.Create_Date_From_External_System__c = dbCreate;
        Integer lcount = memRec.loginCount;
        if((lcount == null) || lcount == 0) cReg.Number_Of_Times_Logged_In__c = null;
            else cReg.Number_Of_Times_Logged_In__c = memRec.loginCount;
        
        string s1 = getSubGroupId( memRec.email,  memRec.communityId);
        if(s1 != null) { memRec.subscriptionGroupId = s1; cReg.Subscription_Group__c = s1; }

        system.debug('New Registrant record before insert....'+cReg);
        // To map the registrant into a community
        try{
        	if(crList.isEmpty())
            	insert cReg;
            else
            	update cReg;
            memRec.cMemberId = cReg.Id;            
            
            system.debug('Calling Create Contact from Registrant Class....');
            CreateConFromReg ccfr = new CreateConFromReg(cReg,memRec.communityId);
            String s = ccfr.checkExistingContact();
            if(s.startsWith('WARNING:'))
                errMsg = getWarningPrefix() + s;
            else
                memRec.contactId = s; // This Contact id has to be passed to create Subscriptions 

            Community_Member__c cMem = new Community_Member__c();
            cMem.Community__c = memRec.communityId;
            cMem.Community_Registrant__c = cReg.Id;
            cMem.Contact__c = memRec.contactId;
            insert cMem;
        }
        catch(Exception e){
        	errMsg = getErrorPrefix() +' Exception Occured in addMemberRecord method : \n' +  memRec + e.getMessage()+' ; '+e.getStackTraceString();
        	se.sendEmail(errMsg);
            return false;
        }
        return true; // Sucessfull Insert on both Registrant & Member Objects       
    }
    
    // This method is called from API class to insert Registrant & Subscriptions details
    public string addCMemberAndSubscriptions(AUMemberRecord memRec, list<SubscriptionManagementAPI.SubscriptionDetails> subList){
        try{
        	subReq = '';
	        system.debug('Given Sub List....'+subList);
	        string result;
	        if(subList[0].subscriptionId != null || subList[0].subscriptionId != '')
	        	if(subList[0].newValue.equalsIgnoreCase('true'))
	        		subReq = 'periodic updates';
	        	else subReq = '';
	        system.debug('Hard Coded Sub Req1111....'+subReq);
	        Boolean b1 = false; Boolean b2 = false;
	        // creating new registrant
	        b1 = addMemberRecord(memRec);
	        if(b1 == true){
	            // Update the Subscription Requested value in Registrant        
	            updateSubReq(memRec.cMemberId,subList);
	            if(memRec.contactId != null){
	                subList[0].contactId = memRec.contactId; // contact id has to be passed from SF during registrant creation
	                b2 = SubscriptionManagementAPI.setSubscriptions(subList);
	            }
	            if(b1 == true && b2 == true)
	                result = memRec.cMemberId; // Success
	                
	            else if(b1 == true && b2 == false){
	                errMsg = getWarningPrefix() + ' Subscription failed but Registrant created Successfully for the id : '+memRec.cMemberId;
	                result = errMsg;
	            }
	        }
	        else
	            result = errMsg; // Failure
	        /*
	        if(result != memRec.cMemberId){
	        	SendEmailNotification se1 = new SendEmailNotification('Notification: Community Management AU Class warning');
	        	se1.sendEmail('Alert from addCMemberAndSubscriptions method : \n' + errMsg);
	        }*/
	        return result;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in addCMemberAndSubscriptions method : \n'+memRec+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }

    /*  This method is used to get the entire Member Record details for Profile Edit page   */
    public AUMemberRecord getMemberRecord(string cMemberId)
    {
        try{
            list<Community_Registrant__c> cRegList = new list<Community_Registrant__c>();
            if(cMemberId != null)
                cRegList = [select Id, User_Name__c, First_Name__c, Last_Name__c, isActive__c, Email__c, Password__c, Contact__c, Lead__c, Company__c, Title__c, Address1__c, Address2__c, 
                    City__c, State__c, Country__c, Zip_Code__c, Phone__c, AUM__c, isFP__c, Investor_Type__c, Subscription_Options__c, External_System_Id__c,
                    Privacy_Policy_Web_Ack_Date__c, Create_Date_From_External_System__c, Last_Login_Date__c, Number_Of_Times_Logged_In__c, Subscription_Group__c
                    from Community_Registrant__c where id =: cMemberId Limit 1];
    
            if(cRegList.size() == 0)
                return null;
            else{
                list<Community_Member__c> cMemList = new list<Community_Member__c>();
                cMemList = [select Community__c from Community_Member__c where Community_Registrant__c =: cMemberId Limit 1];
                if(cMemList.size() == 0) mRec.communityId = null;
                    else mRec.communityId = cMemList[0].Community__c;
                
                mRec.cMemberId = cRegList[0].Id; mRec.contactId = cRegList[0].Contact__c; mRec.leadId = cRegList[0].Lead__c; mRec.email = cRegList[0].Email__c;
                mRec.cUserName = cRegList[0].User_Name__c; mRec.passWord = cRegList[0].Password__c; mRec.firstName = cRegList[0].First_Name__c; 
                mRec.lastName = cRegList[0].Last_Name__c; mRec.companyName = cRegList[0].Company__c; mRec.jobTitle = cRegList[0].Title__c;
                mRec.address1 = cRegList[0].Address1__c; mRec.address2 = cRegList[0].Address2__c; mRec.city = cRegList[0].City__c; mRec.state = cRegList[0].State__c;
                mRec.country = cRegList[0].Country__c; mRec.zipcode = cRegList[0].Zip_Code__c; mRec.phone = cRegList[0].Phone__c; mRec.sourceId = cRegList[0].External_System_Id__c;
                mRec.subscriptionRequested = cRegList[0].Subscription_Options__c; mRec.investorType = cRegList[0].Investor_Type__c; mRec.aum = cRegList[0].AUM__c;
                mRec.isFP = String.valueOf(cRegList[0].isFP__c); mRec.isActive = cRegList[0].isActive__c; mRec.subscriptionGroupId = cRegList[0].Subscription_Group__c;
                Datetime ppAck = cRegList[0].Privacy_Policy_Web_Ack_Date__c;
                if(ppAck != null)
                    mRec.privacyAckDate = ppAck;
                Datetime lLogin = cRegList[0].Last_Login_Date__c;
                if(lLogin != null)
                    mRec.lastLogin = lLogin;
                Date dbCreate = cRegList[0].Create_Date_From_External_System__c;
                if(dbCreate != null)
                    mRec.dbSourceCreateDate = date.NewInstance(dbCreate.year(),dbCreate.month(),dbCreate.day());
                if(cRegList[0].Number_Of_Times_Logged_In__c != null )
                    mRec.loginCount = cRegList[0].Number_Of_Times_Logged_In__c.intValue();
                system.debug('Returned Mem Rec.....'+mRec);
                return mRec;
            }
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getCMember method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    
    /*  This method is used to update the Community Registrant record on profile edit   */
    public boolean setMemberRecord(string cMemberId, AUMemberRecord memRec)
    {
        if(memRec.cMemberId == null){
            return false;
            errMsg = getErrorPrefix()+' Given Registrant Id is not Valid....';
        }
        /*
        To modify a registrant's community, we need to update community_member object
        So dont do it here. add new method to do it if needed
        */
        list<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
        cRegList = [select Id,Investor_Type__c,AUM__c,isFP__c,Privacy_Policy_Web_Ack_Date__c from Community_Registrant__c where id =: cMemberId Limit 1];
        if(cRegList.size() == 0){
            return false; // Record does not exist
            errMsg = getErrorPrefix()+' Given Registrant Id is not Valid....'+cMemberId;
        }
        // user name should not be updated in modify profile
        // If there is no actual update for a field, null value is sent
        // to avoid run time null pointer exception, null is double checked
        if(memRec.firstName != null) cRegList[0].First_Name__c = memRec.firstName;
        if(memRec.lastName != null) cRegList[0].Last_Name__c = memRec.lastName;
        if(memRec.email != null) cRegList[0].Email__c = memRec.email;
        if(memRec.passWord != null) cRegList[0].Password__c = memRec.passWord;
        if(memRec.companyName != null) cRegList[0].Company__c = memRec.companyName;
        if(memRec.jobTitle != null) cRegList[0].Title__c = memRec.jobTitle;
        if(memRec.address1 != null) cRegList[0].Address1__c = memRec.address1;
        if(memRec.address2 != null) cRegList[0].Address2__c = memRec.address2;
        if(memRec.city != null) cRegList[0].City__c = memRec.city;
        if(memRec.state == null || (memRec.state.equals('(Select)'))) cRegList[0].state__c = null; 
            else cRegList[0].State__c = memRec.state;
        if(memRec.country != null) cRegList[0].Country__c = memRec.country;
        if(memRec.zipcode != null) cRegList[0].Zip_Code__c = memRec.zipcode;
        if(memRec.phone != null) cRegList[0].Phone__c = memRec.phone;
        if(memRec.subscriptionRequested != null) cRegList[0].Subscription_Options__c = memRec.subscriptionRequested;
        else cRegList[0].Subscription_Options__c = subReq;
        if(memRec.subscriptionGroupId != null) cRegList[0].Subscription_Group__c = memRec.subscriptionGroupId;
        if(memRec.investorType != null) {           
            if(memRec.investorType == '(Select)') cRegList[0].Investor_Type__c = null;
            else cRegList[0].Investor_Type__c = memRec.investorType;
        }
        if(memRec.aum != null) {            
            if(memRec.aum == '(Select)') cRegList[0].AUM__c = null;
            else cRegList[0].AUM__c = memRec.aum;
        }
        if(memRec.isFP != null){
            cRegList[0].isFP__c = Boolean.valueOf(memRec.isFP);
        }
        else{
            if(cRegList[0].isFP__c == null || cRegList[0].isFP__c == false) 
                cRegList[0].isFP__c = true; // because isFP always comes as null
        }
        if(memRec.isActive != null) cRegList[0].isActive__c = memRec.isActive;
        DateTime ppAck = DateTime.valueOf(memRec.privacyAckDate);
        if(ppAck!=null) cRegList[0].Privacy_Policy_Web_Ack_Date__c = ppAck;
        else{
            if(cRegList[0].Privacy_Policy_Web_Ack_Date__c == null)
                cRegList[0].Privacy_Policy_Web_Ack_Date__c = system.now();
        }
        DateTime lLogin = DateTime.valueOf(memRec.lastLogin);
        if(lLogin!=null) cRegList[0].Last_Login_Date__c = lLogin;
        Date dbCreate = Date.valueOf(memRec.dbSourceCreateDate);
        if(dbCreate != null) cRegList[0].Create_Date_From_External_System__c = dbCreate;
        Integer lcount = memRec.loginCount;
        if(lcount != null) cRegList[0].Number_Of_Times_Logged_In__c = memRec.loginCount;
        try{
            update cRegList[0];
        }
        catch(Exception e){
            errMsg = getErrorPrefix() + ' Exception Occured in setMemberRecord method : \n'+cMemberId+' ; '+memRec+' ; '+e.getMessage()+' ; '+e.getStackTraceString();
            se.sendEmail(errMsg);
            return false;
        }
        return true; // Successfull Update
    }
    // This method is used in login authentication
    public string getCMemberId(string communityId, string userName)
    {
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            Set<Id> cRegIds = new Set<Id>();
            List<Community_Member__c> cMems = new List<Community_Member__c>();
            cMems = [select Community_Registrant__c from Community_Member__c where Community__c =: communityId];
            for(Community_Member__c cm : cMems)
                cRegIds.add(cm.Community_Registrant__c);
            cRegList = [select Id from Community_Registrant__c where id IN: cRegIds and User_Name__c =: userName Limit 1];
            if(cRegList.size() == 0)
                return null;
            else
                return cRegList[0].Id;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getCMemberId method : \n'+userName+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This method checks if the Email id given is alredy in use or not
    public string isEmailTaken(string communityId, string emailAddress)
    {
        try{
            Set<Id> cRegIds = new Set<Id>();
            List<Community_Member__c> cMems = new List<Community_Member__c>();
            cMems = [select Community_Registrant__c from Community_Member__c where Community__c =: communityId];
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            for(Community_Member__c cm : cMems)
                cRegIds.add(cm.Community_Registrant__c);
            cRegList = [select Id, isActive__c from Community_Registrant__c where id IN: cRegIds and Email__c =: emailAddress];
            if(cRegList.isEmpty() || cRegList == null || cRegList.size() == 0)
                return String.ValueOf(emailStatus.AVAILABLE);
            else{
                for(Community_Registrant__c cr : cRegList){
                    if(cr.isActive__c == true)
                         return String.ValueOf(emailStatus.NOT_AVAILABLE);
                    else
                         return String.ValueOf(emailStatus.NOT_ACTIVATED);
                }
                return String.ValueOf(emailStatus.NOT_AVAILABLE);
            }
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in isEmailTaken method : \n'+emailAddress+' ; '+communityId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This method is used to fetch the password of a given Registrant
    public string getPassword(string cMemberId)
    {
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            cRegList = [select Id,Password__c from Community_Registrant__c where id =: cMemberId Limit 1];
            if(cRegList.size() == 0)
                return null;
            else
                return cRegList[0].Password__c;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getPassowrd method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This method sets the new password of the Registrant
    public boolean setPassword(string cMemberId, string strPassword)
    {
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            cRegList = [select Id from Community_Registrant__c where id =: cMemberId Limit 1];
            cRegList[0].Password__c = strPassword;
            update cRegList[0];
            return true;
        }
        catch(Exception e){
        	se.sendEmail('Exception Occurred in setPassword method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return false;
        }
    }
    // This method checks if the Registrant is Activated (ready to use the account) or not
    public boolean isActive(string cMemberId){
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            cRegList = [select Id,isActive__c from Community_Registrant__c where id =: cMemberId Limit 1];
            if(cRegList.size() == 0)
                return false;
            else
                return cRegList[0].isActive__c;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in isActive method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return false;
        }
    }
    // This method sets a Registrant Active once they click the activate link 
    public boolean setActive(string cMemberId, boolean active){
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            cRegList = [select Id,isActive__c from Community_Registrant__c where id =: cMemberId Limit 1];
            cRegList[0].isActive__c = active;
            update cRegList[0];
        }
        catch(Exception e){
        	se.sendEmail('Exception Occurred in setActive method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return false;
        }
        return true;
    }
    // This method fetch all details of a community - its name, description, domain & is_active
    public Community__c getCommunityDetails(string communityId){
        try{
            List<Community__c> c = new List<Community__c>();
            c = [select Id, Name, Description__c, Domain_Name__c, Is_Active__c from Community__c where Id =: communityId Limit 1];
            if(c.size() == 0)
                return null;
            else
                return c[0];
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getCommunityDetails method : \n'+communityId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // returns if the member is contact or lead or just registrant
    public string getCMemberType(string cMemberId)
    {
        try{
            List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
            cRegList = [select Id,Contact__c,Lead__c from Community_Registrant__c where id =: cMemberId Limit 1];
            if(cRegList.size() == 0)
                return null;
            else{
                string con, lead;
                con = cRegList[0].Contact__c; lead = cRegList[0].Lead__c;
                if(con != null && !con.equals(''))
                    return String.ValueOf(memberType.CONTACT);
                else if(lead != null && !lead.equals(''))
                    return String.ValueOf(memberType.LEAD);
                else
                    return String.ValueOf(memberType.REGISTRANT);
            }
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getCMemberType method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    //set the Last Login date to the current date if lastLoginDate is null ; increment 1 to the logincount
    public void setLastLogin(string cMemberId, DateTime lastLoginDate){
        List<Community_Registrant__c> cRegList = new List<Community_Registrant__c>();
        cRegList = [select Id,Last_Login_Date__c,Number_Of_Times_Logged_In__c from Community_Registrant__c where id =: cMemberId Limit 1];
        if(cRegList.size() > 0){
            if(lastLoginDate == null) cRegList[0].Last_Login_Date__c = system.now();
                else cRegList[0].Last_Login_Date__c = lastLoginDate;
            Integer lcount = Integer.valueOf(cRegList[0].Number_Of_Times_Logged_In__c);
            if((lcount == null) || lcount == 0) cRegList[0].Number_Of_Times_Logged_In__c = 1;
                else cRegList[0].Number_Of_Times_Logged_In__c = lcount + 1;
            try{
                update cRegList[0];
            }
            catch(Exception e){
            	se.sendEmail('Exception Occurred in setLastLogin method : \n'+cMemberId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            }
        }
    }

    // This method is called from API Class which inturn calls other method to detemine proper Subscription Group
    public string getSubGroupId(string email,string communityId){
        try{
            String subGrpId = getConSubGrpId(email);
            if(subGrpId == null || subGrpId == '')
                subGrpId = getDomainEligiblity(email);
            if(subGrpId == null || subGrpId == '')
                subGrpId = getDefaultSubGrpId(communityId);
            mRec.subscriptionGroupId = subGrpId;
            // Select stmt returns 18 character Id; But ektron needs 15 char Id
            subGrpId = subGrpId.substring(0,15);
            system.debug('Registrant Subscription Group.....'+subGrpId);
            return subGrpId;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getSubGroupId method : \n'+email+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This method retruns default subscription group id for the given community
    public string getDefaultSubGrpId(string communityId){
        list<Community__c> comList = new list<Community__c>();
        try{
            comList = [select id, Default_Subscription_Group__c from Community__c where id =: communityId];
            if(comList.size()==0)
                return null;
            else
                return comList[0].Default_Subscription_Group__c;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getDefaultSubGrpId method : \n'+communityId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This methid checks if Contact exist for given email Id and return the contact's Subscription group id if any
    public string getConSubGrpId(string emailAddress){
        errMsg = getWarningPrefix();
        List<Contact> conList = new List<Contact>();
        try{
        	/*	Exclude MVIS Contacts and create Intentional Dupe for MVIS Contacts.	*/
            conList = [select id,Subscription_Group__c from Contact where Email =: emailAddress  and DB_Source__c != 'MVIS Registrant' and DB_Source__c != 'MVIS Component'];
            if(conList.isEmpty() || conList == null || conList.size() == 0)
                return null;
            else if (conList.size() == 1)
                return conList[0].Subscription_Group__c;
            else{
                errMsg = errMsg+' More than 1 Contact Record found for the given Email Id: '+ emailAddress +
                            '\n List of Contact ids are : ' + conList +  
                            '\n So Registrant cannot be mapped to a Contact and cannot maintain their Subscriptions too.';
                SendEmailNotification se1 = new SendEmailNotification('Notification: Community Management AU Class warning');
                se1.sendEmail('\n' + errMsg+'\n Appropriate Actions taken.');
                return null;
            }
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getConSubGrpId method : \n'+emailAddress+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    // This method is used to check if the email domain is in any of the restricted wirehouse domains
    public string getDomainEligiblity(string emailAddress){
        try{
            List<Subscription_Group__c> subGrpList = new List<Subscription_Group__c>();
            subGrpList = [select Id,Associated_Domains__c from Subscription_Group__c where IsActive__c =: true];
            List<string> strList = emailAddress.split('@',2);
            string emailDomain = strList[1];
            for(Subscription_Group__c sg: subGrpList){
                if(sg.Associated_Domains__c != null){
                    strList = new List<string>();
                    strList = sg.Associated_Domains__c.split(',',0);
                    for(String s : strList)
                        if(s.equalsIgnoreCase(emailDomain))
                            return sg.id;
                }
            }
            return null;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in getDomainEligiblity method : \n'+emailAddress+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }

    // This method updates the SubscriptionRequested filed in Registrant object
    public void updateSubReq(string cRegId, list<SubscriptionManagementAPI.SubscriptionDetails> subList){
        map<Id,Subscription__c> subIndicatorMap;
        Subscription__c sub;
        list<String> subIdList = new List<string>();
        string strIndr = ''; string s;

        try{
            for(SubscriptionManagementAPI.SubscriptionDetails subMem : subList)
                subIdList.add(subMem.subscriptionId);
    
            subIndicatorMap = new Map<Id, Subscription__c>([Select Id, Subscription_Indicator_Marker__c from Subscription__c where Id IN: subIdlIst]);
            for(SubscriptionManagementAPI.SubscriptionDetails subMem : subList){
                if(submem.newValue == 'true'){
                    sub = new Subscription__c(); s= '';
                    if(subIndicatorMap.containsKey(subMem.subscriptionId))
                        sub = subIndicatorMap.get(subMem.subscriptionId);
                    if(sub.Subscription_Indicator_Marker__c == null)
                        s = '';
                    else
                        s = sub.Subscription_Indicator_Marker__c;
                    if(strIndr.equals(''))
                        strIndr = s;
                    else{
                        if(!(strIndr.contains(s)))
                            strIndr = strIndr + ',' + s;
                    }
                }
            }
            Community_Registrant__c cReg = new Community_Registrant__c(Id = cRegId);
            cReg.Subscription_Options__c = strIndr;
            if(strIndr != '')
                update cReg;
        }catch(Exception e){
            errMsg = getErrorPrefix() + 'Exception Occured in updateSubReq method.... '+cRegId+';'+subList+';'+e.getMessage()+' ; '+e.getStackTraceString();
            se.sendEmail(errMsg);
        }
    }
    // This method is called from API class to update Registrant & Subscriptions details
    public string setCMemberAndSubscriptions(String cMemberId, AUMemberRecord memRec, list<SubscriptionManagementAPI.SubscriptionDetails> subList){
        try{
	        string result;
	        system.debug('Sub List.....'+subList);
	        if(subList[0].subscriptionId != null || subList[0].subscriptionId != '')
	        	if(subList[0].newValue.equalsIgnoreCase('true'))
	        		subReq = 'periodic updates';
	        	else subReq = '';
			system.debug('Hard Coded Sub Req3333....'+subReq);
	        // Update Registrant Record
	        Boolean b1 = setMemberRecord(cMemberId, memRec);
	
	        // Update Subscription Requested of Registrant record
	        updateSubReq(memRec.cMemberId,subList);
	        // update contact subscriptions
	        Boolean b2 = SubscriptionManagementAPI.setSubscriptions(subList);
	
	        if(b1 == true && b2 == true)
	            result = memRec.cMemberId;
	        else{ 
	            if(b1 == true && b2 == false)
	                errMsg = getWarningPrefix() + 'Subscription update failed but Registrant updated Successfully....'+cMemberId;
	            else if(b1 == false && b2 == true)
	                errMsg = errMsg + '\nRegistrant update failed but Subscriptions updated successfully....'+cMemberId ;
	            else if(b1 == false && b2 == false)
	                errMsg = errMsg + 'Both Registrant update & Subscription Update Failed....'+cMemberId;
	    
	            SendEmailNotification se1 = new SendEmailNotification('Notification: Community Management AU Class warning');
	            se1.sendEmail('Alert from setCMemberAndSubscriptions method : \n' + cMemberId+errMsg);
	            result = errMsg;
	        }
	        return result;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred in setCMemberAndSubscriptions method : \n'+cMemberId+';'+e.getMessage()+' ; '+e.getStackTraceString());
            return null;
        }
    }
    public string getErrorPrefix(){
        return errorPrefix;
    }
    public string getWarningPrefix(){
        return warningPrefix;
    }

    // WRAPPER CLASS
    global class AUMemberRecord
    {
        Webservice String subscriptionGroupId {get;set;}
        Webservice String communityId {get; set;} // Community --> Id
        Webservice String cMemberId {get; set;} // Community Registrant --> Id
        Webservice String leadId {get; set;}
        Webservice String contactId {get; set;}
        Webservice String email {get; set;}
        Webservice String cUserName {get; set;}
        Webservice String passWord {get; set;}
        Webservice String firstName {get; set;}
        Webservice String lastName {get; set;}
        Webservice String companyName {get; set;}
        Webservice String jobTitle {get; set;}
        Webservice String address1 {get; set;}
        Webservice String address2 {get; set;}
        Webservice String city {get; set;}
        Webservice String state {get; set;}
        Webservice String country {get; set;}
        Webservice String zipcode {get; set;}
        Webservice String phone {get; set;}
        Webservice String subscriptionRequested {get; set;} // comma delimited string that has list of subscription preferences
        Webservice String investorType {get; set;}
        Webservice String aum {get; set;}
        Webservice String isFP {get; set;}
        Webservice Boolean isActive {get; set;}
        Webservice DateTime privacyAckDate {get; set;} // Privacy policy awk Date
        Webservice DateTime lastLogin {get;set;}
        Webservice Date dbSourceCreateDate {get;set;}
        Webservice Integer loginCount {get;set;} // No of times logged in
        Webservice String sourceId {get;set;} // Record Id (from VeBas/Ektron) of imported records or community id for live data
    }
}