({
    afterScriptsLoaded : function(component, event, helper) {
        helper.showChart(component, event, helper);
    },

    handleChartDblClick : function(component, event, helper) {
        if(component.get('v.drillDownRequired')){
            var drillDownEvent = component.getEvent("drillDownEvent");
            drillDownEvent.setParams({
                "chartId" : component.get('v.chartId')}); 
            drillDownEvent.fire();
        }
    }
})