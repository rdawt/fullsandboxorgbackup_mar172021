public with sharing class DB_InternalWholeSaler_Utill {
   
    /************************************************************************
    * This method calculate the number of working days based on the given date.
    * Before use this method, you should configure Holidays from Salesforce setup.
    * For working days calculation, this will excludes both holidays and weekends.
    *
    * @Param dGivenDate			        Given date for working days calculation
    * @Param mode   					Based on the given 3 modes, method will returns working days
    *
    *  1. 'Remain' -->    How many working days left in a given month from a given date (includes today)                                    
    *  2. 'Fullmonth' --> Total days (minus holidays,weekends)-in a given month
    *  3. 'Elapsed' -->   How many working days are Elapsed from a given date (excludes today)
    * 
    * @Return      						Number of working days
	************************************************************************/
    public static Integer calculateWorkingDays(Date dGivenDate,String mode){

        Date dStartDate;
        Date dEndDate;
        Date dTemp ;
        if(mode == 'Remain'){
            dStartDate = dGivenDate;
            dTemp = dGivenDate.toStartOfMonth(); 
            dEndDate = dTemp.addMonths(1).addDays(-1);
        }else if(mode == 'Fullmonth'){
            dStartDate = dGivenDate.toStartOfMonth();
            dEndDate = dStartDate.addDays(date.daysInMonth(dGivenDate.year() , dGivenDate.month())  - 1);
        }else if(mode == 'Elapsed'){
            dStartDate = dGivenDate.toStartOfMonth();
            dEndDate = dGivenDate.addDays(-1);
        }
        List<Holiday> holidays=[Select h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h];
        Integer allDaysBetween = dStartDate.daysBetween(dEndDate);
        Integer allWorkingDays=0;
        Integer allHolidays=0;

        for(Integer k=0;k<=allDaysBetween ;k++ ){
             if(checkifItisWorkingDay(dStartDate.addDays(k),holidays)){
                allWorkingDays++;
            }else{
                allHolidays++;
            }
        }
        return allWorkingDays;
    }

    
    public static boolean checkifItisWorkingDay(Date currentDate,List<Holiday> holidays){

        Date weekStart  = currentDate.toStartofWeek();
        for(Holiday hDay:holidays){
            if(currentDate.daysBetween(hDay.ActivityDate) == 0){
                return false;
            }
        }

        if(weekStart.daysBetween(currentDate) ==0 || weekStart.daysBetween(currentDate) == 6){
            return false;
        } else{
            return true;
        }
    }

    /************************************************************************
    * This method insert a new record to InternalDBActivityTypeScoreWeightage__c object.
    * If you pass bInactivateOthers param value as true, it will change the all exsisting record 
    * status to false and keep only this current record as active state.
    *
    * @Param iZooms			                    Weightage for Zoom activity type
    * @Param iMeetingFacetoFace   				Weightage for Meeting Face to Face activity type
    * @Param iServiceCalls	                    Weightage for Service Calls activity type
    * @Param iSalesCalls           				Weightage for Sales Calls activity type
    * @Param iFollowupSalesCalls			    Weightage for Followup Sales Calls activity type
    * @Param iSalesEmail           				Weightage for Sales Email activity type
    * @Param iETFLeads			                Weightage for ETF Leads activity type
    * @Param iProfileMetric    				    Weightage for Profile Metric activity type
    * @Param iScheduledMeeting                  Weightage for Scheduled Meeting activity type
    * @Param bInactivateOthers   				If this true, change the status of old records in to false
    *
    * @Return      						        Record id of the newly created record
	************************************************************************/
    public static void insertActivityTypeScoreWeightage(Decimal iZooms,Decimal iMeetingFacetoFace,Decimal iServiceCalls,Decimal iSalesCalls,
                                                        Decimal iFollowupSalesCalls,Decimal iSalesEmail,Decimal iETFLeads,
                                                        Decimal iProfileMetric,Decimal iScheduledMeeting, Decimal iEmailSubscription){
        
        List<InternalDBActivityTypeScoreWeightage__c> lstPrivious = [SELECT Id,ETF_Leads__c,Follow_up_Sales_Calls__c,Meeting_Face_to_Face__c,
                                                                        Profile_Metric__c,Sales_Calls__c,Sales_Email__c,
                                                                        Scheduled_meeting_for_Ext_Specialist__c,Service_Calls__c,
                                                                        Zooms__c,Status__c , Email_Subscription__c
                                                                        FROM InternalDBActivityTypeScoreWeightage__c 
                                                                        ORDER BY createdDate Desc Limit 1];
        
        Boolean isAnyValueChanged = false;

        if(lstPrivious.size() > 0)
        {
            InternalDBActivityTypeScoreWeightage__c  rec = lstPrivious[0];
            System.debug('>>>>>>>>>>>>rec.Email_Subscription__c>>>>>>>>>>>' + rec.Email_Subscription__c);
            System.debug('>>>>>>>>>>>>iEmailSubscription>>>>>>>>>>>' + iEmailSubscription);

            if(rec.ETF_Leads__c != iETFLeads || rec.Follow_up_Sales_Calls__c != iFollowupSalesCalls || 
                rec.Meeting_Face_to_Face__c != iMeetingFacetoFace || rec.Profile_Metric__c != iEmailSubscription ||
                rec.Sales_Calls__c != iSalesCalls || rec.Sales_Email__c != iSalesEmail ||
                rec.Scheduled_meeting_for_Ext_Specialist__c != iScheduledMeeting || 
                rec.Service_Calls__c != iServiceCalls ||  rec.Zooms__c != iZooms ) //|| rec.Profile_Metric__c != iEmailSubscription)
            {
                isAnyValueChanged= true;
            }
        }
        else if(lstPrivious.size() == 0) // This means there is no existing record
        {
            isAnyValueChanged= true;
        }
        
        System.debug('???????????isAnyValueChanged????????????' + isAnyValueChanged);

        if(isAnyValueChanged)
        {
            InternalDBActivityTypeScoreWeightage__c weightage = new InternalDBActivityTypeScoreWeightage__c();
            weightage.Name = UserInfo.getUserId();
            weightage.ETF_Leads__c = iETFLeads;
            weightage.Follow_up_Sales_Calls__c = iFollowupSalesCalls;
            weightage.Meeting_Face_to_Face__c = iMeetingFacetoFace;
            weightage.Profile_Metric__c = iEmailSubscription;
            weightage.Sales_Calls__c = iSalesCalls;
            weightage.Sales_Email__c = iSalesEmail;
            weightage.Scheduled_meeting_for_Ext_Specialist__c = iScheduledMeeting;
            weightage.Service_Calls__c = iServiceCalls;
            weightage.Zooms__c = iZooms;
            //weightage.Email_Subscription__c = iEmailSubscription;
            //weightage.Status__c = true;
            insert weightage;
        }
    }

        /************************************************************************
    * This method insert a new record to InternalDBActivityTypeScoreWeightage__c object.
    * If you pass bInactivateOthers param value as true, it will change the all exsisting record 
    * status to false and keep only this current record as active state.
    *
    * @Param idPriviousRecord                   Id of current record
    * @Param iZooms			                    Weightage for Zoom activity type
    * @Param iMeetingFacetoFace   				Weightage for Meeting Face to Face activity type
    * @Param iServiceCalls	                    Weightage for Service Calls activity type
    * @Param iSalesCalls           				Weightage for Sales Calls activity type
    * @Param iFollowupSalesCalls			    Weightage for Followup Sales Calls activity type
    * @Param iSalesEmail           				Weightage for Sales Email activity type
    * @Param iETFLeads			                Weightage for ETF Leads activity type
    * @Param iProfileMetric    				    Weightage for Profile Metric activity type
    * @Param iScheduledMeeting                  Weightage for Scheduled Meeting activity type
    * @Param bInactivateOthers   				If this true, change the status of old records in to false
    *
    * @Return      						        Record id of the newly created record
	************************************************************************/
    /*
    public static Id insertActivityTypeScoreWeightageWithIdCheck(Id idPriviousRecord,Integer iZooms,Integer iMeetingFacetoFace,Integer iServiceCalls,Integer iSalesCalls,Integer iFollowupSalesCalls,Integer iSalesEmail,Integer iETFLeads,Integer iProfileMetric,Integer iScheduledMeeting,Boolean bInactivateOthers){
        
        List<InternalDBActivityTypeScoreWeightage__c> lstPrivious = [SELECT Id,ETF_Leads__c,Follow_up_Sales_Calls__c,Meeting_Face_to_Face__c,Profile_Metric__c,Sales_Calls__c,Sales_Email__c,Scheduled_meeting_for_Ext_Specialist__c,Service_Calls__c,Zooms__c,Status__c FROM InternalDBActivityTypeScoreWeightage__c WHERE Id =:idPriviousRecord];
        if(lstPrivious.size() > 0){
            InternalDBActivityTypeScoreWeightage__c  rec = lstPrivious[0];
            if(rec.ETF_Leads__c == iETFLeads && rec.Follow_up_Sales_Calls__c == iFollowupSalesCalls && rec.Meeting_Face_to_Face__c == iMeetingFacetoFace && rec.Profile_Metric__c == iProfileMetric && rec.Sales_Calls__c == iSalesCalls && rec.Sales_Email__c == iSalesEmail &&
            rec.Scheduled_meeting_for_Ext_Specialist__c == iScheduledMeeting && rec.Service_Calls__c == iServiceCalls &&  rec.Zooms__c == iZooms && rec.Status__c == true){
                return idPriviousRecord;
            }
        }

        if(bInactivateOthers){
            List<InternalDBActivityTypeScoreWeightage__c> lstRecords = [SELECT Id,Status__c FROM InternalDBActivityTypeScoreWeightage__c WHERE Status__c = true];
            for(InternalDBActivityTypeScoreWeightage__c record: lstRecords){
                record.Status__c = false;
            }
            Update lstRecords;
        }
        InternalDBActivityTypeScoreWeightage__c weightage = new InternalDBActivityTypeScoreWeightage__c();
        weightage.Name = UserInfo.getUserId();
        weightage.ETF_Leads__c = iETFLeads;
        weightage.Follow_up_Sales_Calls__c = iFollowupSalesCalls;
        weightage.Meeting_Face_to_Face__c = iMeetingFacetoFace;
        weightage.Profile_Metric__c = iProfileMetric;
        weightage.Sales_Calls__c = iSalesCalls;
        weightage.Sales_Email__c = iSalesEmail;
        weightage.Scheduled_meeting_for_Ext_Specialist__c = iScheduledMeeting;
        weightage.Service_Calls__c = iServiceCalls;
        weightage.Zooms__c = iZooms;
        weightage.Status__c = true;
        insert weightage;

        return weightage.Id;
    }
    */
}