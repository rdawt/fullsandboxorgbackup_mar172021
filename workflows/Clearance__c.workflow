<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Clearance_Status_Alert</fullName>
        <description>Clearance Status Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>bdonnelly1@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cryser@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mwaldow@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Work_Flow_Templates/Clearance_Status_Alert</template>
    </alerts>
    <alerts>
        <fullName>Clearance_Status_Alert_To_Compliance</fullName>
        <description>Clearance_Status_Alert_To_Compliance</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>bdonnelly1@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svenkateswaran@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wkit@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Work_Flow_Templates/Clearance_Status_Alert_To_Compliance</template>
    </alerts>
    <fieldUpdates>
        <fullName>Compliance_Review_WFU</fullName>
        <description>This will capture the Date/Time whenever a user checks the checkbox of &apos;Compliance Review&apos;</description>
        <field>ComplianceReviewDate__c</field>
        <formula>Now()</formula>
        <name>Compliance Review WFU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Compliance_review_date_delete</fullName>
        <field>ComplianceReviewDate__c</field>
        <name>Compliance review date delete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Clearance Status Alert to Compliance</fullName>
        <actions>
            <name>Clearance_Status_Alert_To_Compliance</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>( ISCHANGED( Status__c ) &amp;&amp; PRIORVALUE(Status__c) ==  &apos;2&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clearance Status Alert to Owners</fullName>
        <actions>
            <name>Clearance_Status_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>( ISCHANGED( Status__c ) &amp;&amp; PRIORVALUE(Status__c) ==  &apos;2&apos; ) || ISCHANGED( Legal_Compliance_Comments__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Compliance review date delete</fullName>
        <actions>
            <name>Compliance_review_date_delete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Clearance__c.Compliance_Review__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Complaince review date value should be deleted when &apos;complaince review&apos; checkbox is unchecked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Compliance review date value</fullName>
        <actions>
            <name>Compliance_Review_WFU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Clearance__c.Compliance_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Complaince review date value should be captured when &apos;complaince review&apos; checkbox is checked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WFR-ClearanceHistory</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Clearance__c.Funds_Cleared__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
