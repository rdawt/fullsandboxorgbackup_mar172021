public class SL_MarketoRecords_Wrapper {

	public Integer id {get;set;} 
	public String marketoGUID {get;set;} 
	public Integer leadId {get;set;} 
	public String activityDate {get;set;} 
	public Integer activityTypeId {get;set;} 
	public Integer campaignId {get;set;} 
	public Integer primaryAttributeValueId {get;set;} 
	public String primaryAttributeValue {get;set;} 
	public String email {get;set;} 
	public String sfdcContactId {get;set;} 
	public String sfdcAccountId {get;set;} 
	public String sfdcLeadOwnerId {get;set;} 
	public List<Attributes> attributes {get;set;} 

	public SL_MarketoRecords_Wrapper(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getIntegerValue();
					} else if (text == 'marketoGUID') {
						marketoGUID = parser.getText();
					} else if (text == 'leadId') {
						leadId = parser.getIntegerValue();
					} else if (text == 'activityDate') {
						activityDate = parser.getText();
					} else if (text == 'activityTypeId') {
						activityTypeId = parser.getIntegerValue();
					} else if (text == 'campaignId') {
						campaignId = parser.getIntegerValue();
					} else if (text == 'primaryAttributeValueId') {
						primaryAttributeValueId = parser.getIntegerValue();
					} else if (text == 'primaryAttributeValue') {
						primaryAttributeValue = parser.getText();
					} else if (text == 'email') {
						email = parser.getText();
					} else if (text == 'sfdcContactId') {
						sfdcContactId = parser.getText();
					} else if (text == 'sfdcAccountId') {
						sfdcAccountId = parser.getText();
					} else if (text == 'sfdcLeadOwnerId') {
						sfdcLeadOwnerId = parser.getText();
					} else if (text == 'attributes') {
						attributes = arrayOfAttributes(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'SL_MarketoRecords consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public static List<SL_MarketoRecords_Wrapper> parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return arrayOfSL_MarketoRecords(parser);
	}
	
	public class Attributes {
		public String name {get;set;} 
		public String value {get;set;} 

		public Attributes(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'value') {
							value = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Attributes consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

    private static List<Attributes> arrayOfAttributes(System.JSONParser p) {
        List<Attributes> res = new List<Attributes>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Attributes(p));
        }
        return res;
    }


    private static List<SL_MarketoRecords_Wrapper> arrayOfSL_MarketoRecords(System.JSONParser p) {
        List<SL_MarketoRecords_Wrapper> res = new List<SL_MarketoRecords_Wrapper>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new SL_MarketoRecords_Wrapper(p));
        }
        return res;
    }

}