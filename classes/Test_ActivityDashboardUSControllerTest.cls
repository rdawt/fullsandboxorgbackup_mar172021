/**
* @File Name          : ActivityDashboardUSControllerTest.cls
* @Description        : Test Class for US Activity Dashboard
* @Author             : Vishruth
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                    Modification
*==============================================================================
* 1.0        07/22/2020               Vishruth
**/
@isTest
public class Test_ActivityDashboardUSControllerTest 
{
    public static integer recordCount = 5;
    public static User objUser1 = [Select Id, EMEADBAccess__c, Division from User where isActive=true and Profile.Name = 'System Administrator' limit 1];
    public static User objUser2 = [Select Id from User where Id !=:objUser1.Id and isActive=true limit 1];

    @TestSetup
    public static void createTestData()
    {
        objUser1.EMEADBAccess__c = 'Edit';
        objUser1.Division = 'Think ETF';
        update objUser1;
        
        User objCurrentuser = [Select Id from User where Id=: UserInfo.getUserId() limit 1];
        System.runAs(objUser1)
        {
            List<Task> lstAllTask = new List<Task>();
            List<Task> lstTaskEmail = (List<Task>)SL_TestDataFactory.createSObjectList(
                                    new Task(Subject='Email', ActivityDate = Date.today(),
                                            OwnerId=objUser1.Id),
                                        recordCount, false);
            List<Task> lstTaskMeeting = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Meeting', ActivityDate = Date.today().addDays(-30),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            List<Task> lstTaskCall = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Call', ActivityDate = Date.today().addDays(-20),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            List<Task> lstTaskUnknown = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Unknown', ActivityDate = Date.today().addDays(-10),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            lstAllTask.addAll(lstTaskEmail);
            lstAllTask.addAll(lstTaskMeeting);
            lstAllTask.addAll(lstTaskCall);
            lstAllTask.addAll(lstTaskUnknown);

            if(!lstAllTask.isEmpty())
                insert lstAllTask;
            
            SL_TestDataFactory.createTestDataForContact();
            Account objAccount = [Select Id from Account limit 1];  
            
            List<Opportunity> lstOpportunity = (List<Opportunity>)SL_TestDataFactory.createSObjectList(
                                                    new Opportunity(Name='Opportuntiy', AccountId=objAccount.Id, 
                                                                    CloseDate= date.today().addDays(10), StageName='Notified win/Pending funding'
                                                                    ),
                                                        1, false);
            if(!lstOpportunity.isEmpty())
                insert lstOpportunity;
        }    

    }   

    @isTest
    public static void testGetOnLoadRecords(){
        ActivityDashboardUS_Controller.getOnLoadRecords();
    }

    @isTest
    public static void testGetProfileName(){
        ActivityDashboardUS_Controller.getProfileName();
    }

    
    @isTest
    public static void testGetTaskAndEventByChannel(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        Test.stopTest();
    }
    
    
    @isTest
    public static void testGetTaskAndEventByChannelMinMaxNullAll() 
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();        
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        Test.stopTest();
    }
    
    @isTest
    public static void testGetTaskAndEventByChannelMinMaxNullNonAll() 
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();        
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        Test.stopTest();
    }
    
    
    @isTest
    public static void testGetTaskAndEventMonthWiseChart() 
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstAllOwner, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);       
        
        Test.stopTest();
    }
    
    
    
    
    @isTest
    public static void testGetTaskAndEventMonthWiseChartMinORMaxNullAll(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    @isTest
    public static void testGetTaskAndEventMonthWiseChartMinORMaxNullNonAll(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstAllOwner, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstAllOwner, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    @isTest
    public static void testGetTaskAndEventMonthWiseChartMinMaxNull() 
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecords_MonthWiseChart(lstAllOwner, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        Test.stopTest();
    }
    
    
    
    @isTest
    public static void testGetTaskAndEventUserWiseChart(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstAllOwner, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);       
        
        Test.stopTest();
    }
    
    
    @isTest
    public static void testGetTaskAndEventUserWiseChartMinORMaxNullNonAll(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    @isTest
    public static void testGetTaskAndEventUserWiseChartMinORMaxNullAll(){
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstAllOwner, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstAllOwner, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    @isTest
    public static void testGetTaskAndEventUserWiseChartMinMaxNull() // with min and max an not blank
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 

        ActivityDashboardUS_Controller.getTaskAndEventRecords_UserWiseChart(lstAllOwner, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    
    
    @isTest
    public static void getTaskAndEventRecordsByChannel() // with min and max are not blank
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);       
        
        Test.stopTest();
    }
    
    
    @isTest
    public static void getTaskAndEventRecordsByChannelMinORMaxNull()  // with min or max are not blank
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);

        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstAllOwner, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1); 
        
        Test.stopTest();
    }
    
    @isTest
    public static void getTaskAndEventRecordsByChannelMinMaxNull() // with min and max are blank
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstAllOwner = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        
        Test.startTest();
        
        ActivityDashboardUS_Controller.getTaskAndEventRecordsByChannel(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1);        
        
        Test.stopTest();
    }
    
}