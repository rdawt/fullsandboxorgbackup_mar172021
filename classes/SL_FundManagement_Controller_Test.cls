@isTest
private class SL_FundManagement_Controller_Test 
{
	@TestSetup
    private static void setUp()
    {
    	User objUser = [Select Id from User where Profile.Name = 'System Administrator' and isActive = true limit 1];
    	System.runAs(objUser)
    	{
    		SL_TestDataFactory.createTestDataForFundManagement();
    	}
    }
	
    @isTest
    private static void testGetUserFocusFundRecords()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);
            	
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserFocusFundRecords_OverrideOnlyfor1()
    {
    	Fund__c objFund = [Select Id from Fund__c limit 1];
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = objFund.Id;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
    		List<SL_FundManagement_Controller.FundManage> returnResult = new List<SL_FundManagement_Controller.FundManage>();
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	
            	returnResult = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);
            Test.stopTest(); 
            
            System.assertEquals(1,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserFocusFundRecords_OverrideOnlyfor5()
    {
    	String strFunds = '';
    	for(Fund__c objFund : [Select Id from Fund__c limit 5])
    	{
    		strFunds += objFund.Id + ','; 
    	}
    	
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = strFunds;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
    		List<SL_FundManagement_Controller.FundManage> returnResult = new List<SL_FundManagement_Controller.FundManage>();
    		
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	returnResult = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(5,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserDefaultFundRecords_Criteria1()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserDefaultFundRecords_Criteria2()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Contact_Criteria1__c = '';
        objUser.Focus_Funds_Criteria1__c =''; 
    	objUser.Focus_Contact_Criteria2__c = 'Channel_NEW__c <> \'\' ';
        objUser.Focus_Funds_Criteria2__c ='Fund_Type__c = \'FI_ETF\' '; 
    	update objUser;
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserDefaultFundRecords_OverrideFor1()
    {
    	Fund__c objFund = [Select Id from Fund__c limit 1];
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = objFund.Id;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserDefaultFundRecords_OverrideFor5()
    {
    	String strFunds = '';
    	for(Fund__c objFund : [Select Id from Fund__c limit 5])
    	{
    		strFunds += objFund.Id + ','; 
    	}
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = strFunds;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetUserOtherFundRecords()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
            	List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.getUserOtherFundRecords(result, objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetOtherFundRecords()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
    		List<SL_FundManagement_Controller.FundManage> result = new List<SL_FundManagement_Controller.FundManage>();	
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
            	
				result = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals(15,result.size());
        }
    }
    
    @isTest
    private static void testGetOtherSearchRecords()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
            	Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
            	
				List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
				
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getOtherSearchRecords('', '', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(15,result.size());
        }
    }
    
    @isTest
    private static void testGetOtherSearchRecords_FundType()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
            
            	Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
            	
				List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
				
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getOtherSearchRecords('', 'MF', objContact.Id, resultOld);
            
            Test.stopTest(); 
        }
    }
    
    @isTest
    private static void testGetOtherSearchRecords_FundName_FundType()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);	
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getOtherSearchRecords('Test Fund', 'MF', objContact.Id,resultOld);
            
            Test.stopTest(); 
            System.assertEquals(5,result.size());
        }
    }
    
    //Focus Search - Override False
    @isTest
    private static void testGetFocusSearchRecords_FundName()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);		
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getFocusSearchRecords('Test Fund', '', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(10,result.size());
        }
    }
    
    //Focus Search - Override TRUE
    @isTest
    private static void testSetNewValuesTOldList()
    {
    	String strFunds = '';
    	for(Fund__c objFund : [Select Id from Fund__c limit 5])
    	{
    		strFunds += objFund.Id + ','; 
    	}
    	
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = strFunds;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);		
				List<SL_FundManagement_Controller.FundManage> resultNew = SL_FundManagement_Controller.getFocusSearchRecords('Test Fund', '', objContact.Id, resultOld);
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.setNewValuesTOldList(resultOld, resultNew);
            
            Test.stopTest(); 
            System.assertEquals(5,result.size());
        }
    }
    
    //Focus Search - Override TRUE
    @isTest
    private static void testGetFocusSearchRecords_FundName_OverrideTrue()
    {
    	String strFunds = '';
    	for(Fund__c objFund : [Select Id from Fund__c limit 5])
    	{
    		strFunds += objFund.Id + ','; 
    	}
    	
    	
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	objUser.Focus_Funds_Override__c = true;
    	objUser.Focus_Fund_Ids__c = strFunds;
    	update objUser;
    	
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);		
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getFocusSearchRecords('Test Fund', '', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(5,result.size());
        }
    }
    
    //Default Search
    @isTest
    private static void testGetDefaultSearchRecords_FundName()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);		
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getDefaultSearchRecords('Test Fund', '', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(10,result.size());
        }
    }
    
    @isTest
    private static void testGetDefaultSearchRecords_FundType()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getDefaultSearchRecords('', 'MF', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(10,result.size());
        }
    }
    
    @isTest
    private static void testGetDefaultSearchRecords_FundName_FundType()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
	            
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getDefaultSearchRecords('Test Fund', 'MF', objContact.Id, resultOld);
            
            Test.stopTest(); 
            System.assertEquals(10,result.size());
        }
    }
    
    
    @isTest
    private static void testSetSelectSingleForFunds()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				Fund__c objFund = [Select Id from Fund__c where Fund_Type__c = 'MF' limit 1];
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
				List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.setSelectSingleForFunds(result, objFund.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testSetSelectSingleForFunds_None()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				Fund__c objFund = [Select Id from Fund__c where Fund_Type__c = 'MF' limit 1];
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
				
				for(SL_FundManagement_Controller.FundManage objFundManage : result)
				{
					objFundManage.strFundInterested = 'None';
					objFundManage.strFundClient = 'No';
				}
				
				List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.setSelectSingleForFunds(result, objFund.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testGetMobileSearchRecords_Search_Other_Funds()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
				
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getMobileSearchRecords('Test Fund', 'MF', 'Search Other Funds', objContact.Id, resultOld);
            
            Test.stopTest(); 
        }
    }
    
    @isTest
    private static void testGetMobileSearchRecords_Search_Default_Funds()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), 5, true);
                                    
	            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
	                                    
	            for(Fund__c objFund : lstFundsMF)
	            {
	            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
	            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='NOT Interested',
	            																Fund__c =  objFund.Id, Fund_Client__c=false), false);
	            	lstFund_Interest.add(objFund_Interest);
	            }
	            
	            if(!lstFund_Interest.isEmpty())
	            	insert lstFund_Interest;
	            	
	            List<SL_FundManagement_Controller.FundManage> resultOld = SL_FundManagement_Controller.getOtherFundRecords(objContact.Id);
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getMobileSearchRecords('Test Fund', 'ETF', 'Search Default Funds', objContact.Id,resultOld);
            
            Test.stopTest(); 
        }
    }
    
    @isTest
    private static void testClearHideClassForSelectSingleForOtherFunds()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				Fund__c objFund = [Select Id from Fund__c where Fund_Type__c = 'MF' limit 1];
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
				List<SL_FundManagement_Controller.FundManage> returnResult = SL_FundManagement_Controller.clearHideClassForSelectSingleForOtherFunds(result, objFund.Id);
            
            Test.stopTest(); 
            System.assertEquals(10,returnResult.size());
        }
    }
    
    @isTest
    private static void testIsAnyCheckBoxSelected()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				Fund__c objFund = [Select Id from Fund__c where Fund_Type__c = 'MF' limit 1];
				List<SL_FundManagement_Controller.FundManage> result = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
				Boolean returnResult = SL_FundManagement_Controller.isAnyCheckBoxSelected(result);
            
            Test.stopTest(); 
            System.assertEquals(true,returnResult);
        }
    }
    
    @isTest
    private static void testGetContactInformation()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				String result = SL_FundManagement_Controller.getContactInformation(objContact.Id);
            
            Test.stopTest(); 
            System.assertEquals('Test Contact', result);
        }
    }
    
    @isTest
    private static void testGetUserLocation()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				String result = SL_FundManagement_Controller.getUserLocation();
            
            Test.stopTest(); 
        }
    }
    
    @isTest
    private static void testGetBaseUrl()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				String result = SL_FundManagement_Controller.getBaseUrl();
            
            Test.stopTest(); 
        }
    }
    
    @isTest
    private static void testSaveFundsRecords()
    {
    	User objUser = [Select Id from User where FirstName = 'Test First Name' and LastName = 'Test Last Name' limit 1];
    	System.runAs(objUser)
    	{
            Test.startTest();
				
				Contact objContact = [Select Id from Contact limit 1];
				Fund__c objFundMF = [Select Id from Fund__c where Fund_Type__c = 'MF' limit 1];
				Fund__c objFundETF = [Select Id from Fund__c where Fund_Type__c = 'ETF' limit 1];
				List<SL_FundManagement_Controller.FundManage> returnFocus = SL_FundManagement_Controller.getUserFocusFundRecords(objContact.Id);
				for(SL_FundManagement_Controller.FundManage objManage : returnFocus)
				{
					objManage.isCurrentBooleanChecked = true;
					objManage.strFundInterested = 'Yes';
					
					if(objManage.strFundId == objFundMF.Id)
					{
						objManage.strFundInterested = 'No';
					}
				}
				
				List<SL_FundManagement_Controller.FundManage> resultDefault = SL_FundManagement_Controller.getUserDefaultFundRecords(objContact.Id);
				for(SL_FundManagement_Controller.FundManage objManage : resultDefault)
				{
					objManage.isCurrentBooleanChecked = true;
					objManage.strFundInterested = 'Yes';
					
					if(objManage.strFundId == objFundMF.Id)
					{
						objManage.strFundInterested = 'No';
					}
				}
				
				List<SL_FundManagement_Controller.FundManage> resultOther = SL_FundManagement_Controller.getUserOtherFundRecords(resultDefault, objContact.Id);
				for(SL_FundManagement_Controller.FundManage objManage : resultOther)
				{
					objManage.isCurrentBooleanChecked = true;
					objManage.strFundInterested = 'Yes';
					
					if(objManage.strFundId == objFundETF.Id)
					{
						objManage.strFundInterested = 'No';
					}
				}
				
				
				SL_FundManagement_Controller.saveFundsRecords(returnFocus, resultDefault, resultOther, objContact.Id);
            
            Test.stopTest(); 
        }
    }
}