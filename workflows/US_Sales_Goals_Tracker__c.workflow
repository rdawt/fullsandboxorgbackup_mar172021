<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Amber_range_end_update</fullName>
        <field>Amber_Range_End__c</field>
        <formula>1000000</formula>
        <name>Amber range end update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Amber_range_start_update</fullName>
        <field>Amber_Range_Start__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000,  Goal_Amount__c -(((CEILING((Goal_Amount__c/1000000000)/3))*1000000000)*2)-1000000 ,  IF(Goal_Amount__c &gt;= 1000000, (Goal_Amount__c-((CEILING((Goal_Amount__c/1000000)/3))*1000000)*2)-1000000, (Goal_Amount__c - ((CEILING((Goal_Amount__c)/3)))*2)-1000000))</formula>
        <name>Amber range start update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Amber_range_update</fullName>
        <field>Amber_Range__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000, Goal_Amount__c -(((CEILING((Goal_Amount__c/1000000000)/3))*1000000000)*2) , IF(Goal_Amount__c &gt;= 1000000, (Goal_Amount__c-((CEILING((Goal_Amount__c/1000000)/3))*1000000)*2), (Goal_Amount__c - ((CEILING((Goal_Amount__c)/3)))*2)))</formula>
        <name>Amber range update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Green_range_end_update</fullName>
        <field>Green_Range_End__c</field>
        <formula>1000000</formula>
        <name>Green range end update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Green_range_start_update</fullName>
        <field>Green_Range_Start__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000,  ((CEILING((Goal_Amount__c/1000000000)/3))*1000000000)-1000000 ,  IF(Goal_Amount__c &gt;= 1000000, ((CEILING((Goal_Amount__c/1000000)/3))*1000000)-1000000, ((CEILING((Goal_Amount__c)/3)))-1000000))</formula>
        <name>Green range start update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Green_range_update</fullName>
        <field>Green_Range__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000, ((CEILING((Goal_Amount__c/1000000000)/3))*1000000000), IF(Goal_Amount__c &gt;= 1000000, ((CEILING((Goal_Amount__c/1000000)/3))*1000000), ((CEILING((Goal_Amount__c)/3)))))</formula>
        <name>Green range update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_range_Start_update</fullName>
        <field>Red_Range_Start__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000,  ((CEILING((Goal_Amount__c/1000000000)/3))*1000000000)-1000000 ,  IF(Goal_Amount__c &gt;= 1000000, ((CEILING((Goal_Amount__c/1000000)/3))*1000000)-1000000, ((CEILING((Goal_Amount__c)/3)))-1000000))</formula>
        <name>Red range Start update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_range_end_update</fullName>
        <field>Red_Range_End__c</field>
        <formula>1000000</formula>
        <name>Red range end update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Red_range_update</fullName>
        <field>Red_Range__c</field>
        <formula>IF(Goal_Amount__c &gt;= 1000000000, ((CEILING((Goal_Amount__c/1000000000)/3))*1000000000) , IF(Goal_Amount__c &gt;= 1000000, ((CEILING((Goal_Amount__c/1000000)/3))*1000000), ((CEILING((Goal_Amount__c)/3)))))</formula>
        <name>Red range update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Display_ETF_Source_Data_to_True</fullName>
        <description>If conditions are met, update Display_ETF_Source_Data field to true</description>
        <field>Display_ETF_Source_Data__c</field>
        <literalValue>1</literalValue>
        <name>Update Display ETF Source Data to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update ETF Source Data Field</fullName>
        <actions>
            <name>Update_Display_ETF_Source_Data_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>US_Sales_Goals_Tracker__c.ETF_Channel__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>US_Sales_Goals_Tracker__c.Fund_Vehicle_Type__c</field>
            <operation>equals</operation>
            <value>ETF</value>
        </criteriaItems>
        <description>If record is a ETF record, automatically update Display_ETF_Source_Data field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Goal Amount-Obsolete</fullName>
        <actions>
            <name>Amber_range_end_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Amber_range_start_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Green_range_end_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Green_range_start_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Red_range_Start_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Red_range_end_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>US_Sales_Goals_Tracker__c.Goal_Year__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Goal Range Values</fullName>
        <actions>
            <name>Amber_range_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Green_range_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Red_range_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED( Goal_Amount__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
