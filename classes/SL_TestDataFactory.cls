@isTest
public class SL_TestDataFactory {
	public static String currentTest = 'NULL';
    private static Map<Boolean, String> mapMessageByResult = new Map<Boolean, String>();
    private static Integer failures = 0;
    private static Integer passes = 0;
    private static Integer recordCount = 10;
    
    
    public static void generateTestData() {
        //to be completed by developer
        
    }
    
    public static void createTestDataForCustomSetting()
	{
		Date currentDate = date.today();
		
		TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
		settings.isActive_Task__c = TRUE;
		settings.isActive_Account__c = TRUE;
		settings.isActive_Contact__c = TRUE;
		settings.isApex_Task_Workflows__c = TRUE;
		upsert settings custSettings__c.Id;
		
		SendEmailNotifications__c objSendEmailNotifications = SendEmailNotifications__c.getOrgDefaults();
		objSendEmailNotifications.Email__c = 'svenkateswaran@vaneck.com';
		upsert objSendEmailNotifications custSettings__c.Id;
		
		PayeeSummaryBatch_Criteria__c objPayeeSummaryBatchCriteria = PayeeSummaryBatch_Criteria__c.getOrgDefaults();
		objPayeeSummaryBatchCriteria.Activity_Count_Criteria__c = 'ActivityDate >= Date.newInstance(10,01,2019) AND ActivityDate <= Date.newInstance(10,31,2019)';
		objPayeeSummaryBatchCriteria.Month__c = String.valueOf(currentDate.Month());
		objPayeeSummaryBatchCriteria.Quarter__c = String.valueOf(Integer.valueOf(currentDate.Month()/3) + 1);
		objPayeeSummaryBatchCriteria.Query_Criteria__c = 'AND (CreatedDate = THIS_QUARTER OR CreatedDate = LAST_QUARTER) AND CreatedDate=THIS_YEAR';
		objPayeeSummaryBatchCriteria.Year__c = String.valueOf(currentDate.year());
		upsert objPayeeSummaryBatchCriteria custSettings__c.Id;
		
	}
	
	public static void createTestDataForCustomSettingSentEmail()
	{
		SendEmailNotifications__c objSendEmailNotifications = SendEmailNotifications__c.getOrgDefaults();
		objSendEmailNotifications.Email__c = 'svenkateswaran@vaneck.com';
		upsert objSendEmailNotifications custSettings__c.Id;
	}
    
    public static void createTestDataForContact()
    {
    	//Get Services User
    	Profile objProfile = [Select Id from Profile where Name = 'System Administrator' limit 1];
        User servicesUser = createTestUser(objProfile.Id, 'Test First Name', 'Test Last Name');
        insert servicesUser;
        
        
        //Get Record Types
        Map<String, Schema.RecordTypeInfo> accountRecordTypeNameToIdMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Id SalesConnectFirm = accountRecordTypeNameToIdMap.get('SalesConnect Firm').getRecordTypeId();
        
        Map<String, Schema.RecordTypeInfo> contactRecordTypeNameToIdMap = Schema.SObjectType.Contact.getRecordTypeInfosByName();
        
        
        //Create test records 
        System.runAs(servicesUser){
            
            Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Bank', 
            																			RecordTypeId = SalesConnectFirm,
            																			BillingCountry='United States'), True);
            system.assert(objAccount.id != NULL);
            
            Subscription_Group__c objSubscription_Group = (Subscription_Group__c)SL_TestDataFactory.createSObject(new Subscription_Group__c
            																			(Name='Subscription', IsActive__c = true, 
            																			Description__c = 'Test Description',
            																			Eligibility_Sort_Order__c=1, KASL_Filter__c = 'ETF Trader'), True);
            system.assert(objSubscription_Group.id != NULL);	
            																		
            Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(AccountId = objAccount.Id, 
            																			Unsubscribe_Public__c = 'Unsubscribe me from future emails',
        																				FirstName='Test', LastName='Contact', 
        																				Email='test@testvaneck.com.test',
        																				MailingCountry = 'United States',
        																				Subscription_Group__c = objSubscription_Group.id
        																				), True);
        }  
    }
    
    public static void createTestDataForFundManagementBySpecificUser(User servicesUser)
    {
        //Get Record Types
        Map<String, Schema.RecordTypeInfo> accountRecordTypeNameToIdMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Id SalesConnectFirm = accountRecordTypeNameToIdMap.get('SalesConnect Firm').getRecordTypeId();
        
        Map<String, Schema.RecordTypeInfo> contactRecordTypeNameToIdMap = Schema.SObjectType.Contact.getRecordTypeInfosByName();
        
        SL_TestDataFactory.createTestDataForCustomSetting();
        //Create test records 
        System.runAs(servicesUser){
            
            Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Bank', 
            																			RecordTypeId = SalesConnectFirm,
            																			BillingCountry='United States'), True);
            system.assert(objAccount.id != NULL);
            
            Subscription_Group__c objSubscription_Group = (Subscription_Group__c)SL_TestDataFactory.createSObject(new Subscription_Group__c
            																			(Name='Subscription', IsActive__c = true, 
            																			Description__c = 'Test Description',
            																			Eligibility_Sort_Order__c=1, KASL_Filter__c = 'ETF Trader'), True);
            system.assert(objSubscription_Group.id != NULL);	
            																		
            Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(AccountId = objAccount.Id, 
            																			Unsubscribe_Public__c = 'Unsubscribe me from future emails',
        																				FirstName='Test', LastName='Contact', 
        																				Email='test@testvaneck.com.test',
        																				MailingCountry = 'United States',
        																				Subscription_Group__c = objSubscription_Group.id
        																				), True);
            
            //Case
            List<Fund__c> lstFundsFI_ETF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='FI_ETF'), recordCount, true);
                                    
            List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), recordCount, true);
                                    
            List<Fund__c> lstFundsETF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='ETF'), recordCount, true); 
            
            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
                                    
            for(Fund__c objFund : lstFundsMF)
            {
            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='Interested',
            																Fund__c =  objFund.Id, Fund_Client__c=true), false);
            	lstFund_Interest.add(objFund_Interest);
            }
            
            if(!lstFund_Interest.isEmpty())
            	insert lstFund_Interest;
        }  
    }
    
    public static void createTestDataForFundManagement()
    {
    	//Get Services User
    	Profile objProfile = [Select Id from Profile where Name = 'System Administrator' limit 1];
        User servicesUser = createTestUser(objProfile.Id, 'Test First Name', 'Test Last Name');
        servicesUser.Focus_Contact_Criteria1__c = 'Channel_NEW__c <> \'\' ';
        servicesUser.Focus_Funds_Criteria1__c ='Fund_Type__c = \'FI_ETF\' '; 
        insert servicesUser;
        
        
        //Get Record Types
        Map<String, Schema.RecordTypeInfo> accountRecordTypeNameToIdMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Id SalesConnectFirm = accountRecordTypeNameToIdMap.get('SalesConnect Firm').getRecordTypeId();
        
        Map<String, Schema.RecordTypeInfo> contactRecordTypeNameToIdMap = Schema.SObjectType.Contact.getRecordTypeInfosByName();
        
        
        //Create test records 
        System.runAs(servicesUser){
            
            Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Bank', 
            																			RecordTypeId = SalesConnectFirm,
            																			BillingCountry='United States'), True);
            system.assert(objAccount.id != NULL);
            
            Subscription_Group__c objSubscription_Group = (Subscription_Group__c)SL_TestDataFactory.createSObject(new Subscription_Group__c
            																			(Name='Subscription', IsActive__c = true, 
            																			Description__c = 'Test Description',
            																			Eligibility_Sort_Order__c=1, KASL_Filter__c = 'ETF Trader'), True);
            system.assert(objSubscription_Group.id != NULL);	
            																		
            Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(AccountId = objAccount.Id, 
            																			Unsubscribe_Public__c = 'Unsubscribe me from future emails',
        																				FirstName='Test', LastName='Contact', 
        																				Email='test@testvaneck.com.test',
        																				MailingCountry = 'United States',
        																				Subscription_Group__c = objSubscription_Group.id
        																				), True);
            
            //Case
            List<Fund__c> lstFundsFI_ETF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='FI_ETF'), recordCount, true);
                                    
            List<Fund__c> lstFundsMF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='MF'), recordCount, true);
                                    
            List<Fund__c> lstFundsETF = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                    new Fund__c(Name='Test Fund', Fund_Type__c='ETF'), recordCount, true); 
            
            List<Fund_Interest__c> lstFund_Interest = new List<Fund_Interest__c>();
                                    
            for(Fund__c objFund : lstFundsMF)
            {
            	Fund_Interest__c objFund_Interest = (Fund_Interest__c)SL_TestDataFactory.createSObject(
            											new Fund_Interest__c(Contact__c=objContact.Id, Level_Of_Interest__c='Interested',
            																Fund__c =  objFund.Id, Fund_Client__c=true), false);
            	lstFund_Interest.add(objFund_Interest);
            }
            
            if(!lstFund_Interest.isEmpty())
            	insert lstFund_Interest;
        }  
    }
    
    public static User createTestUser(Id profID, String fName, String lName) {
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User objTestuser = new User(  firstname = fName,
                            lastName = lName,
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            isActive = true,
                            ProfileId = profId);
        return objTestuser;
    }
    
	public static SObject createSObject(SObject sObj) {
		// Check what type of object we are creating and add any defaults that are needed.
		String objectName = String.valueOf(sObj.getSObjectType());
		// Construct the default values class. Salesforce doesn't allow '__' in class names
		// Special cases can be adjusted on a case by case basis 
		//(i.e. orgs with standard and custom objects that would result in the same objectName)
		String defaultClassName = 'SL_TestDataFactory.' + objectName.replaceAll('__(c|C)$|__', '') + 'Defaults';
		// If there is a class that exists for the default values, then use them
		if (Type.forName(defaultClassName) != null) {
			sObj = createSObject(sObj, defaultClassName);
		}
		return sObj;
	}

	public static SObject createSObject(SObject sObj, Boolean doInsert) {
		SObject retObject = createSObject(sObj);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName) {
		// Create an instance of the defaults class so we can get the Map of field defaults
		Type t = Type.forName(defaultClassName);
		if (t == null) {
			Throw new TestFactoryException('Invalid defaults class.');
		}
		FieldDefaults defaults = (FieldDefaults)t.newInstance();
		addFieldDefaults(sObj, defaults.getFieldDefaults());
		return sObj;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
		SObject retObject = createSObject(sObj, defaultClassName);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
		return createSObjectList(sObj, numberOfObjects, (String)null);
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, (String)null);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
		SObject[] sObjs = new SObject[] {};
		SObject newObj;

		// Get one copy of the object
		if (defaultClassName == null) {
			newObj = createSObject(sObj);
		} else {
			newObj = createSObject(sObj, defaultClassName);
		}

		// Get the name field for the object
		String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
		if (nameField == null) {
			nameField = 'Name';
		}

		//Getting Field map to check if the Name is updateable...
		String type = String.valueOf(sObj.getSObjectType());
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();

        Schema.SObjectType objSchema = schemaMap.get(type);
		Map<String, Schema.SObjectField> fieldMap = objSchema.getDescribe().fields.getMap();

		// Clone the object the number of times requested. Increment the name field so each record is unique
		for (Integer i = 0; i < numberOfObjects; i++) {
			SObject clonedSObj = newObj.clone(false, true);
			//Checking if the Name is Updateable.....
			//If true only then increment the value..
			if(fieldMap.get(nameField).getDescribe().isUpdateable())
				clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
			sObjs.add(clonedSObj);
		}
		return sObjs;
	}

	private static void addFieldDefaults(SObject sObj, Map<Schema.SObjectField, Object> defaults) {
		// Loop through the map of fields and if they weren't specifically assigned, fill them.
		Map<String, Object> populatedFields = sObj.getPopulatedFieldsAsMap();
        	for (Schema.SObjectField field : defaults.keySet()) {
			if (!populatedFields.containsKey(String.valueOf(field))) {
				sObj.put(field, defaults.get(field));
			}
		}
	}

	// When we create a list of SObjects, we need to
	private static Map<String, String> nameFieldMap = new Map<String, String> {
		'Contact' => 'LastName',
		'Case' => 'Subject',
		'Task'=> 'Subject',
		'Event'=> 'Subject'
	};

	public class TestFactoryException extends Exception {}

	// Use the FieldDefaults interface to set up values you want to default in for all objects.
	public interface FieldDefaults {
		Map<Schema.SObjectField, Object> getFieldDefaults();
	}

	// To specify defaults for objects, use the naming convention [ObjectName]Defaults.
	// For custom objects, omit the __c from the Object Name

	public class AccountDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Account.Name => 'Test Account'
			};
		}
	}

	public class ContactDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Contact.FirstName => 'First',
				Contact.LastName => 'Last'
			};
		}
	}

	public class OpportunityDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Opportunity.Name => 'Test Opportunity',
				Opportunity.StageName => 'Closed Won',
				Opportunity.CloseDate => System.today()
			};
		}
	}

	public class CaseDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Case.Subject => 'Test Case'
			};
		}
	}
    
    //used to soft assert primitive data types
	public static void softAssertEquals(Object o1, Object o2){
        String message;
        
        if(o1 == o2) {
            passes++;
            message = '\n\nSoft Assert Succeeded: [' + o1 + ' = ' + o2 + ']\n';
            system.debug(message);
            trackSoftAssertResult(true, message);
        }
        else {
            failures++;
            message = '\n\nSoft Assert Failed: [' + o1 + ' != ' + o2 + ']\n';
			system.debug(message);
			trackSoftAssertResult(false, message);
        }
	}
    //used to soft assert primitive data types
	public static void softAssertNotEquals(Object o1, Object o2){
        String message;
        
        if(o1 == o2) {
            failures++;
            message = '\n\nSoft Assert Failed: [' + o1 + ' = ' + o2 + ']\n';
            system.debug(message);
            trackSoftAssertResult(false, message);
        }
        else {
            passes++;
            message = '\n\nSoft Assert Passed: [' + o1 + ' != ' + o2 + ']\n';
			system.debug(message);
			trackSoftAssertResult(true, message);
        }
	}    
    public static void softAssert(Boolean result, String message) {
        if (result) {
            passes++;
            system.debug(message);
            trackSoftAssertResult(true, message);
        } else {
            failures++;
            system.debug(message);
            trackSoftAssertResult(false, message);
        }
    }
    
    //call after all soft asserts completed in a given method to determine
    // if any or all test passed
    // if one or more soft assert test fail, entire method will fail
    public static void hardAssertAllResults() {
        if (mapMessageByResult.containsKey(false)) {
            //fail
            String failResult = mapMessageByResult.get(false);
            String passResult = mapMessageByResult.containsKey(true) ? ' with results ' + mapMessageByResult.get(true) : '';
            mapMessageByResult.clear(); //clear results for next test
            System.assert(false, 'Fail: ' + failures + ' test(s) fail because of ' + failResult + ' '
                         + passes + ' test(s) passes' + passResult);
        } else {
            //pass
            System.assert(true);
        }
    }
    private static void trackSoftAssertResult(Boolean hasPass, String testResult) {
        if(mapMessageByResult.containsKey(hasPass)) {
            testResult = mapMessageByResult.get(hasPass) + '\n' + testResult;
        }
        mapMessageByResult.put(hasPass, testResult);
    }
	
   
}