<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETF BD West</label>
    <protected>false</protected>
    <values>
        <field>Contact_Mailing_State__c</field>
        <value xsi:type="xsd:string">NM,AZ,TX,OK,MO,LA,KS,AR,WY,WA,UT,SD,OR,NE,ND,MT,ID,CO,WI,MN,IL,IA,NV,HI,CA,AK</value>
    </values>
    <values>
        <field>Firm_Channel__c</field>
        <value xsi:type="xsd:string">Financial Advisor</value>
    </values>
</CustomMetadata>
