/**
    Author: Vidhya Krishnan
    Date Created: 5/9/2012
    Description: Class to test Update Contact Subs Indicator 
    Last Modified:
    	9-5-2012	Wilson Ng	Test class failing due to scheduler time in the past.  Fixed scheduler time for future.
 */
@isTest
private class updateContactSubIndrTest {

    static testMethod void test() {
    	try{
        Test.startTest();
        // Schedule the test job
        string jobId = System.schedule('testConScheduleApex', '0 0 0 5 9 ? 2022',  new updateContactSubIndicator());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals('0 0 0 5 9 ? 2022', ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
        System.assertEquals('2022-09-05 00:00:00',String.valueOf(ct.NextFireTime));
        
        List<Contact> c = new List<Contact>();
        List<Subscription_Member__c> sm = new List<Subscription_Member__c>();
        Account acc = new Account(Name = 'ektron', Channel__c = 'RIA', BillingCountry='US');
        insert acc;
        Subscription__c s1 = new Subscription__c(Name='Test Subscription1', Subscription_Display_Name__c='Test Subscription1', Subscription_Indicator_Marker__c='abc1');
        insert s1;
        Subscription__c s2 = new Subscription__c(Name='Test Subscription2', Subscription_Display_Name__c='Test Subscription2', Subscription_Indicator_Marker__c='abc2');
        insert s2;
        Fund__c f1 = new Fund__c(Name='Test Fund1',Fund_Type__c='MF',Fund_Ticker_Symbol__c='abc1');
        insert f1;
        Fund__c f2 = new Fund__c(Name='Test Fund2',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='abc2');
        insert f2;
        Contact con1 = new Contact(AccountId=acc.Id,LastName='xyz1',FirstName='test',Email='abc1@xyc.com',MailingCountry='US',Schedule_Apex_Indicator__c='Subscription');
        insert con1;
        Subscription_Member__c sub1 = new Subscription_Member__c(Email__c = 'abc1@xyc.com', Contact__c = con1.ID, Subscribed__c = true, Subscription__c = s1.ID);
        insert sub1;
        Subscription_Member__c sub2 = new Subscription_Member__c(Email__c = 'abc2@xyc.com', Contact__c = con1.ID, Subscribed__c = true, Subscription__c = s2.ID);
        insert sub2;
        Contact con = new Contact(AccountId=acc.Id,LastName='xyz',FirstName='test',Email='abc@xyc.com',MailingCountry='US',Schedule_Apex_Indicator__c='Fund');
        insert con;
        Fund_Interest__c fnd1 = new Fund_Interest__c(Fund__c=f1.Id,Contact__c=con.Id,Account__c=acc.Id,Level_Of_Interest__c='Interested');
        insert fnd1;
        Fund_Interest__c fnd2 = new Fund_Interest__c(Fund__c=f2.Id,Contact__c=con.Id,Account__c=acc.Id,Level_Of_Interest__c='Interested');
        insert fnd2;
        

        Test.stopTest();
        //system.assertEquals(null, con.Dynamic_Subscription_Date__c); // will be updated only after the schedule specified above.
    	}catch(Exception e){}
    }
}