({
	doInitialize : function(component,event) {
        component.set("v.showSpinner",true);
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.getBusinessMixDetails");
        action.setParams({
            "recordId" : recordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var returnValue  = response.getReturnValue();
                var conObj = returnValue.conObj;
                var busMixwrapperLst = returnValue.busMixWrapperLst;
                
                var currentRecordName = '';
                if ('FirstName' in conObj) {
                    currentRecordName = currentRecordName+ conObj.FirstName+' ';
                }
                currentRecordName = currentRecordName + conObj.LastName;
                component.set("v.currentRecordName",currentRecordName);
                component.set("v.busMixWrapperLst",JSON.parse(JSON.stringify(busMixwrapperLst)));
                component.set("v.busMixWrapperLstTemp",busMixwrapperLst);
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    handleSubmitClick : function(component,event){
        var busMixWrapperLstTemp = component.get("v.busMixWrapperLstTemp");
        var busMixWrapperLst = component.get("v.busMixWrapperLst");
        var recordId = component.get("v.recordId");
        
        component.set("v.showSpinner",true);
        var action = component.get("c.insertOrUpdateBusMix");
        action.setParams({
            "currentRecordId" : recordId,
            "busMixWrapperLstTemp" : JSON.stringify(busMixWrapperLstTemp),
            "busMixWrapperLstOld" : JSON.stringify(busMixWrapperLst)
        });
        
        action.setCallback(this,function(respose){
            console.log('state------>',respose.getState());
            console.log('retrun -------->',respose.getReturnValue());
            var state = respose.getState();
            if (state === "SUCCESS"){
                if(respose.getReturnValue() == 'Success!'){
                    this.cancelOut(component,event);    
                }
                else{
                    component.set("v.showMessage",true);
                    component.set("v.showSpinner",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //go back to the contact record
    cancelOut : function(component,event){
        var recordId = component.get("v.recordId");
        
        //var action = component.get('c.getBaseUrl')
        //action.setCallback(this, function (response) {
            //var state = response.getState()
            //if (component.isValid() && state === 'SUCCESS') {
                //var result = response.getReturnValue();
                //console.log('base URL --------->',result);
                if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
                {
                    var result = '/lightning/r/Contact/'+recordId+'/view';
                    sforce.one.navigateToURL(result);
                }
                else{
                    var result = '/'+recordId;
                    window.open(result,'_top');
                }
                console.log('result--------->',result);
                //window.open(result,'_top');
			//}
        //})
        //$A.enqueueAction(action);
    }
})