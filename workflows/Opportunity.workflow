<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_U_S_Financial_Advisors_Populate_Fi</fullName>
        <field>Description</field>
        <formula>Contact__r.AccountId</formula>
        <name>FU U.S. Financial Advisors - Populate Fi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Oppty_NAME</fullName>
        <field>Name</field>
        <formula>IF($User.Id != &quot;005A0000009DyZZ&quot;, &quot;US INST - &quot;
+ Name, &quot;US INSR - &quot;
+ Name)</formula>
        <name>FU Update Oppty NAME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Auto-Populate Opportunity name</fullName>
        <actions>
            <name>FU_Update_Oppty_NAME</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>LastModifiedBy.Profile.Id = &quot;00eA0000000RIrB&quot;

&amp;&amp;

NOT BEGINS(Name, &quot;US INSR - &quot;)

&amp;&amp; 

NOT BEGINS(Name, &quot;US INST - &quot;) 

&amp;&amp;

RecordTypeId  = &quot;012A0000000VySg&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>U%2ES%2E Financial Advisors - Populate Firm</fullName>
        <actions>
            <name>FU_U_S_Financial_Advisors_Populate_Fi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>U.S. Financial Advisors</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
