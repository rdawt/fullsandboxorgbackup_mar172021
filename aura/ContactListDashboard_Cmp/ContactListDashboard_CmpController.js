({
    doInit : function(component, event, helper) {
        helper.getOnLoadRecords(component,event);
    },

    getActivityForEachContactCntrl : function(component, event, helper) {
        helper.getMarketoActivityForEachContactHelper(component,event);
        helper.getActivityForEachContactHelper(component,event);
    },

    getContactRecordsCntrl : function(component, event, helper) {
        helper.getContactRecordsHelper(component,event);
    },

    afterScriptsLoaded : function(component, event, helper) 
    {
        helper.loadCharts(component,event,helper);
    }

})