/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-02-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global class Batch_Create_Payee_Atd_Recs_Schedule implements Schedulable 
{
   global void execute(SchedulableContext SC) 
   {
        Batch_Create_Payee_Attendance_Records objClass = new Batch_Create_Payee_Attendance_Records();

        database.executebatch(objClass,200);
   }
}