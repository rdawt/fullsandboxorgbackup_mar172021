/*
    Author  :       Vidhya Krishnan
    Date Created:   7/5/12
    Description:    Used in the Community Registrant detail page. This is called during the button submit of 'Create as New Contact'
*/
public class RegistrantToContact {

	public string regId{get;set;}
	public string conId{get;set;}
	public string communityId{get;set;}
	public string cAccountId{get;set;}
	public string cOwnerId{get;set;}
	public string strFullName{get;set;}
	public string strFirstName{get;set;}
	public string strLastName{get;set;}
	public string strEmail{get;set;}
	public string strCompany{get;set;}
	public string strTitle{get;set;}
	public string strPhone{get;set;}
	public string strAddress1{get;set;}
	public string strAddress2{get;set;}
	public string strCity{get;set;}
	public string strState{get;set;}
	public string strCountry{get;set;}
	public string strZipCode{get;set;}
	public string strDbSource{get;set;}
	public DateTime privacyAckDate {get; set;} // Privacy policy awk Date
	public Contact con;
	public Community_Registrant__c cReg;

    public RegistrantToContact(ApexPages.StandardController controller) {
		this.con = (Contact)controller.getSubject();

		try{
			cReg = [select Id, Full_Name__c, First_Name__c, Last_Name__c, Email__c, Phone__c, Company__c, Title__c, Privacy_Policy_Web_Ack_Date__c, Address1__c, Address2__c, 
				City__c, State__c, Country__c, Zip_Code__c from Community_Registrant__c Where Id=:ApexPages.currentPage().getParameters().get('cId') LIMIT 1];
			if(cReg != null){
				//Registrant Details
				strFirstName = cReg.First_Name__c; strLastName = cReg.Last_Name__c; strEmail = cReg.Email__c; regId = cReg.Id;
				strCompany = cReg.Company__c; strTitle = cReg.Title__c; strPhone = cReg.Phone__c; strFullName = cReg.Full_Name__c;
				strAddress1 = cReg.Address1__c; strAddress2 = cReg.Address2__c; strCity = cReg.City__c; strCountry = cReg.Country__c;
				strState = cReg.State__c; strZipCode = cReg.Zip_Code__c; strDbSource = 'Community Registrant'; privacyAckDate = cReg.Privacy_Policy_Web_Ack_Date__c;
				//Community Member Details
				list<Community_Member__c> memberList = new list<Community_Member__c>();
				memberList = [select Community__c from Community_Member__c where Community_Registrant__c =: regId];
				communityId = memberList[0].Community__c;
				//Community Details
				list<Community__c> communityList = new list<Community__c>();
				communityList = [select Default_Owner__c from Community__c where Id =: communityId];
				cOwnerId = communityList[0].Default_Owner__c;
			}
		}catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
			ApexPages.addMessage(myMsg);
		}
	}

	public PageReference addContact(){
		if(strLastName == null || strLastName.equals('')){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Last Name cannot be blank.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		if(strCountry == null || strCountry.equals('')){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Country Value cannot be blank.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		this.cAccountId = con.AccountId;
		if(cAccountId == null || cAccountId.equals('')){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Firm Name cannot be blank. Please map the Provided Company Name to the existing Firm in lookup.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		if(strFirstName != null && strFirstName != '') con.FirstName = strFirstName;
		if(strLastName != null && strLastName != '') con.LastName = strLastName;
		if(cOwnerId != null && cOwnerId != '') con.OwnerId = cOwnerId;
		if(strEmail != null && strEmail != '') con.Email = strEmail;
		con.AccountId = cAccountId;
		if(strTitle != null && strTitle != '') con.Title = strTitle;
		if(strPhone != null && strPhone != '') con.Phone = strPhone;
		if(strAddress1 != null && strAddress1 != ''){
			if(strAddress2 != null && strAddress2 != '') con.MailingStreet = strAddress1+'\n'+strAddress2;
			else con.MailingStreet = strAddress1;
		}
		if(strCity != null && strCity != '') con.MailingCity = strCity;
		if(strState != null && strState != '') con.MailingState = strState;
		if(strCountry != null && strCountry != '') con.MailingCountry = strCountry;
		if(strZipCode != null && strZipCode != '') con.MailingPostalCode = strZipCode;
		if(strDbSource != null && strDbSource != '') con.DB_Source__c = strDbSource;
		if(privacyAckDate != null) con.Privacy_Policy_Web_Ack_Date__c =DateTime.valueOf(privacyAckDate);
		try{
			insert con;
			cReg.Contact__c = con.Id;
			update cReg;
		}catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
		PageReference conPage = new PageReference('/'+con.Id);
		return conPage;
	}
	
	public PageReference cancelRecord(){
		PageReference regPage = new PageReference('/'+regId);
		return regPage;
	}
}