/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FundInterestTrigger_Test 
{
	@TestSetup
	public static void createSetupUser()
	{
		Profile objProfile1 = [Select Id from Profile where Name = 'ETF-US Sales' limit 1];
        User servicesUser1 = SL_TestDataFactory.createTestUser(objProfile1.Id, 'Test First Name1', 'Test Last Name1');
        servicesUser1.User_Territory__c = 'ETF RIA East';
        servicesUser1.User_Internal_External__c = 'External';
        insert servicesUser1;
        
        Profile objProfile2 = [Select Id from Profile where Name = 'ETF-US Sales' limit 1];
        User servicesUser2 = SL_TestDataFactory.createTestUser(objProfile2.Id, 'Test First Name2', 'Test Last Name2');
        servicesUser2.User_Territory__c = 'ETF RIA West';
        servicesUser2.User_Internal_External__c = 'External';
        insert servicesUser2;
        
        Profile objProfile3 = [Select Id from Profile where Name = 'ETF-US Sales' limit 1];
        User servicesUser3 = SL_TestDataFactory.createTestUser(objProfile3.Id, 'Test First Name3', 'Test Last Name3');
        servicesUser3.User_Territory__c = 'ETF BD West';
        servicesUser3.User_Internal_External__c = 'External';
        insert servicesUser3;
        
        Profile objProfile4 = [Select Id from Profile where Name = 'ETF-US Sales' limit 1];
        User servicesUser4 = SL_TestDataFactory.createTestUser(objProfile4.Id, 'Test First Name4', 'Test Last Name4');
        servicesUser4.User_Territory__c = 'ETF BD East';
        servicesUser4.User_Internal_External__c = 'External';
        insert servicesUser4;
        
        Profile objProfile5 = [Select Id from Profile where Name = 'ETF-US Sales' limit 1];
        User servicesUser5 = SL_TestDataFactory.createTestUser(objProfile5.Id, 'Test First Name5', 'Test Last Name5');
        servicesUser5.User_Territory__c = 'ETF Inst';
        servicesUser5.User_Internal_External__c = 'External';
        insert servicesUser5;	
	}
	
	@isTest	//ETF RIA East
	public static void testETFRIAEast()
	{
		
		User objUser = [Select Id from User where User_Territory__c = 'ETF RIA East' and User_Internal_External__c = 'External' and isActive = true limit 1];
		SL_TestDataFactory.createTestDataForFundManagementBySpecificUser(objUser);
	}
	
	@isTest //ETF RIA West
	public static void testETFRIAWest()
	{
		User objUser = [Select Id from User where User_Territory__c = 'ETF RIA West' and User_Internal_External__c = 'External' and isActive = true limit 1];
		SL_TestDataFactory.createTestDataForFundManagementBySpecificUser(objUser);
	}
	
	@isTest	//ETF BD West
	public static void testETFBDEast()
	{
		User objUser = [Select Id from User where User_Territory__c = 'ETF BD West' and User_Internal_External__c = 'External' and isActive = true limit 1];
		SL_TestDataFactory.createTestDataForFundManagementBySpecificUser(objUser);
	}
	
	@isTest //ETF BD East
	public static void testETFBDWest()
	{
		User objUser = [Select Id from User where User_Territory__c = 'ETF BD East' and User_Internal_External__c = 'External' and isActive = true limit 1];
		SL_TestDataFactory.createTestDataForFundManagementBySpecificUser(objUser);
	}
	
	@isTest //ETF Inst
	public static void testETFInst()
	{
		User objUser = [Select Id from User where User_Territory__c = 'ETF Inst' and User_Internal_External__c = 'External' and isActive = true limit 1];
		SL_TestDataFactory.createTestDataForFundManagementBySpecificUser(objUser);	
	}
}