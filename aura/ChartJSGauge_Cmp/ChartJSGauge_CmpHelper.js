({
    loadChartData : function(component, event, helper) {
      // var action = component.get('c.getGaugeChartData');
      //   action.setParams({
      //     strategy :  component.get('v.strategy')
		  // });

      // action.setCallback(this, function(response) {
      //   component.set('v.showSpinner','false');
      //   if (response.getState() === 'SUCCESS') {
      //     helper.showChart(component,event,helper,response.getReturnValue());
      //   } else if (response.getState() === "ERROR") {
      //     component.set('v.error', 'Error has occurred while loading data.');
      //   }
      // });
      // $A.enqueueAction(action);
    },
    
    showChart: function (component, event, helper) {
     
      // if($A.util.isEmpty(response.needleValue)){
      //   component.set('v.error', 'We can not draw this chart because there is no data.');
      //   return;
      // }

      var needleAmount;
     
      let mapLabel = new Map();
      var netSalesCalculated = component.get('v.netSalesCalculated');
      var goalAmount = component.get('v.goalAmount');
      if($A.util.isEmpty(netSalesCalculated)){
        component.set('v.needleValue', '0.00');
      }else{
        component.set('v.needleValue', '$' +  helper.numFormatter(netSalesCalculated,2));
      }
      if ($A.util.isEmpty(goalAmount)) {
        component.set('v.chartFootnote', 'Goal amount not found.');
        return;
      }
      if($A.util.isEmpty(netSalesCalculated) || netSalesCalculated < 0){
        needleAmount = 0;
      }else if(netSalesCalculated > goalAmount){
        needleAmount = goalAmount;
      }else{
        needleAmount = netSalesCalculated;
      }
      var ctx = component.find("chart").getElement();
      var data = {
        datasets: [
          {
            borderWidth: 0,
            needleValue: needleAmount,
            data: component.get('v.dataRange'),
            backgroundColor: component.get('v.colourCodeRange'),
            datalabels: {
              anchor: 'start',
              align: 'start',
              offset: 2
            }
          }
        ],
        labels: []
      };

      var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        responsive:true,
        plugins: [{
  
          afterDraw: chart => {
            var needleValue = chart.chart.config.data.datasets[0].needleValue;
            var dataTotal = chart.chart.config.data.datasets[0].data.reduce((a, b) => a + b, 0);
            var angle = Math.PI + (1 / dataTotal * needleValue * Math.PI);
            var ctx = chart.chart.ctx;
            var cw = chart.chart.canvas.offsetWidth;
            var ch = chart.chart.canvas.offsetHeight;
            var cx = cw / 2;
            var cy = ch - 6;
  
            ctx.translate(cx, cy);
            ctx.rotate(angle);
            ctx.beginPath();
            ctx.moveTo(0, -3);
            ctx.lineTo(ch - 20, 0);
            ctx.lineTo(0, 3);
            ctx.fillStyle = 'rgb(0, 0, 0)';
            ctx.fill();
            ctx.rotate(-angle);
            ctx.translate(-cx, -cy);
            ctx.beginPath();
            ctx.arc(cx, cy, 5, 0, Math.PI * 2);
            ctx.fill();
          }
        }
        ],
        options: {
          responsive: true,
          layout: {
            padding: {
              bottom: 3
            }
          },
  
          rotation: -Math.PI,
          cutoutPercentage: 92,
          circumference: Math.PI,
          legend: {
            position: 'left'
          },
  
          animation: {
            animateRotate: false,
            animateScale: true
          },
          plugins: {
            datalabels: {
              display: true,
              formatter: function (value,index) {
                if(!component.get('v.labelLoaded')){
                var amount = component.get('v.totalDataAmount');
                amount += value;
                if(amount == goalAmount){
                  component.set('v.totalDataAmount', 0);
                }else{
                  component.set('v.totalDataAmount', amount);
                }

                if (value == 0) {
                  mapLabel.set(index.dataIndex, '$'+ value);
                  return '$' + value;
                } else if (value == 1000000) {
                  mapLabel.set(index.dataIndex,"$" + helper.numFormatter(amount,0));
                  return "$" + helper.numFormatter(amount,0);
                } else {
                  mapLabel.set(index.dataIndex, "");
                  return "";
                }
              }else{
                var mapLabelTemp = component.get('v.mapLabel');
                var label = mapLabelTemp.get(index.dataIndex);
                return label;
              }
              },
              color: '#8C8C8C',
              backgroundColor: null,
              font: {
                size: 12
              },
  
            }
          },
          tooltips: {
            enabled: true,
            mode: 'index',
            intersect: true,
            callbacks: {
              title: function(tooltipItem, data) {
                  return "";
              },
              label: function(tooltipItem, data) {
                  var mapLabelTemp = component.get('v.mapLabel');
                  var index = tooltipItem.index;
                  if($A.util.isEmpty(mapLabelTemp.get(index-1)) || $A.util.isEmpty(mapLabelTemp.get(index+1))){
                    index--;
                  }
                 return mapLabelTemp.get(index-1) + ' - ' + mapLabelTemp.get(index+1);
              }
          }
         },
        }
      });
      component.set('v.mapLabel',mapLabel);
      // component.set('v.totalDataAmount', 0);
      
      component.set('v.labelLoaded', true);
      
      
    },

    numFormatter : function (labelValue,fixed) {
      if(fixed > 0){
          return Math.abs(Number(labelValue)) >= 1.0e+9
        ? (labelValue / 1.0e+9).toFixed(fixed) + "B" : Math.abs(Number(labelValue)) >= 1.0e+6 ? (labelValue / 1.0e+6).toFixed(fixed) + "M" : 
        (labelValue / 1.0e+3).toFixed(fixed) + "K";
      }else{
        return Math.abs(Number(labelValue)) >= 1.0e+9
        ? (labelValue / 1.0e+9) + "B" : Math.abs(Number(labelValue)) >= 1.0e+6 ? (labelValue / 1.0e+6) + "M"
        : (labelValue / 1.0e+3) + "K";
      }
      
  }
})