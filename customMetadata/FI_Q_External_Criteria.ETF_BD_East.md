<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETF BD East</label>
    <protected>false</protected>
    <values>
        <field>Contact_Mailing_State__c</field>
        <value xsi:type="xsd:string">WV,VA,TN,SC,NC,MS,MD,KY,DC,AL,MI,IN,NY,VT,RI,NH,ME,MA,CT,PA,OH,NJ,DE,GA,FL</value>
    </values>
    <values>
        <field>Firm_Channel__c</field>
        <value xsi:type="xsd:string">Financial Advisor</value>
    </values>
</CustomMetadata>
