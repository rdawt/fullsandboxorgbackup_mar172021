/**
      Author: Vidhya Krishnan
      Date Created: 04/04/2012
      Description: Community Registration Trigger that 
            1. updates contact id in the corresponding community member record
            2. updates hyperlink field in contact with the corresponding registrant id
            3. delete related community member when registrant is deleted 
*/
trigger RegistrantTrigger on Community_Registrant__c (after update, before delete) {

	SendEmailNotification se = new SendEmailNotification('Error: Registrant Trigger failed');	
    if(trigger.isBefore && trigger.isDelete){
        
        /**** if Contact's PP Awk date = Registrant Awk Date, then delete the PP Awk date value of Contact too   */
        
        List<Id> regIds = new List<Id>();
        List<Community_Member__c> cmList = new List<Community_Member__c>();
        for(Community_Registrant__c cr : Trigger.old)
            regIds.add(cr.Id);
        cmList.addall([select Id from Community_Member__c where Community_Registrant__c IN: regIds]);
        try{
            delete cmList;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred during deleting Community Member Record(s) : \n'+e.getMessage()+trigger.new);
        }
    }
    if(trigger.isAfter && trigger.isUpdate){
        List<Id> regIds = new List<Id>();
        List<Community_Member__c> cmList = new List<Community_Member__c>();
        List<Community_Member__c> cmUpdList = new List<Community_Member__c>();
        List<Contact> conUpdList = new List<Contact>();
        Map<string,string> regConMap = new Map<string,string>();
        Map<string,string> regConDelMap = new Map<string,string>();
        Contact c = new Contact();
    
        for(Community_Registrant__c cr : Trigger.new)
            // update only if the contact mapping is changed
            if(cr.Contact__c!= null && cr.Contact__c != Trigger.oldMap.get(cr.Id).Contact__c){ 
                regConMap.put(cr.Id, cr.Contact__c);
                regIds.add(cr.Id);
            }
            else if(cr.Contact__c == null && cr.Contact__c != Trigger.oldMap.get(cr.Id).Contact__c){
                regConDelMap.put(cr.Id, Trigger.oldMap.get(cr.Id).Contact__c);
            }
        if(!regConDelMap.isEmpty()){
            cmList = new List<Community_Member__c>();
            cmList.addall([select id, Community_Registrant__c from Community_Member__c where Community_Registrant__c IN: regConDelMap.keySet()]);
            for(String s : regConDelMap.keySet()){
                c = new Contact(id = regConDelMap.get(s));
                c.Community_Registrant__c = null;
                c.Privacy_Policy_Web_Ack_Date__c = null;
                conUpdList.add(c);         
                for(Community_Member__c cm : cmList){
                    cm.Contact__c = null;
                    cmUpdList.add(cm);
                }
            }
        }
        if(!regIds.isEmpty()){
            cmList = new List<Community_Member__c>();
            cmList.addall([select id, Community_Registrant__c from Community_Member__c where Community_Registrant__c IN: regIds]);
            for(Id rid : regIds){
                for(Community_Member__c cm : cmList){
                    if(rid == cm.Community_Registrant__c){
                        string sConId = regConMap.get(rid);
                        if(sConId != null){
                            Id cid = (Id)regConMap.get(rid);
                            cm.Contact__c = cid;
                            cmUpdList.add(cm);
                        }
                        else{
                            Id cid = (Id)regConMap.get(rid);
                            cm.Contact__c = null;
                            cmUpdList.add(cm);
                        }
                    }
                }
                if(regConMap.get(rid) != null){
                    c = [select id, MailingCountry from Contact where id =: regConMap.get(rid)];
                    c.Community_Registrant__c = rid;
                    c.Privacy_Policy_Web_Ack_Date__c = Trigger.newMap.get(rid).Privacy_Policy_Web_Ack_Date__c;
                    if(c.MailingCountry != null)
                        conUpdList.add(c);
                }
                if(Trigger.oldMap.get(rid).Contact__c != null){
                    c = [select id, MailingCountry from Contact where id =: Trigger.oldMap.get(rid).Contact__c];
                    c.Community_Registrant__c = null;
                    c.Privacy_Policy_Web_Ack_Date__c = null;
                    if(c.MailingCountry != null)
                        conUpdList.add(c);
                }
            } // end of selected reg ids loop
        }
        try{
            if(cmList != null) update cmList;
            if(conUpdList != null) update conUpdList;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred during updating Community Member or Contact Records(s): \n'+e.getMessage()+trigger.new);
        }
    }// end of update
}