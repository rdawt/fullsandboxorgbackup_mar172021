global class CalculateContactCount_Controller 
{
    @AuraEnabled
    webservice static String countContactsForEachAccount(String recordId)
    {
        List<Account> lstAccounts = new List<Account>();
        try
        {
            Account objAccount = [Select Id, BillingCountry from Account where Id =: recordId limit 1];

            AggregateResult[] contactCounts = [SELECT AccountId, COUNT(Id) ContactCount FROM Contact WHERE AccountId =: recordId GROUP BY AccountId];
            
            if(contactCounts.size() > 0)
            {
                for ( AggregateResult count : contactCounts ){
                    objAccount.Number_of_Contacts__c = (Integer)count.get( 'ContactCount');
                    if(objAccount.BillingCountry != null)
                        lstAccounts.add(objAccount);
                }
            }
            else // To update the accounts with no of contacts = 0, since AggregateResult doesnt return the accouts with 0 count.
            {
                objAccount.Number_of_Contacts__c = 0;
                
                if(objAccount.BillingCountry != null)
                    lstAccounts.add(objAccount);    
            }   

            if ( !lstAccounts.isEmpty() ) 
                 update lstAccounts;
                 
            return 'Success';     
        }catch(Exception e){
            return 'Failed';
        }
        
    }

}