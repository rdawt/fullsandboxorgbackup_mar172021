@isTest
private class eventCountTriggerTest {

    static testMethod void testeventCountTrigger() {
     
        eventTriggerHandler eth = new eventTriggerHandler();
        DMLException e = null;
        Set<ID> accIds = new Set<ID>();
        List<Contact> conlist = new List<Contact>();
        Account acc = new Account(Name = 'qwerpoiuadsf', BillingCountry='USA',Channel__c='Insurance');
        insert acc;
        Account acc1 = new Account(Name = 'lkjhgfda 987896', BillingCountry='USA',Channel__c='Insurance');
        insert acc1;
        Contact con = new Contact(LastName='xyz',FirstName='test',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',  email = 'con1@con.com');
        insert con;
        Contact con1 = new Contact(LastName='xyz11',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz',MailingCountry= 'United States',  email = 'con2@con.com');
        insert con1;
        Contact con2 = new Contact(LastName='xyz21',FirstName='test2',AccountId=acc1.Id,MailingStreet='xyz1',MailingCountry='United States',  email = 'con3@con.com',OwnerId ='005A0000000Oqpm');
        insert con2;
        Contact con3 = new Contact(LastName='xyz1',FirstName='test1',AccountId=acc.Id,MailingStreet='', MailingCountry='United States',  email = 'con4@con.com',
                                  ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC');
        insert con3;
        //Contact con4 = new Contact(LastName='xyz41',AccountId=acc.Id, Email='test@abc.com', MailingCountry='United States');
        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        insert accx;
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        insert conx;
   
        //mAY 13 2019
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con3342@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000004G1xG',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        insert conx1;
        Contact conx2 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-conxz2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000005D7IK',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        insert conx2;
        //005A00000032QAD
     
        //May 13, 2019
        List<Event> newEventList = new List<Event>();
        List<Event> newEventList2 = new List<Event>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+con2.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
        
        newEventList.add(new Event(Description = Comments,Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId));
        newEventList.add(new Event(Description = Comments,Subject = 'Subject test added by sv+1', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=61,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+2, ReminderDateTime = System.now()+1, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId));
        newEventList.add(new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A0000005DhiSIAS'));
        newEventList.add(new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A00000032QAD'));
        //Start Oct 16-2019
        newEventList.add(new Event(Description = Comments, Subject = 'Conference', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'Conference',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A0000004G1xGIAS'));
        //End Oct 16-2019
        newEventList.add(new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A0000004G1xG'));
                                       
        //cooper-005A0000005D7IK
        newEventList.add(new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A0000005D7IK'));
                                        
        //System.DmlException: Insert failed. First exception on row 1; first error: FIELD_INTEGRITY_EXCEPTION, Assigned To ID: id value of incorrect type: 0032F00000NOAbFQAX: [OwnerId]
        //sv userid in sbx-005A0000005DhiSIAS  
        //vlad 005A0000004vyiWIAQ
   
        newEventList.add(new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conx.id,OwnerId = '005A0000004vyiWIAQ'));                                    
        insert newEventList;
      }

      static testMethod void testeventCountTrigger02() {
        DMLException e = null;
        List<Event> newEventList = new List<Event>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : \n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';

        //Brad pope RIA -005A0000008L9NWIA0
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

    
        User u = new User(
            Username ='testuser-RIA-ETF@rama.com',
            lastname = 'testing-RIA-User',
            Email ='testuser-RIA-ETF@rama.com',
            Alias='alias',
            Internal_Sales_Score_Model__c = 'RIA â€“ ETF',
            CommunityNickname='RIA-Mock',
            TimeZoneSidKey ='America/New_York',
            LocaleSidKey ='en_US',
            EmailEncodingKey='UTF-8',
            ProfileId='00eA0000000f8Xu',
            LanguageLocaleKey='en_US'
            //,
            // UserRole=00EA0000000E1k0
        );

        User u1 = new User(
            Username ='testuser-RIA-ETF@ramau1.com',
            lastname = 'testing-RIA-Useru1',
            Email ='testuser-RIA-ETF@ramau1.com',
            Alias='aliasu1',
            Internal_Sales_Score_Model__c = 'RIA â€“ Active',
            CommunityNickname='XternalRIA-Mock',
            TimeZoneSidKey ='America/New_York',
            LocaleSidKey ='en_US',
            EmailEncodingKey='UTF-8',
            ProfileId='00eA0000000ejql',
            LanguageLocaleKey='en_US',
            User_Internal_External__c='Internal'
            //,
          // UserRole=00EA0000000E1k0
        ); 

        insert u;
        insert u1;
        
        System.runAs (u){
    
          //    newTaskList2.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
          //                                   ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, whoid=conx.id,OwnerId = '005A0000004vyiWIAQ'));
          test.startTest();
          Account accxusr = new Account(Name = 'WFA-Unbranched-Test User-RIA-ETF', BillingCountry='United States',Channel__c='Insurance');
          insert accxusr;
          Contact conxusr = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accxusr.Id,MailingStreet='abc',Email='test-con@abcusr.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =u.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
          insert conxusr;
          //insert newEventList2;
          Event e1 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                          ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxusr.id,OwnerId = '005A0000004vyiWIAQ');
          insert e1;
          // test.stopTest();
        }
    
    
        System.runAs (u1){
    
          //    newTaskList2.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
          //                                   ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, whoid=conx.id,OwnerId = '005A0000004vyiWIAQ'));
          //   test.startTest();
          Account accxusr1 = new Account(Name = 'WFA-Unbranched-U1user', BillingCountry='United States',Channel__c='Insurance');
          insert accxusr1;
          Contact conxusr1 = new Contact(LastName='Aalund',FirstName='Gailusr1',AccountId=accxusr1.Id,MailingStreet='abc',Email='test-con@abcusr1.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =u1.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
          insert conxusr1;
          system.debug('after conxusr1 is created..-->>*****'+ conxusr1.id);
          //    insert newTaskList2;
          //Kol E - 005A0000005trMpIAI
          Event ee1 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                               ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxusr1.id,OwnerId = '005A0000005trMpIAI',etf_team__c='Kol Estreicher');
    
          insert ee1;
          test.stopTest();
        }
    
    
        //OCt 16 2019 Start
        //ETF- US Sales profile id:00eA0000000ejql
        User ETFUSSales = new User(
            Username ='testuser-ETF-USSales@ramau1.com',
            lastname = 'testing-ETF-USSales',
            Email ='testuserETF-USSales@ramau1.com',
            Alias='ETFSales',
            Internal_Sales_Score_Model__c = 'none',
            CommunityNickname='XternalETF-USSales-Mock',
            TimeZoneSidKey ='America/New_York',
            LocaleSidKey ='en_US',
            EmailEncodingKey='UTF-8',
            ProfileId='00eA0000000ejql',
            LanguageLocaleKey='en_US',
            User_Internal_External__c='External'
            //,
            // UserRole=00EA0000000E1k0
        );     
        insert ETFUSSales;
        
        System.runAs (ETFUSSales){
            Account accxetfsales1 = new Account(Name = 'WFA-Unbranched- Test User USETFSalesuser', BillingCountry='United States',Channel__c='Insurance');
            insert accxetfsales1 ;
            Contact conxetfsales1 = new Contact(LastName='Aalund',FirstName='Gailusr1',AccountId=accxetfsales1.Id,MailingStreet='abc',Email='test-con@usetfsalesusr2.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =ETFUSSales.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
            insert conxetfsales1;
            Event ee1 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                  ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxetfsales1.id,OwnerId = '005A0000005trMpIAI',etf_team__c='Kol Estreicher');
            insert ee1;
        }
    
        System.runAs (ETFUSSales){
            Account accxetfsales2 = new Account(Name = 'Test User 02 USETFSalesuser2', BillingCountry='United States',Channel__c='Insurance');
            insert accxetfsales2 ;
            Contact conxetfsales2 = new Contact(LastName='Aalund',FirstName='Gailusr1',AccountId=accxetfsales2.Id,MailingStreet='abc',Email='test-con@usetfsalesusr1.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =ETFUSSales.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
            insert conxetfsales2;
            Event ee2 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                            ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxetfsales2.id,OwnerId = '005A0000005trMpIAI',etf_team__c='Kol Estreicher');
            insert ee2;
        }

        User u3 = new User(
            Username ='testuser-RIA-ETF@ramau01.com',
            lastname = 'testing-RIA-Useru1',
            Email ='testuser-RIA-ETF@ramau1.com',
            Alias='aliasu3',
            Internal_Sales_Score_Model__c = 'FA – ETF',
            CommunityNickname='XternalRIA-u3',
            TimeZoneSidKey ='America/New_York',
            LocaleSidKey ='en_US',
            EmailEncodingKey='UTF-8',
            ProfileId='00eA0000000ejql',
            LanguageLocaleKey='en_US',
            User_Internal_External__c='External'
            //,
            //UserRole=00EA0000000E1k0
        );  
        insert u3;
            
        System.runAs (u3){
            Account accxetfsales2 = new Account(Name = 'USETFSalesuser2 - WFA-Unbranched- Test Account', BillingCountry='United States',Channel__c='Insurance');
            insert accxetfsales2 ;
            Contact conxetfsales2 = new Contact(LastName='Aalundu3',FirstName='Gailusr1u3',AccountId=accxetfsales2.Id,MailingStreet='abc',Email='test-con@usetfsalesusr19.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =u3.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
            insert conxetfsales2;
            system.assert(conxetfsales2.id <> null);
            Event ee2 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                            ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxetfsales2.id,OwnerId = ETFUSSales.id,etf_team__c='Kol Estreicher');
        
            insert ee2;
        }
        /*       
        System.runAs (u3){
        Account accxetfsales3 = new Account(Name = 'WFA-Unbranched-USETFSalesuser2', BillingCountry='United States',Channel__c='Insurance');
        insert accxetfsales3 ;
        Contact conxetfsales3 = new Contact(LastName='Aalund33',FirstName='Gailusr133',AccountId=accxetfsales2.Id,MailingStreet='abc',Email='test-con@usetfsalesusru3u31.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId =ETFUSSales.id,LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        insert conxetfsales3;
        system.assert(conxetfsales2.id <> null);
        Event ee2 = new Event(Description = Comments, Subject = 'Subject test added by sv******', IsReminderSet = true, DurationInMinutes=30,Summary_Recap__c = 'summary recap',
                                        ActivityDateTime= System.today()+3, ReminderDateTime = System.now()+2, whoid=conxetfsales2.id,OwnerId = ETFUSSales.id,etf_team__c='Kol Estreicher');
        insert ee2;
        }
        */    
        //Oct 16 2019 end
    
    
        //insert newTaskList2;
        //new Apr.10 2019
    
        AggregateResult[] eCounts;  
        if (newEventList.size() != 0){
          eCounts  =[select  whoid, count(id) EventCount
                    from Event
                    where  Marketing_Automation__c = false
                    group by whoid];//
  
          system.debug('after agg. count- count is'+ eCounts);
                              
          list<Contact> conListt= new list<Contact>();                             
          for(AggregateResult forLoopCount : eCounts){
              contact parentEventContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),Event_Counter__c=integer.valueof(forLoopCount.get('eventCount')));
              conListt.add(parentEventContact);
          }
          update conListt;
        }
    
    
        //end apr. 10 2019
        System.assert(e==null);
    }

    //Delete by Australia profile user his own eventd and this should be allowed
    @isTest
    static void testOnBeforeDeleteSce01() {

        insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Event_Delete_Profiles__c='Australia', Event_Delete_Users__c= 'Marketo API',Is_Event_Delete_Logic_Active__c = true);
        insert new TriggerOnOff__c(isActive_Event__c = true);

        Profile ausProfile= [SELECT Id FROM profile WHERE Name='Australia' LIMIT 1]; 
        User ausUser = SL_TestDataFactory.createTestUser(ausProfile.Id, 'Aus', 'Test User');
        insert ausUser;

        system.runAs(ausUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);
            //Create 2 Events
            List<Event> newEventList = new List<Event>();
            newEventList.add(new Event(Description = 'Test Comments 01',Subject = 'Test Subject 01', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = objCon.Id, WhatId = objCon.AccountId, OwnerId = ausUser.Id));
            newEventList.add(new Event(Description = 'Test Comments 02',Subject = 'Test Subject 02', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = objCon.Id, WhatId = objCon.AccountId, OwnerId = ausUser.Id));
            insert newEventList;
              List<Event> lstEventBefore = [SELECT Id FROM Event];

            Test.startTest();
            delete newEventList;
            Test.stopTest();

            List<Event> lstEventAfter = [SELECT Id FROM Event];
            system.assertEquals(lstEventBefore.size() , 2);
            system.assertEquals(lstEventAfter.size() , 0);
        }
    }

    //Added by Tharaka De Silva part of ITSALES-1376
    //Delete other user event by System Administrator and this should be allowed
    @isTest
    static void testOnBeforeDeleteSce02() {

        insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Event_Delete_Profiles__c='Australia', Event_Delete_Users__c= 'Marketo API',Is_Event_Delete_Logic_Active__c = true);
        insert new TriggerOnOff__c(isActive_Event__c = true);

        Profile ausProfile= [SELECT Id FROM profile WHERE Name='Australia' LIMIT 1]; 
        User ausUser = SL_TestDataFactory.createTestUser(ausProfile.Id, 'Aus', 'Test User');
        insert ausUser;
        Id eventId;

        system.runAs(ausUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);
            //create 01 Tasks
            Event event = new Event (Description = 'Test Comments 01',Subject = 'Test Subject 01', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = objCon.Id, WhatId = objCon.AccountId, OwnerId = ausUser.Id);
            insert event;
            eventId = event.Id;
        }

        Profile adminProfile= [SELECT Id FROM profile WHERE Name='System Administrator' LIMIT 1]; 
        User adminUser = SL_TestDataFactory.createTestUser(adminProfile.Id, 'Admin', 'Test User');
        insert adminUser;
        system.runAs(adminUser){
            List<Event> lstEventBefore = [SELECT Id FROM Event WHERE Id =:eventId];

            Test.startTest();
            delete lstEventBefore;
            Test.stopTest();

            List<Event> lstEventAfter = [SELECT Id FROM Event WHERE Id =:eventId];
            system.assertEquals(lstEventBefore.size() , 1);
            system.assertEquals(lstEventAfter.size() , 0);
        }
    }

    //Try to delete Standard profile user his own event, but this should not be  allowed
    @isTest
    static void testOnBeforeDeleteSce03() {

        insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Event_Delete_Profiles__c='Australia', Event_Delete_Users__c= 'Marketo API',Is_Event_Delete_Logic_Active__c = true);
        insert new TriggerOnOff__c(isActive_Event__c = true);

        Profile stndProfile= [SELECT Id FROM profile WHERE Name='Standard User' LIMIT 1]; 
        User stndUser = SL_TestDataFactory.createTestUser(stndProfile.Id, 'Standard', 'Test User');
        insert stndUser;

        system.runAs(stndUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);         
            //Create 2 Events
            List<Event> newEventList = new List<Event>();
            newEventList.add(new Event(Description = 'Test Comments 01',Subject = 'Test Subject 01', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = objCon.Id, WhatId = objCon.AccountId, OwnerId = stndUser.Id));
            newEventList.add(new Event(Description = 'Test Comments 02',Subject = 'Test Subject 02', IsReminderSet = true, Summary_Recap__c = 'summary recap',DurationInMinutes=60,
                                        ActivityDateTime=System.today(), ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = objCon.Id, WhatId = objCon.AccountId, OwnerId = stndUser.Id));
            insert newEventList;

            List<Event> lstEventBefore = [SELECT Id FROM Event];

            Test.startTest();
            try{
                delete newEventList;
                System.assert(false);
            }
            catch(Exception e)
            {
                System.assert(true);
                System.assert(e.getMessage().contains(System.Label.Error_Message_Event_Delete_Validation));
            }
            Test.stopTest();

            List<Event> lstEventAfter = [SELECT Id FROM Event];
            system.assertEquals(lstEventBefore.size() , 2);
            system.assertEquals(lstEventAfter.size() , 2);
        }
    }
}