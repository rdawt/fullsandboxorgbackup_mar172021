/**    
      Author: Vidhya Krishnan
      Date Created: 12/06/2011
      Description: Single Contact Trigger that manages all the sub triggers on Contact
        1. No. of contact field in Account is updated
        2. The line break in Mailing Address is removed before storing in custom field
        3. The Sequoia Qualification Field Updates.
        4. Deletes all Subscription Member Record Before Deleting the contact record to avoid orphan sub_mem records.
        5. Dynamic Eligiblity Concept
*/
trigger ContactTrigger on Contact ( before insert, before update, before delete, after insert, after update, after delete) {
    
    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Contact__c == true){
        //added for Payee Summary
        datetime currentDate = Date.today();
        Set<id> userIds= new Set<ID>();
        system.debug('Triggered User Name........'+UserInfo.getUserName());
        SL_ContactTriggerHandler objHandler = new SL_ContactTriggerHandler();
        if(UserInfo.getUserName() != 'alnapper@vaneck.com' ){
        
        if(trigger.isBefore && trigger.isInsert){
            objHandler.onBeforeInsert(Trigger.New);
        }

        if(trigger.isBefore && trigger.isUpdate){
            objHandler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
            System.debug('Number of Queries used in this apex code so far: ' + Limits.getQueries());
        }

        /*  Delete all Sub Mem Records of the particular 
        contact before deleting the contact - Cascading delete is automatic when SM object is child of Contact
        */
        if(trigger.isBefore && trigger.isDelete){
            objHandler.onBeforeDelete(Trigger.old);
        }
            
        
        /*  Handles Dynamic Subscription changes of a contact   */
        if(trigger.isAfter && trigger.isUpdate){
            objHandler.onAfterUpdate(Trigger.New, Trigger.oldMap);
            System.debug('Number of Queries used in this apex code so far: ' + Limits.getQueries());
        }

        if(trigger.isAfter && trigger.isInsert){
            objHandler.onAfterInsert(Trigger.New);
        }
            
        if(trigger.isAfter && trigger.isDelete){
            objHandler.onAfterDelete(Trigger.Old);
        }
        
        }
        if(Trigger.isBefore && Trigger.isUpdate){
            objHandler.createContactScoreTrackerForUpdate(Trigger.New, Trigger.oldMap);
            System.debug('Number of Queries used in this apex code so far: ' + Limits.getQueries());
        }
          
        if(Trigger.isAfter && Trigger.isInsert){
            objHandler.createContactScoreTrackerForInsert(Trigger.new);
        }
    }
}