/**
    Author: Vidhya Krishnan
    Created Date: 4/30/2012
    Description: Schedule Apex Code to update the Contact fileds - Active Subscriptions Indicator
                    which are not updated during Dynamic Subscription Operation
                 Also update the Fund Interest Indicator Fields in Contact
*/
global class updateContactSubIndicator implements Schedulable {

     global void execute(SchedulableContext SC) {
        String strIndr,s,msg;
        Contact conObject;
        set<Id> conIdFndSet = new set<Id>();
        set<Id> conIdSubSet = new set<Id>();
        list<Contact> conList = new list<Contact>();
        list<Contact> conUpdList = new list<Contact>();
        Map<Id,string> subIndtrMap = new Map<Id,string>();   // map that stores value pair of contact_id and concadinated Indicator string for that particular contact_id
        
        try{
            conList = [select Id,Schedule_Apex_Indicator__c from Contact where Schedule_Apex_Indicator__c != null ];
            for(Contact c : conList){
            	if(c.Schedule_Apex_Indicator__c.equals('Fund'))
                	conIdFndSet.add(c.Id);
                if(c.Schedule_Apex_Indicator__c.equals('Subscription'))
                	conIdSubSet.add(c.Id);
            }

	        /*	Fund Interest Update	*/	
			if(!conIdFndSet.isEmpty()){
				FundInterestManagement fim = new FundInterestManagement();
				string result = fim.updateContact(conIdFndSet);
				
				if(result.equals('true')){
					SendEmailNotification se = new SendEmailNotification('Success: Scheduled Apex for updating Contact Fund Indicator Fields Completed');msg = 'Scheduled Apex for updating Contact Fields (Fund Indicator Fields) completed for the day ' 
				    		+system.today()+'. \nNo. of contacts updated are: '+conIdFndSet.size()+'. \nList of Contact Ids updated are: \n'+conIdFndSet; se.sendEmail(msg);
				    
				}else{
					SendEmailNotification se = new SendEmailNotification('ERROR: Scheduled Apex for updating Contact Fund Indicator Fields Failed'); se.sendEmail(result);
				}
			}

            /*	Subscription Indicator Update	*/
            List<Subscription_Member__c> subMemIndicator = [Select sm.Contact__c, sm.Subscription__r.Subscription_Indicator_Marker__c from Subscription_Member__c sm 
                                                where Subscribed__c=:true and Contact__c != null and Contact__c =: conIdSubSet
                                                order by sm.Contact__c, sm.Subscription__r.Subscription_Indicator_Marker__c];
            for(Id strId : conIdSubSet){
            	// To avoid Too Many Script Statements - exit the for loop when only 10,000 script statements left
            	if ((200000 - Limits.getScriptStatements()) < 10000)
					break;

                strIndr = '';
                for(Subscription_Member__c sm : subMemIndicator){
                    s='';
                    if(sm.Contact__c == strId){
                        if(sm.Subscription__r.Subscription_Indicator_Marker__c == null)
                            s='';
                        else
                            s=sm.Subscription__r.Subscription_Indicator_Marker__c;
                        if(strIndr.equals(''))
                            strIndr = s;
                        else{
                            if(!(strIndr.contains(s)))
                                strIndr = strIndr + ',' + s;
                        }
                    }
                }
                subIndtrMap.put(strId,strIndr);
            }
    
            /*  Creating the list of contact objects with the updated values    */
            for(Id strId : subIndtrMap.keySet())
            {
               conObject=new Contact(Id=strId);
               conObject.Active_Subscriptions_Indicator__c = subIndtrMap.get(strId);
               conObject.Schedule_Apex_Indicator__c = null;
               conUpdList.add(conObject);
            }

            /*  Actual Contact Update & Send success notification   */
            if(!(conUpdList.isEmpty())){
                update conUpdList;
                SendEmailNotification se = new SendEmailNotification('Success: Scheduled Apex for updating Contact Subscription Indicator Field completed');
                msg = 'Scheduled Apex for updating Contact Field (Active Subscription Indicator) completed for the day ' 
                		+system.today()+'. \nNo. of contacts updated are: '+conUpdList.size()+'. \nList of Contact updated are: \n'+conUpdList;
                se.sendEmail(msg);
            }
        }catch(Exception e){
        	SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for updating Contact Fields failed'); msg = 'Exception Occurred while updating the Active Subscription Indicator filed of Contact. \n'+e.getMessage()
            							+'. \nList of Contact update failed are: \n'+conUpdList; se.sendEmail(msg);
        }
     }
}