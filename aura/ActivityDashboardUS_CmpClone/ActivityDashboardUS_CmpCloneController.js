({
    afterScriptsLoaded : function(component, event, helper) 
    {
        helper.getOnLoadRecords(component,event,helper);
    },

    getOwnerRecordsCntrl : function(component, event, helper) 
    {
        //helper.loadCharts(component,event,helper);
        //helper.loadChartForMonthWise(component,event,helper);
        //helper.loadChartForUserWise(component,event,helper);
        
        var selectedVal = event.getSource().get('v.name');
		var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
            }
            
            // if any other value is selected
            else{
                component.set("v.SelectedOwnersOption",helper.addNewSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",[]);
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.SelectedOwnersOption",helper.removeSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }    
        
        //helper.loadOpportunityChart(component,event,helper);
        //helper.loadChartsForSelectedUser(component,event,helper);
        //helper.loadChartsForSelectedUserScore(component,event,helper);
    },

    getActivityForEachContactCntrl : function(component, event, helper) 
    {
        /**
        helper.loadCharts(component,event,helper);
        helper.loadChartForMonthWise(component,event,helper);
        helper.loadChartForUserWise(component,event,helper);
        **/
        
        var selectedVal = event.getSource().get('v.name');
        var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.selectedActivityTypeOption",helper.selectAll(component, event, helper, component.get("v.activityTypeOptions")));
                
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
            }
            
            // if any other value is selected
            else{
                component.set("v.selectedActivityTypeOption",helper.addNewSelectedValue(component, event, helper, component.get("v.selectedActivityTypeOption"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.selectedActivityTypeOption",[]);
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.selectedActivityTypeOption",helper.removeSelectedValue(component, event, helper, component.get("v.selectedActivityTypeOption"), selectedVal));                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }         
        //helper.loadChartsForSelectedUserScore(component,event,helper);
        //helper.loadOpportunityChart(component,event,helper);
    },

    handleSaveButtonClick: function(component, event, helper) 
    {
        helper.saveUserScoreWeightage(component,event,helper);
        helper.loadCharts(component,event,helper);
        //helper.loadChartsForSelectedUser(component,event,helper);
        //helper.loadChartsForSelectedUserScore(component,event,helper);
        //helper.loadOpportunityChart(component,event,helper);
    },
    
    applyFilters: function(component, event, helper){        
        helper.loadCharts(component,event,helper);
        helper.loadChartForMonthWise(component,event,helper);
        helper.loadChartForUserWise(component,event,helper);
    },
    
    resetFilters: function(component, event, helper){
        helper.getOnLoadRecords(component,event,helper);
    },
    
})