({
    getFunds : function(component, event, helper) {
        var action = component.get('c.getFundNames');
        action.setParams({
            mutualFundOnly :  false
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var options = response.getReturnValue();
                component.set("v.fundOptions", options);
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    getChannels : function(component, event, helper) {
        var action = component.get('c.getChannelNames');
        action.setParams({
            fundName :  component.get('v.selectedFund')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var options = response.getReturnValue();
                component.set("v.channelOptions", options);
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    calculateAll : function(component, event, helper) {
        var checkTerritory = component.find("isTerritory");
        var checkOrgType = component.find("isOrgType");
        if(checkOrgType.get("v.value") && checkTerritory.get("v.value")){
            helper.showToast('Error', 'Please select one option at a time. (Territory or Org Type)','error');
            component.set('v.showSpinner', false);
            return;
        }
        var checkAllocationETF = component.find("isAllocationETF");
        var action = component.get('c.doForceCalcUSSalesGoals');
        action.setParams({
            type : 'fund',
            portfolio :  component.get('v.selectedFund'),
            channel :  component.get('v.selectedChannel'),
            isOrgType :  checkOrgType.get("v.value"),
            isTerritory : checkTerritory.get("v.value"),
            isForceCalculate : true,
            allocationETF : checkAllocationETF.get("v.value"),
            year : component.get('v.selectedYear')
		  });

        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                helper.showToast('Success', 'All configuration records have been updated successfully.','success');
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while updating records.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);

    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
        // $A.get("e.force:closeQuickAction").fire();

    },
})