/**		
	Author: Vidhya Krishnan
	Date Created: 12/06/2011
	Description: Class to handle Single Contact Trigger that manages all the sub triggers on Contact
		1. No. of contact field in Account is updated
		2. The line break in Mailing Address is removed before storing in custom field
		3. The Sequoia Qualification Field Updates.
		4. Deletes all Subscription Member Record Before Deleting the contact record to avoid orphan sub_mem records.
*/

public class ContactTriggerHandler {
	
	/*	Deletes all the subscription Member Records of the contact 	- Master-Detail relationsip does cascading delete
	public void deleteSubMem(Contact[] oldCon){
    	try{
    		system.debug('Entering... Delete Subscription Member Records on Contact Delete');
    		List<Id> idList = new List<Id>();
	    	List<Subscription_Member__c> smDelList = new List<Subscription_Member__c>();
	    	for(Contact c: oldCon)
	    		idList.add(c.Id);
    		smDelList.add([Select Id from Subscription_Member__c where Contact__c IN: idList]);
	    	if(!SmDelList.isEmpty())
	    		delete SmDelList;
    	}
    	catch(Exception e){
    		System.debug('Error Occured while Deleting the Subscription Member record of a contact.....\n'+e.getMessage());
    	}
	} */
	
	/*	Counts all the contacts of an Account	*/
	public static void updateContactCounts( Set<Id> accountIds ){				
		try{
			//countContactsAsync( accountIds );
		}
		catch ( System.AsyncException ex ){
			//countContacts( accountIds );
		}
	}

	/*
	@future 
	public static void countContactsAsync( Set<Id> accountIds ){
		countContacts( accountIds );
	}

	public static void countContacts( Set<Id> accountIds ){
		SendEmailNotification se = new SendEmailNotification('Error: Cotact Trigger failed');
		try{
			AggregateResult[] contactCounts = [SELECT AccountId, COUNT(Id) ContactCount FROM Contact WHERE AccountId IN :accountIds GROUP BY AccountId];
			List<Account> accounts = new List<Account>();
			for ( AggregateResult count : contactCounts ){
				Account a = [Select Id, BillingCountry from Account where Id =: (Id)count.get( 'AccountId' )]; 
				a.Number_of_Contacts__c = (Integer)count.get( 'ContactCount' );
				//Account a = new Account( Id = (Id)count.get( 'AccountId' ), Number_of_Contacts__c = (Integer)count.get( 'ContactCount' ) );
				if(a.BillingCountry != null)
					accounts.add(a);
			}
			// To update the accounts with no of contacts = 0, since AggregateResult doesnt return the accouts with 0 count.
			if(accountIds.size() != contactCounts.size())
				for(Id a : accountIds){
					if(contactCounts.size()==0){
						//Account acc = new Account( Id = a, Number_of_Contacts__c = (Integer)(0));
						Account acc = [Select Id, BillingCountry from Account where Id =: a]; 
						acc.Number_of_Contacts__c = (Integer)(0);
						if(acc.BillingCountry != null)
							accounts.add(acc);
					}
					else
						for ( AggregateResult c : contactCounts )
							if(a!=c.get('AccountId')){
								Account acc = [Select Id, BillingCountry from Account where Id =: a];
								//Account acc = new Account( Id = a, Number_of_Contacts__c = (Integer)(0));
								acc.Number_of_Contacts__c = (Integer)(0);
								if(acc.BillingCountry != null)
									accounts.add(acc);
							}
				}

			if ( !accounts.isEmpty() ) 
				 update accounts;
		}catch(Exception e){
			se.sendEmail('Exception Occurred in Contact Trigger - Contact Count in Account update : \n'+e.getMessage()+accountIds);
		}		
	}
	*/

	/*	Trigger that removes line break in Mailing Address then copies cleaned values to custom text fields		*/
	public void contactRemoveLineBreak(Contact[] newCon){
		SendEmailNotification se = new SendEmailNotification('Error: Cotact Trigger failed');
		for (Contact a: newCon){
			try{
			List <String> addClean = new List<String>();
			if(a.MailingStreet!=null){
				for(String s: a.MailingStreet.split('\n',0)){
					addClean.add(s);
				}
				if(addClean.size()!=null){
					if(addClean.size()==1)
					{
						a.Shipping_Street_1_Clean__c = addClean[0];
						a.Shipping_Street_2_Clean__c = '';
						a.Shipping_Street_3_Clean__c = '';
					}
					else if(addClean.size()==2)
					{
						a.Shipping_Street_1_Clean__c = addClean[0];
						a.Shipping_Street_2_Clean__c = addClean[1];
						a.Shipping_Street_3_Clean__c = '';
					}
					else if (addClean.size()>=3)
					{
						a.Shipping_Street_1_Clean__c = addClean[0];
						a.Shipping_Street_2_Clean__c = addClean[1];
						a.Shipping_Street_3_Clean__c = addClean[2];
					}
				}
			}
			else
			{
				a.Shipping_Street_1_Clean__c = '';
				a.Shipping_Street_2_Clean__c = '';
				a.Shipping_Street_3_Clean__c = '';
			}
			}catch(Exception e) {
				se.sendEmail('Exception Occurred in Contact Trigger - Line Break Removal : \n'+e.getMessage()+newCon);
			}
		}//end of for loop
	}

	/*	Sequoia Qualification Field Updates.	*/
	public void sequoiaCalcualtions(Contact[] newCon){
		SendEmailNotification se = new SendEmailNotification('Error: Cotact Trigger failed');
		try{
			for(Contact c : newCon){
				/*	SQ_DCI__c field logic	*/
				Integer DCI = 0;
				if(c.ETF_PU__c == 'Yes' || c.ETF_PU__c == 'No') DCI = DCI+1; 
				if(c.QT__c == 'Yes' || c.QT__c == 'No') DCI = DCI+1; 
				if(c.COI__c == 'Yes' || c.COI__c == 'No') DCI = DCI+1; 
				if(c.RepPM__c == 'Yes' || c.RepPM__c == 'No') DCI = DCI+1; 
				if(c.YoYG__c == 'Yes' || c.YoYG__c == 'No') DCI = DCI+1; 
				if(c.NTA__c == 'Yes' || c.NTA__c == 'No') DCI = DCI+1; 
				if(c.MVEP__c == 'Yes' || c.MVEP__c == 'No') DCI = DCI+1; 
				if(c.VE_AtB__c == 'Yes' || c.VE_AtB__c == 'No') DCI = DCI+1; 
				if(c.FB__c == 'Yes' || c.FB__c == 'No') DCI = DCI+1;
				if(c.AUM__c != null) DCI = DCI+1;
				if(c.YEB__c != '' && c.YEB__c != null) DCI = DCI+1;
				if(c.OR__c != '' && c.OR__c != null) DCI = DCI+1;
				c.SQ_DCI__c = DCI;
				
				/*	Yrs_Bus__c field logic	*/
				if(c.YEB__c == '' || c.YEB__c == null) c.Yrs_Bus__c = null;
				else
				{
					if(Integer.valueof(c.YEB__c) != 0)
					{
						Integer givenYear = Integer.valueof(c.YEB__c);
						Integer currentYear = system.today().year();
						Integer calcYear = currentYear - givenYear;
						c.Yrs_Bus__c = calcYear;
					}
					else c.Yrs_Bus__c = 0;
				}
				
				/*	SQ_NPos__c and SQ_NNeg__c field logic */
				Integer NPos = 0; Integer NNeg = 0;
				if(c.ETF_PU__c == 'Yes') NPos = NPos+1; else if(c.ETF_PU__c == 'No') NNeg = NNeg+1;
				if(c.QT__c == 'Yes') NPos = NPos+1; else if(c.QT__c == 'No') NNeg = NNeg+1;
				if(c.COI__c == 'Yes') NPos = NPos+1; else if(c.COI__c == 'No') NNeg = NNeg+1;
				if(c.RepPM__c == 'Yes') NPos = NPos+1; else if(c.RepPM__c == 'No') NNeg = NNeg+1;
				if(c.YoYG__c == 'Yes') NPos = NPos+1; else if(c.YoYG__c == 'No') NNeg = NNeg+1;
				if(c.NTA__c == 'Yes') NPos = NPos+1; else if(c.NTA__c == 'No') NNeg = NNeg+1;
				if(c.MVEP__c == 'Yes') NPos = NPos+1; else if(c.MVEP__c == 'No') NNeg = NNeg+1;
				if(c.VE_AtB__c == 'Yes') NPos = NPos+1; else if(c.VE_AtB__c == 'No') NNeg = NNeg+1;
				if(c.FB__c == 'Yes') NPos = NPos+1; else if(c.FB__c == 'No') NNeg = NNeg+1;
				if(c.Yrs_Bus__c !=null && c.Yrs_Bus__c >= 5) NPos = NPos+1; 
					else if(c.Yrs_Bus__c != null & c.Yrs_Bus__c < 5) NNeg = NNeg+1;
	
				if (c.OR__c == '' || c.OR__c == null) {NNeg = NNeg+0; NPos = NPos+0;}
					else if((c.OR__c).toUpperCase()== 'N/A' || (c.OR__c).toUpperCase()== 'NA') NNeg = NNeg+1; 
					else NPos = NPos+1;
				if(c.Channel_NEW__c != null && c.AUM__c != null){
					if(c.Channel_NEW__c.contains('RIA') && c.AUM__c >=150) NPos = NPos+1;
						else if(c.Channel_NEW__c.contains('RIA') && c.AUM__c < 150) NNeg = NNeg+1;
						else if(!c.Channel_NEW__c.contains('RIA') && c.AUM__c >= 50) NPos = NPos+1;
						else if(!c.Channel_NEW__c.contains('RIA') && c.AUM__c < 50) NNeg = NNeg+1;
				}
	
				c.SQ_NPos__c = NPos;
				c.SQ_NNeg__c = NNeg;
			} // end of for loop
		}
		catch(Exception e){
			se.sendEmail('Exception Occurred in Contact Trigger - Sequoia Calculation : \n'+e.getMessage()+newCon);
		}
	} // end of Sequoia calculations
}