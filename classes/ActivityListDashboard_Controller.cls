global class ActivityListDashboard_Controller 
{
    public static String strDefaultContactOnLoad = 'Martijn Rozemuller';

    @AuraEnabled
    public static Map<String, Object> getOnLoadRecords()
    {
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords.put('OwnersOptions', getAllOwnersOptions());
        mapGetOnLoadRecords.put('SelectedOwnersOption', getDefaultContactOwnerDetails());
         
        return mapGetOnLoadRecords;    
    }

    @AuraEnabled
    public static User getDefaultContactOwnerDetails()
    {
        User objDefaultUser = new User();
        objDefaultUser = [Select Id from User where Name =: strDefaultContactOnLoad limit 1];
        return objDefaultUser;
        
    }

    @AuraEnabled
    public static List<Map<String, String>> getAllOwnersOptions()
    {
        List<Map<String, String>> lstUsers = new List<Map<String, String>>();
        lstUsers.add(new Map<String, String>{'value' =>'ALL', 'label'=>'ALL'} );
        For(User objUser : [Select Id , Name from User where Profile.Name IN: getProfileName() and isActive=true order by Name ASC])
        {
            if(strDefaultContactOnLoad == objUser.Name)
                lstUsers.add(new Map<String, String>{'value' =>objUser.Id, 'label'=>objUser.Name, 'selected' => 'true'} );
            else 
                lstUsers.add(new Map<String, String>{'value' =>objUser.Id, 'label'=>objUser.Name, 'selected' => 'false'} );
        }
        return lstUsers;
    }

    public static Set<String> getProfileName()
    {
        Set<String> setProfileName = new Set<String>();
        setProfileName.add('Institutional Swiss User');
        setProfileName.add('Institutional Swiss Admin');
        //setProfileName.add('Institutional US User');
        return setProfileName;
    }

    @AuraEnabled(cacheable=true)
    public static List<CaseViewResults> getAllCases() {
        system.debug('???????????????in get all cases??????????????');
        LIST<Case> filteredCases = new LIST<Case>();
        LIST<Account> filteredAccounts = new LIST<Account>();
        LIST<CaseViewResults> results = new LIST<CaseViewResults>();
        Map<String, Case> recordSet = new Map<String, Case>();
        SET<String> accountIds = new SET<String>();
        
        //Query to retrieve your cases
        //PS: This is a generic query and you can filter it based on your requirement
        filteredCases = [SELECT Id, toLabel(Status), Account.Name, AccountId from Case
                         where AccountId!='' LIMIT 999];
        
        for(Case record : filteredCases) {
            recordSet.put(record.id, record);
            accountIds.add(record.AccountId);
            system.debug('Accounts are:::'+accountIds);
        }
        filteredAccounts =[Select id, name from account where  id='001A000001WzAclIAF' OR id='001A000000xH900IAC']; // id='001A000000g2DgIIAU' OR
        
        filteredCases = new LIST<Case>();		      
        filteredCases.addAll(recordSet.values());          

        CaseViewResults record = new CaseViewResults();        
        record.cases = filteredCases;
        record.accounts = filteredAccounts;
        results.add(record);
        return results;
    }
    
    //Wrapper class
    public class CaseViewResults {    	
        @AuraEnabled public LIST<Account> accounts;
        @AuraEnabled public LIST<Case> cases;        
        public CaseViewResults() {}
    }
    
}