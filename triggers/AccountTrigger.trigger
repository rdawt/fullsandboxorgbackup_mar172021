/*  
    Author: Vidhya Krishnan
    Date: 1/23/12
    Desc: Master Account Trigger that handles all the sub triggers
        1. If the channel value is updated in a Firm, all its contact is updated with: 
            Sequoia Qualification Field Values &
            Dynamic Eligibility is handled
        2. Updates "Number of Contacts" field of account while merging accounts.
*/

trigger AccountTrigger on Account (after update)  //before update commented out
{
    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Account__c == true)
    {
        system.debug('Triggered User Name........'+UserInfo.getUserName());
        if(UserInfo.getUserName() != 'alnapper@vaneck.com' )
        {
            SL_AccountTriggerHandler objHandler = new SL_AccountTriggerHandler(Trigger.new);
            /*  Updates No of Contacts 
            // Commenting this code as we have commented the code in handler itself
            if(trigger.isUpdate && trigger.isBefore){
                system.debug('Entering.... Account Before Trigger - to update no of contacts');    
                objHandler.onBeforeUpdate(Trigger.new);
            }
             */
    
            /*  Updates contacts Sequoia Qualification Field Values & Dynamic Eligibility   */
            if(trigger.isUpdate && trigger.isAfter){
                objHandler.onAfterUpdate(Trigger.new, Trigger.oldmap);
            }
        }
    }
}