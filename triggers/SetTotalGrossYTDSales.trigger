trigger SetTotalGrossYTDSales on SalesConnect__Contact_Portfolio_Breakdown__c (after update, after insert) 
{
    set<Id> fundGoalIds = new set<Id>();
    list<Fund_Goal__c> fundGoals = new list<Fund_Goal__c>();
    

    
    //add all accounts that are going to need their counters updated to the set
    for(SalesConnect__Contact_Portfolio_Breakdown__c sccpb : trigger.new)
    {
        fundGoalIds.add(sccpb.Fund_Goal__c);
    }
    
   
    //run the query to sum the data
    AggregateResult[] totals = [select  SUM(SalesConnect__YTD_Sales__c)totalYTDGrossSales, 
    									SUM(SalesConnect__MTD_Sales__c)totalMTDGrossSales,
    									SUM(YTD_Net_Sales__c)totalYTDNetSales,
    									SUM(MTD_Net_Sales__c)totalMTDNetSales, 
    									Fund_Goal__c                   
                                From SalesConnect__Contact_Portfolio_Breakdown__c
                                where Fund_Goal__c in :fundGoalIds 
                                and fund_goal__c != null
                                group by Fund_Goal__c];
                                
    system.debug('\n\n\n-------------- Aggregated Records');
    system.debug(totals);
    
    //loop over the result set. One row per account with a totalAge and accountId column
    for(AggregateResult ar: totals)
    {
        Fund_Goal__c objToUpdate = new Fund_Goal__c(id= (Id) ar.get('Fund_Goal__c'));
      
        objToUpdate.Total_Gross_Sales_YTD_MM__c = double.valueOf(ar.get('totalYTDGrossSales'));
        objToUpdate.Total_Gross_Sales_MTD__c = double.valueOf(ar.get('totalMTDGrossSales'));
        objToUpdate.Total_Net_Sales_YTD__c = double.valueOf(ar.get('totalYTDNetSales'));
        objToUpdate.Total_Net_Sales_MTD__c = double.valueOf(ar.get('totalMTDNetSales'));
      
        fundGoals.add(objToUpdate);
    }
    
    if(!fundGoals.isEmpty())
    {
        database.update(fundGoals,false);
    }

    
}