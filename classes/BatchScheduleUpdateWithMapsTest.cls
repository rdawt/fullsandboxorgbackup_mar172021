@isTest
//(SeeAllData=true)
private class BatchScheduleUpdateWithMapsTest
{
    //my notes regarding making sure the exception is tested, just make sure include a wrong reference to an object or refer to null object in the system.debug statement..-how to do it?think-to increase the coverage
    static testmethod void schedulerTest2() 
    {
        //insert  newTaskRelList;
    
        ContactUpdateDateSetting__c contactExtractDays = new  ContactUpdateDateSetting__c ();
        contactExtractDays.Name ='ActivityLastModifiedDateTimeline';
        contactExtractDays.ActivityLastModifiedDateTimeline__c = '14';
        insert contactExtractDays ;
        
       // ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
        
       //     insert ICMDataFeedCriteria;
       // ContactUpdateDateSetting__c contactExtractDays = ContactUpdateDateSetting__c.getInstance('ActivityLastModifiedDateTimeline') ;
        
        string activityLastDays =   ContactUpdateDateSetting__c.getInstance('ActivityLastModifiedDateTimeline').ActivityLastModifiedDateTimeline__c;
        string criteria = 'last_N_days:'+activityLastDays ;
         
        String CRON_EXP = '0 0 0 15 3 ? *';
        List<Contact> conlist = new List<Contact>();
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        //and SalesConnect__Rep_Type__c != \'partnership\' and DB_Source__c != \'MVIS Registrant\' and IsTestContact__c != true' 
        Contact con = new Contact(SalesConnect__Rep_Type__c = 'none',DB_Source__c = 'RIA',IsTestContact__c = false,LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        insert con;
        Event e1 = new Event(Activity_Type__c='Meeting', Description = 'just a test', Result__c = 'Held',  Subject = null, IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-4,ActivityDateTime=System.today()-4, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2);
        Task t1 = new Task(status='email sent',Activity_Type__c='Call', Description = 'just a test', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                         ActivityDate = System.today()-4,ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
        insert e1;
        insert t1;
        
         Test.startTest();
          
           
         //  BatchScheduleContactUpdateForEvents b = new batchscheduleContactUpdateForEvents ();
           // database.executebatch(b,200);
         //  ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
        //  ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
           String jobId = System.schedule('ScheduleApexClassTestForUpdatingLastHumanContact',  CRON_EXP, new BatchScheduleUpdateWithMaps ());
           
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
   }
  //  static testmethod void schedulerTest() 
 //   {
  //      String CRON_EXP = '0 0 0 16 5 ? *';
  //      List<Contact> conlist = new List<Contact>();
   //     Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
   //     insert acc;
   //     Contact con = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
   //     Contact con2 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest2@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
   //     Contact con3 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest3@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
   //     Contact con4 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest4@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
   //     Contact con5 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest5@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');



   //     insert con;
    //    insert con2; 
   //     insert con3;
   //     insert con4;
   //     insert con5;
        
    //    string Comments;
        
    //    Event e1 = new Event(Activity_Type__c='Meeting', Description = 'just a test', Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
     //                                   ActivityDate = System.today()-4, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2,ActivityDateTime=System.today());
        
    //    Task t1 = new Task(status='email sent',Activity_Type__c='Call', Description = 'just a test', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
     //                                   ActivityDate = System.today()-2, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
                                        
         
    //    Task t7 = new Task(status='call completed',Activity_Type__c='Call', Description = 'just a test for con4', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
         //                               ActivityDate = System.today(), ReminderDateTime = System.now()+1, WhoId = con4.Id, WhatId = con4.AccountId, OwnerId = con4.OwnerId);
        
        
    //    Task t8 = new Task(status='call completed',Activity_Type__c='Call', Description = 'just a test for con4-2ndtask with task date > than event date', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
      //                                  ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con5.Id, WhatId = con5.AccountId, OwnerId = con5.OwnerId);
                                        
      //   Comments = Comments+'2nd task for con with who id in task object';
         
         
         
     //   Task t6 = new Task(Activity_Type__c='Call',Description = 'just a test', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                  //                      ActivityDate = System.today()+10, ReminderDateTime = System.now()-2, WhoId = con2.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId);
                                        
     //   Task t5 = new Task(Activity_Type__c='Call',Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                  //                      ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
     //    Comments = Comments+'2nd task for con with who id in task object';
         
     //   event e5 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                       //                 ActivityDate = System.today()+7, ReminderDateTime = System.now()+7, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        
        
     //   event e6 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                       //                 ActivityDate = System.today()+3, ReminderDateTime = System.now()+7, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+37);
     //   event e7 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                         //               ActivityDate = System.today()+3, ReminderDateTime = System.now()+7, WhoId = con3.Id, WhatId = con3.AccountId, OwnerId = con3.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+37);
                                        
    //    event e8 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                          //              ActivityDate = System.today(), ReminderDateTime = System.now(), WhoId = con5.Id, WhatId = con5.AccountId, OwnerId = con5.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today());                                        
                                                                     
                                                                             
     //    insert e1;
      //   insert e5;
      //   insert t1;
      //   insert t5;  
      //   insert t6;  
      //   insert e6; 
      //   insert e7;
      //   insert t7; 
      //   insert t8; 
      //   insert e8;                        
                                        
    //     ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
   //     ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
    //    insert ICMDataFeedCriteria;
   //      string criteria = 'last_N_days:'+ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c;
    
        
   //     Test.startTest();
          
           
         //  BatchScheduleContactUpdateForEvents b = new batchscheduleContactUpdateForEvents ();
           // database.executebatch(b,200);
         //  ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
        //  ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
    //       String jobId = System.schedule('ScheduleApexClassTestForUpdatingLastHumanContact',  CRON_EXP, new BatchScheduleUpdateWithMaps ());
           
    //        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
   //         System.assertEquals(CRON_EXP, ct.CronExpression);
    //        System.assertEquals(0, ct.TimesTriggered);

     //   Test.stopTest();
        // Add assert here to validate result
  //  }
}