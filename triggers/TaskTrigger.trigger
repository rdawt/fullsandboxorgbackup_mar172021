trigger TaskTrigger on Task (before insert, before update, after insert, after update, before delete, after delete) {
    
    
    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Task__c == true)
    {    
        TaskTriggerHandler handler = new TaskTriggerHandler();
        SL_TaskTriggerHandler objHandler = new SL_TaskTriggerHandler();
        SL_TaskTriggerHandler_Workflow objHandlerWorkflow = new SL_TaskTriggerHandler_Workflow();
        SendEmailNotification se = new SendEmailNotification('Error: Task Trigger Exception');
        
        try {
            if (trigger.isBefore && trigger.isInsert) {
                system.debug('>>>>>>>>>>>>>>>I am in before insert>>>>>>>>>>>>>>');
                handler.onBeforeInsert(trigger.new);
                
                if(TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c == true)
                    objHandlerWorkflow.updateTaskBasicInformation(trigger.new, trigger.oldMap, FALSE);
            }
            if (trigger.isInsert) {
                handler.onAfterInsert(trigger.new);
            }
        }
        catch(Exception e){
            se.sendEmail('Exception Occurred in the Task Trigger - '+e.getMessage()+trigger.new);
        }
        
        try 
        {
            if (trigger.isBefore && trigger.isUpdate) 
            {
                
                if(TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c == true)
                {
                    
                    handler.onBeforeUpdate(trigger.new);
                    objHandlerWorkflow.updateTaskBasicInformation(trigger.new, trigger.oldMap, TRUE);
                }
            }
        }
        catch(Exception e){
            se.sendEmail('Exception Occurred in the Task Trigger - '+e.getMessage()+trigger.new);
        }
        
        //All the logic for Activity Count(Task) trigger.
        if(Trigger.isAfter && Trigger.isInsert){
            //objHandler.onAfterInsert(Trigger.New);
        }
        if(Trigger.isAfter && Trigger.isUpdate){
             objHandler.onAfterUpdate(Trigger.New);
        }
        if(Trigger.isAfter && Trigger.isDelete){
             objHandler.onAfterDelete(Trigger.Old);
        }

        //Tharaka De Silva on 19-08-2020 ITSALES-1376
        //Added condition for safe deletion - allowed to delete Tasks owned by Task Owner only
        if(trigger.isBefore && trigger.isDelete){ 
		    handler.onBeforeDelete(Trigger.old); 
		}
    }
}