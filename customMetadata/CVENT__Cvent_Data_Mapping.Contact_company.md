<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Contact_company</description>
    <label>Contact_company</label>
    <protected>false</protected>
    <values>
        <field>CVENT__Cvent_Account_Name__c</field>
        <value xsi:type="xsd:string">Van Eck Global</value>
    </values>
    <values>
        <field>CVENT__Cvent_Data_Type__c</field>
        <value xsi:type="xsd:string">string</value>
    </values>
    <values>
        <field>CVENT__Cvent_Field_Name__c</field>
        <value xsi:type="xsd:string">company</value>
    </values>
    <values>
        <field>CVENT__Cvent_Object_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>CVENT__Default_Value__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__IsTest__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Is_Cvent_Field_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Is_SF_Field_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Sf_Data_Type__c</field>
        <value xsi:type="xsd:string">REFERENCE</value>
    </values>
    <values>
        <field>CVENT__Sf_Field_Name__c</field>
        <value xsi:type="xsd:string">AccountId</value>
    </values>
    <values>
        <field>CVENT__Sf_Object_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
</CustomMetadata>
