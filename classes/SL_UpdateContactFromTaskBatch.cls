global class SL_UpdateContactFromTaskBatch implements Database.Batchable<sObject>{

	global String Query;

	global SL_UpdateContactFromTaskBatch()
	{
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		Query = 'Select Id, WhoId, WhatId, ContactOwnerID__c, OwnerId from Task where CreatedDate = TODAY';
		
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		try
		{
			Set<Id> setWhoIds = new Set<Id>();
			List<Task> lstTask = new List<Task>();
			
			lstTask = (List<Task> ) scope;
			for(Task objTask : lstTask)
			{
				if(objTask.WhoId != NULL)
					setWhoIds.add(objTask.WhoId);
			}
			
			AggregateResult[] tCounts=[select  whoid, count(id) taskCount
	                                   from task
	                                   where whoid in : setWhoIds and Marketing_Automation__c = false 
	                                   group by whoid];//and CALENDAR_MONTH(CreatedDate) = 1 //ActivityDate
	        system.debug('after agg. count- count is'+ tCounts);
	        list<Contact> conList= new list<Contact>();                             
	        for(AggregateResult forLoopCount : tCounts){
	            contact parentTaskContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),TaskCounter__c =  integer.valueof(forLoopCount.get('taskCount')));
	            conList.add(parentTaskContact);
	        }
	        
	        if(!conList.isEmpty())
	        	update conList;
		}
		catch(Exception e)
		{
			Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        	SendEmailNotification se = new SendEmailNotification('Error: UpdateContactFromTaskBatch Exception');
        
			String strErrorBody = '';
            strErrorBody = 'Exception Cause : ' + e.getCause() + '\n' + 
                            'Exception Line Number : ' + e.getLineNumber() + '\n' + 
                            'Exception Type Name : ' + e.getTypeName() + '\n' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + '\n' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
		}
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		
	}
}