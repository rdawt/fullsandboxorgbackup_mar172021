<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Contact Owner_Contact</description>
    <label>Contact Owner_Contact</label>
    <protected>false</protected>
    <values>
        <field>CVENT__Cvent_Account_Name__c</field>
        <value xsi:type="xsd:string">Van Eck Global</value>
    </values>
    <values>
        <field>CVENT__Cvent_Field_Data_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__Cvent_Object_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__Cvt_Field_Name__c</field>
        <value xsi:type="xsd:string">Contact_Owner_Contact</value>
    </values>
    <values>
        <field>CVENT__Is_Contact_Not_Mapped__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Is_Field_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Is_Lead_Not_Mapped__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CVENT__RM_Account_Compound_Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__RM_Account_Field_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__RM_Account_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__RM_Contact_Compound_Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__RM_Contact_Field_Name__c</field>
        <value xsi:type="xsd:string">Contact_Owner__c</value>
    </values>
    <values>
        <field>CVENT__RM_Contact_Object__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>CVENT__RM_Cvent_Contact_Field_Name__c</field>
        <value xsi:type="xsd:string">Contact_Owner__c</value>
    </values>
    <values>
        <field>CVENT__RM_Cvent_Contact_Object__c</field>
        <value xsi:type="xsd:string">CVENT__Cvent_Contact_Record__c</value>
    </values>
    <values>
        <field>CVENT__RM_Lead_Compound_Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__RM_Lead_Field_Name__c</field>
        <value xsi:type="xsd:string">Id</value>
    </values>
    <values>
        <field>CVENT__RM_Lead_Object__c</field>
        <value xsi:type="xsd:string">Lead</value>
    </values>
    <values>
        <field>CVENT__Sf_Field__c</field>
        <value xsi:type="xsd:string">Contact_Owner__c</value>
    </values>
    <values>
        <field>CVENT__Sf_Object_Name__c</field>
        <value xsi:type="xsd:string">CVENT__Cvent_Contact_Record__c</value>
    </values>
    <values>
        <field>CVENT__isTest__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
