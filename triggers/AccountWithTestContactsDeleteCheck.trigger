trigger AccountWithTestContactsDeleteCheck on Account (before delete) {
//Delete Trigger start
    system.debug('--inside**********-------------------');
    try{
    if(trigger.isBefore && trigger.isDelete){
            for (Account account : trigger.old) 
            {
              List <Contact> conList = new List <Contact>();  
              conList = [select Id, AccountId, OwnerId, MailingCountry, DB_Source__c, Service_Level__c, AUM__c , SQ_NPos__c, SQ_NNeg__c, Channel_NEW__c, IsTestContact__c  from Contact where AccountId = :account.Id];
             {
             for (Contact contact: conList)
              {
                system.debug(' inside first loop.......    '+contact.Id);
                if (contact.IsTestContact__c == true)
                { 
                        system.debug(' inside 2nd loop-this one is a test contact.......    '+contact.Id);
                        account.addError('You cannot delete this** Account** - Please contact your Administrator for assistance.');
                }
              }
             }
  
            }
       }
    }catch(Exception e){
         SendEmailNotification se = new SendEmailNotification('Error: Account Trigger Deletion failed');
        }
    //Delete Trigger End
}


/*
trigger AccountWithTestContactsDeleteCheck on Account (before delete) {    
    try{
         if(trigger.isBefore && trigger.isDelete){
               system.debug('--inside**********-------------------');
                system.debug('--Trigger.new-------------------'+Trigger.new);
                system.debug('--Trigger.old--------------------'+Trigger.old);
                system.debug('--Trigger.newMap-----------------'+Trigger.newMap);
                system.debug('--Trigger.oldMap------------------'+Trigger.oldMap);
   
            //This queries all Contacts related to the incoming Account records in a single SOQL query.
            //This is also an example of how to use child relationships in SOQL
             List<Account> accountsWithContacts = [select Id from Account where Id IN :Trigger.oldMap.keySet()];
             System.debug('Account---->>inside');
             System.debug('Account List Count---->'+accountsWithContacts.size());
                List<Contact> contactsToUpdate = new List<Contact>{};
                // For loop to iterate through all the queried Account records 
                    for(Account a: accountsWithContacts){
                    // Use the child relationships dot syntax to access the related Contacts
                    for(Contact c: a.Contacts){
                        System.debug('Contact Id[' + c.Id + '], FirstName[' + c.firstname + '], LastName[' + c.lastname +']');
                        //c.Description=c.salutation + ' ' + c.firstName + ' ' + c.lastname; 
                        if (c.IsTestContact__c == true) { 
                            //system.debug(' inside 2nd loop-this one is a test contact.......    '+contact.Id);
                            a.addError('You cannot delete this** Account** - Please contact your Administrator for assistance.');
                        }
                    }         
                }
         }
       }catch(Exception e){
               SendEmailNotification se = new SendEmailNotification('Error: Account Trigger Deletion failed');
       }
 } */