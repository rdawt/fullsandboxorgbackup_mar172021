global class contactSalesPortfolioBatchProcess implements Database.Batchable < sObject >
{
    public string query = 'select id, Fund_Goal__c, Name, SalesConnect__Contact__c from SalesConnect__Contact_Portfolio_Breakdown__c where Fund_Goal__c = null or Fund_Goal__c = \'\'';
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <sObject> batch)
    {
        contactSalesPortfolioController.setContactPortfolioFundGoal(batch);
    }

    global void finish(Database.BatchableContext BC) {}
}