@isTest
public class SubscriptionEligibilityTest {  

    // this method is ued to cretaing the test data
    @testSetup static void createTestData() {
        
        // insert account
        List<Account> lstAcc = new List<Account>();
        lstAcc.add(new Account(Name = 'Test Account', 
                                    Channel__c = 'Insurance', 
                                    BillingCountry = 'US'));    

        lstAcc.add(new Account(Name = 'Test Account', 
                                    Channel__c = 'Insurance', 
                                    BillingCountry = 'US'));        
                                    
        lstAcc.add(new Account(Name = 'Test Account', 
                                    Channel__c = 'Insurance', 
                                    BillingCountry = 'US'));        
        
        insert lstAcc;
        
        // insert subscription grps
        List<Subscription_Group__c> lstInsertSG = new List<Subscription_Group__c>();
        
        // creating unassigned SG
        lstInsertSG.add(new Subscription_Group__c(Name='Unassigned Eligibility', 
                                                    IsActive__c = true,
                                                    Enable__c = true));
        
        // creating Channel filters Insurance SG
        lstInsertSG.add(new Subscription_Group__c(Name = 'Insurance Eligibility', 
                                                    IsActive__c = true,
                                                    Enable__c = true,
                                                    Firm_Branch__c = lstAcc[0].Id, 
                                                    Channel_Filters__c = 'Insurance',
                                                    Country_Filter__c = 'test'));   
        
        // creating Channel filters Insurance SG
        lstInsertSG.add(new Subscription_Group__c(Name = 'MVIS Eligibility',
                                                    DB_Source__c ='MVIS',
                                                    IsActive__c = true,
                                                    Enable__c = true,
                                                    Country_Filter__c ='USA',
                                                    Profile_Names__c = 'Institutional Swiss Admin'));
                                                    
        lstInsertSG.add(new Subscription_Group__c(Name = 'Country Eligibility',                                                 
                                                    IsActive__c = true,
                                                    Enable__c = true,
                                                    Country_Filter__c = 'USA'));
        
        lstInsertSG.add(new Subscription_Group__c(Name = 'CountryFilter Eligibility',                                                 
                                                    IsActive__c = true,
                                                    Enable__c = true,
                                                    Channel_Filters__c = 'Insurance',
                                                    Country_Filter__c = 'USA')); 
        insert lstInsertSG;
        
        // list of contact to be inserted
        List<Contact> lstcon = new List<Contact>(); 
        
        // insert Contact
        lstcon.add(new Contact(LastName = 'Test', 
                                    AccountId = lstAcc[0].Id, 
                                    Email = 'test@abc.com',
                                    MailingCountry = 'Italy'));
        
                                    
        lstcon.add(new Contact(LastName='Test', 
                                AccountId = lstAcc[1].Id, 
                                Email = 'test@abc1.com',
                                DB_Source__c = 'MVIS',
                                MailingCountry = 'France'));
                                
        lstcon.add(new Contact(LastName='Test', 
                                AccountId = lstAcc[1].Id, 
                                Email = 'test@abc2.com',
                                MailingCountry = 'USA'));
                                
        lstcon.add(new Contact(LastName='Test', 
                                Email = 'test@abc3.com',
                                MailingCountry = 'USA'));
                                
        lstcon.add(new Contact(LastName='Test', 
                                AccountId = lstAcc[2].Id, 
                                Email = 'test@abc4.com',
                                MailingCountry = 'UK'));
        
        lstcon.add(new Contact(LastName='Test', 
                                AccountId = lstAcc[2].Id, 
                                Email = 'test@abc5.com',
                                MailingCountry = 'USA'));
        
                                
        insert lstcon;
    }
    
    
     @isTest
    public static void validate_FirmBranchFilter(){ 
        
        // getting contact
        Contact con = [SELECT ID FROM Contact WHERE Email = 'test@abc.com'];        
        SubscriptionEligibility.updateSubsGrp(con.Id);
        
        // query conact for the Assert
        con = [SELECT ID,Subscription_Group__c FROM Contact WHERE Email = 'test@abc.com'];
        Subscription_Group__c sg = [SELECT ID FROM Subscription_Group__c WHERE Name = 'Insurance Eligibility'];
        System.assertEquals(con.Subscription_Group__c, sg.Id, 'Çorrect Firm SG is not Assigned');
        
    }
    
    
    @isTest
    public static void validate_DBSource(){
        
        Contact con = [SELECT ID FROM Contact WHERE Email = 'test@abc1.com'];
        SubscriptionEligibility.updateSubsGrp(con.Id);
        
        // query conact for the Assert
        con = [SELECT ID,Subscription_Group__c FROM Contact WHERE Email = 'test@abc1.com'];
        Subscription_Group__c sg = [SELECT ID FROM Subscription_Group__c WHERE Name = 'MVIS Eligibility'];
        System.assertEquals(con.Subscription_Group__c, sg.Id, 'Çorrect DBdource SG is not Assigned');
        
    }
    
    @isTest
    public static void validate_Channel(){
        
        Contact con = [SELECT ID FROM Contact WHERE Email = 'test@abc2.com'];
        SubscriptionEligibility.updateSubsGrp(con.Id);
        
        // query conact for the Assert
        con = [SELECT ID,Subscription_Group__c FROM Contact WHERE Email = 'test@abc2.com'];
        Subscription_Group__c sg = [SELECT ID FROM Subscription_Group__c WHERE Name = 'CountryFilter Eligibility'];
        System.assertEquals(con.Subscription_Group__c, sg.Id, 'Çorrect Channel SG is not Assigned');
        
    }
    
    @isTest
    public static void validate_Unassigned(){
        
        Contact con = [SELECT ID FROM Contact WHERE Email = 'test@abc3.com'];
        SubscriptionEligibility.updateSubsGrp(con.Id);
        
        // query conact for the Assert
        con = [SELECT ID,Subscription_Group__c FROM Contact WHERE Email = 'test@abc3.com'];
        Subscription_Group__c sg = [SELECT ID FROM Subscription_Group__c WHERE Name = 'Unassigned Eligibility'];
        System.assertEquals(con.Subscription_Group__c, sg.Id,'Correct country is not assigned.');
        
    }
    
    
    
    
    @isTest
    public static void Validate_Country_Filter(){
        
        Contact con = [SELECT ID FROM Contact WHERE Email = 'test@abc5.com'];
        SubscriptionEligibility.updateSubsGrp(con.Id);
        
         // query conact for the Assert
        con = [SELECT ID,Subscription_Group__c FROM Contact WHERE Email = 'test@abc5.com'];
        Subscription_Group__c sg = [SELECT ID FROM Subscription_Group__c WHERE Name = 'CountryFilter Eligibility'];
        System.assertEquals(con.Subscription_Group__c, sg.Id,'Correct channel is not assigned.');
        
    }
    

    
}