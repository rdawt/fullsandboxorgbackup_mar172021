public class OpportunityTriggerHandler {

    List<Opportunity> records = new List<Opportunity>();
    Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

    public void onAfterUpdate(List<Opportunity> lstNewOpportunity,Map<Id, Opportunity> mapOldOpportunity){
        records = lstNewOpportunity;
        oldMap = mapOldOpportunity;
        validateETFAssetFlowChange();
        createETFAssetsByFirmRecords();
    }

    private void validateETFAssetFlowChange(){
        for(Opportunity opp : records){
            Opportunity oldOpp = oldMap.get(opp.Id);
            if(oldOpp.Create_ETF_Asset_Flow_Data__c == true && opp.Create_ETF_Asset_Flow_Data__c == false){
                if(opp.ETF_Assets_By_Firms_Id__c != null){
                    opp.addError(System.Label.Error_Opportunity_ETF_Asset_Flow);
                }               
            }
            if(oldOpp.Create_ETF_Asset_Flow_Data__c == true && opp.ETF_Assets_By_Firms_Id__c != null){
                if(oldOpp.AccountId != opp.AccountId){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }else if(oldOpp.Fund__c != opp.Fund__c){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }else if(oldOpp.CloseDate != opp.CloseDate){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }else if(oldOpp.Amount != opp.Amount){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }else if(oldOpp.OwnerId != opp.OwnerId){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }else if(oldOpp.StageName != opp.StageName){
                    opp.addError(System.Label.Error_Opportunity_Data_Change);
                }
            }
        }
    }

    private void createETFAssetsByFirmRecords()
    {
        Map<String, Opportunity> mapEligibleOpp = new Map<String, Opportunity>();
        Map<String, ETF_Assets_By_Firm__c> mapExsistingRecords = new Map<String, ETF_Assets_By_Firm__c>();
        Map<Id, String> mapFundTicker = new Map<Id, String>();
        List<ETF_Assets_By_Firm__c> lstNewRecords = new List<ETF_Assets_By_Firm__c>();
        Set<Id> setFundIds = new Set<Id>();
        String validationKey;
        for(Opportunity opp : records){
            if(opp.StageName == 'Funded' && opp.Create_ETF_Asset_Flow_Data__c){
                if(opp.AccountId != null && opp.Fund__c != null && opp.CloseDate != null){
                    if(opp.Amount < 0){
                        opp.addError(System.Label.Error_Opportunity_Negative_Amount);
                    }else{
                        validationKey = opp.AccountId + '' + opp.Fund__c + '' + String.valueOf(opp.CloseDate);
                        mapEligibleOpp.put(validationKey,opp);
                        setFundIds.add(opp.Fund__c);
                    }
                }
            }
        }

        if(setFundIds.size() > 0){
            for(Fund__c fund : [SELECT Id,Fund_Ticker_Symbol__c FROM Fund__c WHERE Id IN : setFundIds]){
                mapFundTicker.put(fund.Id, fund.Fund_Ticker_Symbol__c);
            }
        }

        if(mapEligibleOpp.size() > 0){
            for(ETF_Assets_By_Firm__c etfRecords : [SELECT Id,Unique_Key__c FROM ETF_Assets_By_Firm__c WHERE Unique_Key__c IN :mapEligibleOpp.keySet() AND Insurance_Flow_Opportunity__c = true]){
                mapExsistingRecords.put(etfRecords.Unique_Key__c,etfRecords);
            }
            for(String key: mapEligibleOpp.keySet()){
                Opportunity opp = mapEligibleOpp.get(key);
                if(mapExsistingRecords.containsKey(key)){//ETF_Assets_By_Firm__c already created

                }else{//Create New Record
                    ETF_Assets_By_Firm__c etf = new ETF_Assets_By_Firm__c();
                    etf.Name = mapFundTicker.get(opp.Fund__c);
                    etf.Account__c = opp.AccountId;
                    etf.SFDCFundID__c = opp.Fund__c;
                    etf.Asset_Date__c = opp.CloseDate;
                    etf.Insurance_Flows__c = (opp.Amount / 1000000);
                    etf.Notes__c = opp.Type + ' - ' + opp.Name;
                    etf.Insurance_Flow_Opportunity__c = true;
                    etf.Opportunity_Flow_Owner__c = opp.OwnerId;
                    etf.Opportunity_Id__c = opp.Id;
                    etf.Unique_Key__c = key;
                    lstNewRecords.add(etf);
                }
            }
            if(lstNewRecords.size() > 0){
                upsert lstNewRecords;

                //Update Opportunity Record
                Map<Id, Id> mapInserted = new Map<Id, Id>();
                for(ETF_Assets_By_Firm__c etf : lstNewRecords){
                    mapInserted.put(etf.Opportunity_Id__c,etf.Id);
                }
                List<Opportunity> lstOpp = new List<Opportunity>();
                for(Opportunity opp : [SELECT Id,ETF_Assets_By_Firms_Id__c FROM Opportunity WHERE Id IN:mapInserted.keySet() ]){
                    opp.ETF_Assets_By_Firms_Id__c = mapInserted.get(opp.Id);
                    lstOpp.add(opp);
                }
                update lstOpp;
            }
        }
    }
}