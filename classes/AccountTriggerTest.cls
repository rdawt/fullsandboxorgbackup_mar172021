/*  
    Author: Vidhya Krishnan
    Date: 1/23/12
    Desc: Test class for Master Account Trigger that handles all the sub triggers
*/
@isTest
private class AccountTriggerTest {
	
    @isTest
    static void myUnitTest() 
    {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            SL_TestDataFactory.createTestDataForCustomSetting();
            Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            
            Account ac1 = new Account(Name = 'xlmnd97', Channel__c = 'RIA', BillingCountry='US');
            sObject ac1Inserted = SL_TestDataFactory.createSObject(ac1,true);
            
            Account ac2 = new Account(Name = 'fgsdagh', Channel__c = 'FA', BillingCountry='US');
            sObject ac2Inserted = SL_TestDataFactory.createSObject(ac2,true);
            
            Contact con1 = new Contact(Lastname = 'Test12', AccountId = ac2.Id, MailingCountry='US', Subscription_Group__c = objSG.Id);
            sObject con1Inserted = SL_TestDataFactory.createSObject(con1,true);
            
            Contact con = new Contact(LastName='xyz2',FirstName='test',AccountId=ac1.Id,MailingStreet='abc\n345\nxyz',MailingCountry='US',ETF_PU__c='Yes',QT__c='Yes',
                COI__c='Yes',RepPM__c='Yes',YoYG__c='Yes',NTA__c='Yes',MVEP__c='Yes',VE_AtB__c='Yes',FB__c='Yes',AUM__c=150,YEB__c='2000',OR__c='N/A', Subscription_Group__c = objSG.Id);
            sObject conInserted = SL_TestDataFactory.createSObject(con,true);
            
            ac1.Channel__c = 'FA';
            ac2.Channel__c = 'Bank';
            update ac1;
            update ac2;
            
            Account objAccQueried = [SELECT Id, Number_of_Contacts__c FROM Account WHERE Id =: ac1.Id];
            //system.assertEquals(1,objAccQueried.Number_of_Contacts__c);
            Test.stopTest();
          }
    }

    @isTest
    static void myUnitTestDummy() 
    {
        List<Account> lstAccount = new List<Account>();
        Test.startTest();

        SL_TestDataFactory.createTestDataForCustomSetting();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account ac1 = new Account(Name = 'xlmnd97', Channel__c = 'RIA', BillingCountry='US');
        sObject ac1Inserted = SL_TestDataFactory.createSObject(ac1,true);
        
        Account ac2 = new Account(Name = 'fgsdagh', Channel__c = 'FA', BillingCountry='US');
        sObject ac2Inserted = SL_TestDataFactory.createSObject(ac2,true);

        lstAccount.add(ac1);
        lstAccount.add(ac2);

        SL_AccountTriggerHandler objClass = new SL_AccountTriggerHandler(lstAccount);
        objClass.onBeforeUpdate(lstAccount);
        objClass.onAfterUpdate(lstAccount, new Map<Id, Account>());
        Test.stopTest();

    }
}