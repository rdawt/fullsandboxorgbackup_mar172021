global class BatchScheduleUpdateWithMaps_EMEA  implements Schedulable{

    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        ContactBatchUpdateWithMaps_EMEA b = new ContactBatchUpdateWithMaps_EMEA (); 
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
}