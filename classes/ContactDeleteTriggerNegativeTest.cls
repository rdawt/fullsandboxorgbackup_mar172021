/**
      Author: S Venkateswaran
      Date Created: 03/15/2015
      Description: Nagtive Test Class/Case for Contact Trigger Deletion that allows Deleting  Contacts which are NOT marked as Test Contacts
 */
@isTest
private class ContactDeleteTriggerNegativeTest {

    static testMethod void testContactNegativeTrigger() {
      // To check if TestContacts r allowed to delete
       ContactTriggerHandler cth = new ContactTriggerHandler();
       DMLException e = null;
    Set<ID> accIds = new Set<ID>();
    List<Contact> conlist = new List<Contact>();
    
    Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    
    Account acc = new Account(Name = 'TestAccount01', BillingCountry='USA');
         insert acc;
    Account acc1 = new Account(Name = 'TestAccount02', BillingCountry='Canada');
         insert acc1;
        Contact con = new Contact(LastName='EcktronTestLastName',FirstName='EcktronTestFirstName',AccountId=acc.Id,MailingStreet='abc',IsTestContact__c=False,MailingCountry='US');
        insert con;
        Contact con1 = new Contact(LastName='xyz11',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz',IsTestContact__c=False, MailingCountry='US');
        insert con1;
        Contact con2 = new Contact(LastName='xyz21',FirstName='test2',AccountId=acc1.Id,MailingStreet='xyz1',MailingCountry='US',IsTestContact__c=False);
        insert con2;
        Contact con3 = new Contact(LastName='xyz1',FirstName='test1',AccountId=acc.Id,MailingStreet='', MailingCountry='US',IsTestContact__c=False,
          ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC');
        insert con3;
        Contact con4 = new Contact(LastName='xyz41',AccountId=acc.Id, Email='test@abc.com', MailingCountry='US',IsTestContact__c=False);
        insert con4;
        accIds.add(con.AccountId);
        accIds.add(con1.AccountId);
        accIds.add(con2.AccountId);
 //        conlist.add(con);
//        conlist.add(con1);
 //       conlist.add(con2);
 //     conlist.add(con3);
         delete con;
         delete con1;
         delete con2;
         delete con3;
        delete con4;
        //ContactTriggerHandler.updateContactCounts(accIds);
        //cth.contactRemoveLineBreak(conlist);
        //cth.sequoiaCalcualtions(conlist);
        System.assert(e==null);
    }
}