@isTest
//(SeeAllData=true)
private class BatchScheduleContactUpdateForEventsTest
{

    static testmethod void schedulerTest() 
    {
        String CRON_EXP = '0 0 0 15 3 ? *';
        List<Contact> conlist = new List<Contact>();
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        Contact con = new Contact(LastName='Aalund',FirstName='Gail',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        insert con;
        Event e1 = new Event(Description = 'just a test', Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-4, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2,ActivityDateTime=System.today());
        
         ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
        ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
        insert ICMDataFeedCriteria;
         string criteria = 'last_N_days:'+ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c;
    
        
        Test.startTest();
          
           
         //  BatchScheduleContactUpdateForEvents b = new batchscheduleContactUpdateForEvents ();
           // database.executebatch(b,200);
         //  ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
        //  ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
           String jobId = System.schedule('ScheduleApexClassTestForEventExtractForICM',  CRON_EXP, new batchscheduleContactUpdateForEvents ());
           
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
        // Add assert here to validate result
    }
}