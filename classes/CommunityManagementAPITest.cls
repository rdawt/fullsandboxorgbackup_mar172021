/**
    Author  :       Suresh Anugu        
    Date Created:   3/21/12
    Description:    Test class for Community Management API Class
 */
@isTest
private class CommunityManagementAPITest {

    static testMethod void testCommunityManagementAPI() {
        CommunityManagementAPI cmAPI= new CommunityManagementAPI();
        Subscription__c s = new Subscription__c();
        s.Name = 'Test Subscription'; s.IsActive__c = true; s.Subscription_Indicator_Marker__c = 'test';
        s.Subscription_Display_Name__c = 'Test Subscription';
        insert s;
        Subscription_Group__c sg = new Subscription_Group__c();
        sg.Name = 'Test Group'; insert sg;
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account acc1 = new Account(Name = 'sd45fa', Channel__c = 'RIA', BillingCountry = 'USA');
        insert acc1;
        Contact con = new Contact(FirstName = 'Test', LastName = 'User', AccountId = acc1.Id, MailingCountry = 'USA');
        insert con;
        Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true, Default_Subscription_Group__c = sg.Id);
        insert c;
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
        insert cr1;
        Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id,Community_Registrant__c = cr1.Id);
        insert cm1;
        CommunityManagement.MemberRecord mr = new CommunityManagement.MemberRecord();
        mr.communityId = String.valueOf(c.Id); mr.leadId = '00000'; mr.contactId = con.Id;
        mr.email = 'test@test.com'; mr.cUserName  = 'vaneck'; mr.passWord  = 'global1955'; mr.isActive  = true;
        mr.firstName  = 'test'; mr.lastName  = 'test'; mr.companyName  = 'test'; mr.jobTitle  = 'test';
        mr.address1  = 'test'; mr.address2  = 'test'; mr.city  = 'test'; mr.state  = 'test';
        mr.country  = 'test'; mr.zipcode  = 'test'; mr.phone  = 'test'; mr.loginCount = 1; mr.sourceId = 'test';
        mr.investorType  = 'test'; mr.aum  = 'test'; mr.isFP  = 'true'; mr.subscriptionRequested  = 'test,test';
        mr.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr.communityId =c.Id; mr.cMemberId=cm1.Id; 
        mr.dbSourceCreateDate = system.today(); mr.subscriptionGroupId = sg.Id;
        CommunityManagementAPI.getCMemberId(c.Id,'test1');
        CommunityManagementAPI.isEmailTaken(c.Id,'test1@test.com');
        CommunityManagementAPI.getCMemberType(cm1.Id);
        CommunityManagementAPI.getPassword(cm1.Id);
        CommunityManagementAPI.setPassword(cr1.Id, 'test1');
        CommunityManagementAPI.isActive(cr1.Id);
        CommunityManagementAPI.getActive(cr1.Email__c);
        CommunityManagementAPI.setActive(cr1.Id,true);
        CommunityManagementAPI.getCommunityDetails(c.Id);
        CommunityManagementAPI.addCMember(mr);
        CommunityManagementAPI.setCMember(cm1.Id, mr);
        CommunityManagementAPI.getCMember(cm1.Id);
        CommunityManagementAPI.setLastLogin(cr1.Id, system.today());
        list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
        SubscriptionManagementAPI.SubscriptionDetails sub = new SubscriptionManagementAPI.SubscriptionDetails();
        sub.contactId = c.Id; sub.subscriptionId = s.Id; sub.newValue = '1'; sub.oldValue = '0';
        subList.add(sub);
        CommunityManagementAPI.addCMemberAndSubscriptions(mr, subList);
        CommunityManagementAPI.getErrorPrefix();
        CommunityManagementAPI.getWarningPrefix();
        CommunityManagementAPI.getSubGroupId(mr.email, mr.communityId);
        CommunityManagementAPI.setCMemberAndSubscriptions(mr.cMemberId, mr, subList);
        CommunityManagementAPI.getCMemberV2('test1@test.com');
        CommunityManagementAPI.getCMemberV2('test1@test.gov');
        CommunityManagementAPI.getCMember(c.id);
        CommunityManagementAPI.getCMember('abcder');
    }
}