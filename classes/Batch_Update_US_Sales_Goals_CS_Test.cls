@isTest
private class Batch_Update_US_Sales_Goals_CS_Test 
{
    @testSetup
    static void createTestData()
    {
        BroadridgeAssetDate__c objBroadridgeAssetDate = BroadridgeAssetDate__c.getOrgDefaults();
        objBroadridgeAssetDate.Previous_Asset_Date__c = date.today().addDays(-30);
        objBroadridgeAssetDate.AssetDate__c = date.today();
        upsert objBroadridgeAssetDate custSettings__c.Id;
        
        US_Sales_Goals_Tracker__c objUS_Sales_Goals_Tracker = new US_Sales_Goals_Tracker__c();
        objUS_Sales_Goals_Tracker.Status__c ='Active';
        objUS_Sales_Goals_Tracker.Fund_Vehicle_Type__c='ETF';
        insert objUS_Sales_Goals_Tracker;
    
    }
    //Testing from User 1 for 
    @isTest 
    static void testPositiveScenario() {
        
        Test.startTest();
        
        String jobId = Database.executeBatch(new Batch_Update_US_Sales_Goals_Tracker_CS(), 1);
        
        Test.stopTest();
        
    }
    
    @isTest 
    static void testNegativeScenario() 
    {
        List<BroadridgeAssetDate__c > lstBroadridgeAssetDate_Update = new List<BroadridgeAssetDate__c >();
        
        for(BroadridgeAssetDate__c objBroadridgeAssetDate : [SELECT Id, AssetDate__c, Previous_Asset_Date__c , Name FROM BroadridgeAssetDate__c where AssetDate__c !=NULL])
        {
            BroadridgeAssetDate__c objBroadridgeAssetDateUpdate = new BroadridgeAssetDate__c(Id=objBroadridgeAssetDate.Id);
            objBroadridgeAssetDateUpdate.Previous_Asset_Date__c = objBroadridgeAssetDate.AssetDate__c;
            objBroadridgeAssetDateUpdate.Error_Message__c = '';
            lstBroadridgeAssetDate_Update.add(objBroadridgeAssetDateUpdate);
        }
        
        update lstBroadridgeAssetDate_Update;
                        
        Test.startTest();
        String jobId = Database.executeBatch(new Batch_Update_US_Sales_Goals_Tracker_CS(), 1);
        Test.stopTest();
    }

    @isTest 
    static void testScheduleClass() 
    {
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        Test.startTest();
        
        String jobId = System.schedule('ScheduleApexClassBatch_Update_US_Sales_Goals_Tracker_CS',  CRON_EXP, new BatchSch_Update_US_Sales_Goals_Tracker ());
           
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
    }
    
}