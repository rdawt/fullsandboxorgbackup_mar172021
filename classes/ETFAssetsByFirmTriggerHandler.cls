public class ETFAssetsByFirmTriggerHandler {
   
    List<ETF_Assets_By_Firm__c> records = new List<ETF_Assets_By_Firm__c>();
    Map<Id, ETF_Assets_By_Firm__c> oldMap = new Map<Id, ETF_Assets_By_Firm__c>();

    public void onBeforeInsert(List<ETF_Assets_By_Firm__c> lstNewRecords){
        records = lstNewRecords;
        updateInsuranceFlowInfo();
    }

    public void onBeforeUpdate(List<ETF_Assets_By_Firm__c> lstNewRecords){
        records = lstNewRecords;
        updateInsuranceFlowInfo();
    }

    private void updateInsuranceFlowInfo(){
        Set<Id> setUsers = new Set<Id>();
        List<Insurance_Flow_Users__c> users = Insurance_Flow_Users__c.getall().values();
        for(Insurance_Flow_Users__c user : users){
            setUsers.add(user.User_Id__c);
        }
        if(setUsers.Contains(UserInfo.getUserId())){
            for(ETF_Assets_By_Firm__c etf : records){
                etf.Insurance_Flow_Opportunity__c = true;
                etf.Opportunity_Flow_Owner__c = UserInfo.getUserId();
                etf.Unique_Key__c = etf.Account__c + '' + etf.SFDCFundID__c + '' + String.valueOf(etf.Asset_Date__c);
            }
        }       
    }
}