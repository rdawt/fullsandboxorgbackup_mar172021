global class BatchScheduleRecordsCleanUp implements Schedulable{

    global void execute(SchedulableContext sc) {
        DateTime dateLimit = Date.today().addDays(-90);
        String formatedDt = dateLimit.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String status = 'Completed'; 

        String query = 'Select Id,Status__c from Marketo_Activitiy__c where  ActivityDate__c  < '+ formatedDt + ' and Status__c = \''+String.escapeSingleQuotes(status)+'\' limit 200' ;
        BatchRecordsCleanUp batchable = new BatchRecordsCleanUp(query);
        Database.executeBatch(batchable);
    }
}