({
    getOnLoadRecords : function(component,event) {
        component.set("v.showSpinner",true);
        var action = component.get("c.getOnLoadRecords");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                component.set("v.OwnersOptions", rtnVal.OwnersOptions);
                component.set("v.SelectedOwnersOption", rtnVal.SelectedOwnersOption);
                component.set("v.showSpinner",false);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },
    loadCharts : function(component, event, helper) 
    {
        var action = component.get("c.getOpportunityJSON"); 
        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            //alert(state);
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //jsonData = dataObj;
                console.log('===='+dataObj);
                component.set("v.data",dataObj);
                helper.piechart(component,event,helper);
                // helper.Linechart(component,event,helper);
                // helper.donutchart(component,event,helper);
                // helper.Columnchart(component,event,helper);
            } 
        });
        $A.enqueueAction(action);
    },
    //Generic Method to perform server calls
    callToServer: function(component, method, callback, params) {
        component.set("v.showSpinner", true);
        var action = component.get(method);
        //action.setStorable();
        if(params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                callback.call(this, response.getReturnValue());
            } else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.alertMessage", "Error message: " + errors[0].message);
                        component.set("v.isAlert", true);
                    }
                }
            } else {
                component.set("v.alertMessage", "ERROR: Unknown Error");
                component.set("v.isAlert", true);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Method which retrieves the cases and account details
    getCases: function(component) {
        var self = this;
        self.callToServer(
            component,
            "c.getAllCases",
            function(response) {
                if(response.length > 0) {
                    var result = response[0];
                    if(result.cases !== undefined && result.accounts !== undefined) {
                        var accounts = result.accounts;
                        self.countCases(component, result.cases, accounts);
                    } else {
                        component.set("v.alertMessage", "No Access");
                        component.set("v.isAlert", true);
                    }
                } else {
                    component.set("v.caseList", []);
                }
            },
            {
                
            });
    },
    
    countCases: function(component, cases, accounts) {
        console.log('????????in Count Cases????????');
        var self = this;
        var openCasesMap = {};
        var closedCasesMap = {};
        var result = [];
        
        //Map of account and open cases
        function openCasesToMap(key, value) {
            openCasesMap[key] = openCasesMap[key] || [];
            var temp = openCasesMap[key];
            temp.push(value)
            openCasesMap[key] = temp;
        }
        
        //Map of account and closed cases
        function closedCasesToMap(key, value) {
            closedCasesMap[key] = closedCasesMap[key] || [];
            var temp = closedCasesMap[key];
            temp.push(value)
            closedCasesMap[key] = temp;
        }
        
        //Adding values to map based on case status and accountId
        for(var i=0;i<cases.length;i=i+1) {
            var record = cases[i];
            if(record.Status === "New" || record.Status === "Working" || record.Status === "Escalated") {
                openCasesToMap(record.AccountId, record);
            } else if(record.Status === "Closed") {
                closedCasesToMap(record.AccountId, record);
            }
        }
        
        for(var k=0;k<accounts.length;k=k+1) {
            var openCases = 0;
            var closedCases = 0;
            var rec = {};
            var account = accounts[k];
            rec.AccountId = account.Id;
            rec.AccountName = account.Name;
            
            if(openCasesMap[account.Id] !== undefined) {
                openCases = openCasesMap[account.Id].length;
            }
            if(closedCasesMap[account.Id] !== undefined) {
                closedCases = closedCasesMap[account.Id].length;
            }
            rec.totalOpenCases = openCases;
            rec.totalClosedCases = closedCases;
            rec.totalCases = openCases + closedCases;
            result.push(rec);
        }
        component.set("v.lstResults", result);
        if(result.length > 0) {
            if(result[0].totalCases > 0) {
                component.set("v.isLoaded", true);
            }
        }
		component.set("v.barData",result);
        self.setChartData(component);
    },
    
    setChartData : function(component, event) {
        console.log('????????in setChartData????????');
        var data = component.get("v.barData");
        if(data.length > 0) {
                component.set("v.isLoaded", "true");                
                var barData = [];

                /*
                for(var i=0; i<data.length; i=i+1) {
                    var barItem = {};
                    barItem.label = data[i].AccountName;
                    barItem.OpenCases = data[i].totalOpenCases;
                    barItem.ClosedCases = data[i].totalClosedCases;
                    barItem.TotalCases = data[i].totalCases;
                    barData.push(barItem);
                }
                */
            
                    var barItem1 = {};
                    barItem1.label = 'Jan';
                    barItem1.Emails = 80;
                    barItem1.Meetings = 20;
                    barItem1.Calls = 100;
                    barItem1.Unknown = 100;
                    barData.push(barItem1); 

                    var barItem2 = {};
                    barItem2.label = 'Feb';
                    barItem2.Emails = 50;
                    barItem2.Meetings = 20;
                    barItem2.Calls = 70;
                    barItem2.Unknown = 70;
                    barData.push(barItem2);
                    
                    var barItem3 = {};
                    barItem3.label = 'Mar';
                    barItem3.Emails = 80;
                    barItem3.Meetings = 120;
                    barItem3.Calls = 200;
                    barItem3.Unknown = 200;
                    barData.push(barItem3); 

                    var barItem4 = {};
                    barItem4.label = 'Apr';
                    barItem4.Emails = 40;
                    barItem4.Meetings = 20;
                    barItem4.Calls = 60;
                    barItem4.Unknown = 60;
                    barData.push(barItem4);

                    var barItem5 = {};
                    barItem5.label = 'May';
                    barItem5.Emails = 40;
                    barItem5.Meetings = 20;
                    barItem5.Calls = 60;
                    barItem5.Unknown = 60;
                    barData.push(barItem5);

                    var barItem6 = {};
                    barItem6.label = 'Jun';
                    barItem6.Emails = 5;
                    barItem6.Meetings = 5;
                    barItem6.Calls = 5;
                    barItem6.Unknown = 3;
                    barData.push(barItem6);

                    var barItem7 = {};
                    barItem7.label = 'Jul';
                    barItem7.Emails = 0;
                    barItem7.Meetings = 0;
                    barItem7.Calls = 0;
                    barItem7.Unknown = 0;
                    barData.push(barItem7); 

                    var barItem8 = {};
                    barItem8.label = 'Aug';
                    barItem8.Emails = 0;
                    barItem8.Meetings = 0;
                    barItem8.Calls = 0;
                    barItem8.Unknown = 0;
                    barData.push(barItem8);
                    
                    var barItem9 = {};
                    barItem9.label = 'Sep';
                    barItem9.Emails = 0;
                    barItem9.Meetings = 0;
                    barItem9.Calls = 0;
                    barItem9.Unknown = 0;
                    barData.push(barItem9); 

                    var barItem10 = {};
                    barItem10.label = 'Oct';
                    barItem10.Emails = 0;
                    barItem10.Meetings = 0;
                    barItem10.Calls = 0;
                    barItem10.Unknown = 0;
                    barData.push(barItem10);

                    var barItem11 = {};
                    barItem11.label = 'Nov';
                    barItem11.Emails = 0;
                    barItem11.Meetings = 0;
                    barItem11.Calls = 0;
                    barItem11.Unknown = 0;
                    barData.push(barItem11);

                    var barItem12 = {};
                    barItem12.label = 'Dec';
                    barItem12.Emails = 0;
                    barItem12.Meetings = 0;
                    barItem12.Calls = 0;
                    barItem12.Unknown = 0;
                    barData.push(barItem12);
            
            
                component.set("v.barData", barData);            
        } else {
            component.set("v.nocasesMsg","true");
        }
    }
})