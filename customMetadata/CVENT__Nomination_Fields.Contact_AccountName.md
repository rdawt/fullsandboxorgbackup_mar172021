<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Contact_AccountName</description>
    <label>Contact_AccountName</label>
    <protected>false</protected>
    <values>
        <field>CVENT__Field__c</field>
        <value xsi:type="xsd:string">SalesConnect__AccountName__c</value>
    </values>
    <values>
        <field>CVENT__Object__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>CVENT__isTest__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
