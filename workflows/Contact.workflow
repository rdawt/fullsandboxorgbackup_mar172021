<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_for_MHL_Engaged</fullName>
        <description>Email Alert for MHL &quot;Engaged&quot;</description>
        <protected>false</protected>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Marketo_HL_just_triggered</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_new_VE_Employeee</fullName>
        <description>Email Alert for new VE_Employeee</description>
        <protected>false</protected>
        <recipients>
            <recipient>bdonnelly1@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlevin@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svenkateswaran@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_VE_Employee_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_MVIS_registrant_Alert</fullName>
        <ccEmails>tkettner@vaneck.com;</ccEmails>
        <description>New MVIS registrant Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bdonnelly1@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_MVIS_registrant_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Privacy_Policy_Email</fullName>
        <description>Send Privacy Policy Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Former_iContact_Templates_Obsolete/CORP_Privacy_Policy_Email_Long_Version</template>
    </alerts>
    <fieldUpdates>
        <fullName>Action_Disengage_Marketo_Half_Life</fullName>
        <description>Disable MHL by updating “Visible to Marketo” in “contact” (FALSE)</description>
        <field>Visible_to_Marketo__c</field>
        <literalValue>0</literalValue>
        <name>Action - Disengage Marketo &quot;Half-Life&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_Disengage_Marketo_Half_Life_2</fullName>
        <description>Disable MHL by updating “Email was deleted” in “contact” (FALSE)</description>
        <field>Email_was_deleted__c</field>
        <literalValue>0</literalValue>
        <name>Action - Disengage Marketo &quot;Half-Life&quot; 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_Email_was_deleted</fullName>
        <description>Update field &quot;Contact: Email was deleted&quot;  - Action for WFR &quot; Email Deletion (Maintain Sync with Marketo)&quot; .</description>
        <field>Email_was_deleted__c</field>
        <literalValue>1</literalValue>
        <name>Action - &quot;Email was deleted&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_Engage_Marketo_Half_Life</fullName>
        <description>Enable MHL by updating “Visible to Marketo” in “contact” (TRUE)</description>
        <field>Visible_to_Marketo__c</field>
        <literalValue>1</literalValue>
        <name>Action - Engage Marketo &quot;Half-Life&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_for_AddressTools_Validate_USA</fullName>
        <description>Converts contact mailing country from &quot;United States of America&quot; to &quot;United States&quot;.</description>
        <field>MailingCountry</field>
        <formula>&apos;United States&apos;</formula>
        <name>Action for AddressTools Validate &quot;USA&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_for_IS_VEG</fullName>
        <description>Action for &quot;Trigger WFR &quot;Is VEG&quot; field&quot; - Enforced when the email contains one of the 13 domains from VEG used for emails.</description>
        <field>IS_User__c</field>
        <literalValue>1</literalValue>
        <name>Action for &quot;IS VEG&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Australian_Elegibility_UPDATE</fullName>
        <description>Australian Eligibility - Populate DB Source</description>
        <field>DB_Source__c</field>
        <formula>&apos;MVA&apos;</formula>
        <name>Australian Elegibility - UPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ETF_PU_Tracking_Update</fullName>
        <field>ETF_PU_Tracking__c</field>
        <formula>IF( ISPICKVAL(ETF_PU__c, &apos;Yes&apos;), TODAY(),NULL)</formula>
        <name>ETF PU Tracking Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_ASITracker</fullName>
        <field>ASI_ChangeTrack__c</field>
        <formula>$User.FirstName + &apos;;&apos; + $User.LastName + &apos;;&apos;+ $User.Email +&apos;;&apos;+$User.Username  +&quot; ; &quot;+TEXT((NOW()))</formula>
        <name>FU-ASITracker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Has_Portfolio_Discretion_ValueTracker</fullName>
        <description>Has Portfolio Discretion Value Tracker</description>
        <field>Has_Portfolio_Discretion_Value_Tracker__c</field>
        <formula>text(RepPM__c)</formula>
        <name>FU Has Portfolio Discretion ValueTracker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_MVIS_Team_update_Contact_DB_Source</fullName>
        <field>DB_Source__c</field>
        <formula>&apos;MVIS BlueStar&apos;</formula>
        <name>FU MVIS Team update Contact DB Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_RIA_North</fullName>
        <field>SalesInternal__c</field>
        <lookupValue>cgagnier@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>FU-RIA North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SC</fullName>
        <field>SalesInternal__c</field>
        <lookupValue>csteinberg@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>FU-SC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Sales_Internal_NC</fullName>
        <description>NC still has SI as Will White</description>
        <field>SalesInternal__c</field>
        <name>FU-Update Sales Internal - NC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_web_registrant_s_informal_name</fullName>
        <description>Add contact&apos;s First Name into the Informal name field.</description>
        <field>Informal_Name__c</field>
        <formula>FirstName</formula>
        <name>FU Update web registrant&apos;s informal name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investor_Type_of_Retail_Contacts</fullName>
        <description>Updates Investor Type field for Contacts those dont have Community Registrants not associated to enable them to manage their subscription link in email</description>
        <field>InvestorTpSD__c</field>
        <literalValue>Individual Investor</literalValue>
        <name>Investor Type of Retail Contacts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LatAm_Elegibility_UPDATE</fullName>
        <description>LatAm Eligibility - Populate DB Source</description>
        <field>DB_Source__c</field>
        <formula>&apos;LATAM&apos;</formula>
        <name>LatAm Elegibility - UPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MailingCountryUpdate</fullName>
        <description>To update Mailing Country field with the value from SalesConnect Country</description>
        <field>MailingCountry</field>
        <formula>SalesConnect__Country__c</formula>
        <name>MailingCountryUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rep_As_PM_Tracking_Update</fullName>
        <field>RepPM_Tracking__c</field>
        <formula>IF( ISPICKVAL(RepPM__c, &apos;Yes&apos;), TODAY(),NULL)</formula>
        <name>Rep As PM Tracking Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAsContinueToSendMeEmails</fullName>
        <field>Unsubscribe_Public__c</field>
        <literalValue>Continue to send me emails</literalValue>
        <name>UpdateAsContinueToSendMeEmails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAsUnsubscribeMeFromEmails</fullName>
        <field>Unsubscribe_Public__c</field>
        <literalValue>Unsubscribe me from future emails</literalValue>
        <name>UpdateAsUnsubscribeMeFromEmails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bounced_Email</fullName>
        <description>When Email value is changed, Bounced Email is made FALSE from TRUE</description>
        <field>Bounced_Email__c</field>
        <literalValue>0</literalValue>
        <name>Update Bounced Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SI_Update_RIA_South</fullName>
        <field>SalesInternal__c</field>
        <lookupValue>bpope@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update SI Update RIA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SI_Update_RIA_West</fullName>
        <field>SalesInternal__c</field>
        <lookupValue>mmorelli@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update SI Update RIA West</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_FL</fullName>
        <description>Update Sales Internal - FL</description>
        <field>SalesInternal__c</field>
        <lookupValue>mburns@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - FL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_MA</fullName>
        <description>Updates Sales Internal for MA territory</description>
        <field>SalesInternal__c</field>
        <lookupValue>mhofbauer@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - MA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_NE</fullName>
        <description>Updates Sales Internal for NE Territory</description>
        <field>SalesInternal__c</field>
        <lookupValue>vkasyanov@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - NE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_NW</fullName>
        <description>Updates Sales Internal NW Territory</description>
        <field>SalesInternal__c</field>
        <lookupValue>crogers@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - NW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_NY</fullName>
        <description>Updates the Sales Internal for Territory NY</description>
        <field>SalesInternal__c</field>
        <lookupValue>rhenshaw@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - NY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_SE</fullName>
        <description>Updates Sales Internal for SE territory</description>
        <field>SalesInternal__c</field>
        <lookupValue>jgatling@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - SE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_SW</fullName>
        <description>Updates Sales Internal for SW Territory</description>
        <field>SalesInternal__c</field>
        <lookupValue>nfrasse@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Sales Internal - SW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Internal_ToBlank</fullName>
        <description>Populate Blank(Default) when a Territory is not equal to NE/NY/MA/SE/FL/NC/SC/NW/SW</description>
        <field>SalesInternal__c</field>
        <name>Update Sales Internal -ToBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AddToCampaign</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@vaneck</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AddressTools Validate %22United States Of America%22</fullName>
        <actions>
            <name>Action_for_AddressTools_Validate_USA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>United States Of America</value>
        </criteriaItems>
        <description>Standardizing mailing country for &quot;United States&quot; in conjunction with Address Tools.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Australian Elegibility - Populate DB Source</fullName>
        <actions>
            <name>Australian_Elegibility_UPDATE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.DB_Source__c</field>
            <operation>notContain</operation>
            <value>MVIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>Australia,New Zealand</value>
        </criteriaItems>
        <description>Updates contact&apos;s DB Source when Contact&apos;s mailing country = &quot;Australia&quot; AND, DB Source does not Contain &quot;MVIS&quot;. 
Field update to DB Source = &quot;MVA&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Disengage Marketo %22Half-Life%22</fullName>
        <active>false</active>
        <description>This Time-Based WFR will update “Visible to Marketo” OR &quot;Email was Deleted&quot; in “contact” (FALSE) 90 days, after the action for “Engage Marketo Half-Life&quot; or “Engage Marketo Half-Life&quot; (2), allowing enough time, to complete the sync with Marketo.</description>
        <formula>(Visible_to_Marketo__c = true
||
Email_was_deleted__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Action_Disengage_Marketo_Half_Life</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Action_Disengage_Marketo_Half_Life_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ETF PU Tracking Update</fullName>
        <actions>
            <name>ETF_PU_Tracking_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.ETF_PU__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Deletion %28Maintain Sync with Marketo%29</fullName>
        <actions>
            <name>Action_Email_was_deleted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Inherited from Marketo Half Life – If the email address was deleted, this WFR will update the field “Email was deleted/ TRUE”, affecting the Sharing rule and maintaining the synchronization with Marketo.</description>
        <formula>(ISCHANGED(Email) &amp;&amp; ISBLANK(Email))
&amp;&amp;
Subscription_Group_Name__c &lt;&gt; &quot;MVIS Registrant&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Engage Marketo %22Half-Life%22</fullName>
        <actions>
            <name>Email_Alert_for_MHL_Engaged</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Action_Engage_Marketo_Half_Life</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>IF, the “Active Subscription Indicator” drops form ANY to “0”, this WFR will update “Visible to Marketo” in “contact” (TRUE). This update will affect the Sharing Rule with Marketo, creating an overlapping criteria(Keeping the sync) + email alert.</description>
        <formula>(ISCHANGED(Active_Subscriptions_Indicator__c) &amp;&amp; ISBLANK(Active_Subscriptions_Indicator__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LatAm Elegibility - Populate DB Source</fullName>
        <actions>
            <name>LatAm_Elegibility_UPDATE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.DB_Source__c</field>
            <operation>notContain</operation>
            <value>MVIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>Chile</value>
        </criteriaItems>
        <description>Updates contact&apos;s DB Source when Contact&apos;s mailing country = &quot;Chile&quot; AND, DB Source does not Contain &quot;MVIS&quot;. 
Field update to DB Source = &quot;LATAM&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MVIS Team update Contact DB Source</fullName>
        <actions>
            <name>FU_MVIS_Team_update_Contact_DB_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Owner__c</field>
            <operation>equals</operation>
            <value>Emre Camlilar,Thomas Kettner,Josh Kaplan,Tzlil Keren-Blum,Joe Levin,Steven Schoenfeld,Joy Yang</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New MVIS Registrant</fullName>
        <actions>
            <name>New_MVIS_registrant_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.DB_Source__c</field>
            <operation>equals</operation>
            <value>MVIS Registrant</value>
        </criteriaItems>
        <description>Tracks registrants from MVIS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Van Eck Employee</fullName>
        <actions>
            <name>Email_Alert_for_new_VE_Employeee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>contains</operation>
            <value>Van Eck</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>notEqual</operation>
            <value>Van Eck Securities Corp-MF Sales Data</value>
        </criteriaItems>
        <description>Tracks registrants from MVIS and send e-mail alerts when the registrant is an current VEG employee.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Non Registred Retail Contact</fullName>
        <actions>
            <name>Investor_Type_of_Retail_Contacts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Subscription_Group_Derived_Id__c</field>
            <operation>equals</operation>
            <value>a0cA0000005ZNcW</value>
        </criteriaItems>
        <description>Used to deal with the Contacts in Individual Investor group that dont have Registrant associated to Manage their Subscription without error</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OptInSync</fullName>
        <actions>
            <name>UpdateAsContinueToSendMeEmails</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Rule to Sync. Email Opt-in for MKTO subscription/Campaigns and sync. the Unsubscribe_Public text value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OptOutSync</fullName>
        <actions>
            <name>UpdateAsUnsubscribeMeFromEmails</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Rule to Sync. Email Opt-in for MKTO subscription/Campaigns and sync. the Unsubscribe_Public text value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Privacy Policy Email Sent</fullName>
        <actions>
            <name>Send_Privacy_Policy_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Email_sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rep As PM Tracking Update</fullName>
        <actions>
            <name>Rep_As_PM_Tracking_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RepPM__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Trigger WFR %22Is VEG%22 field</fullName>
        <actions>
            <name>Action_for_IS_VEG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This WFR will check true for &quot;IS VEG&quot; when the contact&apos;s email contains one of the 13 VEG&apos;s domains used on emails (See rule with values) except on the account &quot;Van Eck Securities Corp-MF Sales Data&quot;.</description>
        <formula>((CONTAINS(Email,&apos;enfoca-vaneck.com&apos;)||
CONTAINS(Email,&apos;enfoca-vaneck.com.co&apos;)||
CONTAINS(Email,&apos;g175capital.com&apos;)||
CONTAINS(Email,&apos;marketvectors.com.au&apos;)||
CONTAINS(Email,&apos;marketvectors-australia.com&apos;)||
CONTAINS(Email,&apos;marketvectorsetns.com&apos;)||
CONTAINS(Email,&apos;marketvectors-europe.com&apos;)||
CONTAINS(Email,&apos;marketvectors-luxembourg.com&apos;)||
CONTAINS(Email,&apos;marketvectors-sa.com&apos;)||
CONTAINS(Email,&apos;marketvectors-sa.eu&apos;)||
CONTAINS(Email,&apos;vaneck.com&apos;)||
CONTAINS(Email,&apos;vaneck-enfoca.com&apos;)||
CONTAINS(Email,&apos;vaneck-enfoca.com.co&apos;)||
CONTAINS(Account.Name,&apos;VanEck&apos;)||
CONTAINS(Account.Name,&apos;Van Eck&apos;)||
CONTAINS(Account.Name,&apos;Market Vectors&apos;))
&amp;&amp;
(
NOT(CONTAINS(AccountId,&apos;001A0000016GFmI&apos;))
||
NOT(CONTAINS(Email,&apos;@mgw&apos;)))

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22munchkinTokenListJSON%22</fullName>
        <active>false</active>
        <description>Update to {}</description>
        <formula>LastModifiedDate  &lt;  NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Bounced Email Rule</fullName>
        <actions>
            <name>Update_Bounced_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Bounced Email is true &amp; If the Email value is changed from previous value</description>
        <formula>Bounced_Email__c == True &amp;&amp; ISCHANGED(Email)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal - FL</fullName>
        <actions>
            <name>Update_Sales_Internal_FL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>FL</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal - NC</fullName>
        <actions>
            <name>FU_Update_Sales_Internal_NC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>NC</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal - ToBlank</fullName>
        <actions>
            <name>Update_Sales_Internal_ToBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>notContain</operation>
            <value>FL,NC,NE,NY,MA,SE,SC,NW,SW,RIA North,RIA South,RIA West</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -MA</fullName>
        <actions>
            <name>Update_Sales_Internal_MA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>MA</value>
        </criteriaItems>
        <description>**updated to reflect the MC Territory reassignment**Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -NE</fullName>
        <actions>
            <name>Update_Sales_Internal_NE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>NE</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -NW</fullName>
        <actions>
            <name>Update_Sales_Internal_NW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>NW</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -NY</fullName>
        <actions>
            <name>Update_Sales_Internal_NY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>NY</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -RIA North</fullName>
        <actions>
            <name>FU_RIA_North</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>RIA North</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -RIA South</fullName>
        <actions>
            <name>Update_SI_Update_RIA_South</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>RIA South</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -RIA West</fullName>
        <actions>
            <name>Update_SI_Update_RIA_West</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>RIA West</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -SC</fullName>
        <actions>
            <name>FU_SC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>SC</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration) - Restored to update Sales Internal to &quot;Clayton Steinberg&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -SE</fullName>
        <actions>
            <name>Update_Sales_Internal_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>SE</value>
        </criteriaItems>
        <description>Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration) - Restored to update Sales Internal to &quot;Clayton Steinberg&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Internal -SW</fullName>
        <actions>
            <name>Update_Sales_Internal_SW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Territory__c</field>
            <operation>contains</operation>
            <value>SW</value>
        </criteriaItems>
        <description>**updated as of 4/24 for MC&apos;s reassignment**-Evaluates the Sales Internal  and assigns to the Contact based on Territory rules (Territory rules are governed outside of SFDC in the first iteration)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update web registrant%27s informal name</fullName>
        <actions>
            <name>FU_Update_web_registrant_s_informal_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>For email personalization: the informal name for contacts under VE Communication&apos;s ownership + Individual Investor - Retail or Default Registrant Firm cant be left blank.</description>
        <formula>/*
VanEck Communication ID  = 005A0000008C61I
Individual Investor - Retail ID  = 001A000000xH900
Default Registrant Firm ID = 001A000000u9gkE
App User ID (Created By = App User) = 005A0000001vH2xIAE
App User ID (User) = 005A0000001vH2xIAE
*/ 


(
OwnerId = &quot;005A0000008C61I&quot;
&amp;&amp;
(Account.Id = &quot;001A000000u9gkE&quot;
||
Account.Id = &quot;001A000000xH900&quot;)
&amp;&amp;
Informal_Name__c = &quot;&quot;
&amp;&amp;
FirstName != &quot;&quot;
&amp;&amp;
CreatedBy.Id = &quot;005A0000001vH2x&quot;
&amp;&amp;
$User.Id = &quot;005A0000001vH2x&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateMailingCountry</fullName>
        <actions>
            <name>MailingCountryUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.SalesConnect__Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow sets the Mailing Country value to the value SalesConnect brings in SalesConnect Country Field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF-ASITracker</fullName>
        <actions>
            <name>FU_ASITracker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Active_Subscriptions_Indicator__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF-Has Portfolio Discretion Value Tracker</fullName>
        <actions>
            <name>FU_Has_Portfolio_Discretion_ValueTracker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Has Portfolio Discretion Value Tracker</description>
        <formula>ISCHANGED(RepPM__c ) &amp;&amp; Text (RepPM__c)   = &apos;Yes&apos;
||
ISCHANGED(RepPM__c ) &amp;&amp; Text (RepPM__c)   = &apos;No&apos;
||
ISCHANGED(RepPM__c ) &amp;&amp; Text (RepPM__c)   = &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Assign_Eligibility</fullName>
        <assignedToType>owner</assignedToType>
        <description>The particular Contact have no subscription eligibility. Please assign a Subscription Group.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Assign Eligibility</subject>
    </tasks>
</Workflow>
