trigger ETFAssetsByFirmTrigger on ETF_Assets_By_Firm__c (before insert,before update) {

    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_ETFAssetsByFirm__c == true)
    {
        ETFAssetsByFirmTriggerHandler objHandler = new ETFAssetsByFirmTriggerHandler();
        
        if(trigger.isInsert && trigger.isBefore){
            objHandler.onBeforeInsert(Trigger.new);
        }

        if(trigger.isUpdate && trigger.isBefore){
            objHandler.onBeforeUpdate(Trigger.new);
        }
    }
}