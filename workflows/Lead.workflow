<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>HOLDRS_Send_Privacy_Policy_to_User_Added_Leads</fullName>
        <description>HOLDRS Send Privacy Policy to User Added Leads</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Former_iContact_Templates_Obsolete/CORP_Privacy_Policy_Email_Long_Version</template>
    </alerts>
    <rules>
        <fullName>HOLDRS Send Privacy Policy to User Added Leads</fullName>
        <actions>
            <name>HOLDRS_Send_Privacy_Policy_to_User_Added_Leads</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.DB_Source__c</field>
            <operation>equals</operation>
            <value>HOLDRSSFDCUserAdded</value>
        </criteriaItems>
        <description>HOLDRS Send Privacy Policy to User Added Leads</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
