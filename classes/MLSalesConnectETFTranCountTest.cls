@isTest
//(SeeAllData=true)

   private class MLSalesConnectETFTranCountTest {
     //this test case tests the  SCTranCountAggResult for Greater than Zero values so that it goes into positive test case
    @isTest static void testMLSalesConnectETFTranCountTest() {
        Integer SCTranCountAggResult=0 ;
        SalesConnect__NewTransaction__c Tran0 = new SalesConnect__NewTransaction__c ( SalesConnect__Transaction_ID__c='12399',name = 'tran1', SalesConnect__Gross_Amount__c = 1000,SalesConnect__Source_Description__c= 'Merrill ETF Trades' );
        insert tran0;
            //Datetime yesterday = Datetime.now().addDays(-1);
            Datetime today= Datetime.now();
            Test.setCreatedDate(tran0.Id, today);
            Tran0 = [Select CreatedDate, Fund_Vehicle_Type__c From SalesConnect__NewTransaction__c where id =: tran0.Id];
             System.debug('created date in test case 1 value: '+Tran0.CreatedDate);
              System.debug('id in test case 1 value: '+Tran0.Id);
               System.debug('Fund Veh Type : '+Tran0.Fund_Vehicle_Type__c);
            SalesConnect__NewTransaction__c Tran1 = new SalesConnect__NewTransaction__c (SalesConnect__Transaction_ID__c ='456',name = 'tran2', SalesConnect__Gross_Amount__c = 1000,SalesConnect__Source_Description__c= 'ETF');
            insert tran1;
            Test.setCreatedDate(tran1.Id, today);
            
        Test.startTest();
        try{
                System.assert([SELECT COUNT() FROM SalesConnect__NewTransaction__c where CreatedDate = today ]  > 0);
                System.debug('created date in test case 1 value: '+Tran0.CreatedDate);
                // System.assertEquals('2018-04-30 00:00:00',String.valueOf(Tran0.CreatedDate));
                  System.assertEquals(String.valueOf(today),String.valueOf(Tran0.CreatedDate));
                   //System.assertEquals('2018-04-30 00:00:00',String.valueOf(Tran1.CreatedDate)); 
                 List<EmailTemplate> lstEmailTemplates = null;
                 lstEmailTemplates = [SELECT Id, Body, Subject,HTMLValue,developername  from EmailTemplate where developername = 'ML_DST_ETF_DataLoadAlert'];
               
                 //System.assertEquals('00X2F000000MvDq', lstEmailTemplates[0].Id);
                 if ((lstEmailTemplates.size()) <= 0 || (lstEmailTemplates[0] == null)){ 
                  
                   SendEmailNotification see = new SendEmailNotification('Test - unable to get the template with developername - ML_DST_ETF_DataLoadAlert');
         
                 } 
                string msg;
                SendEmailNotification se;
                string jobId = System.schedule('testTaskScheduleApex', '0 0 0 5 7 ? 2022',  new MLSalesConnectETFTranCount());
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
                System.assertEquals('0 0 0 5 7 ? 2022', ct.CronExpression);
                System.assertEquals(0, ct.TimesTriggered);
                System.assertEquals('2022-07-05 00:00:00',String.valueOf(ct.NextFireTime));
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                SCTranCountAggResult = [SELECT count() FROM SalesConnect__NewTransaction__c WHERE Fund_Vehicle_Type__c = 'ETF' AND CreatedDate = Today AND SalesConnect__Source_Description__c = 'Merrill ETF Trades'];
                System.debug(SCTranCountAggResult );
                System.assertNotEquals(0, SCTranCountAggResult);
                System.debug('Executing Test Case 1 for row count: '+SCTranCountAggResult );
                if (SCTranCountAggResult > 0 ) {
                    Messaging.SingleEmailMessage Testemail;
                    List<Messaging.SendEmailresult> Testemailresults;
                    Testemail = new Messaging.SingleEmailMessage();
                    Testemail.setTemplateId(lstEmailTemplates[0].Id);
                    Testemail.setToAddresses(new String[] {'svenkateswaran@vaneck.com'});
                    Testemail.setHtmlBody(lstEmailTemplates[0].HTMLValue);
                    mail.setToAddresses(new String[] {'svenkateswaran@vaneck.com','Help.Sales@vaneck.com'});
                    mail.setSubject('ML ETF Transaction Data Load Alert');
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { Testemail });
    
               }
              else{
                   SendEmailNotification se2 = new SendEmailNotification('No ML ETF Data is loaded today and the Count in Transaction is: ' + SCTranCountAggResult );
                   msg = 'ML ETF Data is NOT loaded and the Count in Transaction is: \n ' + ' ' +SCTranCountAggResult;
                   se2.sendEmail(msg);
               }
              
            }catch(Exception e){  SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Counting ML ETF in SC Transaction Object Failed');
                string msg3 = 'Exception Occurred in Apex for Counting ML ETF in SC Transaction Object for today Failed \n '+e.getMessage() + ' ' +SCTranCountAggResult;
                se.sendEmail(msg3);
                }
                
         
    Test.stopTest();
    }
   //this test case tests the  SCTranCountAggResult for Zero values so that it goes into negative test case
  @isTest static void testMLSalesConnectETFTranCountTest2() {
        Integer SCTranCountAggResult=0 ;
       
        Test.startTest();
        try{
            List<EmailTemplate> lstEmailTemplates = null;
            lstEmailTemplates = [SELECT Id, Body, Subject,HTMLValue,developername  from EmailTemplate where developername = 'ML_DST_ETF_DataLoadAlert'];
            string msg;
            SendEmailNotification se;
            string jobId = System.schedule('testTaskScheduleApex', '0 0 0 5 7 ? 2022',  new MLSalesConnectETFTranCount());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('0 0 0 5 7 ? 2022', ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2022-07-05 00:00:00',String.valueOf(ct.NextFireTime));
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            SCTranCountAggResult = [SELECT count() FROM SalesConnect__NewTransaction__c WHERE Fund_Vehicle_Type__c = 'ETF' AND CreatedDate = Today AND SalesConnect__Source_Description__c = 'Merrill ETF Trades'];
            System.debug(SCTranCountAggResult );
            System.assertEquals(0, SCTranCountAggResult);
            System.debug('Executing Test Case 2 for row count: '+SCTranCountAggResult );
            if (SCTranCountAggResult > 0 ) {
                Messaging.SingleEmailMessage Testemail;
                List<Messaging.SendEmailresult> Testemailresults;
                Testemail = new Messaging.SingleEmailMessage();
                Testemail.setTemplateId(lstEmailTemplates[0].Id);
                Testemail.setToAddresses(new String[] {'svenkateswaran@vaneck.com'});
                Testemail.setHtmlBody(lstEmailTemplates[0].HTMLValue);
                mail.setToAddresses(new String[] {'svenkateswaran@vaneck.com','Help.Sales@vaneck.com'});
                mail.setSubject('ML ETF Transaction Data Load Alert');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { Testemail });

               }
              else{
                   SendEmailNotification se2 = new SendEmailNotification('No ML ETF Data is loaded today and the Count in Transaction is: ' + SCTranCountAggResult );
                   msg = 'ML ETF Data is loaded and the Count in Transaction is: \n ' + ' ' +SCTranCountAggResult;
                   se2.sendEmail(msg);
               }
          
        }catch(Exception e){  SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Counting ML ETF in SC Transaction Object Failed');
            string msg3 = 'Exception Occurred in Apex for Counting ML ETF in SC Transaction Object for today Failed \n '+e.getMessage() + ' ' +SCTranCountAggResult;
            se.sendEmail(msg3);
            }
            
     
    Test.stopTest();
    }
     //this test case tests the  system exception situation if a wrong email template was configured or email emplates for email was removed or not found
    @isTest static void testMLSalesConnectETFTranCountTest3() {
    
       Integer SCTranCountAggResult=0 ;
        //SalesConnect__NewTransaction__c Tran0 = new SalesConnect__NewTransaction__c ( SalesConnect__Transaction_ID__c='123',name = 'tran1', SalesConnect__Gross_Amount__c = 1000,SalesConnect__Source_Description__c= 'ETF Trades' );
        //insert tran0;
            //Datetime yesterday = Datetime.now().addDays(-1);
           // Datetime today= Datetime.now();
            //Test.setCreatedDate(tran0.Id, today);
            //Tran0 = [Select CreatedDate, Fund_Vehicle_Type__c From SalesConnect__NewTransaction__c where id =: tran0.Id];
            // System.debug('created date in test case 3 value: '+Tran0.CreatedDate);
            //  System.debug('id in test case 3 value: '+Tran0.Id);
             //  System.debug('Fund Veh Type in teat case 3 : '+Tran0.Fund_Vehicle_Type__c);
            //SalesConnect__NewTransaction__c Tran1 = new SalesConnect__NewTransaction__c (SalesConnect__Transaction_ID__c ='456',name = 'tran2', SalesConnect__Gross_Amount__c = 1000,SalesConnect__Source_Description__c= 'ETF');
            //insert tran1;
            //Test.setCreatedDate(tran1.Id, today);
           
        Test.startTest();
        try{
                 List<EmailTemplate> lstEmailTemplates = null;
                 lstEmailTemplates = [SELECT Id, Body, Subject,HTMLValue,developername  from EmailTemplate where id = '00X2F000000MiMB']; //DST_ETF_DataLoadAlert
               
                 System.assertNotEquals('00X2F000000MiumUAC', lstEmailTemplates[0].Id);
                 System.assertNotEquals(null, lstEmailTemplates[0].Id); //should be equal to 00X2F000000MiMB
                 System.assertEquals('00X2F000000MiMBUA0', lstEmailTemplates[0].Id);
                  System.assertEquals('SFDC_DST_DataLoad_Alerts',lstEmailTemplates[0].developername);
                 
                 System.debug('Email Template Developer id in Test Case 3 is : '+lstEmailTemplates[0].developername);
                 if (lstEmailTemplates[0].developername == 'DST_ETF_DataLoadAlert')
                 
                 { 
                       //  System.assert([SELECT COUNT() FROM SalesConnect__NewTransaction__c where CreatedDate = today ]  > 0);
                  //  System.debug('created date in test case 3 value: '+Tran0.CreatedDate);
                 //   //System.assertEquals('2018-04-30 00:00:00',String.valueOf(Tran0.CreatedDate));
                  //  System.assertEquals(String.valueOf(today),String.valueOf(Tran0.CreatedDate));
                    //System.assertEquals('2018-04-30 00:00:00',String.valueOf(Tran1.CreatedDate)); 
                    System.debug('Email Template Developer id in Test Case 3 is -inside if : '+lstEmailTemplates[0].developername);
                    string msg;
                    SendEmailNotification se;
                    string jobId = System.schedule('testTaskScheduleApex', '0 0 0 5 8 ? 2022',  new MLSalesConnectETFTranCount());
                    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
                    System.assertEquals('0 0 0 5 8 ? 2022', ct.CronExpression);
                    System.assertEquals(0, ct.TimesTriggered);
                    System.assertEquals('2022-08-05 00:00:00',String.valueOf(ct.NextFireTime));
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    SCTranCountAggResult = [SELECT count() FROM SalesConnect__NewTransaction__c WHERE Fund_Vehicle_Type__c = 'ETF' AND CreatedDate = Today AND SalesConnect__Source_Description__c = 'Merrill ETF Trades'];
                    System.debug(SCTranCountAggResult );
                    System.assertNotEquals(0, SCTranCountAggResult);
                    System.debug('Executing Test Case 1 for row count: '+SCTranCountAggResult );
                    if (SCTranCountAggResult > 0 ) {
                        Messaging.SingleEmailMessage Testemail;
                        List<Messaging.SendEmailresult> Testemailresults;
                        Testemail = new Messaging.SingleEmailMessage();
                        Testemail.setTemplateId(lstEmailTemplates[0].Id);
                        Testemail.setToAddresses(new String[] {'svenkateswaran@vaneck.com'});
                        Testemail.setHtmlBody(lstEmailTemplates[0].HTMLValue);
                        mail.setToAddresses(new String[] {'svenkateswaran@vaneck.com','Help.Sales@vaneck.com'});
                        mail.setSubject('MS ETF Transaction Data Load Alert');
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { Testemail });
        
               }
              else{
                   SendEmailNotification se2 = new SendEmailNotification('No ML ETF Data is loaded today and the Count in Transaction is: ' + SCTranCountAggResult );
                   msg = 'ML ETF Data is loaded and the Count in Transaction is: \n ' + ' ' +SCTranCountAggResult;
                   se2.sendEmail(msg);
               }
                    
         
                } 
          else{
                 System.debug('Email Template Developer id in Test Case 3 is -in ELSE** : '+lstEmailTemplates[0].developername);
                 //SendEmailNotification see = new SendEmailNotification('Test - unable to get the template with developername - DST_ETF_DataLoadAlert');
                 //String msg = 'Please recreate a html Email Template without letterhead with developername as DST_ETF_DataLoadAlert as soon as possible to avoid this error';
                 //see.sendEmail(msg); 
                }
              
              
           }catch(Exception e){  SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Counting ML ETF in SC Transaction Object Failed');
                string msg3 = 'Exception Occurred in Apex for Counting ML ETF in SC Transaction Object for today Failed \n '+e.getMessage() + ' ' +SCTranCountAggResult;
                se.sendEmail(msg3);
                }
                
         
    Test.stopTest();
    }
  }