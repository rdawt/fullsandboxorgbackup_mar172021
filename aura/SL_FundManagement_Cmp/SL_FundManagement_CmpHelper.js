({
	doInit : function(component, event, helper) {
		helper.getContactInformation(component, event);
		
	},
	
	getUserLocation : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var action = component.get("c.getUserLocation");
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = JSON.parse(response.getReturnValue());
           
           if (component.isValid() && state === "SUCCESS") {
        	   component.set("v.userLocation", returnValue);
        	   component.set("v.showSpinner", false);	
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getContactInformation : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var recordId = component.get("v.recordId");
		var action = component.get("c.getContactInformation");
		action.setParams({
            "contactRecordId" : recordId
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") {
        	   component.set("v.contactName", returnValue);
        	   component.set("v.showSpinner", false);	
        	   self.getUserFocusFundRecords(component, event);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getUserFocusFundRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var contactId = component.get("v.recordId");
		var self = this;
		
		var action = component.get("c.getUserFocusFundRecords");
		action.setParams({
            "strContactId" : contactId
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           console.log('??????????????User Focus Funds returnValue?????????????' + returnValue);
           if (component.isValid() && state === "SUCCESS") {
        	   component.set("v.focusFundRecords", returnValue);
        	   component.set("v.focusFundRecordsLastSearch", returnValue);
        	   
        	   if(returnValue == null || returnValue == 'null')
        		   component.set("v.noFocusRecordsFound", true); 
        	   else
        		   component.set("v.noFocusRecordsFound", false);
        		   
        	   component.set("v.showSpinner", false);
        	   self.getUserDefaultFundRecords(component, event);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getUserDefaultFundRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var contactId = component.get("v.recordId");
		var self = this;
		
		var action = component.get("c.getUserDefaultFundRecords");
		action.setParams({
            "strContactId" : contactId
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           console.log('??????????????User default returnValue?????????????' + returnValue);
           if (component.isValid() && state === "SUCCESS") {
        	   component.set("v.defaultFundRecords", returnValue);
        	   component.set("v.defaultFundRecordsLastSearch", returnValue);
        	   
        	   if(returnValue == null || returnValue == 'null')
        		   component.set("v.noDefaultRecordsFound", true); 
        	   else
        		   component.set("v.noDefaultRecordsFound", false);
        		   
        	   component.set("v.showSpinner", false);
        	   self.getUserOtherFundRecords(component, event);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getUserOtherFundRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var recordId = component.get("v.recordId");
		var currentDefaultRecords = component.get("v.defaultFundRecords");
		
		var action = component.get("c.getUserOtherFundRecords");
		action.setParams({
            "lstCurrentDefaultFunds" : currentDefaultRecords,
            "strContactId" : recordId
        });
        
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") {
        	   component.set("v.otherFundRecords", returnValue);
        	   component.set("v.otherFundRecordsLastSearch", returnValue);
        	   
        	   if(returnValue == null || returnValue == 'null')
        		   component.set("v.noOtherRecordsFound", true); 
        	   else
        		   component.set("v.noOtherRecordsFound", false);
        	   
        	   component.set("v.showSpinner", false);
        	   
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getFocusSearchRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		
		var self = this;
		var fundName = component.get("v.defaultFundNameSearch");
		var fundType = component.get("v.defaultFundTypeSearch");
		var contactId = component.get("v.recordId");
		var oldValues = component.get("v.focusFundRecordsLastSearch");
		
		var action = component.get("c.getFocusSearchRecords");
		action.setParams({
            "strFundNameSearch" : fundName,
            "strFundTypeSearch" : fundType,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.focusFundRecords", returnValue);
        	   
        	   if(returnValue == null)
        		   component.set("v.noFocusRecordsFound", true); 
        	   else
        		   component.set("v.noFocusRecordsFound", false); 
        	
        	   component.set("v.showSpinner", false);
        	   
        	   self.getDefaultSearchRecords(component, event, helper);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getDefaultSearchRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var fundName = component.get("v.defaultFundNameSearch");
		var fundType = component.get("v.defaultFundTypeSearch");
		var contactId = component.get("v.recordId");
		var oldValues = component.get("v.defaultFundRecordsLastSearch");
		
		var action = component.get("c.getDefaultSearchRecords");
		action.setParams({
            "strFundNameSearch" : fundName,
            "strFundTypeSearch" : fundType,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.defaultFundRecords", returnValue);
        	   
        	   if(returnValue == null)
        		   component.set("v.noDefaultRecordsFound", true); 
        	   else
        		   component.set("v.noDefaultRecordsFound", false); 
        	
        	   component.set("v.showSpinner", false);
        	   
        	   self.getOtherSearchRecords(component, event, helper);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getOtherSearchRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var contactId = component.get("v.recordId");
		var fundName = component.get("v.defaultFundNameSearch");
		var fundType = component.get("v.defaultFundTypeSearch");
		
		var oldValues = component.get("v.otherFundRecordsLastSearch");
		
		var action = component.get("c.getOtherSearchRecords");
		action.setParams({
            "strFundNameSearch" : fundName,
            "strFundTypeSearch" : fundType,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.otherFundRecords", returnValue);
        	   
        	   if(returnValue == null)
        		   component.set("v.noOtherRecordsFound", true); 
        	   else
        		   component.set("v.noOtherRecordsFound", false); 
        	
        	   component.set("v.showSpinner", false);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	
	getMobileSearchRecords : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var mobileFundName = component.get("v.mobileFundNameSearch");
		var mobileFundType = component.get("v.mobileFundTypeSearch");
		var contactId = component.get("v.recordId");
		var mobileSearchButton = 'Search Focus Funds';
		var oldValues = component.get("v.focusFundRecordsLastSearch");
		
		var action = component.get("c.getMobileSearchRecords");
		action.setParams({
            "strMobileFundNameSearch" : mobileFundName,
            "strMobileFundTypeSearch" : mobileFundType,
            "mobileSearchButton" : mobileSearchButton,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
    		   component.set("v.focusFundRecords", returnValue);
    		   
    		   if(returnValue == null || returnValue == 'null')
    			   component.set("v.noFocusRecordsFound", true); 
    		   else
    			   component.set("v.noFocusRecordsFound", false);
        	
        	   component.set("v.showSpinner", false);
        	   
        	   self.getMobileSearchRecordsCurrent(component,event,helper);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getMobileSearchRecordsCurrent : function(component,event, helper)
	{
		component.set("v.showSpinner", true);
		var self = this;
		var mobileFundName = component.get("v.mobileFundNameSearch");
		var mobileFundType = component.get("v.mobileFundTypeSearch");
		var contactId = component.get("v.recordId");
		var mobileSearchButton = 'Search Current Funds';
		var oldValues = component.get("v.defaultFundRecordsLastSearch");
		
		var action = component.get("c.getMobileSearchRecords");
		action.setParams({
            "strMobileFundNameSearch" : mobileFundName,
            "strMobileFundTypeSearch" : mobileFundType,
            "mobileSearchButton" : mobileSearchButton,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
    		   component.set("v.defaultFundRecords", returnValue);
    		   
    		   if(returnValue == null || returnValue == 'null')
    			   component.set("v.noDefaultRecordsFound", true); 
    		   else
    			   component.set("v.noDefaultRecordsFound", false);
        	
        	   component.set("v.showSpinner", false);
        	   
        	   self.getMobileSearchRecordsOthers(component,event,helper);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	getMobileSearchRecordsOthers : function(component,event, helper)
	{
		console.log('?????????????????I came in Other mobile search ??????????????????');
		component.set("v.showSpinner", true);
		var self = this;
		var mobileFundName = component.get("v.mobileFundNameSearch");
		var mobileFundType = component.get("v.mobileFundTypeSearch");
		var contactId = component.get("v.recordId");
		var mobileSearchButton = 'Search Other Funds';
		var oldValues = component.get("v.otherFundRecordsLastSearch");
		
		var action = component.get("c.getMobileSearchRecords");
		action.setParams({
            "strMobileFundNameSearch" : mobileFundName,
            "strMobileFundTypeSearch" : mobileFundType,
            "mobileSearchButton" : mobileSearchButton,
            "strContactId" : contactId,
            "lstOldValues" : oldValues
        });
		
		action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           
           if (component.isValid() && state === "SUCCESS") 
           {
    		   component.set("v.otherFundRecords", returnValue);
    		   
    		   if(returnValue == null || returnValue == 'null')
    			   component.set("v.noOtherRecordsFound", true); 
    		   else
    			   component.set("v.noOtherRecordsFound", false);
        	
        	   component.set("v.showSpinner", false);
           }
    	});
    	$A.enqueueAction(action); 
	},
	
	storeFocusOldInformation : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	var self = this;
        var currentFundsRecords = component.get("v.focusFundRecords");
        var firstFundsRecords = component.get("v.focusFundRecordsLastSearch");
        
        var action = component.get("c.setNewValuesTOldList");
        action.setParams({
            "lstOldValues"		: firstFundsRecords,
            "lstCurrentValues"	: currentFundsRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.focusFundRecordsLastSearch", returnValue);
        	   component.set("v.showSpinner", false);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    storeDefaultOldInformation : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	
    	var self = this;
        var currentFundsRecords = component.get("v.defaultFundRecords");
        var firstFundsRecords = component.get("v.defaultFundRecordsLastSearch");
        
        var action = component.get("c.setNewValuesTOldList");
        action.setParams({
            "lstOldValues"		: firstFundsRecords,
            "lstCurrentValues"	: currentFundsRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.defaultFundRecordsLastSearch", returnValue);
        	   component.set("v.showSpinner", false);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    storeOtherOldInformation : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	
    	var self = this;
        var currentFundsRecords = component.get("v.otherFundRecords");
        var firstFundsRecords = component.get("v.otherFundRecordsLastSearch");
        
        var action = component.get("c.setNewValuesTOldList");
        action.setParams({
            "lstOldValues"		: firstFundsRecords,
            "lstCurrentValues"	: currentFundsRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.otherFundRecordsLastSearch", returnValue);
        	   component.set("v.showSpinner", false);
           }
    	});
    	$A.enqueueAction(action); 
    },
	
	handleFocusSingleSelect : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	
    	var self = this;
        var focusFundsAllRecords = component.get("v.focusFundRecords");
        var fundId = event.getSource().get("v.label");
        
        var action = component.get("c.setSelectSingleForFunds");
        action.setParams({
            "lstCurrentAllFunds"	: focusFundsAllRecords,
            "paramStrFundId"			: fundId
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.focusFundRecords", returnValue);
        	   component.set("v.showError", false);
        	   component.set("v.errorMessage",'');
        	   component.set("v.showSpinner", false);
        	   self.anyCheckBoxSelectedFocus(component, event);
           }
    	});
    	$A.enqueueAction(action); 
    },
	
    handleDefaultSingleSelect : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	
    	var self = this;
        var currentAllRecords = component.get("v.defaultFundRecords");
        var fundId = event.getSource().get("v.label");
        
        var action = component.get("c.setSelectSingleForFunds");
        action.setParams({
            "lstCurrentAllFunds"	: currentAllRecords,
            "paramStrFundId"			: fundId
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.defaultFundRecords", returnValue);
        	   component.set("v.showError", false);
        	   component.set("v.errorMessage",'');
        	   component.set("v.showSpinner", false);
        	   self.anyCheckBoxSelectedDefault(component, event);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    handleOtherSingleSelect : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	
    	var self = this;
        var currentAllRecords = component.get("v.otherFundRecords");
        var fundId = event.getSource().get("v.label");
        
        var action = component.get("c.setSelectSingleForFunds");
        action.setParams({
            "lstCurrentAllFunds"	: currentAllRecords,
            "paramStrFundId"			: fundId
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.otherFundRecords", returnValue);
        	   component.set("v.showError", false);
        	   component.set("v.errorMessage",'');
        	   component.set("v.showSpinner", false);
        	   self.anyCheckBoxSelectedOther(component, event);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    anyCheckBoxSelectedFocus : function(component,event){
    	
    	var self = this;
    	component.set("v.showSpinner", true);
        var currentAllRecords = component.get("v.focusFundRecords");
        
        var action = component.get("c.isAnyCheckBoxSelected");
        action.setParams({
            "lstCurrentAllFunds"	: currentAllRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.atleastOnePresent", returnValue);
        	   component.set("v.showSpinner", false);
        	   
        	   self.storeFocusOldInformation(component,event);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    anyCheckBoxSelectedDefault : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	var self = this;
        var currentAllRecords = component.get("v.defaultFundRecords");
        var action = component.get("c.isAnyCheckBoxSelected");
        action.setParams({
            "lstCurrentAllFunds"	: currentAllRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.atleastOnePresent", returnValue);
        	   component.set("v.showSpinner", false);
        	   
        	   self.storeDefaultOldInformation(component,event);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    anyCheckBoxSelectedOther : function(component,event){
    	
    	component.set("v.showSpinner", true);
    	var self = this;
        var currentAllRecords = component.get("v.otherFundRecords");
        
        var action = component.get("c.isAnyCheckBoxSelected");
        action.setParams({
            "lstCurrentAllFunds"	: currentAllRecords
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.atleastOnePresent", returnValue);
        	   component.set("v.showSpinner", false);
        	   
        	   self.storeOtherOldInformation(component,event);
           }
    	});
    	$A.enqueueAction(action); 
    },
    
    handleClearHideClassOtherSingleSelect : function(component,event)
    {
    	component.set("v.showSpinner", true);
        var currentAllRecords = component.get("v.otherFundRecords");
        var fundId = event.getSource().get("v.value");
        
        var action = component.get("c.clearHideClassForSelectSingleForOtherFunds");
        action.setParams({
            "lstCurrentAllOtherFunds"	: currentAllRecords,
            "paramStrFundId"			: fundId
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.otherFundRecords", returnValue);
        	   component.set("v.showSpinner", false);	
           }
    	});
    	$A.enqueueAction(action); 
    },
	
	callCancelButton : function(component,event){
        var recordId = component.get("v.recordId");
        
        if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
        {
            var result = '/lightning/r/Contact/'+recordId+'/view';
            sforce.one.navigateToURL(result);
        }
        else{
            var result = '/'+recordId;
            window.open(result,'_top');
        }
    },
    
    saveFundsRecords : function(component,event, helper){
    
    	component.set("v.showSpinner", true);
    	var self = this;
        var recordId = component.get("v.recordId");
        
        var focusFundsRecords = component.get("v.focusFundRecords");
        var currentDefaultRecords = component.get("v.defaultFundRecords");
        var currentOtherRecords = component.get("v.otherFundRecords");
        
        var action = component.get("c.saveFundsRecords");
        action.setParams({
        	"lstFocusFundsFunds"		: focusFundsRecords,
            "lstCurrentDefaultFunds"	: currentDefaultRecords,
            "lstCurrentOtherFunds"		: currentOtherRecords,
            "strContactId"				: recordId
        }); 
        
        action.setCallback(this, function(response) {
           var state = response.getState();
           var returnValue = response.getReturnValue();
           if (component.isValid() && state === "SUCCESS") 
           {
        	   component.set("v.showSpinner", false);	
        	   component.set("v.atleastOnePresent", true);
        	   //self.doInit(component,event, helper);
        	   
		        if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
		        {
		            var result = '/lightning/r/Contact/'+recordId+'/view';
		            sforce.one.navigateToURL(result);
		        }
		        else{
		            var result = '/'+recordId;
		            window.open(result,'_top');
		        }
        	   
           }
           else if(state === "ERROR")
           {
        	   var errors = response.getError();
        	   component.set("v.showSpinner", false);	
        	   component.set("v.showError", true);
        	   component.set("v.errorMessage",errors[0].message);

           }
    	});
    	$A.enqueueAction(action); 
    },
    
	
})