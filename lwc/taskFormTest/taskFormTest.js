import { LightningElement } from 'lwc';
import TASK_OBJECT from '@salesforce/schema/Task';
import NAME_FIELD from '@salesforce/schema/Task.WhoId';
import RELATED_TO_FIELD from '@salesforce/schema/Task.WhatId';

export default class TaskFormTest extends LightningElement {
    taskObject = TASK_OBJECT;
    nameField = NAME_FIELD;
    relatedToField = RELATED_TO_FIELD;

    handleTaskCreated(){
        // Run code when account is created.

        
    }

}