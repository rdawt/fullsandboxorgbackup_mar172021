global class SL_Batch_MoveMarketoJSONActivityToTask implements Database.Batchable<sObject>, Database.Stateful
{
    global Set<String> setContactIdsNotPresent;
	global Set<String> setAccountIdsNotPresent;
	global Set<String> setOwnerIdsNotPresent;
	global List<Marketo_JSON_Activity__c> lstMarketo_JSON_Activity;
	global List<Task> lstTask;
	
	global SL_Batch_MoveMarketoJSONActivityToTask()
	{
		setContactIdsNotPresent = new Set<String>();
		setAccountIdsNotPresent = new Set<String>();
		setOwnerIdsNotPresent = new Set<String>();
		lstMarketo_JSON_Activity = new List<Marketo_JSON_Activity__c>();
		lstTask = new List<Task>();
	}
			
    global Database.querylocator start(Database.BatchableContext BC){
        String strQuery = 'SELECT Id, Activity_Date__c, Activity_Owner__c, Email__c, Processing_Source__c, Subject__c ,' + 
        					' Summary_Recap__c, WhatId__c, WhoId__c ' + 
        					' from Marketo_JSON_Activity__c';
            return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<Marketo_JSON_Activity__c> lstMarketo_JSON_ActivityProcessing)
    {
    	system.debug('>>>>>>>>>>>>>>>lstMarketo_JSON_ActivityProcessing>>>>>>>>>>>>>>>' + lstMarketo_JSON_ActivityProcessing);
    	system.debug('>>>>>>>>>>>>>>>lstMarketo_JSON_ActivityProcessing>>>>>>>>>>>>>>>' + lstMarketo_JSON_ActivityProcessing.size());
    	Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        SendEmailNotification se = new SendEmailNotification('Error: SL_Batch_MoveMarketoJSONActivityToTask Batch Exception');
        List<Task> lstTaskInsert = new List<Task>();
    	try
    	{
	    	if(!lstMarketo_JSON_ActivityProcessing.isEmpty())
	    	{
		        Set<String> setContactIds = new Set<String>();
				Set<String> setAccountIds = new Set<String>();
				Set<String> setOwnerIds = new Set<String>();
				
				for(Marketo_JSON_Activity__c objRecord : lstMarketo_JSON_ActivityProcessing)
				{
					if(objRecord.WhatId__c != NULL && objRecord.WhatId__c != '')
						setAccountIds.add(objRecord.WhatId__c);
					
					if(objRecord.WhoId__c != NULL && objRecord.WhoId__c != '')
						setContactIds.add(objRecord.WhoId__c);
					
					if(objRecord.Activity_Owner__c != NULL && objRecord.Activity_Owner__c != '')
						setOwnerIds.add(objRecord.Activity_Owner__c);		
				}
				
				Map<String, Id> mapAccount = new Map<String, Id>();
				Map<String, Id> mapContact = new Map<String, Id>();
				Map<Id, Boolean> mapUser = new Map<Id, Boolean>();
				
				if(!setAccountIds.isEmpty())
				{
					for(Account objAccount : [Select Id from Account where Id IN: setAccountIds])
					{
						mapAccount.put(objAccount.Id, objAccount.Id);
					}
				}
				
				if(!setContactIds.isEmpty())
				{
					for(Contact objContact : [Select Id from Contact where Id IN: setContactIds])
					{
						mapContact.put(objContact.Id, objContact.Id);
					}
				}
				
				if(!setOwnerIds.isEmpty())
				{
					for(User objUser : [Select Id , isActive from User where Id IN: setOwnerIds and isActive = true])
					{
						mapUser.put(objUser.Id, objUser.isActive);
					}
				}
					
				for(Marketo_JSON_Activity__c objRecord : lstMarketo_JSON_ActivityProcessing)
				{
				    if(objRecord != NULL)
				    {
				        Task objTask = new Task();
				        objTask.Email__c = objRecord.Email__c;
				        if(objRecord.Activity_Date__c != NULL)
				        	objTask.ActivityDate = Date.valueOf(objRecord.Activity_Date__c);
				        else
				        	objTask.ActivityDate = Date.today();
				        
				        if(mapContact.containskey(objRecord.WhoId__c))		
				        	objTask.WhoId = objRecord.WhoId__c;
				        else
				        	setContactIdsNotPresent.add(objRecord.WhoId__c);	
				        
				        if(mapAccount.containskey(objRecord.WhatId__c))
				        	objTask.WhatId = objRecord.WhatId__c;
				        else
				        	setAccountIdsNotPresent.add(objRecord.WhatId__c);	
				        
				        if(mapUser.containskey(objRecord.Activity_Owner__c))
				        	objTask.OwnerId = objRecord.Activity_Owner__c;
				        else
				        {
				        	setOwnerIdsNotPresent.add(objRecord.Activity_Owner__c);
				        	objTask.OwnerId = system.UserInfo.getUserId();	
				        }
				        
			        	objTask.Subject = objRecord.Subject__c;
			        	objTask.Summary_Recap__c = objRecord.Summary_Recap__c;
				        objTask.Processing_Source__c = objRecord.Processing_Source__c;
				        	
				        lstTaskInsert.add(objTask);
				    }
				}
				
				system.debug('>>>>>>>>>>>lstMarketo_JSON_ActivityProcessing Size>>>>>>>' + lstMarketo_JSON_ActivityProcessing.size());
				system.debug('>>>>>>>>>>>lstMarketo_JSON_ActivityProcessing Size>>>>>>>' + lstMarketo_JSON_ActivityProcessing);
				
				system.debug('>>>>>>>>>>>lstTaskInsert Size>>>>>>>' + lstTaskInsert.size());
				system.debug('>>>>>>>>>>>lstTaskInsert>>>>>>>' + lstTaskInsert);
				
				system.debug('>>>>>>>>>>>lstTask Size>>>>>>>' + lstTask.size());
				system.debug('>>>>>>>>>>>lstMarketo_JSON_Activity Size>>>>>>>' + lstMarketo_JSON_Activity.size());
				
				if(!lstTaskInsert.isEmpty())
				{
					SL_MarketoRecords_Service.isProcssingFromJSON = true;
					insert lstTaskInsert;
					
					lstTask.addAll(lstTaskInsert);
					lstMarketo_JSON_Activity.addAll(lstMarketo_JSON_ActivityProcessing);
				}
	    	}
    	}
    	catch(Exception e)
    	{
            String strErrorBody = '';
            strErrorBody = 'Exception Line Number : ' + e.getLineNumber() + ' \n ' + 
                            'Exception Type Name : ' + e.getTypeName() +  ' \n ' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + ' \n ' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
    	}
    }
    
    global static Marketo_JSON__c createMarketoJSONRecord(Set<String> setAccountIdsNotPresent, Set<String> setContactIdsNotPresent, 
															Set<String> setOwnerIdsNotPresent, 
															Integer intTotalFoundCount, Integer intTotalProcessedCount,
															staticresource sr)
	{
		List<String> lstContactIdsNotPresent = new List<String>();
		List<String> lstAccountIdsNotPresent = new List<String>();
		List<String> lstOwnerIdsNotPresent = new List<String>();
		
		if(!setAccountIdsNotPresent.isEMpty())
			lstAccountIdsNotPresent.addAll(setAccountIdsNotPresent);
			
		if(!setContactIdsNotPresent.isEMpty())
			lstContactIdsNotPresent.addAll(setContactIdsNotPresent);
		
		if(!setOwnerIdsNotPresent.isEMpty())
			lstOwnerIdsNotPresent.addAll(setOwnerIdsNotPresent);		
		
		Marketo_JSON__c objMarketo_JSON_Insert = new Marketo_JSON__c();
		objMarketo_JSON_Insert.Activity_Date__c = Date.today();
		objMarketo_JSON_Insert.Processed_Datetime__c = Datetime.now();
		objMarketo_JSON_Insert.isActive__c = false;
		objMarketo_JSON_Insert.Ids_Not_Processed_Account__c = String.join(lstAccountIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Ids_Not_Processed_Contact__c = String.join(lstContactIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Ids_Not_Processed_InActive_Users__c = String.join(lstOwnerIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Total_Count_of_Records_Found__c = intTotalFoundCount;
		objMarketo_JSON_Insert.Total_Count_of_Records_Processed__c = intTotalProcessedCount;
		if(sr.body != NULL)
		{
			objMarketo_JSON_Insert.JSON__c = sr.body.toString().substring(0, 50000);
		}
		
		return objMarketo_JSON_Insert;
		
	}
	
	global static StaticResource getStaticResource()
	{
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'MarketoJSONData' LIMIT 1];
		return sr;
		
	}
    
    global void finish(Database.BatchableContext BC)
    {
    	system.debug('>>>>>>>>>>>>>>I am Finish Method>>>>>>>>>>>>>>>>' + lstMarketo_JSON_Activity);
    	system.debug('>>>>>>>>>>>>>>I am Finish Method>>>>>>>>>>>>>>>>' + lstTask);
    	system.debug('>>>>>>>>>>>>>>I am Finish Method>>>>>>>>>>>>>>>>' + setAccountIdsNotPresent);
    	system.debug('>>>>>>>>>>>>>>I am Finish Method>>>>>>>>>>>>>>>>' + setContactIdsNotPresent);
    	system.debug('>>>>>>>>>>>>>>I am Finish Method>>>>>>>>>>>>>>>>' + setOwnerIdsNotPresent);
    	
    	StaticResource sr = getStaticResource();
    	Marketo_JSON__c objMarketo_JSON_Insert = new Marketo_JSON__c();
		objMarketo_JSON_Insert = createMarketoJSONRecord(setAccountIdsNotPresent, setContactIdsNotPresent, 
															setOwnerIdsNotPresent, 
															lstMarketo_JSON_Activity.size(), lstTask.size(), sr);
		insert objMarketo_JSON_Insert;	
		
		delete lstMarketo_JSON_Activity;
		
		// Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	      TotalJobItems, CreatedBy.Email
	      FROM AsyncApexJob WHERE Id =
	      :BC.getJobId()];
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   String[] toAddresses = new String[] {'rehan.dawt@silverlinecrm.com', 'svenkateswaran@vaneck.com'};
	   mail.setToAddresses(toAddresses);
	   mail.setSubject('Apex SL_Batch_MoveMarketoJSONActivityToTask ' + a.Status);
	   mail.setPlainTextBody
	   ('The batch Apex job processed ' + a.TotalJobItems +
	   ' batches with '+ a.NumberOfErrors + ' failures.');
	   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });												
																
    }    
}