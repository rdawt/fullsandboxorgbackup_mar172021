({
    handleInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.getFunds(component, event, helper);
    },
    handleChange : function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.getChannels(component, event, helper);
    },
    
    handleCalculate : function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.calculateAll(component, event, helper); 
    }
})