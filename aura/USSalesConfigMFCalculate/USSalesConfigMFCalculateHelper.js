({
    getFunds : function(component, event, helper) {
        var action = component.get('c.getFundNames');
        action.setParams({
            mutualFundOnly :  true
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.fundOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    getChannels : function(component, event, helper) {
        var action = component.get('c.getChannelNames');
        action.setParams({
            fundName :  component.get('v.selectedFund')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.channelOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    getTerritory : function(component, event, helper) {
        var action = component.get('c.getTerritoryNames');
        action.setParams({
            fundName :  component.get('v.selectedFund'),
            channelName : component.get('v.selectedChannel')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var options = response.getReturnValue();
                component.set("v.territoryOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    calculateNetSales : function(component, event, helper) {
        if ($A.util.isEmpty(component.get('v.selectedFund')) || $A.util.isEmpty(component.get('v.selectedChannel')) || $A.util.isEmpty(component.get('v.selectedTerritory')) || $A.util.isEmpty(component.find("fromDate").get("v.value"))) {
            helper.showToast('Error', 'Please complete all required fields','error');
            component.set("v.showSpinner",false);
            return;
        }
        var dataSourceList = component.get('v.selectedDataSources');
        if(dataSourceList.length == 0){
            helper.showToast('Error', 'Please select atleast one data source.','error');
            component.set("v.showSpinner",false);
            return;
        }
        var dateFrom = component.find("fromDate").get("v.value");
        var dateTo = component.find("toDate").get("v.value");
        var action = component.get('c.getCalculatedMFNetSales');
        action.setParams({
            fundName :  component.get('v.selectedFund'),
            channelName :  component.get('v.selectedChannel'),
            territoryName :  component.get('v.selectedTerritory'),
            dateFrom :  dateFrom,
            dateTo : dateTo,
            fromDST : dataSourceList.includes('DST'),
            fromCO : dataSourceList.includes('CO')
		  });
        debugger;
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var netSalesAmountDST = 0.00;
                var netSalesAmountCO = 0.00;
                console.log(response.getReturnValue());
                if(!$A.util.isEmpty(response.getReturnValue())){
                    var result = response.getReturnValue();
                    if(dataSourceList.includes('CO')){
                        var co = result.CO;
                        if(!$A.util.isEmpty(co.Current_Net_Sales_for_ETF_Source__c)){
                            netSalesAmountCO = co.Current_Net_Sales_for_ETF_Source__c;
                        }
                    }
                    if(dataSourceList.includes('DST')){
                        var dst = result.DST;
                        if(!$A.util.isEmpty(dst.Current_Net_Sales_for_ETF_Source__c)){
                            netSalesAmountDST = dst.Current_Net_Sales_for_ETF_Source__c;
                        }
                    }
                    component.set('v.coSection',dataSourceList.includes('CO'));
                    component.set('v.dstSection',dataSourceList.includes('DST'));
                    component.set('v.netSalesAmountDST',netSalesAmountDST);
                    component.set('v.netSalesAmountCO',netSalesAmountCO);
                }
               
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while processing.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);

    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
})