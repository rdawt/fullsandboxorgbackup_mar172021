public with sharing class PortfolioChannelRollupController {

    @AuraEnabled
    public static List<picklistOptions> getFundTickers(String strFundVehicleType,Boolean boolExcludeProcessed,String strYear,Integer intQuarter,Id idChannel){
        List<picklistOptions> lstFunds = new List<picklistOptions>();
        Set<Id> setExistingFunds = getProcessedFunds(strFundVehicleType,boolExcludeProcessed,strYear,intQuarter,idChannel);
        for(Fund__c fund: [SELECT Id,Fund_Ticker_Symbol__c FROM Fund__c WHERE Fund_Vehicle_Type__c = :strFundVehicleType and Fund_Status__c = 'Open' AND BU_Region__c='USA'])
        {
            if(!setExistingFunds.contains(fund.Id)){
                lstFunds.add(new picklistOptions(fund.Fund_Ticker_Symbol__c,fund.Id));
            }
        }
        return lstFunds; 
    }

    private static Set<Id> getProcessedFunds(String strFundVehicleType,Boolean boolExcludeProcessed,String strYear,Integer intQuarter,Id idChannel){
        Set<Id> setFunds = new Set<Id>();
        if(!boolExcludeProcessed){
            return setFunds;
        }else{
            List<Portfolio_Channel_Rollup_Quarterly__c> lstRecords;
            if(intQuarter == 1){
                lstRecords = [SELECT Id,Fund__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Channel__c = :idChannel AND 
                Fund_Vehicle_Type__c = :strFundVehicleType AND Year__c = :strYear AND (Q1_AUM_BR__c != null OR Q1_AUM__c != null 
                OR Q1_NetSales_BR__c != null OR Q1_NetSales_Last_Month_BR__c != null OR Q1_NetSales_Last_Month__c != null OR Q1_NetSales__c != null)];
            }else if(intQuarter == 2){
                lstRecords = [SELECT Id,Fund__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Channel__c = :idChannel AND 
                Fund_Vehicle_Type__c = :strFundVehicleType AND Year__c = :strYear AND (Q2_AUM_BR__c != null OR Q2_AUM__c != null 
                OR Q2_NetSales_BR__c != null OR Q2_NetSales_Last_Month_BR__c != null OR Q2_NetSales_Last_Month__c != null OR Q2_NetSales__c != null)];
            }else if(intQuarter == 3){
                lstRecords = [SELECT Id,Fund__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Channel__c = :idChannel 
                AND Fund_Vehicle_Type__c = :strFundVehicleType AND Year__c = :strYear AND (Q3_AUM_BR__c != null OR Q3_AUM__c != null OR Q3_NetSales_BR__c != null 
                OR Q3_NetSales_Last_Month_BR__c != null OR Q3_NetSales_Last_Month__c != null OR Q3_NetSales__c != null)];
            }else if(intQuarter == 4){
                lstRecords = [SELECT Id,Fund__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Channel__c = :idChannel AND 
                Fund_Vehicle_Type__c = :strFundVehicleType AND Year__c = :strYear AND (Q4_AUM_BR__c != null OR Q4_AUM__c != null 
                OR Q4_NetSales_BR__c != null OR Q4_NetSales_Last_Month_BR__c != null OR Q4_NetSales_Last_Month__c != null OR Q4_NetSales__c != null)];
            }
            for(Portfolio_Channel_Rollup_Quarterly__c record : lstRecords){
                setFunds.add(record.Fund__c);
            }
        }
        return setFunds;
    }

    @AuraEnabled
    public static List<picklistOptions> getChannels(){
        List<picklistOptions> lstChannels = new List<picklistOptions>();
        for(Channel__c channel: [SELECT Id,Channel_Description__c FROM Channel__c WHERE Status__c = 'Active'])
        {
            lstChannels.add(new picklistOptions(channel.Channel_Description__c,channel.Id));
        }
        return lstChannels; 
    }

    @AuraEnabled
    public static Boolean calculatePortfolioChannelRollup(List<Id> lstPortfolio,Id idChannel,String strYear,Integer intQuarter,String strVehicleType)
    {
        try{
            PortfolioChannelRollupService service = new PortfolioChannelRollupService();
            return service.upsertRollupRecords(lstPortfolio,idChannel,strYear,intQuarter,strVehicleType); 
        }catch(Exception ex){
            System.debug('Error '+ex);
            return false;
        }
    }

    @AuraEnabled
    public static List<ChannelLevelDetails> getCalculatedDataFundWise(Id idChannel,String strYear,Integer intQuarter,String strVehicleType)
    {
        List<Portfolio_Channel_Rollup_Quarterly__c> lstResults = new List<Portfolio_Channel_Rollup_Quarterly__c> ();
        List<ChannelLevelDetails> lstWrappers = new List<ChannelLevelDetails>();
        if(strVehicleType == 'ETF'){
            if(intQuarter == 1){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q1_AUM_BR__c,Q1_AUM__c,Q1_NetSales_BR__c,Q1_NetSales_Last_Month_BR__c,Q1_NetSales_Last_Month__c,Q1_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND (Q1_AUM_BR__c != null OR Q1_AUM__c != null OR Q1_NetSales_BR__c != null OR Q1_NetSales_Last_Month_BR__c != null OR Q1_NetSales_Last_Month__c != null OR Q1_NetSales__c  != null)];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q1_AUM_BR__c,result.Q1_AUM__c,result.Q1_NetSales_BR__c,result.Q1_NetSales__c,result.Q1_NetSales_Last_Month_BR__c,result.Q1_NetSales_Last_Month__c));
                }
            }else if(intQuarter == 2){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q2_AUM_BR__c,Q2_AUM__c,Q2_NetSales_BR__c,Q2_NetSales_Last_Month_BR__c,Q2_NetSales_Last_Month__c,Q2_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND (Q2_AUM_BR__c != null OR Q2_AUM__c != null OR Q2_NetSales_BR__c != null OR Q2_NetSales_Last_Month_BR__c != null OR Q2_NetSales_Last_Month__c != null OR Q2_NetSales__c  != null)];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q2_AUM_BR__c,result.Q2_AUM__c,result.Q2_NetSales_BR__c,result.Q2_NetSales__c,result.Q2_NetSales_Last_Month_BR__c,result.Q2_NetSales_Last_Month__c));
                }
            }else if(intQuarter == 3){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q3_AUM_BR__c,Q3_AUM__c,Q3_NetSales_BR__c,Q3_NetSales_Last_Month_BR__c,Q3_NetSales_Last_Month__c,Q3_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND (Q3_AUM_BR__c != null OR Q3_AUM__c != null OR Q3_NetSales_BR__c != null OR Q3_NetSales_Last_Month_BR__c != null OR Q3_NetSales_Last_Month__c != null OR Q3_NetSales__c  != null)];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q3_AUM_BR__c,result.Q3_AUM__c,result.Q3_NetSales_BR__c,result.Q3_NetSales__c,result.Q3_NetSales_Last_Month_BR__c,result.Q3_NetSales_Last_Month__c));
                }
            }else if(intQuarter == 4){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q4_AUM_BR__c,Q4_AUM__c,Q4_NetSales_BR__c,Q4_NetSales_Last_Month_BR__c,Q4_NetSales_Last_Month__c,Q4_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND (Q4_AUM_BR__c != null OR Q4_AUM__c != null OR Q4_NetSales_BR__c != null OR Q4_NetSales_Last_Month_BR__c != null OR Q4_NetSales_Last_Month__c != null OR Q4_NetSales__c  != null)];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q4_AUM_BR__c,result.Q4_AUM__c,result.Q4_NetSales_BR__c,result.Q4_NetSales__c,result.Q4_NetSales_Last_Month_BR__c,result.Q4_NetSales_Last_Month__c));
                }
            }
            
        }else{
            if(intQuarter == 1){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q1_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND Q1_NetSales__c != null];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q1_NetSales__c));
                }
            }else if(intQuarter == 2){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q2_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND Q2_NetSales__c != null];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q2_NetSales__c));
                }
            }else if(intQuarter == 3){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q3_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND Q3_NetSales__c != null];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q3_NetSales__c));
                }
            }else if(intQuarter == 4){
                lstResults = [SELECT Id,Channel__c,Channel_Name_Drived__c,Fund__c,Fund__r.Name,Name,Q4_NetSales__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund_Vehicle_Type__c =:strVehicleType AND Year__c =:strYear AND Channel__c =:idChannel AND Q4_NetSales__c != null];
                for(Portfolio_Channel_Rollup_Quarterly__c result : lstResults){
                    lstWrappers.add(new ChannelLevelDetails(result.Id,result.Channel_Name_Drived__c,result.Name,result.Fund__r.Name,result.Fund__c,result.Q4_NetSales__c));
                }
            }
        }   
        return lstWrappers;
    }

    @AuraEnabled
    public static List<DrillDownDetails> getMFFirmwiseDrillDown(Id idChannel,String strYear,Integer intQuarter,String strVehicleType,Id idFund)
    {
        PortfolioChannelRollupService service = new PortfolioChannelRollupService();
        Date dStartDate = service.getQuarterDates(strYear,intQuarter,'startDate','Quarter');
        Date dEndDate =  service.getQuarterDates(strYear,intQuarter,'endDate','Quarter');
        Integer iYear = Integer.valueof(strYear);
        String strChannel = service.getChannelInfo(idChannel,'Discription');
        List<DrillDownDetails> lstDrillDownDetails = new List<DrillDownDetails>();
       
        if(strVehicleType == 'ETF'){
            List<Firm_Focus_Fund_Channel_Territory__c> lstRecords = new List<Firm_Focus_Fund_Channel_Territory__c>();
            if(strChannel == 'Financial Advisor'){
                lstRecords = [SELECT Net_Flows_YTD__c,FundAssetBalance__c,Net_Flows_Last_Month__c,BroadridgeFirmName__c,Firm_Branch__c,Firm_Branch__r.Name
                                FROM Firm_Focus_Fund_Channel_Territory__c 
                                WHERE  Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate and Asset_Date__c = null and Line_of_Business_Description__c='Financial Advisor' and OfficeType__c='actual' and country__c='united states' and ((NOT SFDC_Parent_Firm_Name__c  like '%Bank Parent%') AND SFDC_Firm_Org_Type__c NOT in ('bank','private bank','Private Bank/Wealth Management')) and Fund__c = :idFund and territory__c<> null  AND DataView__c = 'Salesforce' ];
            }else if(strChannel == 'Asset Manager'){
                lstRecords = [SELECT Net_Flows_YTD__c,FundAssetBalance__c,Net_Flows_Last_Month__c,BroadridgeFirmName__c,Firm_Branch__c,Firm_Branch__r.Name
                                FROM Firm_Focus_Fund_Channel_Territory__c 
                                WHERE (Channel__c IN ('ASSET MANAGER') AND (Channel__c NOT IN ('INSTITUTIONAL') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL'))  AND Fund__c = :idFund AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce'];
            }else if(strChannel == 'Institutional'){
                lstRecords = [SELECT Net_Flows_YTD__c,FundAssetBalance__c,Net_Flows_Last_Month__c,BroadridgeFirmName__c,Firm_Branch__c,Firm_Branch__r.Name
                                FROM Firm_Focus_Fund_Channel_Territory__c 
                                WHERE ((Channel__c = 'INSTITUTIONAL' AND Territory__c ='INSTITUTIONAL') OR (Channel__c ='ASSET MANAGER' AND Territory__c = 'ASSET MANAGER INSTITUTIONAL'))  AND Fund__c = :idFund AND Asset_As_Of_Date__c >= :dStartDate AND Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' ];
            }else if(strChannel == 'RIA'){
                lstRecords = [SELECT Net_Flows_YTD__c,FundAssetBalance__c,Net_Flows_Last_Month__c,BroadridgeFirmName__c,Firm_Branch__c,Firm_Branch__r.Name
                                FROM Firm_Focus_Fund_Channel_Territory__c  
                                WHERE Fund__c = :idFund AND Territory__c IN ('RIA West', 'RIA South','Asset Manager West','Asset Manager East','RIA North') AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate  AND Asset_Date__c = null AND DataView__c = 'Salesforce' ];
            }

            for(Firm_Focus_Fund_Channel_Territory__c record : lstRecords){
                lstDrillDownDetails.add(new DrillDownDetails(record));  
            }
        }else{
            if(iYear < 2021){
                for(History_Firm_Portfolio_Breakdown__c result : [SELECT Id,SalesConnect_Account__c,SalesConnect_Current_Assets__c,SalesConnect_Account__r.Name,SFDC_Channel_Derived__c,Name FROM History_Firm_Portfolio_Breakdown__c WHERE SFDC_Fund_Id__c =:idFund AND SFDC_Channel_Id__c =: idChannel AND Fund_Vehicle_Type__c = :strVehicleType AND  Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate ]){
                    lstDrillDownDetails.add(new DrillDownDetails(result.Id,result.SalesConnect_Account__c,result.SalesConnect_Current_Assets__c,result.SalesConnect_Account__r.Name,result.SFDC_Channel_Derived__c,result.Name));
                }
            }else{
                for(SalesConnect__Firm_Portfolio_Breakdown__c result : [SELECT Id,SalesConnect__Account__c,SalesConnect__Current_Assets__c,SalesConnect__Account__r.Name,SFDC_Channel_Derived__c,Name FROM SalesConnect__Firm_Portfolio_Breakdown__c WHERE SFDC_Fund_Id__c =:idFund AND SFDC_Channel_Id__c =: idChannel AND Fund_Vehicle_Type__c = :strVehicleType AND  LastModifiedDate >= :dStartDate AND LastModifiedDate <= :dEndDate ]){
                    lstDrillDownDetails.add(new DrillDownDetails(result.Id,result.SalesConnect__Account__c,result.SalesConnect__Current_Assets__c,result.SalesConnect__Account__r.Name,result.SFDC_Channel_Derived__c,result.Name));  
                }
            }   
        }
        System.debug('lstDrillDownDetails --> '+lstDrillDownDetails);   
        return lstDrillDownDetails;
    }

    public class picklistOptions
    {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public picklistOptions(String label,String value)
        {
            this.label = label;
            this.value = value;
        }
    }

    public class DrillDownDetails
    {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public Id accountId;
        @AuraEnabled
        public Decimal currentAssets;
        @AuraEnabled
        public String accountName;
        @AuraEnabled
        public String channelName;
        @AuraEnabled
        public String fundName;
        @AuraEnabled
        public String accountNameBR;
        @AuraEnabled
        public Decimal netSales;
        @AuraEnabled
        public Decimal aum;
        @AuraEnabled
        public Decimal netSalesLastMonth;
        @AuraEnabled
        public String accountLink;

        public DrillDownDetails(Id recordId,Id accountId,Decimal dCurrentAssets,String strAccountName,String strChannelName,String strFundName)
        {
            this.id = recordId;
            this.accountId = accountId;
            this.currentAssets = dCurrentAssets;
            this.channelName = strChannelName;
            this.fundName = strFundName;
            if(strAccountName != null){
                this.accountLink = '/' + accountId;
                this.accountName = strAccountName;
            }
        }

        public DrillDownDetails(Firm_Focus_Fund_Channel_Territory__c record){
            this.id = record.Id;
            this.accountId = record.Firm_Branch__c;
            this.accountNameBR = record.BroadridgeFirmName__c;
            this.netSales =  record.Net_Flows_YTD__c;
            this.aum =  record.FundAssetBalance__c;
            this.netSalesLastMonth =  record.Net_Flows_Last_Month__c;
            if(record.Firm_Branch__c != null){
                this.accountLink = '/' + record.Firm_Branch__c;
                this.accountName = record.Firm_Branch__r.Name;
            }
            
        }
    }

    public class ChannelLevelDetails
    {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String channelName;
        @AuraEnabled
        public String fundTicker;
        @AuraEnabled
        public String fundName;
        @AuraEnabled
        public Id fundId;
        @AuraEnabled
        public Decimal aum_br;
        @AuraEnabled
        public Decimal aum_sfdc;
        @AuraEnabled
        public Decimal netSales_br;
        @AuraEnabled
        public Decimal netSales_sfdc;
        @AuraEnabled
        public Decimal netSalesLastMonth_br;
        @AuraEnabled
        public Decimal netSalesLastMonth_sfdc;
        @AuraEnabled
        public String fundLink;

        public ChannelLevelDetails(Id id,String channelName,String fundTicker,String fundName,Id fundId,Decimal aum_br,Decimal aum_sfdc,Decimal netSales_br,Decimal netSales_sfdc,Decimal netSalesLastMonth_br,Decimal netSalesLastMonth_sfdc)
        {
            this.id = id;
            this.channelName = channelName;
            this.fundTicker = fundTicker;
            this.fundName = fundName;
            this.fundId = fundId;
            this.fundLink = '/'+fundId;
            this.aum_br = aum_br;
            this.aum_sfdc = aum_sfdc;
            this.netSales_br = netSales_br;
            this.netSales_sfdc = netSales_sfdc;
            this.netSalesLastMonth_br = netSalesLastMonth_br;
            this.netSalesLastMonth_sfdc = netSalesLastMonth_sfdc;
        }

        public ChannelLevelDetails(Id id,String channelName,String fundTicker,String fundName,Id fundId,Decimal netSales_sfdc)
        {
            this.id = id;
            this.channelName = channelName;
            this.fundTicker = fundTicker;
            this.fundName = fundName;
            this.fundId = fundId;
            this.fundLink = '/'+fundId;
            this.netSales_sfdc = netSales_sfdc;
        }
    }

}