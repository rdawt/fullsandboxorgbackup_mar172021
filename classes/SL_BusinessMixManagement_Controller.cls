/*
    Class Name  :   SL_BusinessMixManagement_Controller
    JIRA Ticket :   VANECK-37
    Description :   Controller for ligthning component SL_BusinessMixManagement_Cmp 
*/
public class SL_BusinessMixManagement_Controller {
    /*
        Method Name :   getBusinessMixDetails
        Params      :   recordId -> Current Contact Id, for which business mix management is in progress
        Descritpion :   returning Contact and business mix details, 
                        calling getBusMixwrapperLst
    */
    @AuraEnabled
    public static conAndBusMixDetailWrapper getBusinessMixDetails(Id recordId){
        Contact conObj = [SELECT Id,FirstName,LastName,
                                   (SELECT Id,Business_Mix__c,Comments__c,Used__c FROM Business_Mix__r)
                            FROM Contact 
                            WHERE Id=:recordId LIMIT 1];
        
        conAndBusMixDetailWrapper objWrapper = new conAndBusMixDetailWrapper();
        objWrapper.conObj = conObj;
        objWrapper.busMixWrapperLst = getBusMixwrapperLst(conObj.Business_Mix__r); 
        
        return objWrapper;
    }
    
    /*
        Method Name :   getBusinessMixDetails
        Params      :   List<Business_Mix__c> busMixLst : related Business mix records
        Descritpion :   returning Recordset list
    */
    public static List<Recordset> getBusMixwrapperLst(List<Business_Mix__c> busMixLst)
    {
        List<Recordset> wrapperLst =new List<Recordset>();
        
        Schema.DescribeFieldResult fieldResult = Business_Mix__c.Business_Mix__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> currentFilledPLsLst = new List<String>();
        
        for(Business_Mix__c busMixObj : busMixLst){
            wrapperLst.add(new Recordset(busMixObj.Id, busMixObj.Business_Mix__c, busMixObj.Comments__c, busMixObj.Used__c));
            currentFilledPLsLst.add(busMixObj.Business_Mix__c);
        }
           
        for( Schema.PicklistEntry plObj : ple)
        {
            if(!currentFilledPLsLst.contains(plObj.getValue())){
                wrapperLst.add(new Recordset('', plObj.getValue(), '', false));
            }
        }
        return wrapperLst;
    }
    
    /*
        Method Name :   insertOrUpdateBusMix
        Params      :   String currentRecordId : contact Id
                        String busMixWrapperLst : busMix list to upsert
        Descritpion :   Inserting new bus mix records or updating existing records as per the list made from lightning component
    */
    @AuraEnabled
    public static String insertOrUpdateBusMix(String currentRecordId, String busMixWrapperLstTemp, String busMixWrapperLstOld){
        List<Recordset> wrapperLstToUpsert = (List<Recordset> )JSON.deserialize(busMixWrapperLstTemp, List<Recordset>.class);
        List<Recordset> wrapperOldLstToCompare = (List<Recordset> )JSON.deserialize(busMixWrapperLstOld, List<Recordset>.class);
        
        String resultStr = '';
        List<Business_Mix__c> busMixLstToUpsert =  new List<Business_Mix__c>();
        Map<String,Boolean>  plToCBMap = new Map<String,Boolean>();
        Map<String,String>  plToInterestPLMap = new Map<String,String>();
        Map<String,String>  plToCommentMap = new Map<String,String>();
        
        for(Recordset wrapperObj : wrapperOldLstToCompare){
            plToCBMap.put(wrapperObj.busMixPL,wrapperObj.checked);
            plToCommentMap.put(wrapperObj.busMixPL,wrapperObj.comment);
        }
        
        try{
            for(Recordset wrapperObj : wrapperLstToUpsert){
                if(wrapperObj.checked != plToCBMap.get(wrapperObj.busMixPL) 
                   || wrapperObj.comment != plToCommentMap.get(wrapperObj.busMixPL))
                {
                    Business_Mix__c busMixObj = new Business_Mix__c();
                    if(wrapperObj.busMixId != ''){
                        busMixObj.Id = wrapperObj.busMixId;
                    }
                    busMixObj.Used__c = wrapperObj.checked;
                    busMixObj.Business_Mix__c = wrapperObj.busMixPL;
                    busMixObj.Comments__c = wrapperObj.comment;
                    busMixObj.Contact__c = currentRecordId;
                    
                    busMixLstToUpsert.add(busMixObj);
                }
            }
          upsert busMixLstToUpsert;
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        }
        resultStr = 'Success!';
        return resultStr;
    }
    
    /*
        Method Name :   getBaseUrl
        Description :   returning base URL of the org, 
                        Used in lightning component for redirect on cancelling and conversion
    */
    @AuraEnabled
    public static String getBaseUrl () {
        return system.URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    public class conAndBusMixDetailWrapper {
        @AuraEnabled    public Contact conObj {get; set;}
        @AuraEnabled    public list<Recordset> busMixWrapperLst {get; set;}
    }
    
    public class Recordset {
        @AuraEnabled    public String busMixId {get; set;}
        @AuraEnabled    public String busMixPL {get; set;}
        @AuraEnabled    public String comment {get; set;}
        @AuraEnabled    public Boolean checked {get; set;}
        
        public Recordset(String busMixId,String busMixPL,String comment,Boolean checked){
            this.busMixId = busMixId;
            this.busMixPL = busMixPL;
            this.comment = comment;
            this.checked = checked;
        }
    }
}