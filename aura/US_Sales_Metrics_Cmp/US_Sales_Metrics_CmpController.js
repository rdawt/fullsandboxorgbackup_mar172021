({
    afterScriptsLoaded : function(component, event, helper) 
    {
        component.set('v.isScriptLoaded', true);
        helper.getOnLoadRecords(component,event,helper);
    },

    applyFilters: function(component, event, helper){        
        helper.loadChartForSalesByFund(component, event, helper);
        helper.loadTableForSalesByFund_Active(component, event, helper);
    },
    
    resetFilters: function (component, event, helper) {
        if (component.get('v.isScriptLoaded')) {
            helper.getOnLoadRecords(component,event,helper);
        }
    },

    applyFiltersETF: function(component, event, helper){        
        helper.loadChartForSalesByFund_ETF(component, event, helper);
    },
    
    resetFiltersETF: function(component, event, helper){
        helper.getOnLoadRecordsETF(component,event,helper);
        helper.loadTableForSalesByFund_ETF(component, event, helper);
    },

    loadChartForSalesByFund: function(component, event, helper){        
        helper.loadChartForSalesByFund(component, event, helper);
    },

    loadChartForSalesByFundETF: function(component, event, helper){        
        helper.loadChartForSalesByFund_ETF(component, event, helper);
    },

    getOwnerRecordsCntrl : function(component, event, helper) 
    {
        var selectedVal = event.getSource().get('v.name');
		var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
            }
            
            // if any other value is selected
            else{
                component.set("v.SelectedOwnersOption",helper.addNewSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",[]);
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.SelectedOwnersOption",helper.removeSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }    
    },

    getOwnerRecordsCntrlETF : function(component, event, helper) 
    {
        var selectedVal = event.getSource().get('v.name');
		var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOptionETF",helper.selectAll(component, event, helper, component.get("v.OwnersOptionsETF")));
                
                // select all checkbox   
                try{             
                    component.find("ownerCheckboxETF").forEach( function(itemcmp) {
                        itemcmp.set("v.checked",true);     
                    } );
                }catch(err){
                    console.log('ownerCheckboxETF not found');
                }
                
            }
            
            // if any other value is selected
            else{
                component.set("v.SelectedOwnersOptionETF",helper.addNewSelectedValue(component, event, helper, component.get("v.SelectedOwnersOptionETF"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOptionETF",[]);
                // select all checkbox   
                try{
                    component.find("ownerCheckboxETF").forEach( function(itemcmp) {
                        itemcmp.set("v.checked",false);     
                    } );
                } catch(err){
                    console.log('ownerCheckboxETF not found')
                }            
                
            }
            else{
                component.set("v.SelectedOwnersOptionETF",helper.removeSelectedValue(component, event, helper, component.get("v.SelectedOwnersOptionETF"), selectedVal));                
                try{
                    component.find("ownerCheckboxETF").forEach( function(itemcmp) {
                        if(itemcmp.get("v.label") == 'ALL')
                            itemcmp.set("v.checked",false);     
                    } );
                } catch(err){
                    console.log('ownerCheckboxETF not found')
                }  
            }
        }    
    }

})