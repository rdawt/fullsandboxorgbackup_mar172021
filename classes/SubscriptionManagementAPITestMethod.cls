/*  Modified By Vidhya Krishnan to increse the coverage.    */
@isTest
private class SubscriptionManagementAPITestMethod// extends DataStructures
{
    static testMethod void runSubscriptionManagementAPITestMethod() 
    { 
       Account acc = new Account(Name = 'abc123', Channel__c = 'RIA', BillingCountry='US');
       insert acc;
       Community__c c = new Community__c(Name = 'Test Community');
       insert c; 
       Community__c c1 = new Community__c();
       c1 = [select Id from Community__c where Name =: 'Financial Advisor'];
       Subscription__c sub = new Subscription__c(Name = 'abc', Subscription_Display_Name__c = 'ABC', IsActive__c = true, Community__c = c1.Id, SubName_Enum__c = 'MF_MONTHLY');
       insert sub;
       Subscription__c s = new Subscription__c(Name = 'abcd', Subscription_Display_Name__c = 'ABCD', IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       insert s;
       Subscription__c s1 = new Subscription__c(Name = 'abcd1', Subscription_Display_Name__c = 'ABCD1', IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       //insert s1;
       Subscription__c s2 = new Subscription__c(Name = 'abcd2', Subscription_Display_Name__c = 'ABCD2', IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       //insert s2;
       Subscription_Type__c subType = new Subscription_Type__c(Name = 'abcd');
       insert subType;
       Subscription_Group__c subGrp = new Subscription_Group__c(Name = 'test', IsActive__c = true);
       insert subGrp; 
       Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = sub.Id );
       insert  SGS;
       Subscription_Group_Subscription__c SGS1 = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = s.Id );
       insert  SGS1;
       Subscription_Group_Subscription__c SGS2 = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = s1.Id );
       //insert  SGS2;
       Subscription_Group_Subscription__c SGS3 = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = s2.Id );
       //insert  SGS3;
       Contact con = new Contact(FirstName='ABC', LastName='ABC', Email='abc@gmail.com', Subscription_Group__c=subGrp.Id, AccountId=acc.Id, MailingCountry='US');
       insert con;
       Subscription_Member__c sm = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, Subscription__c = sub.Id, Email__c = con.Email);
       insert sm;
       Subscription_Member__c sm1 = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, Subscription__c = s.Id, Email__c = con.Email);
       insert sm1;
       SubscriptionManagementAPI smAPI = new SubscriptionManagementAPI();
       SubscriptionManagementAPI.RecordSet[] rs;
       list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
       SubscriptionManagementAPI.SubscriptionDetails sub1 = new SubscriptionManagementAPI.SubscriptionDetails();
       SubscriptionManagementAPI.SubscriptionDetails sub2 = new SubscriptionManagementAPI.SubscriptionDetails();
       SubscriptionManagementAPI.SubscriptionDetails sub3 = new SubscriptionManagementAPI.SubscriptionDetails();
       SubscriptionManagementAPI.SubscriptionDetails sub4 = new SubscriptionManagementAPI.SubscriptionDetails();
       SubscriptionManagementAPI.SubscriptionDetails sub5 = new SubscriptionManagementAPI.SubscriptionDetails();
       sub1.contactId = con.Id; sub1.subscriptionId = sub.Id; sub1.newValue = 'true'; sub1.oldValue = 'false';
       sub3.contactId = con.Id; sub3.subscriptionId = s.Id; sub3.newValue = 'false'; sub3.oldValue = 'true';
       subList.add(sub1); subList.add(sub3);
       SubscriptionManagementAPI.GetContactName(con.Id);
       SubscriptionManagementAPI.showPrivacyAck(con.Id);
       SubscriptionManagementAPI.GetSubscriptionGroupID(con.Id);
       SubscriptionManagementAPI.GetSubscriptions(con.Id,subGrp.Id);
       SubscriptionManagementAPI.GetSubscriptionsWithCommunity(con.id,subGrp.Id,c.Id);
       SubscriptionManagementAPI.GetSubscriptionsWithSubGrp(subGrp.Id, c.Id);
       SubscriptionManagementAPI.GetInvestorType(con.Id);
       SubscriptionManagementAPI.GetSubscriptionId(con.Id,'MF_MONTHLY',c.Id);
       SubscriptionManagementAPI.setsubscription(sub.Id,con.Id,true,false);
       SubscriptionManagementAPI.setsubscription(s.Id,con.Id,true,false);
       SubscriptionManagementAPI.setsubscription(sub.Id,con.Id,false,true);
       SubscriptionManagementAPI.setsubscription(sub.Id,con.Id,true,true);
       //SubscriptionManagementAPI.setsubscription(s1.Id,con.Id,true,false);
       SubscriptionManagementAPI.UpdateContactPrivacy(con.Id,System.now());
       SubscriptionManagementAPI.setSubscriptions(subList);
       sub2.contactId = con.Id; sub2.subscriptionId = sub.Id; sub2.newValue = 'false'; sub2.oldValue = 'true';
       sub4.contactId = con.Id; sub4.subscriptionId = s.Id; sub4.newValue = 'true'; sub4.oldValue = 'false';
       //sub5.contactId = con.Id; sub5.subscriptionId = s2.Id; sub5.newValue = 'true'; sub5.oldValue = 'false';
       subList.add(sub2); subList.add(sub4); subList.add(sub5);
       SubscriptionManagementAPI.setSubscriptions(subList);
    }
    
    static testMethod void runSubscriptionManagementAPITestMethod2() 
    {
       Account acc = new Account(Name = 'abc123', Channel__c = 'RIA', BillingCountry='US');
       insert acc;
       Community__c c = new Community__c(Name = 'Test Community 2');
       insert c; 
       Community__c c1 = new Community__c();
       c1 = [select Id from Community__c where Name =: 'Financial Advisor'];
       Subscription__c sub = new Subscription__c(Name = 'abc', Subscription_Display_Name__c = 'ABC', 
IsActive__c = true, Community__c = c1.Id, SubName_Enum__c = 'MF_MONTHLY');
       insert sub;
       Subscription__c s = new Subscription__c(Name = 'abcd', Subscription_Display_Name__c = 'ABCD', 
IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       insert s;
       Subscription__c s1 = new Subscription__c(Name = 'abcd1', Subscription_Display_Name__c = 'ABCD1', 
IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       //insert s1;
       Subscription__c s2 = new Subscription__c(Name = 'abcd2', Subscription_Display_Name__c = 'ABCD2', 
IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
       //insert s2;
       Subscription_Type__c subType = new Subscription_Type__c(Name = 'abcd2');
       insert subType;
       Subscription_Group__c subGrp = new Subscription_Group__c(Name = 'test2', IsActive__c = true);
       insert subGrp; 
       Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c
(Subscription_Group__c = subGrp.Id, Subscription__c = sub.Id );
       insert  SGS;
       Subscription_Group_Subscription__c SGS1 = new Subscription_Group_Subscription__c
(Subscription_Group__c = subGrp.Id, Subscription__c = s.Id );
       insert  SGS1;
       Subscription_Group_Subscription__c SGS2 = new Subscription_Group_Subscription__c
(Subscription_Group__c = subGrp.Id, Subscription__c = s1.Id );
       //insert  SGS2;
       Subscription_Group_Subscription__c SGS3 = new Subscription_Group_Subscription__c
(Subscription_Group__c = subGrp.Id, Subscription__c = s2.Id );
       //insert  SGS3;
       Contact con = new Contact(FirstName='ABC', LastName='ABC', Email='abc@gmail.com', 
Subscription_Group__c=subGrp.Id, AccountId=acc.Id, MailingCountry='US');
       insert con;
       Subscription_Member__c sm = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, 
Subscription__c = sub.Id, Email__c = con.Email);
       insert sm;
       Subscription_Member__c sm1 = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, 
Subscription__c = s.Id, Email__c = con.Email);
       insert sm1;
        
       //Added by Geeta
       SubscriptionManagementAPI.setOptout(con.Id,true);
      // SubscriptionManagementAPI.setEmail(con.Id,'1@1.com');    
       SubscriptionManagementAPI.ContactRecordDetails conRecord = 
       SubscriptionManagementAPI.getContactRecordDetails(con.Id);
       system.debug('Test record is:conRecord:'+conRecord);   
       conRecord.privacyPolicyAck =system.now().format();   
       conRecord.hasOptedOut = 'False';
       conRecord.emailId  = '112@2.com';
       SubscriptionManagementAPI.setContactRecordDetails(conRecord);    
       
       conRecord.privacyPolicyAck = system.now().format();    
       conRecord.hasOptedOut = 'False';
       conRecord.emailId  = '2@2.com';
       SubscriptionManagementAPI.setContactRecordDetails(conRecord); 
       
       conRecord.privacyPolicyAck = system.now().addDays(1).format();
       conRecord.hasOptedOut = 'true';
       conRecord.emailId  = '3@2.com';
       SubscriptionManagementAPI.setContactRecordDetails(conRecord);       
                  
    }
    
    static testMethod void sfdctest(){
        SubscriptionManagementAPI sm1 = new SubscriptionManagementAPI();
        
        list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
        SubscriptionManagementAPI.SubscriptionDetails sub1 = new SubscriptionManagementAPI.SubscriptionDetails();
        SubscriptionManagementAPI.SubscriptionDetails sub2 = new SubscriptionManagementAPI.SubscriptionDetails();
                
        subList.add(sub1);
        subList.add(sub2);
        
        map<Id,Subscription_Member__c> subValueMap = new map<Id,Subscription_Member__c>();
        
        Account acc = new Account(Name = 'abc123', Channel__c = 'RIA', BillingCountry='US');
        insert acc;
        Community__c c = new Community__c(Name = 'Test Community');
        insert c; 
        Subscription__c sub = new Subscription__c(Name = 'abc', Subscription_Display_Name__c = 'ABC', IsActive__c = true, Community__c = c.Id, SubName_Enum__c = 'MF_MONTHLY');
        insert sub;
        Subscription_Type__c subType = new Subscription_Type__c(Name = 'abcd');
        insert subType;
        Subscription_Group__c subGrp = new Subscription_Group__c(Name = 'test', IsActive__c = true);
        insert subGrp; 
        Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c(Subscription_Group__c = subGrp.Id, Subscription__c = sub.Id );
        insert  SGS;
        Contact con = new Contact(FirstName='ABC', LastName='ABC', Email='abc@gmail.com', Subscription_Group__c=subGrp.Id, AccountId=acc.Id, MailingCountry='US');
        Subscription_Member__c sm = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, Subscription__c = sub.Id, Email__c = con.Email);
        
        SubscriptionManagementAPI.getEmail(con.id);
        
        SubscriptionManagementAPI.setSubscriptions(subList);

    }
}