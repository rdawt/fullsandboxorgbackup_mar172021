trigger ContactScoreTrackerSummary on ContactScoreTracker__c (after insert) {

   //buggy  datetime currentDate = Date.today();
    date currentDate = Date.today();
    set<id> ids= new set<id>();
    set<id> userIds= new set<id>();
    
    List<User> addUserIds; //= new set<User>([Select id from User where id in : e.createdbyid and Year__c ]);
    
    if ((trigger.isInsert) && trigger.isAfter) {
      for(ContactScoreTracker__c ct: trigger.new){
        //ids.addAll(trigger.newMap.accountid);
       //exclude outside research fill-in field
       if ((ct.Contact__c!= null) && ct.TrackingFieldAPIName__c != 'OR__c' ) {
            system.debug('inside insert trigger condition for Contact Tracker - contact id'+ct.Contact__c);
            ids.add(ct.Contact__c);
            userIds.add(ct.lastmodifiedbyid);
            }
        } 
        //put processing code block here
        //this is for user object count
        
        try{
             AggregateResult[] contactTracker4PayeesQCount; 
             if (userIds.size() != 0){
             
                 contactTracker4PayeesQCount = [select  count(id) contactTrackerQCount4Payee,lastmodifiedbyid
                                                      from ContactScoreTracker__c 
                                                          where createdbyid in : userIds  and  createddate = THIS_Quarter and createddate = THIS_YEAR
                                                                  and TrackingFieldAPIName__c != 'OR__c'
                                                               group by lastmodifiedbyid]; //
                 
                 
                if ((contactTracker4PayeesQCount.size() == 0)){
                     system.debug('after agg. Payee Q Count for Contact Tracker is-ZERO'+ 0); 
                     return;
                } 
                 system.debug('after agg. Payee Q Count for Contact Tracker is'+ contactTracker4PayeesQCount);                                 
               
                 list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
                 list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();
                 
                 Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
                 Integer j =0;
                 
                 system.debug('what is !!Today!! '+ currentDate );
                 system.debug('what is this ***month*** '+ currentDate.Month());
                 system.debug('what is this ***calendar month*** '+ (system.today().month()));
                 system.debug('what is this ***year***'+ currentDate.Year());
                 system.debug('current month after using string of  :'+String.ValueOf(currentDate.Month()));
                 system.debug('current year after using string of  :'+String.ValueOf(currentDate.Year()));
                 
                 
                 for(PayeeSummary__c payeeSummary: [Select id, user__c from PayeeSummary__c  Where user__c in :userIds and Month__c = :String.ValueOf(currentDate.Month()) and Year__c =:String.ValueOf(currentDate.Year()) ])
                 {
                    modPayeeSummaryList.add(payeeSummary);
                    payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
                    system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
                   
                 }
                 
            
                  //addPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c !=: String.ValueOf(currentDate.Month()) and Year__c !=: String.ValueOf(currentDate.Year()) ];
                  //modPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ];
                  
                 // system.debug('after agg. User-Payee FA Activity Score Sum- Payee List is'+ taskUsersCount  );
                
                  //system.debug('after agg. User-Payee RIA Activity Score Sum- Sum is'+ RIAActivityScore);
                                    
                 list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
                 list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
                 
                 for(AggregateResult forLoopCount : contactTracker4PayeesQCount){
                 
                    system.debug('entering forloop to chase the matching payee ids'+ contactTracker4PayeesQCount);
        
                     if((payeeListMap != null) && payeeListMap.containsKey(string.valueof(forLoopCount.get('lastmodifiedbyid'))) && (string.valueof(forLoopCount.get('lastmodifiedbyid')) != null) )
                     {
                         integer cTrackerPayeeQCount = 0;
                         PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('lastmodifiedbyid')));
                         system.debug('inside else of **MODIFYING** lastmodifiedbyid'+ string.valueof(forLoopCount.get('lastmodifiedbyid')));
                          if (index != null)
                          { system.debug('inside else of **MODIFYING** index'+ index);
                          }
                          else
                          {
                              system.debug('inside else of **MODIFYING** index---->>>>'+ 'index is null');
                          }
                         system.debug('inside else of **MODIFYING** new payee summary for the current month'+ contactTracker4PayeesQCount);
                 
                         PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof(String.ValueOf(index.id)),
                                                             Year__c=String.ValueOf(currentDate.Year()),
                                                             Month__c=String.ValueOf(currentDate.Month()),
                                                             User__c=string.valueof(forLoopCount.get('lastmodifiedbyid')),
                                                             contactTracker4PayeeQCount__c= integer.valueof(forLoopCount.get('contactTrackerQCount4Payee'))); 
                         
                          
                        updatePayeeList.add(payeeSummary);      
                     }
                     else if((string.valueof(forLoopCount.get('lastmodifiedbyid')) != null)){
                         integer cTrackerPayeeQCount = 0;
                         
                         PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('lastmodifiedbyid')));
                         system.debug('inside else of **ADDING** new payee summary for the current month'+ contactTracker4PayeesQCount);
                 
                         PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),
                                                             Month__c=String.ValueOf(currentDate.Month()),
                                                             User__c=string.valueof(forLoopCount.get('lastmodifiedbyid')),
                                                             Total_Days_for_this_Calendar_Month__c=1, Total_Days_for_this_Calendar_Quarter__c=1,
                                                             No_Of_Days_in_Office__c=1, No_Of_Days_in_Office_Quarter__c=1,
                                                             contactTracker4PayeeQCount__c= integer.valueof(forLoopCount.get('contactTrackerQCount4Payee')));
                         addPayeeList.add(payeeSummary);
                     }
                 }
                if (updatePayeeList.size() != 0) {
                    update updatePayeeList;
                }
                if (addPayeeList.size() != 0) {
                    
                     if(Test.isRunningTest()){
                        addPayeeList[0].Total_Days_for_this_Calendar_Month__c = 30;
                        addPayeeList[0].No_Of_Days_in_Office__c = 25;
                        
                    }
                    insert addPayeeList;
                }
             }
         
         
        }catch(Exception e){
                string error = 'Exception occured in  Contact Tracker Trigger to Summarize count for the quarter***. '+e.getMessage()+userIds;
                system.debug('error catch at the exeption level-error catch bloc ------' +error);
                SendEmailNotification se = new SendEmailNotification('Error: Trigger Error to summarize Contact Tracker for Payees Q');
                String    msg = 'Exception occured in  Contact Tracker Trigger to Summarize count for the quarter***.  \n '+e.getMessage() + ' ' +ids;
                se.sendEmail(msg);
                //system.debug('List of Record Types........'+'rt');
        
        
         }
    }
      
}