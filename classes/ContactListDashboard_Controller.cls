public class ContactListDashboard_Controller 
{
    public static String strDefaultContactOnLoad = 'Brendan Gundersen';
    @AuraEnabled
    public static Map<String, Object> getOnLoadRecords()
    {
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        Contact firstContactRecord;
        List<Contact> lstContact = new List<Contact>();
        List<Marketo_Activitiy__c> lstMarketoActivity = new List<Marketo_Activitiy__c>();
        List<Sobject> lstActivitiy = new List<Sobject>();
        lstContact = getContactRecords();

        if(!lstContact.isEmpty())
        {
            firstContactRecord  = lstContact[0];
            Map<String, Object> mapGetMarketoActivityRecords = new Map<String, Object>();
            mapGetMarketoActivityRecords = getMarketoActivityForEachContact(firstContactRecord.Id, NULL, NULL);
            lstMarketoActivity = (List<Marketo_Activitiy__c>) mapGetMarketoActivityRecords.get('lstMarketoActivity');

            Map<String, Object> mapGetActivityRecords = new Map<String, Object>();
            mapGetActivityRecords = getActivityForEachContact(firstContactRecord.Id, NULL, NULL);
            lstActivitiy = (List<Sobject>) mapGetActivityRecords.get('lstActivity');

        }
        mapGetOnLoadRecords.put('currentSelectedContactId', firstContactRecord.Id);
        mapGetOnLoadRecords.put('currentSelectedContactName', firstContactRecord.Name);
        mapGetOnLoadRecords.put('OwnersOptions', getAllContactOwners());
        mapGetOnLoadRecords.put('SelectedOwnersOption', getDefaultContactOwnerDetails());
        mapGetOnLoadRecords.put('FirmTypeOptions', getFirmTypeOptions());
        mapGetOnLoadRecords.put('lstContact', lstContact);
        mapGetOnLoadRecords.put('hasContactRecords', lstContact.size());
        mapGetOnLoadRecords.put('lstMarketoActivity', lstMarketoActivity);
        mapGetOnLoadRecords.put('hasMarketoActivityRecords', lstMarketoActivity.size());
        mapGetOnLoadRecords.put('lstActivitiy', lstActivitiy);
        mapGetOnLoadRecords.put('hasActivityRecords', lstActivitiy.size());
         
        return mapGetOnLoadRecords;    
    }

    @AuraEnabled
    public static User getDefaultContactOwnerDetails()
    {
        User objDefaultUser = new User();
        objDefaultUser = [Select Id from User where Name =: strDefaultContactOnLoad limit 1];
        return objDefaultUser;
        
    }

    @AuraEnabled
    public static Contact getCurrentContactDetails(Id contactId)
    {
        Contact objContact = new Contact();
        objContact = [Select Id, Name from Contact where Id=: contactId limit 1];
        return objContact;
        
    }

    @AuraEnabled
    public static List<Contact> getContactRecords()
    {
        List<Contact> lstContact = new List<Contact>();
        lstContact = [Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                        Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                        Contact_Status__c
                        from Contact where Owner.Name =: strDefaultContactOnLoad and Account.ETF_Asset__c > 0 
                        Order by Account.ETF_Asset__c DESC limit 500 ];
                        //where Owner.profileId =: UserInfo.getProfileId() ];

        return lstContact;
    }

    @AuraEnabled
    public static Map<String, Object> getFilterContactRecords(String contactOwner, Date lastHContDateMin, Date lastHContDateMax)
    {
        Map<String, Object> mapGetContactRecords = new Map<String, Object>();
        List<Contact> lstContact = new List<Contact>();

        // If Condition will fire only for Specific Owner
        if(contactOwner != 'ALL')
        {
            if(lastHContDateMin != NULL && lastHContDateMax != NULL)  // Both Dates are populated
            {
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where OwnerId =: contactOwner and 
                                    LastHumanContactDate__c >=: lastHContDateMin and LastHumanContactDate__c <=: lastHContDateMax ];
            }
            else if(lastHContDateMax != NULL){ // Max Date is populated
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where OwnerId =: contactOwner and 
                                    LastHumanContactDate__c <=: lastHContDateMax ];
            }
            else if(lastHContDateMin != NULL){ // Min Date is populated
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where OwnerId =: contactOwner and 
                                    LastHumanContactDate__c >=: lastHContDateMin];
            }
            else
            {
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where OwnerId =: contactOwner ];
            }
        
        }// Else part will execute Query for All Contact Owner
        else {
            if(lastHContDateMin != NULL && lastHContDateMax != NULL) // Both Dates are populated
            {
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where LastHumanContactDate__c >=: lastHContDateMin and LastHumanContactDate__c <=: lastHContDateMax ];
            }
            else if(lastHContDateMax != NULL){ // Max Date is populated
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where LastHumanContactDate__c <=: lastHContDateMax ];
            }
            else if(lastHContDateMin != NULL){ // Min Date is populated
                lstContact = [ Select Id, Name, AccountId, Account.Name, Account.ETF_Asset__c, FirstName , LastName, 
                                Email, Phone , Owner.Profile.Name, Owner.Name, Title, LastHumanContactDate__c,
                                Contact_Status__c
                                from Contact 
                                where LastHumanContactDate__c >=: lastHContDateMin];
            }
        }
        mapGetContactRecords.put('lstContact',lstContact);
        mapGetContactRecords.put('hasContactRecords',lstContact.size());
        return mapGetContactRecords;
    }

    @AuraEnabled
    public static Map<String, Object> getMarketoActivityForEachContact(Id contactId, Date activityDateMin, Date activityDateMax)
    {
        Map<String, Object> mapGetActivityRecords = new Map<String, Object>();
        List<Marketo_Activitiy__c> lstMarketo_Activity = new List<Marketo_Activitiy__c>();
        Contact objContact = new Contact();
        // If Condition will fire only for Specific Owner
        if(contactId != NULL)
        {
            objContact = getCurrentContactDetails(contactId);
            if(activityDateMin != NULL && activityDateMax != NULL)  // Both Dates are populated
            {
                lstMarketo_Activity = [Select Id , ActivityDate__c, Subject__c, Status__c, Email__c 
                                        from Marketo_Activitiy__c
                                        where Who__c =: contactId and 
                                        ActivityDate__c >=: activityDateMin and ActivityDate__c <=: activityDateMax
                                        Order By ActivityDate__c ASC ]; 
            }
            else if(activityDateMax != NULL){ // Max Date is populated
                lstMarketo_Activity = [Select Id , ActivityDate__c, Subject__c, Status__c, Email__c 
                                        from Marketo_Activitiy__c
                                        where Who__c =: contactId and 
                                        ActivityDate__c <=: activityDateMax
                                        Order By ActivityDate__c ASC ];
            }
            else if(activityDateMin != NULL){ // Min Date is populated
                lstMarketo_Activity = [Select Id , ActivityDate__c, Subject__c, Status__c, Email__c 
                                        from Marketo_Activitiy__c
                                        where Who__c =: contactId and 
                                        ActivityDate__c >=: activityDateMin
                                        Order By ActivityDate__c ASC ];
            }
            else {
                lstMarketo_Activity = [Select Id , ActivityDate__c, Subject__c, Status__c, Email__c 
                                        from Marketo_Activitiy__c
                                        where Who__c =: contactId 
                                        Order By ActivityDate__c ASC ];
            }
        }// Else part will execute Query for All Contact Owner
        else {

        }
        mapGetActivityRecords.put('currentSelectedContactId', objContact.Id);
        mapGetActivityRecords.put('currentSelectedContactName', objContact.Name);
        mapGetActivityRecords.put('lstMarketoActivity',lstMarketo_Activity);
        mapGetActivityRecords.put('hasMarketoActivityRecords',lstMarketo_Activity.size());
        return mapGetActivityRecords;
    }

    @AuraEnabled
    public static Map<String, Object> getActivityForEachContact(Id contactId, Date activityDateMin, Date activityDateMax)
    {
        Map<String, Object> mapGetActivityRecords = new Map<String, Object>();
        List<Sobject> lstActivity = new List<Sobject>();
        List<Contact> lstContact = new List<Contact>();
        Contact objContact = new Contact();
        // If Condition will fire only for Specific Owner
        if(contactId != NULL)
        {
            objContact = getCurrentContactDetails(contactId);
            if(activityDateMin != NULL && activityDateMax != NULL)  // Both Dates are populated
            {
                lstContact = [SELECT Id, 
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM OpenActivities 
                                    WHERE ActivityDate>=:activityDateMin and ActivityDate<=:activityDateMax Order By ActivityDate ASC),
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM ActivityHistories 
                                    WHERE ActivityDate>=:activityDateMin and ActivityDate<=:activityDateMax Order By ActivityDate ASC) 
                                FROM Contact 
                                WHERE Id=: contactId]; 
            }
            else if(activityDateMax != NULL){ // Max Date is populated
                lstContact = [SELECT Id, 
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM OpenActivities 
                                    WHERE ActivityDate<=:activityDateMax Order By ActivityDate ASC),
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM ActivityHistories
                                    WHERE ActivityDate<=:activityDateMax Order By ActivityDate ASC) 
                                FROM Contact 
                                WHERE Id=: contactId];
            }
            else if(activityDateMin != NULL){ // Min Date is populated
                lstContact = [SELECT Id, 
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM OpenActivities
                                    WHERE ActivityDate>=:activityDateMin Order By ActivityDate ASC),
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId 
                                    FROM ActivityHistories
                                    WHERE ActivityDate>=:activityDateMin Order By ActivityDate ASC) 
                                FROM Contact 
                                WHERE Id=: contactId];
            }
            else {
                lstContact = [SELECT Id, 
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId FROM OpenActivities Order By ActivityDate ASC),
                                (SELECT Subject, WhoId, IsTask, ActivityDate, Status, Priority, OwnerId FROM ActivityHistories Order By ActivityDate ASC) 
                                FROM Contact 
                                WHERE Id=: contactId];
                
            }
            lstActivity.addAll(lstContact[0].OpenActivities);
            lstActivity.addAll(lstContact[0].ActivityHistories);
        }

        System.debug('???????????????lstActivity????????????' + lstActivity);
        mapGetActivityRecords.put('currentSelectedContactId', objContact.Id);
        mapGetActivityRecords.put('currentSelectedContactName', objContact.Name);
        mapGetActivityRecords.put('lstActivity',lstActivity);
        mapGetActivityRecords.put('hasActivityRecords',lstActivity.size());
        return mapGetActivityRecords;
    }
    

    @AuraEnabled
    public static List<Map<String, String>> getAllContactOwners()
    {
        List<Map<String, String>> lstUsers = new List<Map<String, String>>();
        lstUsers.add(new Map<String, String>{'value' =>'ALL', 'label'=>'ALL'} );
        For(User objUser : [Select Id , Name from User where Profile.Name IN: getProfileName() and isActive=true order by Name ASC])
        {
            if(strDefaultContactOnLoad == objUser.Name)
                lstUsers.add(new Map<String, String>{'value' =>objUser.Id, 'label'=>objUser.Name, 'selected' => 'true'} );
            else 
                lstUsers.add(new Map<String, String>{'value' =>objUser.Id, 'label'=>objUser.Name, 'selected' => 'false'} );
        }
        return lstUsers;
    }

    @AuraEnabled
    public static List<Map<String, String>> getFirmTypeOptions()
    {
        List<Map<String, String>> lstFirmTypeOptions = new List<Map<String, String>>();
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'0 - Not Interested', 'label'=>'0 - Not Interested'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'1 - No Contact', 'label'=>'1 - No Contact'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'2 - Initial Contact Made', 'label'=>'2 - Initial Contact Made'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'3 - Had Initial Meeting', 'label'=>'3 - Had Initial Meeting'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'4 - Interested - Due Diligence', 'label'=>'4 - Interested - Due Diligence'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'5 - Investor', 'label'=>'5 - Investor'} );
        lstFirmTypeOptions.add(new Map<String, String>{'value' =>'6 - Top Investor', 'label'=>'6 - Top Investor'} );

        return lstFirmTypeOptions;
    }

    public static Set<String> getProfileName()
    {
        Set<String> setProfileName = new Set<String>();
        setProfileName.add('Australia');
        setProfileName.add('Institutional US User');
        //setProfileName.add('Advisor User');
        return setProfileName;
    }

    @AuraEnabled
    public static String getOpportunityJSON(){
        
       List<opportunity> lstopp = [SELECT Id, stagename FROM opportunity 
                                    where Owner.Profile.Name = 'Institutional US User' OR Owner.Profile.Name = 'Institutional Swiss User' ];
        Map<String,Integer> mapLeadSource = new Map<String,Integer>();
        
        for(opportunity l : lstopp)
        {
            if(mapLeadSource.containsKey(l.stagename))
            {
                mapLeadSource.put(l.stagename, mapLeadSource.get(l.stagename)+1) ;
            }else{
                mapLeadSource.put(l.stagename, 0) ;        
            }
        }
        system.debug('map values--'+mapLeadSource);
        list<RadarDataWrapper> radarData = new list<RadarDataWrapper>();
        
        for(String key : mapLeadSource.keySet())
        {            
           RadarDataWrapper rdw = new RadarDataWrapper();
            rdw.name=key;
            rdw.y=mapLeadSource.get(key);
            radarData.add(rdw);
        }
        system.debug('rdw---'+radarData);
        return System.json.serialize(radarData);
        //return null;
    }
    
    /**
     * Wrapper class to serialize as JSON as return Value
     * */
    class RadarDataWrapper
    {
       @AuraEnabled
       public String name;
       @AuraEnabled
       public integer y;
      
    }
    
}