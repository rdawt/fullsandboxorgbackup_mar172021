@isTest
private class MarketoContactTriggerTest// extends DataStructures
{
    static testMethod void testTrigger() {
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    	
        Campaign testCamp = new Campaign(Name = 'Test Marketo Contact Trigger', IsActive = True);
        insert testCamp;        
        Account acc = new Account(Name = '65sdfas', Channel__c = 'RIA', BillingCountry = 'USA');
        insert acc;
        Contact con=new Contact(FirstName='Test',LastName='User', AccountId = acc.Id, MailingCountry = 'USA');
        insert con;
        CampaignMember cm = new CampaignMember(CampaignId = testCamp.Id, ContactId = con.id, Status = 'Sent');
        insert cm;
        system.debug('Conact IsMemberOfCampaign__c..........'+con.IsMemberOfCampaign__c);
        //system.assert(con.IsMemberOfCampaign__c == true);
        con.IsMemberOfCampaign__c = false;
        cm.Status = 'Responded';
        update cm;
        //system.assert(con.IsMemberOfCampaign__c == true);
        system.debug('Conact IsMemberOfCampaign__c..........'+con.IsMemberOfCampaign__c);
    }
}