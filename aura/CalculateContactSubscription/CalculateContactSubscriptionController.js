({
    //Initialize
    doInIt : function(component, event, helper) {
        helper.doInitialize(component, event);
	},
	
	//called on search of records
    handleSingleCheckboxClick : function(component,event,helper){
        helper.handleSingleCheckboxClick(component, event);
    },
    
    //called on search of records
    onSearchRecords : function(component,event,helper){
        helper.onSearchOrecords(component, event);
    },
    
    //called on click of checkmark(task2 icon) icon
    unSubscribe : function(component,event,helper){
        helper.unsubscribeFromWrapper(component,event);
    },
    
    /*
    //called on submitting
    handleSubmitClick : function(component,event,helper){
        helper.handleSubmitClick(component,event);
    },
    */
    
    //called on cancel click
    handleCancelClick : function(component, event, helper){
        helper.cancelOut(component,event);
    },
    
    // toggle remaining rows onclick of Fund Name for iterated items
    toggleRows : function(component, event, helper) {
		// first get the div element. by using aura:id
		var target = event.currentTarget;
        var panel1 = target.nextElementSibling;
        var panel2 = panel1.nextElementSibling;
        $A.util.toggleClass(panel1, "hideCell");
        $A.util.toggleClass(panel2, "hideCell");       
      }
})