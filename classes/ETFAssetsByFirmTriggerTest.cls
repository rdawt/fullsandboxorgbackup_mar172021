@isTest
public with sharing class ETFAssetsByFirmTriggerTest {

    @TestSetup
    public static void createTestData()
    {
		TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
		settings.isActive_ETFAssetsByFirm__c = TRUE;
		upsert settings custSettings__c.Id;

        Profile profile = [Select Id from profile where Name='System Administrator']; 
        User user = new User(firstname = 'Test', 
                lastName = 'User', 
                email = 'test@salesforce.com', 
                Username = 'etfAssetsByFirm@test.com',  
                ProfileId = profile.Id,
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US'
            ); 
        Insert user;

        Insurance_Flow_Users__c cs = new Insurance_Flow_Users__c();
        cs.Name='Test User';
        cs.User_Id__c = user.Id;
        Insert cs;

        Account[] acctList = new list<Account>();
        acctList.add(new Account(Name='Test Acct 1', channel__c = 'Institutional', BillingCountry = 'USA'));
        insert acctList;

        Fund__c fundAngl = new Fund__c(Name='Fallen Angel High Yield Bond ETF (ANGL)',Fund_Type__c='FI-ETF',Fund_Ticker_Symbol__c='ANGL');
        insert fundAngl; 
    }

    @isTest
    public static void  testOnBeforeInsert() {
        User u = [SELECT Id from User WHERE Name = 'Test User' limit 1];
        Account acc = [SELECT Id FROM Account limit 1];
        Fund__c fund = [SELECT Id FROM Fund__c limit 1];
        System.runAs(u){
            ETF_Assets_By_Firm__c etf = new ETF_Assets_By_Firm__c();
            etf.Name = 'ANGL';
            etf.Account__c = acc.Id;
            etf.SFDCFundID__c = fund.Id;
            etf.Asset_Date__c =  System.today();
            etf.Insurance_Flows__c = -12;
            etf.Notes__c = 'New Test';

            Test.startTest();
            Insert etf;
            Test.stopTest();
        }
        List<ETF_Assets_By_Firm__c> lstEtfs =[SELECT Id,Insurance_Flow_Opportunity__c,Opportunity_Flow_Owner__c,Unique_Key__c FROM ETF_Assets_By_Firm__c];
        if(lstEtfs.size() > 0){
            ETF_Assets_By_Firm__c etf = lstEtfs[0];
            System.assertEquals(etf.Insurance_Flow_Opportunity__c, true);
            System.assert(etf.Opportunity_Flow_Owner__c != null);
            System.assert(etf.Unique_Key__c != null);
        }
    }

    @isTest
    public static void  testOnBeforeUpdate() {
        User u = [SELECT Id from User WHERE Name = 'Test User' limit 1];
        Account acc = [SELECT Id FROM Account limit 1];
        Fund__c fund = [SELECT Id FROM Fund__c limit 1];
        ETF_Assets_By_Firm__c etf = new ETF_Assets_By_Firm__c();
        etf.Name = 'ANGL';
        etf.Account__c = acc.Id;
        etf.SFDCFundID__c = fund.Id;
        etf.Asset_Date__c =  System.today();
        etf.Insurance_Flows__c = -12;
        etf.Notes__c = 'New Test';
        Insert etf;

        System.runAs(u){
            Test.startTest();
            Update etf;
            Test.stopTest();
        }
        List<ETF_Assets_By_Firm__c> lstEtfs =[SELECT Id,Insurance_Flow_Opportunity__c,Opportunity_Flow_Owner__c,Unique_Key__c FROM ETF_Assets_By_Firm__c];
        if(lstEtfs.size() > 0){
            ETF_Assets_By_Firm__c etfRecord = lstEtfs[0];
            System.assertEquals(etfRecord.Insurance_Flow_Opportunity__c, true);
            System.assert(etfRecord.Opportunity_Flow_Owner__c != null);
            System.assert(etfRecord.Unique_Key__c != null);
        }
    }

}