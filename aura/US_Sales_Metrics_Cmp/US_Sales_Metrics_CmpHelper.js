({
    getOnLoadRecords: function (component, event, helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getOnLoadRecords");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                
                component.set("v.OwnersOptions", rtnVal.OwnersOptions);
                component.set("v.SelectedOwnersOption", rtnVal.SelectedOwnersOption);
                component.set("v.activityDateMin", rtnVal.activityDateMin);
                component.set("v.activityDateMax", rtnVal.activityDateMax);

                component.set("v.SelectedOwnersOption",helper.defaultSelectedValue(component, event, helper, component.get("v.OwnersOptions"), rtnVal.SelectedOwnersOption));
                var defaultOwners = component.get("v.SelectedOwnersOption");
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(defaultOwners.includes(itemcmp.get("v.name")))
                        itemcmp.set("v.checked",true);     
                } );

                // setting the owner check box to true                
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );

                helper.loadTableForSalesByFund_Active(component, event, helper);
                helper.loadChartForSalesByFund(component, event, helper);
                helper.loadTableForSalesByPortfolio(component, event, helper);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    getOnLoadRecordsETF : function(component,event,helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getOnLoadRecordsETF");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                
                component.set("v.OwnersOptionsETF", rtnVal.OwnersOptionsETF);
                component.set("v.SelectedOwnersOptionETF", rtnVal.SelectedOwnersOptionETF);
                component.set("v.activityDateMinETF", rtnVal.activityDateMinETF);
                component.set("v.activityDateMaxETF", rtnVal.activityDateMaxETF);

                component.set("v.SelectedOwnersOptionETF",helper.defaultSelectedValue(component, event, helper, component.get("v.OwnersOptionsETF"), rtnVal.SelectedOwnersOptionETF));
                var defaultOwners = component.get("v.SelectedOwnersOptionETF");
                var checkboxETF =  component.get("v.OwnersOptionsETF");
                for(var i =0 ; i<checkboxETF.length ; i++ ){

                }

                try{
                    component.find("ownerCheckboxETF").forEach( function(itemcmp) {
                        if(defaultOwners.includes(itemcmp.get("v.name")))
                            itemcmp.set("v.checked",true);     
                    } );
                 }catch(err){
                     console.log('ownerCheckboxETF not found');
                 }

                // setting the owner check box to true                
                component.set("v.SelectedOwnersOptionETF",helper.selectAll(component, event, helper, component.get("v.OwnersOptionsETF")));
                try{
                // select all checkbox                
                    component.find("ownerCheckboxETF").forEach( function(itemcmp) {
                        itemcmp.set("v.checked",true);     
                    } );
                }catch(err){
                    console.log('ownerCheckboxETF not found');
                }

                helper.loadChartForSalesByFund_ETF(component, event, helper);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    loadChartForSalesByFund : function(component, event, helper) 
    {
        debugger;
        component.set("v.showSpinner", true);
        var OwnerId = component.get("v.SelectedOwnersOption");
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");

        var action = component.get("c.getSalesByFundChart"); 
        action.setParams({
            "lstSelectedFund" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByFund",dataObj);
                helper.stackedChartForSalesByFund(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    stackedChartForSalesByFund : function(component,event,helper) {
        debugger;
        var jsonSalesByFund = component.get("v.dataSalesByFund");
        var barChartSalesByFund = JSON.parse(jsonSalesByFund);

        if(window.myBarSalesByFund && window.myBarSalesByFund !== null){
            window.myBarSalesByFund.destroy();
        }
        
        var ctxSalesByFund = document.getElementById('salesByFund').getContext('2d');
        window.myBarSalesByFund = new Chart(ctxSalesByFund, {
            type: 'bar',
            data: barChartSalesByFund,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return data.datasets[tooltipItems.datasetIndex].label + " $ " + helper.numFormatter(tooltipItems.yLabel, 2);
                        }
                    }
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: false//Changed by TD
                    }],
                    yAxes: [{
                        stacked: false,//Changed by TD
                        ticks: {
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) 
                            {
                                 return "$" + helper.numFormatter(value, 0);
                                // if(Math.abs(Number(value)) >= 1.0e+9)
                                //     return '$' + Math.abs(Number(value)) / 1.0e+9 + "B"
                                // else if(Math.abs(Number(value)) >= 1.0e+6) 
                                //     return '$' + Math.abs(Number(value)) / 1.0e+6 + "M"
                                // else if(Math.abs(Number(value)) >= 1.0e+3) 
                                //     return '$' + Math.abs(Number(value)) / 1.0e+3 + "K"
                            }
                        }   
                    }]
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },

    loadChartForSalesByFund_ETF : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var OwnerId = component.get("v.SelectedOwnersOptionETF");
        var activityDtMin = component.get("v.activityDateMinETF");
        var activityDtMax = component.get("v.activityDateMaxETF");

        var action = component.get("c.getSalesByFundChart_ETF"); 
        action.setParams({
            "lstSelectedFund" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByFund_ETF",dataObj);
                helper.stackedChartForSalesByFund_ETF(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    stackedChartForSalesByFund_ETF : function(component,event,helper) {
        var jsonSalesByFundETF = component.get("v.dataSalesByFund_ETF");
        var barChartSalesByFundETF = JSON.parse(jsonSalesByFundETF);
        console.log(barChartSalesByFundETF);

        if(window.myBarSalesByFundETF && window.myBarSalesByFundETF !== null){
            window.myBarSalesByFundETF.destroy();
        }

        var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};

        var ctxSalesByFundETF = document.getElementById('salesByFund_ETF').getContext('2d');
        window.myBarSalesByFundETF = new Chart(ctxSalesByFundETF, {
            type: 'bar',
            data: barChartSalesByFundETF,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return " $" +  helper.numFormatter(tooltipItems.yLabel,2);
                        }
                    }
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        ticks: {
                            fontSize : 12,
                            fontStyle : 'italic'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            fontSize : 12,
                            fontStyle : 'italic',
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) 
                            {
                                return "$"+helper.numFormatter(value, 0);
                                // if(Math.abs(Number(value)) >= 1.0e+9)
                                //     return '$' + Math.abs(Number(value)) / 1.0e+9 + "B"
                                // else if(Math.abs(Number(value)) >= 1.0e+6) 
                                //     return '$' + Math.abs(Number(value)) / 1.0e+6 + "M"
                                // else if(Math.abs(Number(value)) >= 1.0e+3) 
                                //     return '$' + Math.abs(Number(value)) / 1.0e+3 + "K"
                            }
                        }   
                    }]
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },

    defaultSelectedValue : function(component, event, helper, allOptions, defaultOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(defaultOptions.includes(allOptions[ele].value)){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  

 	selectAll : function(component, event, helper, allOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(allOptions[ele].value){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  
    
    addNewSelectedValue : function(component, event, helper, selectecoptions, newSelection){ 
        
        // if the value is not already present
        if(!selectecoptions.includes(newSelection)){
            selectecoptions.push(newSelection);
        }          
        return selectecoptions;
    },
    
    removeSelectedValue : function(component, event, helper, selectedOpts, removeEle){         
        // this will save the options already Selected
        var newSelectedOpts = [];
        
        // for each selected option set
        for(var ele in selectedOpts){
            
            if (selectedOpts[ele] != removeEle
               	&& selectedOpts[ele] != 'ALL'){                
                newSelectedOpts.push(selectedOpts[ele]);
            }
        }
		return newSelectedOpts;
    },

    numFormatter : function (labelValue,fixed) {
        if(fixed > 0){
            return Math.abs(Number(labelValue)) >= 1.0e+9
                ? (labelValue / 1.0e+9).toFixed(fixed) + "B" : Math.abs(Number(labelValue)) >= 1.0e+6 ? (labelValue / 1.0e+6).toFixed(fixed) + "M" :
                Math.abs(Number(labelValue)) >= 1.0e+3 ? (labelValue / 1.0e+3).toFixed(fixed) + "K" : labelValue;
                ;
        }else{
            return Math.abs(Number(labelValue)) >= 1.0e+9
                ? (labelValue / 1.0e+9) + "B" : Math.abs(Number(labelValue)) >= 1.0e+6 ? (labelValue / 1.0e+6) + "M"
                : Math.abs(Number(labelValue)) >= 1.0e+3 ? (labelValue / 1.0e+3) + "K" : labelValue;
        }    
    },

    loadTableForSalesByFund_Active : function(component,event,helper){
debugger;
        console.log('Calling Plain Report');
        
        var selectedFunds = component.get("v.SelectedOwnersOption");
        
        var action = component.get("c.getPlainReportforActiveFunds"); 
        action.setParams({
            "postingDateType" : component.get('v.selectedOption'),
            "lstSelectedFunds" : selectedFunds
            });    
        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") {                
                component.set('v.lstActiveFundsReport',response.getReturnValue());
                //-----------
                var resultData = response.getReturnValue();
                for (var i=0; i<resultData.length; i++ ) {
                    resultData[i]._children = resultData[i]['lstChildData']; 
                    
                    for(var j=0; j<resultData[i]._children.length; j++ ){
                        resultData[i]._children[j]._children = resultData[i]._children[j]['lstChildData'];
                        delete  resultData[i]._children[j].lstChildData; 
                    }
                    delete resultData[i].lstChildData; 
                }
                component.set('v.gridData', resultData);
                component.set('v.dataBasedOn',component.get('v.selectedOption'));
                //----------
                 helper.getMaxProcessed_ActiveFunds(component,event,helper);
            }
            else
            {
                console.log('>>>>>>>>Else Part>>>>>>>>>');
            }
        });
        $A.enqueueAction(action);
    },

    getMaxProcessed_ActiveFunds : function(component,event,helper){
        var action = component.get("c.getMAXProcessedDate_ActiveFunds"); 

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") {                
                component.set('v.maxProcessedDate_ActiveFunds',response.getReturnValue());
            }
            else
            {
                console.log('>>>>>>>>Else Part>>>>>>>>>');
            }
        });
        $A.enqueueAction(action);
    },

    loadTableForSalesByFund_ETF : function(component,event,helper){
        var action = component.get("c.getTableDataForSalesByFund_ETF"); 

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var resultMap= response.getReturnValue(); 
                var tabelSize = Math.ceil(Object.keys(resultMap).length / 2);
                var dataSet1 = new Array();
                var dataSet2 = new Array();
                
                for(var key in resultMap) { 
                    if(dataSet1.length < tabelSize){
                        dataSet1.push({AllocaionETF : key , amount : '$ '+helper.numFormatter(resultMap[key],2)});
                    }else{
                        dataSet2.push({AllocaionETF : key , amount : '$ '+helper.numFormatter(resultMap[key],2)});
                    }
                    
                }
                component.set('v.tableDataSet1',dataSet1);
                component.set('v.tableDataSet2',dataSet2);
            }
        });
        $A.enqueueAction(action);
    },

    loadTableForSalesByPortfolio : function(component,event,helper){
        var action = component.get("c.getTableDataForPortfolioNetSales"); 

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var resultMap= response.getReturnValue(); 
                var dataSet = new Array();
                
                for(var key in resultMap) { 
                    dataSet.push({portfolio : key , amount : '$ '+helper.numFormatter(resultMap[key],2)});
                }
                component.set('v.tableDataSet3',dataSet);
            }
        });
        $A.enqueueAction(action);
    }


    
})