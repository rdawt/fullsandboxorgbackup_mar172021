<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Composite_Key_On_Sub_Grp_Sub</fullName>
        <description>Set Composite Key On Subscription Group Subscription</description>
        <field>Composite_Key_ExternalId_SId_SGId__c</field>
        <formula>Subscription__c  &amp; &quot;_&quot; &amp;  Subscription_Group__c</formula>
        <name>Set Composite Key On Sub Grp Sub</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Subscription Group Subscription -Subscription Group Assigned</fullName>
        <actions>
            <name>Set_Composite_Key_On_Sub_Grp_Sub</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(isnull(  Subscription__c  ))  &amp;&amp; not(isnull(  Subscription_Group__c  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
