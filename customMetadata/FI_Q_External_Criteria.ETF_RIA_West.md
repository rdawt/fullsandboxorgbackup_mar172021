<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETF RIA West</label>
    <protected>false</protected>
    <values>
        <field>Contact_Mailing_State__c</field>
        <value xsi:type="xsd:string">WY,WI,WA,UT,TX,SD,OR,OK,NV,NM,NE,ND,MT,MO,MN,MI,LA,KS,IN,IL,ID,IA,HI,CO,CA,AZ,AR,AK</value>
    </values>
    <values>
        <field>Firm_Channel__c</field>
        <value xsi:type="xsd:string">RIA,Asset Manager</value>
    </values>
</CustomMetadata>
