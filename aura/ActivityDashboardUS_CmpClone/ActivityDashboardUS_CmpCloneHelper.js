({
    getOnLoadRecords : function(component,event,helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getOnLoadRecords");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                component.set("v.OwnersOptions", rtnVal.OwnersOptions);                
                component.set("v.SelectedOwnersOption", rtnVal.SelectedOwnersOption);
                component.set("v.activityTypeOptions", rtnVal.activityTypeOptions);                
                component.set("v.selectedActivityTypeOption", rtnVal.selectedActivityTypeOption);
                component.set("v.activityDateMin", rtnVal.activityDateMin);
                component.set("v.activityDateMax", rtnVal.activityDateMax);
                component.set("v.objCurrentUser", rtnVal.objCurrentUser);
                component.set("v.objMasterRegionUser", rtnVal.objMasterRegionUser);

                component.set("v.SelectedOwnersOption",helper.defaultSelectedValue(component, event, helper, component.get("v.OwnersOptions"), rtnVal.SelectedOwnersOption));
                var defaultOwners = component.get("v.SelectedOwnersOption");
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(defaultOwners.includes(itemcmp.get("v.name")))
                        itemcmp.set("v.checked",true);     
                } );

                component.set("v.selectedActivityTypeOption",helper.defaultSelectedValue(component, event, helper, component.get("v.activityTypeOptions"), rtnVal.selectedActivityTypeOption));
                var defaultActivity = component.get("v.selectedActivityTypeOption");
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    if(defaultActivity.includes(itemcmp.get("v.label")))
                        itemcmp.set("v.checked",true);     
                } );

                helper.loadCharts(component, event, helper);
                helper.loadChartForMonthWise(component, event, helper);
                helper.loadChartForUserWise(component, event, helper);

                

                /*
                // setting the owner check box to true                
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
                // setting the activity checkbox to true
                component.set("v.selectedActivityTypeOption",helper.selectAll(component, event, helper, component.get("v.activityTypeOptions")));
                
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                */
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    saveUserScoreWeightage : function(component,event,helper) {
        component.set("v.showSpinner", true);
        var objMasterRegionUser = component.get("v.objMasterRegionUser");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");
        
        var Conference_Call = component.get("v.Conference_Call");
        var Conference_Meeting = component.get("v.Conference_Meeting");
        var Meeting_Educational = component.get("v.Meeting_Educational");
        var Meeting_One_on_One = component.get("v.Meeting_One_on_One");
        var Meeting_Social = component.get("v.Meeting_Social");
        var Seminar_Workshop = component.get("v.Seminar_Workshop");
        var Webinar = component.get("v.Webinar");
        var Other = component.get("v.Other");

        var action = component.get("c.saveCurrentUserDetails");  
        action.setParams({
            "objParamUser" : objMasterRegionUser,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight,
            "Conference_Call" : Conference_Call,
            "Conference_Meeting" : Conference_Meeting,
            "Meeting_Educational" : Meeting_Educational,
            "Meeting_One_on_One" : Meeting_One_on_One,
            "Meeting_Social" : Meeting_Social,
            "Seminar_Workshop" : Seminar_Workshop,
            "Webinar" : Webinar,
            "Other" : Other,
        });
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                component.set("v.objMasterRegionUser", rtnVal);
                component.set("v.showSpinner", false);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    loadCharts : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        console.log('???????????In Load ChartactivityType???????????'+ activityType);
        var OwnerId = component.get("v.SelectedOwnersOption");
        console.log('??????????In Load ?OwnerId???????????'+ OwnerId);
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecordsByChannel"); 
        action.setParams({
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.data",dataObj);
                helper.stackedChart(component,event,helper);
                component.set("v.showSpinner", false);
                component.set("v.selectedActivityTypeOption", activityType);
                component.set("v.SelectedOwnersOption", OwnerId);
            }
        });
        $A.enqueueAction(action);
        
    },

    loadChartForMonthWise : function(component, event, helper) 
    {
        console.log('???????????calling load chanrt month wise???????????');
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        var OwnerId = component.get("v.SelectedOwnersOption");

        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecords_MonthWiseChart"); 
        action.setParams({
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataMonthWiseChart",dataObj);
                helper.stackedChartForMonthWise(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    loadChartForUserWise : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        var OwnerId = component.get("v.SelectedOwnersOption");
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecords_UserWiseChart"); 
        action.setParams({
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataUserWiseChart",dataObj);
                helper.stackedChartForUserWise(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    stackedChart : function(component,event,helper) {
        var jsonDataChannel = component.get("v.data");
        var barChartDataChannel = JSON.parse(jsonDataChannel);

        if(window.myBarChannel && window.myBarChannel !== null){
            window.myBarChannel.destroy();
        }
        
        var ctxChannel = document.getElementById('channelWiseChart').getContext('2d');
        window.myBarChannel = new Chart(ctxChannel, {
            type: 'bar',
            data: barChartDataChannel,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    stackedChartForMonthWise : function(component,event,helper) {
        var jsonDataMonth = component.get("v.dataMonthWiseChart");
        var barChartDataMonth = JSON.parse(jsonDataMonth);

        if(window.myBarSelectedMonth && window.myBarSelectedMonth !== null){
            window.myBarSelectedMonth.destroy();
        }
        
        var ctxSelectedMonth = document.getElementById('monthWiseChart').getContext('2d');
        window.myBarSelectedMonth = new Chart(ctxSelectedMonth, {
            type: 'bar',
            data: barChartDataMonth,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    stackedChartForUserWise : function(component,event,helper) {
        var jsonDataUser = component.get("v.dataUserWiseChart");
        var barChartDataUser = JSON.parse(jsonDataUser);

        if(window.myBarSelectedUser && window.myBarSelectedUser !== null){
            window.myBarSelectedUser.destroy();
        }
        
        var ctxSelectedUser = document.getElementById('userWiseChart').getContext('2d');
        window.myBarSelectedUser = new Chart(ctxSelectedUser, {
            type: 'bar',
            data: barChartDataUser,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },
    
    defaultSelectedValue : function(component, event, helper, allOptions, defaultOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(defaultOptions.includes(allOptions[ele].value)){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  

 	selectAll : function(component, event, helper, allOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(allOptions[ele].value){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  
    
    addNewSelectedValue : function(component, event, helper, selectecoptions, newSelection){ 
        
        // if the value is not already present
        if(!selectecoptions.includes(newSelection)){
            selectecoptions.push(newSelection);
        }          
        return selectecoptions;
    },
    
    removeSelectedValue : function(component, event, helper, selectedOpts, removeEle){         
        // this will save the options already Selected
        var newSelectedOpts = [];
        
        // for each selected option set
        for(var ele in selectedOpts){
            
            if (selectedOpts[ele] != removeEle
               	&& selectedOpts[ele] != 'ALL'){                
                newSelectedOpts.push(selectedOpts[ele]);
            }
        }
		return newSelectedOpts;
    }
 
    
})