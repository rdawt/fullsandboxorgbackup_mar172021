@isTest
private class activityCountTriggerTest {

    static testMethod void testeventCountTrigger() {
     Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
                sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            //create account
      TaskTriggerHandler eth = new TaskTriggerHandler();
       DMLException e = null;
    Set<ID> accIds = new Set<ID>();
    List<Contact> conlist = new List<Contact>();
    
    Account acc = new Account(Name = 'qwerpoiuadsf', BillingCountry='United States',Channel__c='Insurance');
         insert acc;
    Account acc1 = new Account(Name = 'lkjhgfda 987896', BillingCountry='United States',Channel__c='Insurance');
         insert acc1;
        Contact con = new Contact(LastName='xyz',FirstName='test',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States', email = 'con1@con.com');
        insert con;
        Contact con1 = new Contact(LastName='xyz11',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz', MailingCountry='United States',email = 'con1@con1.com');
        insert con1;
        Contact con2 = new Contact(LastName='xyz21',FirstName='test2',AccountId=acc1.Id,MailingStreet='xyz1', MailingCountry='United States',email = 'con1@con2.com');
        insert con2;
        Contact con3 = new Contact(LastName='xyz1',FirstName='test1',AccountId=acc.Id,MailingStreet='', MailingCountry='US',
          ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC',email = 'con1@con3.com');
        insert con3;
        Contact con4 = new Contact(LastName='xyz41',AccountId=acc.Id, MailingCountry='US',email = 'con1@con4.com');
        
        
         Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
    insert accx;
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
    insert conx;
    
    Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
    insert conx1;
 //005A00000032QAD
 
        List<Task> newTaskList = new List<Task>();
        string Comments;
    Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
    Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
    Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+con2.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
   
    //newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
    //                                    ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId));
    
    
    
    
    newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = conx.Id, WhatId = conx.AccountId, OwnerId = '005A0000005DhiSIAS',Glance__c=true,TimeTrade__c=true,status = 'Call Completed'));
    
    newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, OwnerId = conx.OwnerId));
    
     newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, OwnerId = '005A00000032QAD'));


                                        //System.DmlException: Insert failed. First exception on row 1; first error: FIELD_INTEGRITY_EXCEPTION, Assigned To ID: id value of incorrect type: 0032F00000NOAbFQAX: [OwnerId]
     //sv userid in sbx-005A0000005DhiSIAS
     
     newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, whoid=conx.id,OwnerId = '005A0000005DhiSIAS'));
                                        
    insert newTaskList;
    //3/29/2019
    
        AggregateResult[] tCounts;  
    if (newTaskList.size() != 0){   
        tCounts  =[select  whoid, count(id) taskCount
                                  from task
                                      where whoid != null and Marketing_Automation__c = false 
                                          group by whoid];//and CALENDAR_MONTH(CreatedDate) = 1 //ActivityDate
        system.debug('after agg. count- count is'+ tCounts);

     
     //AggregateResult[] eCounts  =[select  whoid, count(id) eventCount
       //                           from event task
         //                         where
           //                        (whoid in :ids and Marketing_Automation__c = false)
             //                      group by whoid,whoid];//   
                                                          
         list<Contact> conListt= new list<Contact>();                             
         for(AggregateResult forLoopCount : tCounts){
           contact parentTaskContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),TaskCounter__c =  integer.valueof(forLoopCount.get('taskCount')));
           conListt.add(parentTaskContact);
              
 
        }

    

        update conList;
    }
    //end 3/29/2019
    
    
       System.assert(e==null);
    }
}