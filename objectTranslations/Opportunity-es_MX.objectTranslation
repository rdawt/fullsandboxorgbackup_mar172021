<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Opportunity Consultant. --></label>
        <name>Account__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The actual funded amount, regardless of what the original opportunity amount was. If there is an expectation of additional funds, set the &quot;Amount&quot; value to the funded value and start a new Opportunity with the remaining amount. --></help>
        <label><!-- Amount Funded --></label>
        <name>Amount_Funded__c</name>
    </fields>
    <fields>
        <label><!-- Billing City --></label>
        <name>Billing_City__c</name>
    </fields>
    <fields>
        <label><!-- Billing State --></label>
        <name>Billing_State__c</name>
    </fields>
    <fields>
        <label><!-- Consultant Led? --></label>
        <name>Consultant_Led__c</name>
    </fields>
    <fields>
        <label><!-- Contact --></label>
        <name>Contact__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Create ETF Asset Flow Data --></label>
        <name>Create_ETF_Asset_Flow_Data__c</name>
    </fields>
    <fields>
        <label><!-- DB Competitor --></label>
        <name>DB_Competitor__c</name>
        <picklistValues>
            <masterLabel>Competitor A</masterLabel>
            <translation><!-- Competitor A --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor B</masterLabel>
            <translation><!-- Competitor B --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor C</masterLabel>
            <translation><!-- Competitor C --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Default fee, set at the Fund level, or if a Separately Managed Account, defaulted to 75 bps. --></help>
        <label><!-- Default Fee (bps) --></label>
        <name>Default_Fee_bps__c</name>
    </fields>
    <fields>
        <label><!-- ERISA? --></label>
        <name>ERISA__c</name>
    </fields>
    <fields>
        <label><!-- ETF Assets By Firms Id --></label>
        <name>ETF_Assets_By_Firms_Id__c</name>
    </fields>
    <fields>
        <label><!-- External ID --></label>
        <name>External_ID__c</name>
    </fields>
    <fields>
        <help><!-- The default fee is set at the fund level, or in the case of Separately Managed Account opportunities, it will be defaulted to 75bps.  If a different fee is to be used, set it here. --></help>
        <label><!-- Fee Override (bps) --></label>
        <name>Fee_Override_bps__c</name>
    </fields>
    <fields>
        <help><!-- Fee in basis points, based on the Default Fee, or the Fee Override if one is specified. --></help>
        <label><!-- Fees (bps) --></label>
        <name>Fees_bps__c</name>
    </fields>
    <fields>
        <label><!-- Fund --></label>
        <name>Fund__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Funding Percentage --></label>
        <name>Funding_Percentage__c</name>
    </fields>
    <fields>
        <help><!-- Projected first year fee revenue based on Amount * fee in basis points. --></help>
        <label><!-- Initial Annual Revenue --></label>
        <name>Initial_Annual_Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Number of Consultants --></label>
        <name>Number_of_Consultants__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Consultant --></label>
        <name>Opportunity_Consultant__c</name>
    </fields>
    <fields>
        <help><!-- As defined at the Firm/Branch level. --></help>
        <label><!-- Organization Type --></label>
        <name>Organization_Type__c</name>
    </fields>
    <fields>
        <help><!-- Contacts within the firm record. --></help>
        <label><!-- Primary Contact --></label>
        <name>Primary_Contact__c</name>
        <relationshipLabel><!-- Opportunities (Primary Contact) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Initial Annual Revenue times the probability %. --></help>
        <label><!-- Probability-Adjusted Revenue --></label>
        <name>Probability_Adjusted_Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Sales Priority Level --></label>
        <name>Sales_Priority_Level__c</name>
    </fields>
    <fields>
        <label><!-- Separately Managed Account? --></label>
        <name>Separately_Managed_Account__c</name>
    </fields>
    <fields>
        <help><!-- Primary Contact&apos;s Territory --></help>
        <label><!-- Territory --></label>
        <name>Territory__c</name>
    </fields>
    <fields>
        <label><!-- Won / Lost --></label>
        <name>Won_Lost__c</name>
    </fields>
    <fields>
        <help><!-- Primary Contact&apos;s Zone --></help>
        <label><!-- Zone --></label>
        <name>Zone__c</name>
    </fields>
    <layouts>
        <layout>Institutional Opportunity Layout</layout>
        <sections>
            <label><!-- Firm Information --></label>
            <section>Firm Information</section>
        </sections>
        <sections>
            <label><!-- Inflow &amp; Fee Information --></label>
            <section>Inflow &amp; Fee Information</section>
        </sections>
    </layouts>
    <layouts>
        <layout>US Institutional Opportunity Layout</layout>
        <sections>
            <label><!-- Firm Information --></label>
            <section>Firm Information</section>
        </sections>
        <sections>
            <label><!-- Inflow &amp; Fee Information --></label>
            <section>Inflow &amp; Fee Information</section>
        </sections>
    </layouts>
    <recordTypes>
        <description><!-- Tailored for International Institutional Team --></description>
        <label><!-- International Institutional --></label>
        <name>International_Institutional</name>
    </recordTypes>
    <recordTypes>
        <description><!-- Tailored for MVIS Team --></description>
        <label><!-- MVIS Team --></label>
        <name>MVIS_Team</name>
    </recordTypes>
    <recordTypes>
        <description><!-- Tailored for U.S. FAs/ RIAs Team --></description>
        <label><!-- U.S. Financial Advisors --></label>
        <name>U_S_Financial_Advisors</name>
    </recordTypes>
    <recordTypes>
        <description><!-- Tailored for US Institutional Team --></description>
        <label><!-- U.S. Institutional --></label>
        <name>U_S_Institutional</name>
    </recordTypes>
    <validationRules>
        <errorMessage><!-- Please specify the actual amount funded. --></errorMessage>
        <name>Must_Add_Amount_Funded</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You must add a Fund before moving an opportunity to stage 2 or above. --></errorMessage>
        <name>Must_add_Fund</name>
    </validationRules>
    <webLinks>
        <label><!-- Clone --></label>
        <name>Clone</name>
    </webLinks>
    <webLinks>
        <label><!-- Mass_Edit --></label>
        <name>Mass_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Mass_Update --></label>
        <name>Mass_Update</name>
    </webLinks>
</CustomObjectTranslation>
