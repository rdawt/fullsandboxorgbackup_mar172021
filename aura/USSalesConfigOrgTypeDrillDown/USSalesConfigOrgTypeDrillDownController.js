({
    handleInit : function(component, event, helper) {
        if(component.get('v.fundType') == 'Mutual Fund'){
            component.set('v.columns', [
                {label: 'Transaction Id', fieldName: 'transactionLink', type: 'url', 
                typeAttributes: {label: { fieldName: 'SFDC_Transaction_Id__c' }, target: '_blank'}},
                {label: 'Firm Name', fieldName: 'linkName', type: 'url', 
                typeAttributes: {label: { fieldName: 'AccountName' }, target: '_blank'} , sortable: true },
                {label: 'Firm Owner', fieldName: 'AccountOwner', type: 'text', sortable: true },
                {label: 'Fund Name', fieldName: 'Fund_Name__c', type: 'text'},
                {label: 'Fund Ticker', fieldName: 'fundTicker', type: 'text', initialWidth: 100},
                {label: 'Gross Amount', fieldName: 'Gross_Amount__c', type: 'currency' , sortable: true ,typeAttributes: { currencyCode: 'USD'}},
            ]);   
        }else if(component.get('v.fundType') == 'ETF'){

            if(component.get('v.drillDownType') == 'Firm'){
                component.set('v.columns', [
                    {label: 'Asset Date', fieldName: 'Asset_Date__c', type: 'date_local'},
                    {label: 'Fund Name', fieldName: 'Fund_Name__c', type: 'text'},
                    {label: 'SFDC Firm Name', fieldName: 'linkName', type: 'url', 
                    typeAttributes: {label: { fieldName: 'AccountName' }, target: '_blank'} , sortable: true },
                    {label: 'Channel', fieldName: 'firmChannel', type: 'text', sortable: true },
                    {label: 'Broadridge Firm Id', fieldName: 'Broadridge_Firm_Id__c', type: 'text'},
                    {label: 'Broadridge Firm Name', fieldName: 'Broadridge_Firm_Name__c', type: 'text',sortable: true },
                    {label: 'ETF AUM', fieldName: 'Fund_Asset_Balance__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'ETF Net Flow : YTD', fieldName: 'Gross_Amount__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'ETF Net Flow : Month', fieldName: 'Net_Flows_Last_Month__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                ]);   
            }else if(component.get('v.drillDownType') == 'Territory'){
                component.set('v.columns', [
                    {label: 'Asset Date', fieldName: 'Asset_Date__c', type: 'date_local'},
                    {label: 'Fund Name', fieldName: 'Fund_Name__c', type: 'text'},
                    {label: 'Territory', fieldName: 'Territory__c', type: 'text' , sortable: true ,},
                    {label: 'ETF AUM', fieldName: 'Fund_Asset_Balance__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'ETF Net Flow : YTD', fieldName: 'Gross_Amount__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'ETF Net Flow : Month', fieldName: 'Net_Flows_Last_Month__c', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                ]);   
            }
        }
    },

    handleSortSubData: function(cmp, event,helper) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var data = cmp.get('v.data');
        var cloneData = data.slice(0);
        cloneData.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.data', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBySub', sortedBy);
    }
})