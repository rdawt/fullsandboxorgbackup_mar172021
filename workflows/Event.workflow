<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_EventCustomLastModDate</fullName>
        <field>ActivityCustomLastModDate__c</field>
        <formula>now()</formula>
        <name>FU-EventCustomLastModDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_NOTES_Populate_Event_Summary_recap</fullName>
        <description>For Events</description>
        <field>Summary_Recap__c</field>
        <formula>IF (Summary_Recap__c &lt;&gt; NULL, &apos;Summary Recap: &apos; + Summary_Recap__c &amp; BR()+ &apos;Description: &apos; + Description &amp; BR(),
&apos;&apos;)</formula>
        <name>FU NOTES Populate Event Summary recap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_NOTES_Populate_Event_Summary_recap1</fullName>
        <field>Description</field>
        <formula>IF ((Summary_Recap__c &lt;&gt; NULL &amp;&amp; Description &lt;&gt; NULL),  &apos;Summary Recap: &apos; + Summary_Recap__c &amp; BR(), 
&apos;&apos;)</formula>
        <name>FU NOTES Populate Event Summary recap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_RIA_Event_Score</fullName>
        <field>RIA_Activity_Score__c</field>
        <formula>IF((IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot;
&amp;&amp; ISPICKVAL(Result__c, &apos;Held&apos;) 
&amp;&amp; Glance__c = True
&amp;&amp; CreatedBy.Id =  Owner:User.Id)
,20, 


IF((IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot;
&amp;&amp; ISPICKVAL(Result__c, &apos;Held&apos;) 
&amp;&amp; Glance__c = True
&amp;&amp; CreatedBy.Id =  Owner:User.Id)
,10,





IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 1000 &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 40, 
IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 1000 &amp;&amp; CreatedBy.Id !=  Owner:User.Id,18, 

IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 700 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 20, 
IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 700 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 12, 

IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 250 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 15, 
IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 250 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 10, 

IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 100 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 12, 
IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &gt;= 100 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 8, 

IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – ETF&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &lt;= 100 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 4, 
IF(IsTask = False &amp;&amp; CreatedBy.Internal_Sales_Score_Model__c = &quot;RIA – Active&quot; &amp;&amp; Account.DDB_Firm_Total_AUM__c &lt;= 100 &amp;&amp; NOT(ISPICKVAL(Account.Channel__c,&quot;&quot;)) &amp;&amp; CreatedBy.Id !=  Owner:User.Id, 2,
0 
))))))))))))</formula>
        <name>FU RIA Event Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FieldUpdate_Event_SumRecapCommentPreview</fullName>
        <field>Comments_Preview__c</field>
        <formula>Trim(Left(Description,250))</formula>
        <name>FieldUpdate-Event-SumRecapCommentPreview</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Report_Opt_Activity_Type_CF_update</fullName>
        <field>Report_Opt_Activity_Type_CF__c</field>
        <formula>Report_Optimization_Activity_Type__c</formula>
        <name>Report Opt Activity Type CF update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Firm_Channel_Primary_Contacts_CF</fullName>
        <field>Firm_Channel_Primary_Contacts_CF__c</field>
        <formula>Firm_Channel_Primary_Contacts_CF__c</formula>
        <name>Update Firm Channel Primary Contacts CF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject</fullName>
        <field>Subject</field>
        <formula>Text(Activity_Type__c)</formula>
        <name>Update Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Event Product Interest</fullName>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Event.Activity_Type__c</field>
            <operation>equals</operation>
            <value>Meeting,Call</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Event Summary recap</fullName>
        <actions>
            <name>FU_NOTES_Populate_Event_Summary_recap1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>For Events</description>
        <formula>Summary_Recap__c &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RIA Event Score</fullName>
        <actions>
            <name>FU_RIA_Event_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Internal_Sales_Score_Model__c</field>
            <operation>equals</operation>
            <value>RIA – ETF,RIA – Active</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Event Task</fullName>
        <actions>
            <name>Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Event.Activity_Type__c</field>
            <operation>equals</operation>
            <value>Office Visits,Conference Call,Email,Call,Webinar,Other,Meeting,Conference,Regional Meeting</value>
        </criteriaItems>
        <description>Updates event&apos;s Subject according to the Activity Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Report Opt Activity Type CF event</fullName>
        <actions>
            <name>Report_Opt_Activity_Type_CF_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Firm_Channel_Primary_Contacts_CF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.CreatedById</field>
            <operation>notEqual</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WorkFlowRule-Event-SummRecapUpdateForPreview</fullName>
        <actions>
            <name>FieldUpdate_Event_SumRecapCommentPreview</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Description &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
