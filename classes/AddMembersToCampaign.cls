global class AddMembersToCampaign implements Database.Batchable<sObject>,Database.Stateful {
	private final String campSubGroupId;
	private final String campId;
	global integer Summary;
	String query;
	global Integer strLimit;
	
	global AddMembersToCampaign(String campSubGroupId,String campId) {
		this.campId= campId;
		this.campSubGroupId= campSubGroupId;
		Summary=0;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		// Access initialState here
		list<String> lstSubId = new list<String>();
		string str = '';
		list<Campaign_Subscription_Group_Member__c> subGrpMemList = new list<Campaign_Subscription_Group_Member__c>();
		subGrpMemList = [SELECT s.Id, s.Name,S.Subscriber_Count__c,s.Campaign_Subscription_Group__c,s.Subscription__c 
							from Campaign_Subscription_Group_Member__c s Where s.Campaign_Subscription_Group__c=:campSubGroupId 
							limit:Limits.getLimitQueryRows()];
		for(Campaign_Subscription_Group_Member__c subs : subGrpMemList)
		{ 
			str = str + '\'' + subs.Subscription__c + '\',';
			lstSubId.add(subs.Subscription__c);
		}
		str = str.substring(0,str.length()-1);
		query = 'SELECT s.Id, s.Name,s.Subscription__c,s.Subscribed__c,s.Contact__c,s.Subscription_Unsubscribed_Date__c from Subscription_Member__c s ' + 
				'WHERE s.Subscribed__c= true and s.Subscription__c IN (' + str + ') Limit 50000';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, list<Subscription_Member__c> scope) {
		try
		{
			List<CampaignMember> insertCampMem = new List<CampaignMember>();
			Set<Id> existContIdSet =  new Set<Id>();
			list<String> conIds=new List<String>();
			for(Subscription_Member__c sm : scope)
				conIds.add(sm.Contact__c);
			
			list<CampaignMember> existCampMem = [Select Id, ContactId from CampaignMember where CampaignId =: campId];
			for(CampaignMember camp: existCampMem) 
				existContIdSet.add(camp.ContactId);
			
			for(String contactId: conIds) 
			{
				if(!(existContIdSet.contains(contactId))) 
				{
					if(ContactId!=null && campId!=null)
					{
						CampaignMember cmp=new CampaignMember();
						cmp.ContactId=contactId;
						cmp.CampaignId=campId;
						insertCampMem.add(cmp);
					}
				}
			}
			if(!(insertCampMem.isEmpty())) 
			{
				Database.SaveResult[] sr = Database.insert(insertCampMem,false); 
				Summary+=insertCampMem.size();
			} 
		}
		catch(Exception ex)
		{
			System.Debug('ex'+ex);
		}
	}

	global void finish(Database.BatchableContext BC) {

		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
		
		// Send an email to the Apex job's submitter notifying of job completion. 
		Campaign_Subscription_Group__c lstCSGId= [Select Id, Name from Campaign_Subscription_Group__c where Id =: campSubGroupId limit 1];
		Campaign lstCmpId = [Select Id, Name from Campaign where Id=:campId limit 1];

		//---------------------------------------------
		List<Id>lstSubId=new List<Id>();
		Set<Id> contactIdList = new Set<Id>();
		list<Campaign_Subscription_Group_Member__c> csgmList = [SELECT s.Id, s.Name,s.Campaign_Subscription_Group__c,s.Subscription__c,s.Subscription__r.Name 
										from Campaign_Subscription_Group_Member__c s Where s.Campaign_Subscription_Group__c=:campSubGroupId limit:Limits.getLimitQueryRows()];
		for(Campaign_Subscription_Group_Member__c subs : csgmList)
			lstSubId.add(subs.Subscription__c);

		list<Subscription_Member__c> subMemList = [SELECT s.Id, s.Name,s.Subscription__c,s.Subscribed__c,s.Contact__c,s.Subscription_Unsubscribed_Date__c 
										from Subscription_Member__c s Where s.Subscribed__c=:true and s.Subscription__c IN:lstSubId limit:Limits.getLimitQueryRows()];
		for(Subscription_Member__c subs : subMemList)
			contactIdList.add(subs.Contact__c);
		//---------------------------------------------

		//The email notification subject and the message text is drafted below and if neede to change we need to change here in the future..
		SendEmailNotification se = new SendEmailNotification('Populate Campaign Notification Status is ' + a.Status + ' for: ' +lstCmpId.Name);
		se.sendEmail('The Batch Apex Function (Populate Campaign from CSG) has used the Source Campaign-Subscription-Group: "' 
			+lstCSGId.Name + '" (' + Summary + ' subscribers )' + ' to populate the campaign: "' +  lstCmpId.Name + '."\n\n\n\nSystem Information \n'+  a.TotalJobItems + 
			' batches with '+ a.NumberOfErrors + ' failures.');
	}
}