global class BatchRecordsCleanUp implements Database.Batchable<sObject>{

    private final String queryString;

    global BatchRecordsCleanUp(String query) {
        this.queryString = query;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext bc, List<sObject> records) {
        try{
            System.debug('size : '+records.size());
            System.debug(records);
            //delete records;
        }catch (Exception ex){
            System.debug('BatchRecordsCleanUp-Exception ::: '+ex.getMessage());
        }

    }

    global void finish(Database.BatchableContext bc) {
        // Send notification email ?
    }


}