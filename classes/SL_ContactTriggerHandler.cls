/**
* @ClassName    : SL_ContactTriggerHandler 
* @JIRATicket   : 
* @CreatedOn    : 30th May 2019
* @CreatedBy    : Piyush Pathak
* @ModifiedBy   : 
* @Description  : Handler class after refactoring the trigger and moved all the logic here.
*/
public with sharing class SL_ContactTriggerHandler {    
    
    //Do all the logic for Before insert.
    public void onBeforeInsert(List<Contact> lstNewContact){
        ContactTriggerHandler handler = new ContactTriggerHandler();
        //DynamicEligibility dehandler = new DynamicEligibility();
        List<Contact> conList = new List<Contact>();
        for(Contact c : lstNewContact){
            if(c.Subscription_Group__c == null) conList.add(c);
        }
        
        if(!conList.isEmpty())
            //DynamicEligibility.handleEligibility(conList,'insert');
            try{        
                handler.sequoiaCalcualtions(lstNewContact);
            }
        catch(Exception ex){
            SendEmailNotification se = new SendEmailNotification('Error: Contact Trigger failed');
            se.sendEmail('Exception Occurred while updating Sequoia Fields : \n'+lstNewContact+ex.getMessage());
        }
        
        //Update Mailing Address.
        handler.contactRemoveLineBreak(lstNewContact);

        //Update Mailing Address.
        updateContactInformalName(lstNewContact);
    }
    
    //Logic for Before update.
    public void onBeforeUpdate(List<Contact> lstNewContact, Map<Id,Contact> mapOldContacts){
        Set<String> s = new Set<String>();
        Set<String> s1 = new Set<String>();
        ContactTriggerHandler handler = new ContactTriggerHandler();
        //DynamicEligibility dehandler = new DynamicEligibility();
        List<Contact> lstUpdateContactMailingaddress = new List<Contact>();
        
        // Handle Eligibility Only if the Account/Firm, Service Level, Owner, Mailing Country or DB Source is changed
        for(Contact objNewContact : lstNewContact){
            Contact objOldContact = mapOldContacts.get(objNewContact.Id);
            if(objNewContact.AccountId != objOldContact.AccountId 
               ||  objNewContact.Service_Level__c != objOldContact.Service_Level__c 
               ||  objNewContact.DB_Source__c != objOldContact.DB_Source__c 
               ||  objNewContact.OwnerId != objOldContact.OwnerId 
               || objNewContact.MailingCountry != objOldContact.MailingCountry){
                   s.add('update');
                   system.debug(Logginglevel.INFO,'\n--s--'+s);
               }
            
            //Update only if any of the Sequoia Field actually changed
            if(objNewContact.MailingCountry != NULL 
               && (objNewContact.Channel_NEW__c != objOldContact.Channel_NEW__c 
                   || (objNewContact.AccountId != objOldContact.AccountId 
                       && objNewContact.AUM__c != null) 
                   ||  objNewContact.ETF_PU__c != objOldContact.ETF_PU__c 
                   ||  objNewContact.QT__c != objOldContact.QT__c 
                   ||  objNewContact.COI__c != objOldContact.COI__c 
                   ||  objNewContact.RepPM__c != objOldContact.RepPM__c 
                   ||  objNewContact.YoYG__c != objOldContact.YoYG__c 
                   ||  objNewContact.NTA__c != objOldContact.NTA__c 
                   ||  objNewContact.MVEP__c != objOldContact.MVEP__c 
                   ||  objNewContact.VE_AtB__c != objOldContact.VE_AtB__c 
                   ||  objNewContact.FB__c != objOldContact.FB__c 
                   ||  objNewContact.AUM__c != objOldContact.AUM__c 
                   ||  objNewContact.YEB__c != objOldContact.YEB__c || objNewContact.OR__c != objOldContact.OR__c)){
                       
                       s1.add('update');
                       system.debug(Logginglevel.INFO,'\n--s1--'+s1);
                   }
            
            if(objNewContact.MailingCountry != NULL 
               && objNewContact.MailingStreet != objOldContact.MailingStreet){
                   
                   lstUpdateContactMailingaddress.add(objNewContact);
                   system.debug(Logginglevel.INFO,'\n--lstUpdateContactMailingaddress--'+lstUpdateContactMailingaddress);
               }               
        }
        
        if(!s.isEmpty()){
            //dehandler.handleEligibility(lstNewContact,'update');
            system.debug(Logginglevel.INFO,'\n--Here--');
        }
        
        try{
            if(!s1.isEmpty()){
                handler.sequoiaCalcualtions(lstNewContact);
                system.debug(Logginglevel.INFO,'\n--Here2--');
            }
        }
        catch(Exception ex){
            system.debug(Logginglevel.ERROR,'\n--ex.getMessage()--'+ex.getMessage());
            SendEmailNotification se = new SendEmailNotification('Error: Contact Trigger failed');
            se.sendEmail('Exception Occurred while updating Sequoia Fields : \n'+lstNewContact+ex.getMessage());
        }        
        
        //Update mailing address line break.
        handler.contactRemoveLineBreak(lstUpdateContactMailingaddress);   
        updateContactInformalName(lstNewContact); 
        system.debug(Logginglevel.INFO,'\n--Here3--');
        //createContactScoreTrackerForUpdate(lstNewContact,mapOldContacts);
        
    }
    
    
    
    //Logic for before delete.
    public void onBeforeDelete(List<Contact> lstOldContact){
        try{
            for (Contact objOldContact : lstOldContact){
                if (objOldContact.IsTestContact__c){
                    objOldContact.addError('You cannot delete this Contact - Please contact your Administrator for assistance.');
                }
            }
        }
        catch(Exception ex){
            SendEmailNotification se = new SendEmailNotification('Error: Contact Trigger Deletion failed');
        }
    }
    
    // logic for after insert.
    public void onAfterInsert(List<Contact> lstNewContact){
        
        updateAccounts(lstNewContact,NULL); //Commented on 25-Sept-2020
        // createContactScoreTrackerForInsert(lstNewContact);
    }
    
    
    //Logic for after update.
    public void onAfterUpdate(List<Contact> lstNewContact, Map<Id,Contact> mapOldContacts){
        
        Set<String> s = new Set<String>();
        // Handle Eligibility Only if the Account/Firm, Service Level, Owner, Mailing Country or DB Source is changed
        for(Contact objNewContact : lstNewContact){
            Contact objOldContact = mapOldContacts.get(objNewContact.Id);
            if(objNewContact.MailingCountry != null && (objNewContact.AccountId != objOldContact.AccountId || objNewContact.Service_Level__c != objOldContact.Service_Level__c ||
                                                        objNewContact.DB_Source__c != objOldContact.DB_Source__c|| objNewContact.OwnerId != objOldContact.OwnerId || objNewContact.MailingCountry != objOldContact.MailingCountry)){
                                                            s.add('update');
                                                        }
        }
        if(!s.isEmpty())
        {
            DynamicSubscription ds = new DynamicSubscription(lstNewContact,'contact');
            ds.handleSubscription();
        }
        updateAccounts(lstNewContact,mapOldContacts);
        
    } 
    
    //Logic for after delete.
    public void onAfterDelete(List<Contact> lstOldContact){
        updateAccounts(lstOldContact,NULL);    //Commented on 25-Sept-2020      
    }

    /*
        VanEck Communication ID = 005A0000008C61I
        Individual Investor - Retail ID = 001A000000xH900
        Default Registrant Firm ID = 001A000000u9gkE
        App User ID (Created By = App User) = 005A0000001vH2x
        App User ID (User) = 005A0000001vH2x
    */
    public void updateContactInformalName(List<Contact> lstNewContact)
    {
        system.debug('???????????????????' + lstNewContact);
        
        if(!lstNewContact.isEmpty())
        {
            for(Contact objContact : lstNewContact)
            {
                system.debug('???????????????????' + objContact.OwnerId);
                system.debug('???????????????????' + objContact.AccountId);
                system.debug('???????????????????' + objContact.Informal_Name__c);
                system.debug('???????????????????' + objContact.FirstName);
                system.debug('???????????????????' + objContact.CreatedById);
                system.debug('???????????????????' + UserInfo.getUserId());

                if(objContact.OwnerId == '005A0000008C61IIAS' &&
                (objContact.AccountId == '001A000000u9gkEIAQ' || objContact.AccountId == '001A000000xH900IAC') && 
                objContact.Informal_Name__c == null && String.isNotBlank(objContact.FirstName) && 
                objContact.CreatedById == '005A0000001vH2xIAE' &&  UserInfo.getUserId() == '005A0000001vH2xIAE')
                {
                    objContact.Informal_Name__c = objContact.FirstName;
                }
            }
        }
    }
    
    /**
* 
* Method name :  updateAccounts
* params      :  List of Contacts and Map of old Contacts.
* Description :  This method will call the method from the ContactTriggerHandler to update the Number of Contacts count on Account.
****/
    public void updateAccounts(List<Contact> lstcontacts,Map<Id, Contact> mapOldContacts){
        Set<ID> accIds = new Set<ID>();
        if(mapOldContacts != NULL){        
            
            // Only update the parent account on contact update if the contact's AccountId actually changed.        
            for(Contact objContact : lstcontacts){            
                
                if(objContact.AccountId != mapOldContacts.get(objContact.Id).AccountId){                
                    if (objContact.AccountId != null)                    
                        accIds.add(objContact.AccountId);                
                    
                    if (mapOldContacts.get(objContact.Id).AccountId != null)                    
                        accIds.add(mapOldContacts.get(objContact.Id).AccountId);            
                }        
            }    
        }   
        else {        
            for ( Contact objCon : lstcontacts ){ 
                if ( objCon.AccountId != null )                
                    accIds.add( objCon.AccountId );        
            }    
        }       
        // Update accounts if any need to be updated.    
        if ( !accIds.isEmpty() )        
            ContactTriggerHandler.updateContactCounts(accIds);
    }
    
    
    /**
*Method name :  createContactScoreTrackerForInsert
*params      :  List of New Contacts.
*Description :  This method is being called directly from Contact trigger on after insert.
**/
    public void createContactScoreTrackerForInsert(List<Contact> lstNewContacts){
        List<ContactScoreTracker__c> conScoreTrackList= new List<ContactScoreTracker__c>();
        for(Contact contactChanges:lstNewContacts){
            //userIds.add(contactChanges.lastmodifiedbyid);
            ContactScoreTracker__c contactScoreTracker;
            contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                              TrackingFieldLabelName__c = 'Has Portfolio Discretion' ,TrackingFieldAPIName__c = 'RepPM__c' ,
                                                              TrackingFieldNewValue__c  = contactChanges.RepPM__c, 
                                                              TrackingFieldPreviousValue__c = 'None', User__c =UserInfo.getUserid(), Comments__c = 'Contact is created fresh' );
            conScoreTrackList.add(contactScoreTracker );
            
            // Model Constructor/User,Model_Constructor_User__c
            
            contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                              TrackingFieldLabelName__c = 'Model Constructor/User' ,TrackingFieldAPIName__c = 'Model_Constructor_User__c' ,
                                                              TrackingFieldNewValue__c  = contactChanges.Model_Constructor_User__c, 
                                                              TrackingFieldPreviousValue__c = 'None', User__c =UserInfo.getUserid(), Comments__c = 'Contact is created fresh' );
            conScoreTrackList.add(contactScoreTracker );
            
            //Investment Perspective,Investment_Perspective__c
            
            contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                              TrackingFieldLabelName__c = 'Investment Perspective' ,TrackingFieldAPIName__c = 'Investment_Perspective__c' ,
                                                              TrackingFieldNewValue__c  = contactChanges.Investment_Perspective__c, 
                                                              TrackingFieldPreviousValue__c = 'None', User__c =UserInfo.getUserid(), Comments__c = 'Contact is created fresh' );
            conScoreTrackList.add(contactScoreTracker );
            
            //Outside Research 
            //Outside Research,Outside_Research__c
            
            contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                              TrackingFieldLabelName__c = 'Outside Research' ,TrackingFieldAPIName__c = 'Outside_Research__c' ,
                                                              TrackingFieldNewValue__c  = contactChanges.Outside_Research__c, 
                                                              TrackingFieldPreviousValue__c = 'None', User__c =UserInfo.getUserid(), Comments__c = 'Contact is created fresh' );
            conScoreTrackList.add(contactScoreTracker );
            
            //Outside Research Fill-in;OR__c
            contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                              TrackingFieldLabelName__c = 'Outside Research Fill-in' ,TrackingFieldAPIName__c = 'OR__c' ,
                                                              TrackingFieldNewValue__c  = contactChanges.OR__c, 
                                                              TrackingFieldPreviousValue__c = 'None', User__c = UserInfo.getUserid(), Comments__c = 'Contact is created fresh' );
            conScoreTrackList.add(contactScoreTracker );
            
        }
        if (conScoreTrackList.size() != 0) {          
            insert conScoreTrackList;
        }
    }
    
    /**
*Method name :  createContactScoreTrackerForUpdate
*params      :  List of New Contacts and Map of Old contacts.
*Description :  This method is being called directly from Contact trigger on before update.
**/
    public void createContactScoreTrackerForUpdate(List<Contact> lstNewContacts, Map<Id,Contact> mapOldContacts){
        List<ContactScoreTracker__c> conScoreTrackList= new List<ContactScoreTracker__c>();
        for(Contact contactChanges:lstNewContacts){
            //Portfolio discretion
            if(contactChanges.RepPM__c != mapOldContacts.get(contactChanges.Id).RepPM__c){
                ContactScoreTracker__c contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                                                         TrackingFieldLabelName__c = 'Has Portfolio Discretion' ,TrackingFieldAPIName__c = 'RepPM__c' ,
                                                                                         TrackingFieldNewValue__c  = contactChanges.RepPM__c, 
                                                                                         TrackingFieldPreviousValue__c = mapOldContacts.get(contactChanges.Id).RepPM__c, User__c =UserInfo.getUserid(),Comments__c = 'Portfolio Discreation Field is Modified' );
                conScoreTrackList.add(contactScoreTracker );
            }
            // Model Constructor/User,Model_Constructor_User__c
            if(contactChanges.Model_Constructor_User__c != mapOldContacts.get(contactChanges.Id).Model_Constructor_User__c){
                ContactScoreTracker__c contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                                                         TrackingFieldLabelName__c = 'Model Constructor/User' ,TrackingFieldAPIName__c = 'Model_Constructor_User__c' ,
                                                                                         TrackingFieldNewValue__c  = contactChanges.Model_Constructor_User__c, 
                                                                                         TrackingFieldPreviousValue__c = mapOldContacts.get(contactChanges.Id).Model_Constructor_User__c, User__c =UserInfo.getUserid(),Comments__c = 'Model Constructor/User Field is Modified' );
                conScoreTrackList.add(contactScoreTracker );
            }
            //Investment Perspective,Investment_Perspective__c
            if(contactChanges.Investment_Perspective__c != mapOldContacts.get(contactChanges.Id).Investment_Perspective__c){
                ContactScoreTracker__c contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                                                         TrackingFieldLabelName__c = 'Investment Perspective' ,TrackingFieldAPIName__c = 'Investment_Perspective__c' ,
                                                                                         TrackingFieldNewValue__c  = contactChanges.Investment_Perspective__c, 
                                                                                         TrackingFieldPreviousValue__c = mapOldContacts.get(contactChanges.Id).Investment_Perspective__c, User__c =UserInfo.getUserid(),Comments__c = 'Investment Perspective Field is Modified' );
                conScoreTrackList.add(contactScoreTracker );
            }
            //Outside Research 
            //Outside Research,Outside_Research__c
            if(contactChanges.Outside_Research__c != mapOldContacts.get(contactChanges.Id).Outside_Research__c){
                ContactScoreTracker__c contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                                                         TrackingFieldLabelName__c = 'Outside Research' ,TrackingFieldAPIName__c = 'Outside_Research__c' ,
                                                                                         TrackingFieldNewValue__c  = contactChanges.Outside_Research__c, 
                                                                                         TrackingFieldPreviousValue__c = mapOldContacts.get(contactChanges.Id).Outside_Research__c, User__c =UserInfo.getUserid(),Comments__c = 'Outside Research Field is Modified' );
                conScoreTrackList.add(contactScoreTracker );
            }
            //userIds.add(contactChanges.lastmodifiedbyid);
            //Outside Research Fill-in;OR__c
            if(contactChanges.OR__c != mapOldContacts.get(contactChanges.Id).OR__c){
                ContactScoreTracker__c contactScoreTracker = new ContactScoreTracker__c (Contact__c = contactChanges.Id, 
                                                                                         TrackingFieldLabelName__c = 'Outside Research Fill-in' ,TrackingFieldAPIName__c = 'OR__c' ,
                                                                                         TrackingFieldNewValue__c  = contactChanges.OR__c, 
                                                                                         TrackingFieldPreviousValue__c = mapOldContacts.get(contactChanges.Id).OR__c, User__c =UserInfo.getUserid(), Comments__c = 'Outside Research Fill-in Field is Modified' );
                conScoreTrackList.add(contactScoreTracker );
            }
        }
        if (conScoreTrackList.size() != 0) {          
            insert conScoreTrackList;
        }
    }
    
}