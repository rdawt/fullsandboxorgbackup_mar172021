@isTest
private class Batch_UpdatePayeeSummaryBatchCrt_Test 
{
    //Testing from User 1 for 
    @isTest 
    static void testPayeeSummaryBatch_User1() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
        
        Test.startTest();
        
        String jobId = Database.executeBatch(new Batch_UpdatePayeeSummaryBatchCriteria(), 1);
        
        Test.stopTest();
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_ExceptionQuery() 
    {
        SL_TestDataFactory.createTestDataForCustomSettingSentEmail();
        Test.startTest();
        String jobId = Database.executeBatch(new Batch_UpdatePayeeSummaryBatchCriteria(), 1);
        Test.stopTest();
    }
    
}