/*
    Author:         Srikanth Suravaram
    Description:    Class that manages Fund Interest of a Contact. Similar to Subscription Management
    
    Modified By:    Vidhya Krishnan
    Description:    Unwanted Code copy from Subscription Management Deleted.
                    Exception & Error Message on VF Page Handling Included.
                    Repeated update of none interest value is fixed.
                    Account Value is added to the new Fund Interest insert.
                    Removed the hardcoded value of Interest Level & fetched it from Database
                    getFundTypeOptions() not used anymore - to make use of custom Record Types
                    Manage Fund Interest Page & its JS altered according to user's requirements
*/
global class FundInterestManagement
{
    private string conId;
    private string accId;
    public string strFirstName{get;set;}
    public string strLastName{get;set;}
    public List<RecordSet> lstRSet = new List<RecordSet>();
    
    public FundInterestManagement(){}
    public FundInterestManagement(ApexPages.StandardController controller) {
        try{
            Contact con=[Select Id,FirstName,LastName,AccountId from Contact Where Id=:ApexPages.currentPage().getParameters().get('cId') LIMIT 1];
            strFirstName=con.FirstName;
            strLastName=con.LastName;
            conId = con.Id;
            accId = con.AccountId;      
        }
        catch(Exception e){
            //To handle invalid or deleted contact id or the page is loaded with no contact id provided
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'ERROR: Invalid Contact Id.');
            ApexPages.addMessage(msg);
        }
    }

    public List<SelectOption> getIntLevelOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        for(Schema.PicklistEntry ple: Schema.getGlobalDescribe().get('Fund_Interest__c').getDescribe().fields.getMap().get('Level_Of_Interest__c').getDescribe().getPicklistValues()) {  
            options.add(new SelectOption(ple.getValue(), ple.getLabel()));  
        }
        return options;
    }
 // public List<SelectOption> getFundClientOptions() {
 //       List<SelectOption> options = new List<SelectOption>();
  //      options.add(new SelectOption('--None--','--None--'));
  //      for(Schema.PicklistEntry ple: Schema.getGlobalDescribe().get('Fund_Interest__c').getDescribe().fields.getMap().get('Fund_Client__c').getDescribe().getPicklistValues()) {  
   //         options.add(new SelectOption(ple.getValue(), ple.getLabel()));  
  //      }
   //     return options;
   // }
    /* Method is not used in VF page. But has some useful codes. Please do not delete it.   */
    /* public List<SelectOption> getFundTypeOptions() {
        Schema.DescribeSObjectResult R = Fund__c.SObjectType.getDescribe();
        Schema.DescribeFieldResult F = Fund__c.Fund_Type__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        Object O = F.getDefaultValue();
        List<Schema.RecordTypeInfo> fndRecTypes = R.getRecordTypeInfos();
        Map<ID, Schema.RecordTypeInfo> fndRecTypesByID = R.getRecordTypeInfosByID();
        Map<String, Schema.RecordTypeInfo> fndRecTypesByName = R.getRecordTypeInfosByName();
        Map<String, Fund__c> mapChanges = new Map<String, Fund__c>();

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('all','Show All'));
        List<Fund__c> lstFund = new List<Fund__c>();
        try{
            system.debug('List of Fund Types111111......'+Fund__c.Fund_Type__c.getDescribe().getPicklistValues());
            List<RecordType> rt = [select id,name from recordtype where sobjecttype=:'Fund__c'];
            system.debug('List of Record Types........'+rt);
            //system.debug('Fund Record set Result......'+R);
            system.debug('Profiles Available Record Type.....'+Fund__c.RecordTypeId);
            system.debug('Fund Type Values......'+P);
            system.debug('Fund Type Default......'+O);
            //system.debug('List of Record Type Info.......'+fndRecTypes);
            //system.debug('List of Record Type Info By Id.......'+fndRecTypesByID);
            system.debug('List of Record Type Info By Name.......'+fndRecTypesByName);
            RecordTypeInfo rt1;
            for(Id s : fndRecTypesByID.keySet()){
                system.debug('record type id.......'+s);
                rt1 = fndRecTypesByID.get(s);
                system.debug('deault is true?????????'+rt1.isDefaultRecordTypeMapping());
                if(rt1.isDefaultRecordTypeMapping() && rt1.isAvailable()){
                    // This will display all the types even if no Fund record exist in that particular type
                    for(Schema.PicklistEntry ple: Schema.getGlobalDescribe().get('Fund__c').getDescribe().fields.getMap().get('Fund_Type__c').getDescribe().getPicklistValues()) {
                        if(ple.isActive())
                            options.add(new SelectOption(ple.getValue(), ple.getLabel()));  
                    }
                }
            }
            /****** 
            // Code provided by Sri that looks Fund data/records to get the Fund Type
            
            lstFund = [SELECT F.Id, F.Fund_Type__c FROM Fund__c F WHERE F.Fund_Status__c = 'Open' and F.Fund_Type__c != 'MF Share Class' order by F.Fund_Type__c];
            if(!lstFund.isEmpty())
            {
                for(Fund__c fnd: lstFund)
                {
                    if(mapChanges.containsKey(fnd.Fund_Type__c) == false)
                    {
                        //rs.Fund_Type_Trimmed = sgs.Fund_Type__c != null ? sgs.Fund_Type__c.replaceAll(' ','').replaceAll('-','').replaceAll('/','') : sgs.Fund_Group__c;
                         mapChanges.put(fnd.Fund_Type__c, fnd);
                         options.add(new SelectOption(fnd.Fund_Type__c.replaceAll(' ','').replaceAll('-','').replaceAll('/',''),fnd.Fund_Type__c));
                    }
                }
            }****
            return options;
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Exception Occured due to Invalid Fund Data (open Fund with NO Fund Type defined).');
            ApexPages.addMessage(msg);
            return options;
        }
    }   */
    public List<Recordset> GetFundInterest()
    {
        try{
        List<Fund__c> lstFund = new List<Fund__c>();
        /*  Hard Coded Value of Fund Type is used in where clause   */
        lstFund = [SELECT F.Id, F.Name, F.Fund_Type__c, F.Fund_Group__c, F.Fund_Ticker_Symbol__c FROM Fund__c F WHERE F.Fund_Status__c = 'Open' and F.Fund_Type__c != 'MF_Share_Class' order by F.Name];
        system.debug('@@@@@@@@lstFund'+lstFund);
        List<Fund_Interest__c> lstFI = [SELECT FI.Contact__c, FI.Fund__c, FI.Level_Of_Interest__c,FI.Fund_Client__c FROM Fund_Interest__c FI where FI.Contact__c =: conId];      
    system.debug('@@@@@@@@lstFI'+lstFI);
        if(!lstFund.isEmpty())
        {
            if(!lstRSet.isEmpty()) 
                lstRSet.clear();
            for(Fund__c fnd: lstFund)
            {
                RecordSet rs = new RecordSet();
                rs.Fund_Name = fnd.Name;
                rs.Fund_Type= fnd.Fund_Type__c; 
                //rs.Fund_Type_Trimmed = fnd.Fund_Type__c != null ? fnd.Fund_Type__c.replaceAll(' ','').replaceAll('-','').replaceAll('/','') : fnd.Fund_Group__c; 
                rs.Fund_Ticker_Symbol = fnd.Fund_Ticker_Symbol__c;
                rs.FundId = fnd.Id;
                rs.ReConfirmedCheckBoxRendered = False; // Disable checkbox if no FundInterest record Exists
                //rs.FundClientCheckBoxRendered = False; // Disable checkbox if no FundInterest record Exists
                system.debug('@@@@@@@@fnd.Id'+fnd.Id);
                for(Fund_Interest__c fi: lstFI)
                {
                    if(fi.Fund__c == fnd.Id)  
                    {
                        rs.ReConfirmedCheckBoxRendered = True; 
                        rs.FundInterestId = fi.Id;
                        rs.Level_Of_Interest = fi.Level_Of_Interest__c;
                        rs.FundClientCheckBoxRendered = fi.Fund_Client__c;
                    }
                }
                lstRSet.add(rs);
            }
            if(!lstRSet.isEmpty())
                return lstRSet;
        }
        return null;
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: '+e.getMessage());
            ApexPages.addMessage(msg);
            return null;
        }
    }

    public PageReference setFundInterests()  //This is what is called on the submit button click
    {
        Map<String, Fund_Interest__c> mapChanges = new Map<String, Fund_Interest__c>();
        List<Fund_Interest__c> lstRecInsert = new List<Fund_Interest__c>();
        List<Fund_Interest__c> lstRecUpdate = new List<Fund_Interest__c>();
        List<Fund_Interest__c> lstRec=[SELECT FI.Id, FI.Contact__c, FI.Fund__c, FI.Level_Of_Interest__c, FI.Fund_Client__c FROM Fund_Interest__c FI where FI.Contact__c =: conId];
        for(Fund_Interest__c fic : lstRec)
            mapChanges.put(fic.Fund__c, fic);

        for(RecordSet rs : lstRSet)
        {
            if(rs.Level_Of_Interest==null){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                'Error: Table Sorting is not Compatable with your Browser. If possible try in different version of browser if you need sorting functionality. Else refresh/reload the page and try again without sorting any columns. You can use the Filter & CNTL-F to find the fund you need.');
                ApexPages.addMessage(msg);
                return null;
            }
            
           
            
            /*  Hard Coded value of Level of Interest is used in this check - so if the value of Level of Interest Picklist is changed, this has to be changed  */
            else if(rs.Level_Of_Interest=='Interested' || rs.Level_Of_Interest=='NOT Interested' || rs.Confirmed==true || rs.FundClientCheckBoxRendered==true )
            {
                if(mapChanges.containsKey(rs.FundId))
                {
                    Fund_Interest__c fiUpdate = mapChanges.get(rs.FundId);
                    
                    if(fiUpdate.Level_Of_Interest__c != rs.Level_Of_Interest || rs.Confirmed==true || fiUpdate.Fund_Client__c != rs.FundClientCheckBoxRendered)
                    { // update only if reconfirmed or the previous value changed
                    
                        fiUpdate.Fund_Client__c = rs.FundClientCheckBoxRendered;
                        fiUpdate.Level_Of_Interest__c = rs.Level_Of_Interest;
                        
                        //fiUpdate.Fund_Client__c = false;
                        lstRecUpdate.add(fiUpdate);
                    }
                }
               
             
                else
                { // insert
                    Fund_Interest__c fiInsert = new Fund_Interest__c();
                    fiInsert.Contact__c = conId;
                    fiInsert.Fund__c = rs.FundId;
                    fiInsert.Level_Of_Interest__c = rs.Level_Of_Interest;
                    fiInsert.Account__c = accId;
                    fiInsert.Fund_Client__c = rs.FundClientCheckBoxRendered;
                    //rs.Fund_Client__c;
                    lstRecInsert.add(fiInsert);
                }               
            }
            else
            { // Level of Interested = None
                if(mapChanges.containsKey(rs.FundId))
                {
                    Fund_Interest__c fiUpdate = mapChanges.get(rs.FundId);
                    if(fiUpdate.Level_Of_Interest__c == null && rs.Confirmed==true){
                        // if previously updated null
                        fiUpdate.Level_Of_Interest__c = rs.Level_Of_Interest;
                        
                        lstRecUpdate.add(fiUpdate);
                    }
                    else if(fiUpdate.Level_Of_Interest__c != rs.Level_Of_Interest || rs.Confirmed==true){
                        // updating interested/not_interested to None
                        fiUpdate.Level_Of_Interest__c = rs.Level_Of_Interest;
                          
                        lstRecUpdate.add(fiUpdate);
                    
                    }
                     else if(fiUpdate.Fund_Client__c != rs.FundClientCheckBoxRendered ){
                         
                         fiUpdate.Fund_Client__c = rs.FundClientCheckBoxRendered;
                         lstRecUpdate.add(fiUpdate);
                     }
                }
            }
        }
        if(!lstRecUpdate.isEmpty())
        { 
            Database.SaveResult[] sr = Database.update(lstRecUpdate,false);
            for(Database.SaveResult srRec : sr)
            {
                if(!srRec.isSuccess())
                {
                    Database.Error err = srRec.getErrors()[0];
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error occured while Updating the Fund Interest Record. '+err.getMessage());
                    ApexPages.addMessage(msg);
                    return null;
                }
            }
        }
        if(!lstRecInsert.isEmpty())
        { 
            Database.SaveResult[] sr = Database.Insert(lstRecInsert,false);
            for(Database.SaveResult srRec : sr)
            {
                if(!srRec.isSuccess())
                {
                    Database.Error err = srRec.getErrors()[0];
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error occured while Adding the Fund Interest Record. '+err.getMessage());
                    ApexPages.addMessage(msg);
                    return null;
                }
            }
        }
        
        PageReference contPage = new PageReference('/'+conId);
        return contPage;
    }
    public PageReference cancelRecord()
    {
        PageReference contPage = new PageReference('/'+conId);
        return contPage;
    }

    public class Recordset
    {
        public Boolean Confirmed { get; set; } //not stored in db - everytime it shd be false while loading the page - updated from VF page only
        public Boolean ReConfirmedCheckBoxRendered { get; set; }        
        public String FundInterestId {get; set;}
        public Boolean FundClientCheckBoxRendered { get; set; }
        public string Level_Of_Interest {get; set;}
        public String FundId {get; set;}  
        public String Fund_Name {get;set;}
        public String Fund_Type {get;set;}
        public String Fund_Ticker_Symbol{get;set;}
        //public String Fund_Group {get;set;}
        //public String Fund_Type_Trimmed {get;set;} // not needed as long as the values in Fund Type field has special characters & extra spaces
    }
    
    public string updateContact(Set<Id> conIdList)
    {
        Map<String,string> etfFundClientMap = new Map<String,string>(); // To store ETF & ETN -Fund Client
        Map<String,string> mfFundClientMap = new Map<String,string>(); // To store MF -Fund Client
        Map<String,string> etfPosMap = new Map<String,string>(); // To store ETF & ETN +ve interest
        Map<String,string> etfNegMap = new Map<String,string>(); // To store ETF & ETN -ve interest
        Map<String,string> mfPosMap = new Map<String,string>(); // To store MF,GI,FI_ETF,SEP,HEDGE,WWIT and other +ve interest
        Map<String,string> mfNegMap = new Map<String,string>(); // To store MF,GI,FI_ETF,SEP,HEDGE,WWIT and other -ve interest
        List<Contact> conList =new List<Contact>();
        String strIndr1,strIndr2,strIndr3,strIndr4,s1,s2,s3,s4,error;
        String mfClientStr,etfClientStr,s5,s6;
        try{
            /*  For updating the Fund Interest fields of Contact */
            List<Fund_Interest__c> fndIntTicker = [Select fi.Contact__c, fi.Level_Of_Interest__c, fi.Fund__r.Fund_Ticker_Symbol__c,fi.Fund_Client__c, fi.Fund__r.Fund_Type__c from Fund_Interest__c fi 
                                                    where Contact__c != null and Contact__c =: conIdList 
                                                    order by fi.Contact__c, fi.Fund__r.Fund_Ticker_Symbol__c];
            for(String strId : conIdList){
                // To avoid Too Many Script Statements - exit the for loop when only 10000 script statements left
                if ((200000 - Limits.getScriptStatements()) < 11000)
                    break;
                
                strIndr1 = ''; strIndr2 = ''; strIndr3 = ''; strIndr4 = '';
                mfClientStr= ''; etfClientStr= '';
                for(Fund_Interest__c fi : fndIntTicker){
                    s1 = ''; s2 = ''; s3 = ''; s4 = '';
                    s5 = ''; s6 = '';
                    if(fi.Contact__c == strId){
                        /*  Hard Coded value of Level of Interest is used in this check - so if the value of Level of Interest Picklist is changed, this has to be changed  */
                        if(fi.Level_Of_Interest__c == 'Interested'){
                            if(fi.Fund__r.Fund_Ticker_Symbol__c == null)
                                s1 = '';
                            else{
                                /*  Hard Coded value of Fund Type is used in this check - so if the value of Fund Type Picklist is changed, this has to be changed  */
                                if(fi.Fund__r.Fund_Type__c == 'ETF' || fi.Fund__r.Fund_Type__c == 'ETN' || fi.Fund__r.Fund_Type__c == 'FI_ETF' )
                                    s1 = fi.Fund__r.Fund_Ticker_Symbol__c;
                                else // all other fund type
                                    s2 = fi.Fund__r.Fund_Ticker_Symbol__c;
                            }
                            if(strIndr1.equals(''))
                                strIndr1 = s1;
                            else{
                                if(!(strIndr1.contains(s1)))
                                    strIndr1 = strIndr1 + ',' + s1;
                            }
                            if(strIndr2.equals(''))
                                strIndr2 = s2;
                            else{
                                if(!(strIndr2.contains(s2)))
                                    strIndr2 = strIndr2 + ',' + s2;
                            }
                        }
                        /*  Hard Coded value of Level of Interest is used in this check - so if the value of Level of Interest Picklist is changed, this has to be changed  */
                        else if(fi.Level_Of_Interest__c == 'NOT Interested'){
                            if(fi.Fund__r.Fund_Ticker_Symbol__c == null)
                                s3 = '';
                            else{
                                if(fi.Fund__r.Fund_Type__c == 'ETF' || fi.Fund__r.Fund_Type__c == 'ETN' || fi.Fund__r.Fund_Type__c == 'FI_ETF')
                                    s3 = fi.Fund__r.Fund_Ticker_Symbol__c;
                                else
                                    s4 = fi.Fund__r.Fund_Ticker_Symbol__c;
                            }
                            if(strIndr3.equals(''))
                                strIndr3 = s3;
                            else{
                                if(!(strIndr3.contains(s3)))
                                    strIndr3 = strIndr3 + ',' + s3;
                            }
                            if(strIndr4.equals(''))
                                strIndr4 = s4;
                            else{
                                if(!(strIndr4.contains(s4)))
                                    strIndr4 = strIndr4 + ',' + s4;
                            }
                        }
                           /*  Fund Client Processing begings  */
                        if(fi.Fund_Client__c == true){
                            if(fi.Fund__r.Fund_Ticker_Symbol__c == null)
                                s5 = '';
                            else{
                                //etf
                                if(fi.Fund__r.Fund_Type__c == 'ETF' || fi.Fund__r.Fund_Type__c == 'ETN' || fi.Fund__r.Fund_Type__c == 'FI_ETF')
                                    s5 = fi.Fund__r.Fund_Ticker_Symbol__c;
                                //mf
                                else
                                    s6 = fi.Fund__r.Fund_Ticker_Symbol__c;
                            }
                            if(etfClientStr.equals(''))
                                etfClientStr= s5;
                            else{
                                if(!(etfClientStr.contains(s5)))
                                    etfClientStr = etfClientStr + ',' + s5;
                            }
                            if(mfClientStr.equals(''))
                                mfClientStr = s6;
                            else{
                                if(!(mfClientStr.contains(s6)))
                                    mfClientStr =mfClientStr + ',' + s6;
                            }
                        }
                    }
                }
                etfPosMap.put(strId,strIndr1);  // map that stores value pair of contact_id and concadinated ETF +ve interest string for that particular contact_id
                etfNegMap.put(strId,strIndr3);
                mfPosMap.put(strId,strIndr2);
                mfNegMap.put(strId,strIndr4);
                //adding mfclient list
                //etfFundClientMap.put(strId,strIndr1);
                
                mfFundClientMap.put(strId,mfClientStr);
                //adding etfclient list
                //mfFundClientMap.put(strId,strIndr2);
                etfFundClientMap.put(strId,etfClientStr);
            }
            /*  Creating the list of contact objects with the updated values    */
            List<Contact> cList = new List<Contact>();
            cList = [select id, MailingCountry from Contact where Id IN: conIdList];
            for(Contact c : cList)
            {
                c.Schedule_Apex_Indicator__c = null;
                if(etfPosMap.containsKey(c.Id))
                    c.ETF_Fund_Interest__c = ((etfPosMap.get(c.Id)).length() > 247) ? (etfPosMap.get(c.Id)).substring(0,247) : etfPosMap.get(c.Id);
                if(etfNegMap.containsKey(c.Id))
                    c.ETF_Neg_Fund_Interest__c = ((etfNegMap.get(c.Id)).length() > 247) ? (etfNegMap.get(c.Id)).substring(0,247) : etfNegMap.get(c.Id);
                if(mfPosMap.containsKey(c.Id))
                    c.MF_Fund_Interest__c = ((mfPosMap.get(c.Id)).length() > 247) ? (mfPosMap.get(c.Id)).substring(0,247) : mfPosMap.get(c.Id);
                if((mfNegMap.containsKey(c.Id)))
                    c.MF_Neg_Fund_Interest__c = ((mfNegMap.get(c.Id)).length() > 247) ? (mfNegMap.get(c.Id)).substring(0,247) : mfNegMap.get(c.Id);
                if((mfFundClientMap.containsKey(c.Id)))
                    c.MF_Client__c = ((mfFundClientMap.get(c.Id)).length() > 1500) ? (mfFundClientMap.get(c.Id)).substring(0,1500) : mfFundClientMap.get(c.Id);
                    
                if((etfFundClientMap.containsKey(c.Id)))
                    c.ETF_Client__c = ((etfFundClientMap.get(c.Id)).length() > 1500) ? (etfFundClientMap.get(c.Id)).substring(0,1500) : etfFundClientMap.get(c.Id);
                  
                c.Fund_Client_All__c = ((mfFundClientMap.get(c.Id)).length() > 1500) ? (mfFundClientMap.get(c.Id)).substring(0,1500) : mfFundClientMap.get(c.Id) ;
                
                If (c.MF_Client__c != '' && c.MF_Client__c !=null && c.MF_Client__c != '' && c.MF_Client__c !=' ') {
                
                    If (c.ETF_Client__c != '' && c.ETF_Client__c !=null && c.ETF_Client__c != '' && c.ETF_Client__c !=' ') {
                        c.Fund_Client_All__c = c.Fund_Client_All__c + ',' +( ((etfFundClientMap.get(c.Id)).length() > 13000) ? (etfFundClientMap.get(c.Id)).substring(0,13000) : etfFundClientMap.get(c.Id));  
                    }
                   
                }
                else {
                
                    c.Fund_Client_All__c = ( ((etfFundClientMap.get(c.Id)).length() > 13000) ? (etfFundClientMap.get(c.Id)).substring(0,13000) : etfFundClientMap.get(c.Id)); 
                }
              
                if(!etfPosMap.containsKey(c.Id) && !etfNegMap.containsKey(c.Id) && !mfPosMap.containsKey(c.Id) && !mfNegMap.containsKey(c.Id)&& (!mfFundClientMap.containsKey(c.Id) || etfFundClientMap.containsKey(c.Id))){
                    // The Fund Interest Indicator is not updated due to Too many Script Statements
                    // So update them in the Schedules Apex job
                    c.Schedule_Apex_Indicator__c = 'Fund';
                }
                if(c.MailingCountry != null)
                    conList.add(c);
            }
            /*  Actual Contact Update   */
            if(!(conList.isEmpty()))
              update conList;
              
        }catch(Exception e){
            error = 'Exception occured in updateContact method..... '+e.getMessage()+conIdList;
            return error;
        }
        return 'true';
    }
}