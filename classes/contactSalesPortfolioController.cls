global class contactSalesPortfolioController
{   
    WebService static id launchBatchJob()
    {
        contactSalesPortfolioBatchProcess b = new contactSalesPortfolioBatchProcess();
        return database.executebatch(b, 1);
    }

    //mean to be invoked via a scheduled batch job. Takes a bunch of SalesConnect__Contact_Portfolio_Breakdown__c records that do not have their fund goal field populated.
    //based on the territory of the related contact, and the name of the record itself a fund goal is located and linked. So if a record had the name "Test Goal" and the contact had
    //a territory of MN and the current year was 2013 the record would attempt to link to a fund goal named Test Goal-MN-2013 
    public static list<SalesConnect__Contact_Portfolio_Breakdown__c> setContactPortfolioFundGoal(list<SalesConnect__Contact_Portfolio_Breakdown__c> salesFunds)
    {
        list<SalesConnect__Contact_Portfolio_Breakdown__c> salesFundsToUpdate = new list<SalesConnect__Contact_Portfolio_Breakdown__c>();
        
        map<string,id> fundGoalNameToIdMap = new map<string,id>();
        map<id,contact> contactMap = new map<id,contact>();
        
        //as a heads up, this trigger is really ugly. There is a lot of having to loop over stuff to build maps. I wish there was a better way, but alas. So yeah
        //lots of maps and loops coming up.
        
        //first we need to create a map of all the referenced contacts so we can get their territory. First, build the map
        for(SalesConnect__Contact_Portfolio_Breakdown__c sf : salesFunds)
        {
            if(sf.SalesConnect__Contact__c != null)
            {
                contactMap.put(sf.SalesConnect__Contact__c,null);
            }
        }
        
        //now that we have a map of the contacts, lets run the query
        for(Contact contact : [select name, id, territory__c from contact where id in :contactMap.keySet()])
        {
            contactMap.put(contact.id,contact);
        }
        
        //great now we know all the contacts and their territories. So now we can build the string that represents the name of the fund goal to link to this record.            
        for(SalesConnect__Contact_Portfolio_Breakdown__c sf : salesFunds)
        {
            if(sf.SalesConnect__Contact__c != null && contactMap.containsKey(sf.SalesConnect__Contact__c))
            {
                
                string fundGoalName = sf.name+'-'+contactMap.get(sf.SalesConnect__Contact__c).territory__c+'-'+date.today().Year();
                fundGoalNameToIdMap.put(fundGoalName,null);
            }
        }
        
        //now we have another map that has all the fund goal names we need to find ID's for.       
        for(Fund_Goal__c fundGoal : [select name, id from Fund_Goal__c where name in :fundGoalNameToIdMap.keySet()])
        {
            fundGoalNameToIdMap.put(fundGoal.Name,fundGoal.id);
        }
        
        //finally we iterate over the original data set again, build a list of records to update, and do so.
        for(SalesConnect__Contact_Portfolio_Breakdown__c sf : salesFunds)
        {
            if(sf.SalesConnect__Contact__c != null && contactMap.containsKey(sf.SalesConnect__Contact__c))
            {           
                string fundGoalName = sf.name+'-'+contactMap.get(sf.SalesConnect__Contact__c).territory__c+'-'+date.today().Year();
                if(fundGoalNameToIdMap.containsKey(fundGoalName))
                {
                    SalesConnect__Contact_Portfolio_Breakdown__c thisSf = new SalesConnect__Contact_Portfolio_Breakdown__c(id=sf.id);
                    
                    thisSf.Fund_Goal__c = fundGoalNameToIdMap.get(fundGoalName);
                    salesFundsToUpdate.add(thisSf);
                }
            }            
        }
        
        if(!salesFundsToUpdate.isEmpty())
        {
            update salesFundsToUpdate;
        }
        
        return salesFundsToUpdate;
    }
}