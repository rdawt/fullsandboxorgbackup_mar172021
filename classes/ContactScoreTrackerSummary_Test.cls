@isTest
public class ContactScoreTrackerSummary_Test 
{
	public static Integer recordCount = 100;
	
	@testsetup
	public static void createTestData()
	{
		TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
		settings.isActive_Task__c = TRUE;
		settings.isApex_Task_Workflows__c = TRUE;
		upsert settings custSettings__c.Id;
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
	}
	
	@isTest
	public static void testInsertContactScoreTrackerSummary()
	{
		user objUser1 = [Select Id from User where isActive = true limit 1];
        
        system.runAs(objUser1)
        {
        	Date currentDate = Date.today();
        	
	        Test.startTest();
	        
	        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
	        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
	
	        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
	        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
	        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
	        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
	        
	        Contact conxConverted = (Contact) conxInserted;
	        
	        List<ContactScoreTracker__c> lstContactScoreTracker = new List<ContactScoreTracker__c>();
	        
	        List<ContactScoreTracker__c> lstSet1 = (List<ContactScoreTracker__c>)SL_TestDataFactory.createSObjectList(
                                    					new ContactScoreTracker__c(Contact__c = conxConverted.Id, TrackingFieldAPIName__c = 'Outside_Research__c'),
                                    					recordCount, false);
            List<ContactScoreTracker__c> lstSet2 = (List<ContactScoreTracker__c>)SL_TestDataFactory.createSObjectList(
                                    					new ContactScoreTracker__c(Contact__c = conxConverted.Id, TrackingFieldAPIName__c = 'Investment_Perspective__c'),
                                    					recordCount, false);
            
            if(!lstSet1.isEmpty())
            	lstContactScoreTracker.addAll(lstSet1);   
            
            if(!lstSet2.isEmpty())
            	lstContactScoreTracker.addAll(lstSet2);
            	
            
            if(!lstContactScoreTracker.isEmpty())	   	                     					                        					
	        	insert lstContactScoreTracker;
	        
	        Test.stopTest();
        
	        List<PayeeSummary__c> lstPayee = [SELECT Id FROM PayeeSummary__c WHERE Year__c =: String.valueOf(currentDate.year())];
	        system.assert(lstPayee.size()>0);
	        	
	        
		}
	}
	
	@isTest
	public static void testUpdateContactScoreTrackerSummary()
	{
		user objUser1 = [Select Id from User where isActive = true limit 1];
        
        system.runAs(objUser1)
        {
	        Test.startTest();
	        
	        Date currentDate = Date.Today();
	        PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),
	                                                    	User__c=objUser1.Id,TaskFAScore__c =0,TaskFAQScore__c =0 ,
	                                                    	Total_Days_for_this_Calendar_Month__c = 30,
		                									No_Of_Days_in_Office__c = 25,
	                                                        TaskRIAScore__c =0,TaskRIAQScore__c = 0 , timeTradeQCount__c = 0,GlaceQCount__c =  0, 
	                                                        IWSQTasksCount__c = 0, IWSToExternalTasksQCount__c = 0, ETF_Task_Referral_Q_Count__c = 0, 
	                                                        ETF_Referral_Q_Count_4_Xternals_ETF_Spl__c = 0, ETF_Referral_Q_Count_4_Xternals_RIA_Spl__c = 0);
	        insert payeeSummary;	
	        system.assert(payeeSummary.Id != NULL);
	        
	        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
	        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
	
	        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
	        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
	        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
	        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
	        
	        Contact conxConverted = (Contact) conxInserted;
	        
	        List<ContactScoreTracker__c> lstContactScoreTracker = new List<ContactScoreTracker__c>();
	        
	        List<ContactScoreTracker__c> lstSet1 = (List<ContactScoreTracker__c>)SL_TestDataFactory.createSObjectList(
                                    					new ContactScoreTracker__c(Contact__c = conxConverted.Id, TrackingFieldAPIName__c = 'Outside_Research__c'),
                                    					recordCount, false);
            List<ContactScoreTracker__c> lstSet2 = (List<ContactScoreTracker__c>)SL_TestDataFactory.createSObjectList(
                                    					new ContactScoreTracker__c(Contact__c = conxConverted.Id, TrackingFieldAPIName__c = 'Investment_Perspective__c'),
                                    					recordCount, false);
            
            if(!lstSet1.isEmpty())
            	lstContactScoreTracker.addAll(lstSet1);   
            
            if(!lstSet2.isEmpty())
            	lstContactScoreTracker.addAll(lstSet2);
            	
            
            if(!lstContactScoreTracker.isEmpty())	   	                     					                        					
	        	insert lstContactScoreTracker;
	        
	        Test.stopTest();
        
	        List<PayeeSummary__c> lstPayee = [SELECT Id FROM PayeeSummary__c WHERE Year__c =: String.valueOf(currentDate.year())];
	        system.assert(lstPayee.size()>0);
	        	
		}
	}
	
}