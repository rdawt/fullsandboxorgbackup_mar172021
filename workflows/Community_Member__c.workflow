<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_CompositeKey_On_Community_Member</fullName>
        <field>Composite_Key__c</field>
        <formula>Community__c&amp;&quot;_&quot;&amp;Community_Registrant__c</formula>
        <name>Set CompositeKey On Community Member</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Community Member Composite Key</fullName>
        <actions>
            <name>Set_CompositeKey_On_Community_Member</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(isnull( Community__c ))  || not(isnull( Community_Registrant__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
