global class SL_UpdateFundInterestOneTime implements Database.Batchable<sObject>{
    
    global Database.querylocator start(Database.BatchableContext BC){
        String strQuery = 'SELECT Id, Contact__c, Fund__c, Level_Of_Interest__c,Fund_Client__c, Fund_Client_New__c FROM Fund_Interest__c where Fund_Client_New__c = \'\' ';
            return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<Fund_Interest__c> lstFund_Interest){
        
        List<Fund_Interest__c> lstFund_InterestToUpdate = new List<Fund_Interest__c>();
        
        for(Fund_Interest__c objFI :lstFund_Interest){
             if(objFI.Fund_Client__c == true)
				objFI.Fund_Client_New__c = 'Yes';
			else
				objFI.Fund_Client_New__c = 'None';
			
			lstFund_InterestToUpdate.add(objFI);
        }
        
        if(!lstFund_InterestToUpdate.isEmpty()){
            update lstFund_InterestToUpdate;
        }
        
    }
    
    global void finish(Database.BatchableContext BC){}    
}