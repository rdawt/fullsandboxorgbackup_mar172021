({
    getOnLoadRecords : function(component,event,helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getOnLoadRecords");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                
                component.set("v.OwnersOptions", rtnVal.OwnersOptions);
                component.set("v.SelectedOwnersOption", rtnVal.SelectedOwnersOption);
                component.set("v.activityTypeOptions", rtnVal.activityTypeOptions);
                component.set("v.selectedActivityTypeOption", rtnVal.selectedActivityTypeOption);
                component.set("v.activityDateMin", rtnVal.activityDateMin);
                component.set("v.activityDateMax", rtnVal.activityDateMax);
                component.set("v.objCurrentUser", rtnVal.objCurrentUser);
                component.set("v.objMasterRegionUser", rtnVal.objMasterRegionUser);

                // setting the owner check box to true                
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
                // setting the activity checkbox to true
                component.set("v.selectedActivityTypeOption",helper.selectAll(component, event, helper, component.get("v.activityTypeOptions")));
                
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );

                if(rtnVal.objMasterRegionUser.Call_Score_Weightage__c != '')
                    component.set("v.callWeight", rtnVal.objMasterRegionUser.Call_Score_Weightage__c);
                if(rtnVal.objMasterRegionUser.Meeting_Score_Weightage__c != '')
                    component.set("v.meetingWeight", rtnVal.objMasterRegionUser.Meeting_Score_Weightage__c);   
                if(rtnVal.objMasterRegionUser.Email_Score_Weightage__c != '')
                    component.set("v.emailWeight", rtnVal.objMasterRegionUser.Email_Score_Weightage__c);
                if(rtnVal.objMasterRegionUser.Unknown_Score_Weightage__c != '')
                    component.set("v.unknownWeight", rtnVal.objMasterRegionUser.Unknown_Score_Weightage__c);
                if(rtnVal.objMasterRegionUser.Conference_Call__c != '')
                    component.set("v.Conference_Call", rtnVal.objMasterRegionUser.Conference_Call__c);
                if(rtnVal.objMasterRegionUser.Conference_Meeting__c != '')
                    component.set("v.Conference_Meeting", rtnVal.objMasterRegionUser.Conference_Meeting__c);   
                if(rtnVal.objMasterRegionUser.Meeting_Educational__c != '')
                    component.set("v.Meeting_Educational", rtnVal.objMasterRegionUser.Meeting_Educational__c);
                if(rtnVal.objMasterRegionUser.Meeting_One_on_One__c != '')
                    component.set("v.Meeting_One_on_One", rtnVal.objMasterRegionUser.Meeting_One_on_One__c);
                if(rtnVal.objMasterRegionUser.Meeting_Social__c != '')
                    component.set("v.Meeting_Social", rtnVal.objMasterRegionUser.Meeting_Social__c);
                if(rtnVal.objMasterRegionUser.Seminar_Workshop__c != '')
                    component.set("v.Seminar_Workshop", rtnVal.objMasterRegionUser.Seminar_Workshop__c);   
                if(rtnVal.objMasterRegionUser.Webinar__c != '')
                    component.set("v.Webinar", rtnVal.objMasterRegionUser.Webinar__c);
                if(rtnVal.objMasterRegionUser.Other__c != '')
                    component.set("v.Other", rtnVal.objMasterRegionUser.Other__c);

                helper.loadCharts(component, event, helper);
                helper.loadOpportunityChart(component, event, helper);
                helper.loadChartsForSelectedUser(component, event, helper);
                helper.loadChartsForSelectedUserScore(component, event, helper);
                helper.loadChartsForAllSalesUserScore(component, event, helper);
                //helper.loadLineChart(component, event, helper);
                
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    saveUserScoreWeightage : function(component,event,helper) {
        component.set("v.showSpinner", true);
        var objMasterRegionUser = component.get("v.objMasterRegionUser");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");
        
        var Conference_Call = component.get("v.Conference_Call");
        var Conference_Meeting = component.get("v.Conference_Meeting");
        var Meeting_Educational = component.get("v.Meeting_Educational");
        var Meeting_One_on_One = component.get("v.Meeting_One_on_One");
        var Meeting_Social = component.get("v.Meeting_Social");
        var Seminar_Workshop = component.get("v.Seminar_Workshop");
        var Webinar = component.get("v.Webinar");
        var Other = component.get("v.Other");

        var action = component.get("c.saveCurrentUserDetails");  
        action.setParams({
            "objParamUser" : objMasterRegionUser,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight,
            "Conference_Call" : Conference_Call,
            "Conference_Meeting" : Conference_Meeting,
            "Meeting_Educational" : Meeting_Educational,
            "Meeting_One_on_One" : Meeting_One_on_One,
            "Meeting_Social" : Meeting_Social,
            "Seminar_Workshop" : Seminar_Workshop,
            "Webinar" : Webinar,
            "Other" : Other,
        });
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                component.set("v.objMasterRegionUser", rtnVal);
                component.set("v.showSpinner", false);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    loadCharts : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        var OwnerId = component.get("v.SelectedOwnersOption");
        if(OwnerId.includes('ALL'))
        {
            if(OwnerId.length > 1)
            {
                for( var i = 0; i < OwnerId.length; i++)
                { 
                    if ( OwnerId[i] === 'ALL') 
                    { 
                        OwnerId.splice(i, 1); 
                    }
                }
            }
        }
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecords"); 
        action.setParams({
            //"strOwnerId" : OwnerId,
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //console.log('===='+dataObj);
                component.set("v.data",dataObj);
                helper.stackedChart(component,event,helper);
                component.set("v.showSpinner", false);
                component.set("v.selectedActivityTypeOption", activityType);
                component.set("v.SelectedOwnersOption", OwnerId);
            }
        });
        $A.enqueueAction(action);
        
    },

    loadChartsForSelectedUser : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        var OwnerId = component.get("v.SelectedOwnersOption");
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecordsByUser"); 
        action.setParams({
            //"strOwnerId" : OwnerId,
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //console.log('===='+dataObj);
                component.set("v.dataEachUser",dataObj);
                helper.stackedChartForSelectedUser(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    loadChartsForAllSalesUserScore : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        /*
        if(activityType.includes('ALL'))
        {
            if(activityType.includes('Email', 'Call', 'Meeting', 'Unknown','Conference call','Conference - Meeting', 
                                        'Meeting - One on One', 'Meeting - Educational', 'Meeting - Social','Webinar' ,
                                        'Seminar - Workshop', 'Other'))
            {
                for( var i = 0; i < activityType.length; i++)
                { 
                    if ( activityType[i] === 'ALL') 
                    { 
                        activityType.splice(i, 1); 
                    }
                }
            }
        }
        */
        var OwnerId = component.get("v.SelectedOwnersOption");
        /*if(OwnerId.includes('ALL'))
        {
            if(OwnerId.length > 1)
            {
                for( var i = 0; i < OwnerId.length; i++)
                { 
                    if ( OwnerId[i] === 'ALL') 
                    { 
                        OwnerId.splice(i, 1); 
                    }
                }
            }
        }*/
        console.log('????????activityType??????????', activityType);
        console.log('????????OwnerId??????????', OwnerId);
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecordsBySalesUserScore"); 
        action.setParams({
            //"strOwnerId" : OwnerId,
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //console.log('===='+dataObj);
                component.set("v.dataByAllSalesUserScoreOnly",dataObj);
                helper.stackedChartForAllSalesUserScore(component,event,helper); //canvasSelectedSalesUsers
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    loadChartsForSelectedUserScore : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var activityType = component.get("v.selectedActivityTypeOption");
        /*
        if(activityType.includes('ALL'))
        {
            if(activityType.includes('Email', 'Call', 'Meeting', 'Unknown'))
            {
                for( var i = 0; i < activityType.length; i++)
                { 
                    if ( activityType[i] === 'ALL') 
                    { 
                        activityType.splice(i, 1); 
                    }
                }
            }
        }
        */
        var OwnerId = component.get("v.SelectedOwnersOption");
        /*if(OwnerId.includes('ALL'))
        {
            if(OwnerId.length > 1)
            {
                for( var i = 0; i < OwnerId.length; i++)
                { 
                    if ( OwnerId[i] === 'ALL') 
                    { 
                        OwnerId.splice(i, 1); 
                    }
                }
            }
        }*/
        var activityDtMin = component.get("v.activityDateMin");
        var activityDtMax = component.get("v.activityDateMax");
        var activityScore = component.get("v.sliderValueSelected");
        var emailWeight = component.get("v.emailWeight");
        var callWeight = component.get("v.callWeight");
        var meetingWeight = component.get("v.meetingWeight");
        var unknownWeight = component.get("v.unknownWeight");

        var action = component.get("c.getTaskAndEventRecordsByUserScore"); 
        action.setParams({
            //"strOwnerId" : OwnerId,
            "lstOwnerId" : OwnerId,
            "activityDateMin" : activityDtMin,
            "activityDateMax" : activityDtMax,
            "lstFilterActivityTypeSelected": activityType,
            "intScoreSelected" : activityScore,
            "emailWeight" : emailWeight,
            "callWeight" : callWeight,
            "meetingWeight" : meetingWeight,
            "unknownWeight" : unknownWeight
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //console.log('===='+dataObj);
                component.set("v.dataEachUserScore",dataObj);
                helper.stackedChartForSelectedUserScore(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    loadOpportunityChart : function(component, event, helper) 
    {
        component.set("v.showSpinner", true);
        var OwnerId = component.get("v.SelectedOwnersOption");
        /*if(OwnerId.includes('ALL'))
        {
            if(OwnerId.length > 1)
            {
                for( var i = 0; i < OwnerId.length; i++)
                { 
                    if ( OwnerId[i] === 'ALL') 
                    { 
                        OwnerId.splice(i, 1); 
                    }
                }
            }
        }*/

        var action = component.get("c.getOpportunityRecords"); 
        action.setParams({
            "lstOwnerId" : OwnerId
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //console.log('===='+dataObj);
                component.set("v.oppData",dataObj);
                helper.barChart(component,event,helper);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },

    barChart : function(component,event,helper) {
        var jsonData2 = component.get("v.oppData");
        var barChartOppData = JSON.parse(jsonData2);

        if(window.myBar2 && window.myBar2 !== null){
            window.myBar2.destroy();
        }
        
        var ctx2 = document.getElementById('canvasOpp').getContext('2d');
        window.myBar2 = new Chart(ctx2, {
            type: 'bar',
            data: barChartOppData,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    stackedChart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var barChartData = JSON.parse(jsonData);

        if(window.myBar && window.myBar !== null){
            window.myBar.destroy();
        }
        
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
                // onClick: function(event) {
                //     var elements = window.myBar.getElementAtEvent(event);
                //     console.log("elements");
                //     console.log(elements);
                //     if (elements.length === 1) {
                //         var ownerId = component.get("v.SelectedOwnersOption");
                //         var dtMin = component.get("v.activityDateMin");
                //         var dtMax = component.get("v.activityDateMax");
                //         var month = barChartData.labels[elements[0]._index];
                //         var type = barChartData.datasets[elements[0]._datasetIndex].label;
                //         var chartEvent = $A.get("e.c:ChartJSEvent");
                //         chartEvent.setParams({
                //             "ownerId":ownerId,
                //             "type":type,
                //             "month":month,
                //             "dtMin": dtMin,
                //             "dtMax": dtMax
                //             }
                //         );
        		// 		chartEvent.fire();
                //     }
                // }
            }
        });
    },

    stackedChartForSelectedUser : function(component,event,helper) {
        var jsonData = component.get("v.dataEachUser");
        
        var barChartData = JSON.parse(jsonData);

        if(window.myBarSelected && window.myBarSelected !== null){
            window.myBarSelected.destroy();
        }
        
        var ctxSelected = document.getElementById('canvasSelectedUser').getContext('2d');
        window.myBarSelected = new Chart(ctxSelected, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'point',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    stackedChartForAllSalesUserScore : function(component,event,helper) {
        var jsonDataSU = component.get("v.dataByAllSalesUserScoreOnly");
        
        var barChartDataSU = JSON.parse(jsonDataSU);

        if(window.myBarSelectedScoreSU && window.myBarSelectedScoreSU !== null){
            window.myBarSelectedScoreSU.destroy();
        }
        
        var ctxSelectedScoreSU = document.getElementById('canvasSelectedSalesUsers').getContext('2d');
        window.myBarSelectedScoreSU = new Chart(ctxSelectedScoreSU, {
            type: 'bar',
            data: barChartDataSU,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'point',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    stackedChartForSelectedUserScore : function(component,event,helper) {
        var jsonData = component.get("v.dataEachUserScore");
        
        var barChartData = JSON.parse(jsonData);

        if(window.myBarSelectedScore && window.myBarSelectedScore !== null){
            window.myBarSelectedScore.destroy();
        }
        
        var ctxSelectedScore = document.getElementById('canvasSelectedUserScore').getContext('2d');
        window.myBarSelectedScore = new Chart(ctxSelectedScore, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'point',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    },

    loadLineChart : function(component,event,helper)
    {
        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'line',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December'],
				datasets: [{
					label: 'score',
					backgroundColor: 'rgba(237, 104, 186, 1)',
					borderColor: 'rgba(237, 104, 186, 1)',
					data: [
						0, 1, 5, 3, 24, 5, 4, 5, 6, 3, 1, 12
					],
					fill :false
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: ''
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Score'
						}
					}]
				}
			}
		};

    
        var ctx1 = document.getElementById('canvasLine').getContext('2d');
        window.myLine = new Chart(ctx1, config);
	
    },
    
    selectAll : function(component, event, helper, allOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(allOptions[ele].value){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  
    
    addNewSelectedValue : function(component, event, helper, selectecoptions, newSelection){ 
        
        // if the value is not already present
        if(!selectecoptions.includes(newSelection)){
            selectecoptions.push(newSelection);
        }          
        return selectecoptions;
    },
    
    removeSelectedValue : function(component, event, helper, selectedOpts, removeEle){         
        // this will save the options already Selected
        var newSelectedOpts = [];
        
        // for each selected option set
        for(var ele in selectedOpts){
            
            if (selectedOpts[ele] != removeEle
               	&& selectedOpts[ele] != 'ALL'){                
                newSelectedOpts.push(selectedOpts[ele]);
            }
        }
		return newSelectedOpts;
    }
})