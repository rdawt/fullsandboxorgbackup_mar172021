/*
    Author: Vidhya Krishnan
    Date: 12/07/11
    Description: Subscription Member Insert, Update, Deletion, UnDelete & Exception handled.
            Active Subscription Indicator field in contact is updated.
            Sub Mem is made as child object of contact. So rollup summery is automated - no subscription count coding needed
*/
trigger SubscriptionTrigger on Subscription_Member__c (after insert, after update, after undelete, after delete) 
{
     //buggy datetime currentDate = Date.today();
     date currentDate = Date.today();
     set<id> userIds= new set<id>();
    SendEmailNotification se = new SendEmailNotification('Error: Subscription Trigger failed');
    try{
        Set<Id> lstIds =new Set<Id>();
        Map<String,string> subIndtrMap =new Map<String,string>();
        List<Contact> conList =new List<Contact>();
        List<Subscription_Member__c> subMemList = new List<Subscription_Member__c>();
        String strIndr,s;
        
         //if Delete fired this trigger, consider the old set of records before deletion for updating the count
        if (Trigger.isDelete)
            subMemList = Trigger.old; 
        else if(Trigger.isInsert || Trigger.isUnDelete)
            subMemList = Trigger.new;

        if(subMemList != null)
            for(Subscription_Member__c subMem:subMemList){
                //if(submem.source__c == null || (submem.source__c != null && submem.source__c != 'Dynamic Subscription'))
                    lstIds.add(subMem.Contact__c);
                    //added for Subscription Summary for Payees
                    userIds.add(subMem.createdbyid);
             }

        if(Trigger.isUpdate){
            Set<String> c = new Set<String>();
            for(Subscription_Member__c subMem:Trigger.new){
                // consider only if there is actual update
                // end user can only edit the subscribed value through contact's edit subscription page or Email's Manage My Subscription Page.
                if(subMem.Subscribed__c != Trigger.oldMap.get(subMem.Id).Subscribed__c)
                    //if(submem.source__c == null || (submem.source__c != null && submem.source__c != 'Dynamic Subscription'))
                        lstIds.add(subMem.Contact__c);
                        userIds.add(subMem.createdbyid);
            }
        }
        system.debug('List of Contact in the Sub_Mem trigger.....'+lstIds);

        /*  For updating the Active Subscription Indicator field of Contact */
        List<Subscription_Member__c> subMemIndicator = [Select sm.Contact__c, sm.Subscription__r.Subscription_Indicator_Marker__c from Subscription_Member__c sm 
                                                where Subscribed__c=:true and Contact__c != null and Contact__c =:lstIds 
                                                order by sm.Contact__c, sm.Subscription__r.Subscription_Indicator_Marker__c];
        //Tharaka De Silva on 2020-07-27 : ITSALES-1551 Start
        Map<Id,List<Subscription_Member__c>> subMemIndicatorMap = new Map<Id,List<Subscription_Member__c>>();
        for(Subscription_Member__c subMember : subMemIndicator){
            if(subMemIndicatorMap.containsKey(subMember.Contact__c)){
                subMemIndicatorMap.get(subMember.Contact__c).add(subMember);
            }else{
                subMemIndicatorMap.put(subMember.Contact__c,new List<Subscription_Member__c>{subMember});
            }
        }
        //Tharaka De Silva on 2020-07-27 : ITSALES-1551 End
        for(String strId : lstIds){
            //Tharaka De Silva on 2020-07-27  commented line no 66-67 (No longer enforced ScriptStatements limits)
            // To avoid Too Many Script Statements - exit the for loop when only 10000 script statements left
            //if ((200000 - Limits.getScriptStatements()) < 10000)
            //    break;
            
            strIndr = '';   
            if(subMemIndicatorMap != null && subMemIndicatorMap.containsKey(strId)){//Tharaka De Silva on 2020-07-27 : ITSALES-1551
            	for(Subscription_Member__c sm : subMemIndicatorMap.get(strId)){
                	s='';
                	if(sm.Contact__c == strId){
                    	if(sm.Subscription__r.Subscription_Indicator_Marker__c == null)
                        	s='';
                    	else
                        	s=sm.Subscription__r.Subscription_Indicator_Marker__c;
                    	if(strIndr.equals(''))
                        	strIndr = s;
                    	else{
                        	if(!(strIndr.contains(s)))
                            	strIndr = strIndr + ',' + s;
                    	}
                	}
            	}
        	}
            subIndtrMap.put(strId,strIndr); // map that stores value pair of contact_id and concadinated Indicator string for that particular contact_id
        }
        system.debug('The Final Map value....'+subIndtrMap);

        /*  Creating the list of contact objects with the updated values    */
        List<Contact> cList = new List<Contact>();
        cList = [select id, MailingCountry from Contact where Id IN: lstIds];
        for(Contact c : cList)
        {
            if(subIndtrMap.containsKey(c.Id)){
                c.Active_Subscriptions_Indicator__c = subIndtrMap.get(c.Id);
            }
            else{
                // The Subscription Indicator is not updated due to Too many Script Statements
                // So update them in the Schedules Apex job
                c.Schedule_Apex_Indicator__c = 'Subscription';
            }
            if(c.MailingCountry != null)
                conList.add(c);
        }

        /*  Actual Contact Update   */
        if(!(conList.isEmpty()))
         update conList;
    }
    catch(Exception e){
         se.sendEmail('Exception Occurred : \n'+e.getMessage()+trigger.new);
    }
    
    try{
        
        //this is for user object count

        AggregateResult[] emailSubscriptionCount ; 
        AggregateResult[] emailSubscriptionQCount ;
        
        list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
        list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();
        
        //Get Current Date
        
        
         
            if (userIds.size() != 0){
                emailSubscriptionCount  =[SELECT count(id) emailSubscriptionCount,sum(UnSubscribedCounter__c) UnSubscribedCounter, createdbyid FROM Subscription_Member__c 
                                            where Email_Opt_Out__c = false and Contact_IsVe__c = false                         
                                              and createdbyid in : userIds and createddate = THIS_MONTH and createddate = THIS_YEAR 
                                              
                                                   group by createdbyid]; //and subscribed__c = true and createdby.isactive = true and Subscribed__c = true
         
                 if (emailSubscriptionCount.size() == 0){
                      system.debug('**********Inside emailSubscriptionCount size check as ZERO*******************'+ emailSubscriptionCount.size());
                     //return;
                 }
                 Map<Id, PayeeSummary__c> payeeListQSubscriptionCountMap = new Map<Id, PayeeSummary__c>();
                 emailSubscriptionQCount = [SELECT count(id) emailSubscriptionQCount,sum(UnSubscribedCounter__c) UnSubscribedQCounter, createdbyid FROM Subscription_Member__c 
                                            where Email_Opt_Out__c = false and Contact_IsVe__c = false                         
                                              and createdbyid in : userIds and createddate = THIS_QUARTER and createddate = THIS_YEAR 
                                              
                                                   group by createdbyid];//and Subscribed__c = true
                 for (AggregateResult emailSubscriptionQ2Count : emailSubscriptionQCount ) //and subscribed__c = true and createdby.isactive = true
                   {
                       //payeeListQSubscriptionMap.put(string.valueof(emailSubscriptionQ2Count.get('createdbyid')),emailSubscriptionQ2Count);
                       string payeeId = string.valueof(emailSubscriptionQ2Count.get('createdbyid'));
                       integer emailSubscriptionCounter = integer.valueof(emailSubscriptionQ2Count.get('emailSubscriptionQCount'));
                       integer emailUnSubscribedCounter = integer.valueof(emailSubscriptionQ2Count.get('UnSubscribedQCounter'));
                       PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,ThisQuarterEmailSubscription__c = emailSubscriptionCounter, ThisQuarterEmailUnSubscription__c=emailUnSubscribedCounter );
                       payeeListQSubscriptionCountMap.put(payeeId,pSummary);
                                                     
                   }
         
                 if (emailSubscriptionQCount.size() == 0){
                      system.debug('**********Inside emailSubscriptionQCount size check as ZERO*******************'+ emailSubscriptionQCount.size());
                     //return;
                 }
                 system.debug('**********eventUserCount!!*******************'+ emailSubscriptionCount.size());
                // Map<String,Integer> payeeListMap = new Map<String,Integer>();
                 Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
                 Integer j =0;
                 for(PayeeSummary__c payeeSummary: [Select id, user__c,EmailSubscription__c from PayeeSummary__c  Where user__c in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ]){
                    modPayeeSummaryList.add(payeeSummary);
                    //if (((emailSubscriptionCount.size() == 0)) && (( payeeSummary.EmailSubscription__c != 0)))  {
                    //    PayeeSummary__c payeeSummaryReset = new PayeeSummary__c(id=string.valueof( String.ValueOf(payeeSummary.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(payeeSummary.get('createdbyid')),EmailSubscription__c =0) ;
                    //    updatePayeeList.add(payeeSummaryReset);  
                   // }
                   //added user who are already present in payeesummary object for current month
                    payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
                    system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
                    //payeeListMap.put(payeeSummary.User__c,j++);
                 }
                 
        
                  //addPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c !=: String.ValueOf(currentDate.Month()) and Year__c !=: String.ValueOf(currentDate.Year()) ];
                  //modPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ];
                  
                  system.debug('after agg. User-Payee Email Subscription count- Payee List is'+ emailSubscriptionCount  );
                  //system.debug('after agg. User-Payee RIA Activity Score Sum- Sum is'+ RIAActivityScore);
                                    
                 list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
                 list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
                 
                 
                                         
                 for(AggregateResult forLoopCount : emailSubscriptionCount){
                 
                    system.debug('entering forloop to chase the matching payee ids'+ emailSubscriptionCount  );
                    
        
                 
                   if((payeeListMap != null) && (  payeeListMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ) //  ||  payeeListMap.containsKey(string.valueof(forLoopCount.get('lastmodifiedbyid'))))  
                   {
                     //get the index
                     integer qSubscribeCount=0;
                     integer qUnSubcribedCount=0;
                     
                      PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('createdbyid')));
                       If (index != null){
                           //do something if Payee  exists in CO for the same month and year already so use update instead of insert in the PayeeSummary CO
                           system.debug('inside else of **MODIFYING-index-for createdbyid** new payee summary for the current month'+ emailSubscriptionCount  );
                          // payeeListMap.get(createdbyid);
                           if((payeeListQSubscriptionCountMap != null) && (  payeeListQSubscriptionCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                               PayeeSummary__c qIndex = payeeListQSubscriptionCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                               qSubscribeCount = integer.valueOf(qIndex.ThisQuarterEmailSubscription__c );
                               qUnSubcribedCount = integer.valueOf(qIndex.ThisQuarterEmailUnSubscription__c ) ;
                               
                           
                           }
                           PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof( String.ValueOf(index.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),EmailSubscription__c =integer.valueof(forLoopCount.get('emailSubscriptionCount')),EmailUnSubscribed__c =integer.valueof(forLoopCount.get('UnSubscribedCounter')), ThisQuarterEmailSubscription__c = qSubscribeCount, ThisQuarterEmailUnSubscription__c = qUnSubcribedCount );
                           updatePayeeList.add(payeeSummary); 
                                          
                           //PayeeSummary__c index2 = payeeListMap.get(string.valueof(forLoopCount.get('lastmodifiedbyid')));
                          // if ((index2 != null) && (index.id != index2.id)){
                          //     system.debug('inside else of **MODIFYING-index2-for modifiedbyid** new payee summary for the current month'+ emailSubscriptionCount  );
                          //      PayeeSummary__c payeeSummary4LastModId = new PayeeSummary__c(id=string.valueof( String.ValueOf(index2.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('lastmodifiedbyid')),EmailSubscription__c =integer.valueof(forLoopCount.get('emailSubscriptionCount')),EmailUnSubscribed__c =integer.valueof(forLoopCount.get('UnSubscribedCounter')) );
                          //      updatePayeeList.add(payeeSummary); 
                          // }
                           
                       }
                    }
                    else{
                    
                        integer qSubscribeCount=0;
                        integer qUnSubcribedCount=0;
                        system.debug('inside else of **ADDING** new payee summary for the current month'+ emailSubscriptionCount  );
                        if((payeeListQSubscriptionCountMap != null) && (  payeeListQSubscriptionCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                               PayeeSummary__c qIndex = payeeListQSubscriptionCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                               qSubscribeCount = integer.valueOf(qIndex.ThisQuarterEmailSubscription__c );
                               qUnSubcribedCount = integer.valueOf(qIndex.ThisQuarterEmailUnSubscription__c ) ;
                               
                           
                        }
                        PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),EmailSubscription__c =integer.valueof(forLoopCount.get('emailSubscriptionCount')),EmailUnSubscribed__c =integer.valueof(forLoopCount.get('UnSubscribedCounter')), ThisQuarterEmailSubscription__c = qSubscribeCount, ThisQuarterEmailUnSubscription__c = qUnSubcribedCount );
                        addPayeeList.add(payeeSummary);
                    
                    }
                 
                      
        
                        
                }
                    
        
                if (updatePayeeList.size() != 0) {
                    update updatePayeeList;
                }
                if (addPayeeList.size() != 0) {
                    insert addPayeeList;
                }
                
            }
            
            
            }
             catch(Exception e){
                se.sendEmail('Exception Occurred in processing count for Payee Details or Summary : \n'+e.getMessage()+trigger.new);
            }
    
}