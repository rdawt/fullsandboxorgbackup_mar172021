@isTest
public with sharing class OpportunityTriggerTest {

    @TestSetup
    public static void createTestData()
    {
		TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
		settings.isActive_Opportunity__c = TRUE;
		upsert settings custSettings__c.Id;

        Account[] acctList = new list<Account>();
        acctList.add(new Account(Name='Test Acct 1', channel__c = 'Institutional', BillingCountry = 'USA'));
        insert acctList;

        Fund__c fundAngl = new Fund__c(Name='Fallen Angel High Yield Bond ETF (ANGL)',Fund_Type__c='FI-ETF',Fund_Ticker_Symbol__c='ANGL');
        insert fundAngl; 

        Opportunity[] opptyList = new list<Opportunity>();
        opptyList.add(new Opportunity(Name='Test acct 1 - Oppty 1', StageName='Prospecting', CloseDate=(system.today()+1), AccountId=acctList[0].Id, Amount=5000000,Fund__c=fundAngl.Id));
        insert opptyList;
    }

    @isTest
    public static void  testOpportunityTrigger() {
        List<Opportunity> lstOpportunity = [SELECT Id,StageName,Create_ETF_Asset_Flow_Data__c FROM Opportunity limit 1];

        if(lstOpportunity.size() > 0){
            Opportunity opp = lstOpportunity[0];
            Test.startTest();
            opp.StageName = 'Funded';
            opp.Create_ETF_Asset_Flow_Data__c = true;
            update opp;
            Test.stopTest();

            List<ETF_Assets_By_Firm__c> lstEtfs = [SELECT Id FROM ETF_Assets_By_Firm__c];
            System.assert(lstEtfs.size() > 0);
        }

    }
}