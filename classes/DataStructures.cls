/*
	WRAPPER CLASS FOR ALL WEB SERVICE CLASSES
*/
global virtual class DataStructures {
	/*	Subscription Management Data Structure	*/
    global class Recordset
    {
      webService Boolean Subscribed {get; set;} //Sub Mem --> Subscribed
      webService String SubMemId {get; set;} //Sub Mem --> Sub Mem Id
      webService String SubId {get; set;} //Sub Mem --> Subscription --> Id
      webService String SubName {get; set;} //Subscription --> Display Name
      webService String SubDesc {get; set;} //Subscription --> Description
      webService DateTime subUnsubDate{get;set;}
      webService String SubTypeId{get; set;} //Subscription Type --> Id  
      webService String SubType{get; set;} //Subscription Type --> Name
      webService String SubGroupId{get; set;} //SG --> Id
      webService String SubGroup{get; set;} //SG --> Name
    }
    global class SubscriptionDetails
    {
    	Webservice String contactId {get; set;}
    	Webservice String subscriptionId {get; set;}
    	// boolean value is always passes as null in webservice calls - so passing them as string
    	Webservice string oldValue {get; set;}
    	Webservice string newValue {get; set;}
    }
    
	/*	Community Registration Data Structure	*/
    global class MemberRecord
    {
        Webservice String subscriptionGroupId {get;set;}
        Webservice String communityId {get; set;} // Community --> Id
        Webservice String cMemberId {get; set;} // Community Registrant --> Id
        Webservice String leadId {get; set;}
        Webservice String contactId {get; set;}
        Webservice String email {get; set;}
        Webservice String cUserName {get; set;}
        Webservice String passWord {get; set;}
        Webservice String firstName {get; set;}
        Webservice String lastName {get; set;}
        Webservice String companyName {get; set;}
        Webservice String jobTitle {get; set;}
        Webservice String address1 {get; set;}
        Webservice String address2 {get; set;}
        Webservice String city {get; set;}
        Webservice String state {get; set;}
        Webservice String country {get; set;}
        Webservice String zipcode {get; set;}
        Webservice String phone {get; set;}
        Webservice String subscriptionRequested {get; set;} // comma delimited string that has list of subscription preferences
        Webservice String investorType {get; set;}
        Webservice String aum {get; set;}
        Webservice String isFP {get; set;}
        Webservice Boolean isActive {get; set;}
        Webservice DateTime privacyAckDate {get; set;} // Privacy policy awk Date
        Webservice DateTime lastLogin {get;set;}
        Webservice Date dbSourceCreateDate {get;set;}
        Webservice Integer loginCount {get;set;} // No of times logged in
        Webservice String sourceId {get;set;} // Record Id (from VeBas/Ecktron) of imported records or community id for live data
    }

}