@isTest
public class SL_MoveMarketoTaskToCustomMarketo_Test {

 @isTest
 static void testBatch(){
      Profile pf= [Select Id from profile where Name='Marketo User']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'Marketo', 
                         lastName = 'API', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            
            Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
		    Account objAccount =  new Account(Name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
		    insert objAccount;
		    
		    Contact cont1 = new Contact(accountid = objAccount.id, lastname = 'Testcontact', MailingCountry = 'Australia');
		    insert cont1;
		    
		    Task objTask = new Task(ownerid = objUser.Id, whatid = objAccount.id, activityDate = date.today(), status = 'Completed', whoid = cont1.id, subject = 'Clicked Link : Marketo Test', Description = 'abc', Processed_From_Batch__c = false);
		    insert objTask;
		    
		    Database.executeBatch(new SL_MoveMarketoTaskToCustomMarketo(), 200);
		    
		    Test.stopTest();
		    
		    List<Marketo_Activitiy__c> lstMA = [SELECT Id FROM Marketo_Activitiy__c WHERE Task_Id__c =:objTask.Id];
		    system.assert(lstMA.size()>0);
		    Task objUpdatedTask = [SELECT Id,Processed_From_Batch__c FROM Task WHERE Id =:objTask.Id];
		    system.assert(objUpdatedTask.Processed_From_Batch__c);
        }
 }
}