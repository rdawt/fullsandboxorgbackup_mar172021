/*    
    Author  :       Vidhya Krishnan
    Date Created:   10/10/12
    Description:    Community Registration Management API Class for Australia Market Vectors
*/
global class CommunityManagementAUAPI{

    global static CommunityManagementAU cmAU {get;set;}
    
    // This method is used in login authentication
    WebService static string getCMemberId(string communityId, string userName)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        String s = cmAU.getCMemberId(communityId,userName);
        return s;
    }
    // This method is used to authenticate the email
    WebService static string isEmailTaken(string communityId, string emailAddress)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        string s = cmAU.isEmailTaken(communityId, emailAddress);
        system.debug('Is Email Taken......'+s);
        return s;
    }
    // returns if the member is contact or lead or just registrant
    WebService static string getCMemberType(string CMemberId)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        string s = cmAU.getCMemberType(CMemberId); // returns null if the given Id is wrong or if it is deleted Id
        return s;
    }
    // This method is used in login authentication
    WebService static string getPassword(string CMemberId)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        string s = cmAU.getPassword(CMemberId); // returns encryped password
        return s;
    }
    // This method resets user encrypted password
    WebService static boolean setPassword(string CMemberId, string strPassword)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        boolean b = cmAU.setPassword(CMemberId, strPassword);
        return b;
    }
    // This method checks if the Registrant is Activated (ready to use the account) or not
    WebService static boolean isActive(string CMemberId){
        if(cmAU == null) cmAU = new CommunityManagementAU();
        boolean b = cmAU.isActive(CMemberId);
        return b;
    }    
    // This method sets a Registrant Active once they click the activate link 
    WebService static boolean setActive(string CMemberId, boolean active){
        if(cmAU == null) cmAU = new CommunityManagementAU();
        boolean b = cmAU.setActive(CMemberId, active);
        return b;
    }
    // This method fetch all details of a community - its name, description, domain & is_active
    WebService static  Community__c getCommunityDetails(string communityId){
        if(cmAU == null) cmAU = new CommunityManagementAU();
        Community__c c = new Community__c();
        c = cmAU.getCommunityDetails(communityId);
        return c;
    }
    // This method is used to fetch details in profile edit page
    WebService static CommunityManagementAU.AUMemberRecord getCMember(string CMemberId)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        CommunityManagementAU.AUMemberRecord memRec = new CommunityManagementAU.AUMemberRecord();
        memRec = cmAU.getMemberRecord(CMemberId);
        system.debug('Get Member Record...'+memRec);
        return memRec;
    }
    // This method is used to set last login time stamp when user log in
    WebService static void setLastLogin(string CMemberId, DateTime lastLoginDate)
    {
        if(cmAU == null) cmAU = new CommunityManagementAU();
        cmAU.setLastLogin(CMemberId, DateTime.valueOf(lastLoginDate));
    }
    /*	Registration 2.0 changes */
    WebService static string getErrorPrefix(){
        if(cmAU == null) cmAU = new CommunityManagementAU();
        return cmAU.getErrorPrefix(); 
    }
    WebService static string getWarningPrefix(){
        if(cmAU == null) cmAU = new CommunityManagementAU();
        return cmAU.getWarningPrefix(); 
    }
    // This method returns the eligibility for the given email id
    Webservice static string getSubGroupId(string email,string communityId){
        if(cmAU == null) cmAU = new CommunityManagementAU();
    	string subGrpId = cmAU.getSubGroupId(email, communityId);
    	return subGrpId; 
    	// Returns null if the community Id provided is not valid
    	// Returns Subscription Group Id up on success
    }
    /*	Only this method uses cmAU Australia Base Class	*/
    WebService static string addCMemberAndSubscriptions(CommunityManagementAU.AUMemberRecord memRec, List<SubscriptionManagementAPI.SubscriptionDetails> subList){
        if(cmAU == null) cmAU = new CommunityManagementAU();
    	string result = cmAU.addCMemberAndSubscriptions(memRec, subList);
    	return result;
    	// Returns cmemberId on success
    	// Returns Error Message that starts with either 'sf_error:' or 'sf_warning:'
    	// When it has 'sf_warning:' - the Registrant is created but it failed with either contact mapping or subscriptions
    }
    // This method is called from edit profile
    WebService static string setCMemberAndSubscriptions(String CMemberId, CommunityManagementAU.AUMemberRecord memRec, List<SubscriptionManagementAPI.SubscriptionDetails> subList){
        if(cmAU == null) cmAU = new CommunityManagementAU();
    	string result = cmAU.setCMemberAndSubscriptions(CMemberId, memRec, subList);
    	return result;
    	// Returns cmemberId on success
    	// Returns Error Message that starts with either 'sf_error:' or 'sf_warning:'
    	// When it has 'sf_warning:' - the Registrant is created but it failed with either contact mapping or subscriptions
    }
}