/*  Contact Pace module is not used anymore.    */
trigger EventTrigger on Event (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    /****** 
        EventTriggerHandler handler = new EventTriggerHandler();
        SendEmailNotification se = new SendEmailNotification('Error: Event Trigger Exception');
        
        try {
            if (trigger.isBefore) {
                if (trigger.isDelete) {
                    handler.onBeforeDelete(trigger.old);
                }
            } else {
                if (trigger.isInsert) {
                    handler.onAfterInsert(trigger.new);
                } else if (trigger.isUpdate) {
                    handler.onAfterUpdate(trigger.oldmap, trigger.new);
                }  else if (trigger.isUndelete) {
                    handler.onAfterUndelete(trigger.oldmap, trigger.new);
                }
            }
        }catch(Exception e){
            se.sendEmail('Exception Occurred in the Event Trigger - '+e.getMessage()+trigger.new);
        }
    ******/
}