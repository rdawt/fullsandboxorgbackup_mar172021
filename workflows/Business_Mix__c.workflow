<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_BizMixUniqueId</fullName>
        <field>BizMixContact__c</field>
        <formula>Contact__r.Id +&quot;.&quot;+
TEXT(Business_Mix__c )</formula>
        <name>FU-BizMixUniqueId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>WF-BizMixContact</fullName>
        <actions>
            <name>FU_BizMixUniqueId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Mix__c.Business_Mix__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
