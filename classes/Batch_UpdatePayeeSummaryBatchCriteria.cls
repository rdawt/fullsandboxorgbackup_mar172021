global class Batch_UpdatePayeeSummaryBatchCriteria implements Database.Batchable<sObject>
{
    global Batch_UpdatePayeeSummaryBatchCriteria ()
    {
        
    }
            
    global Database.querylocator start(Database.BatchableContext BC){
        String strQuery = 'SELECT Id, Month__c, Quarter__c, Year__c ' + 
                            ' From PayeeSummaryBatch_Criteria__c ';
            return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<PayeeSummaryBatch_Criteria__c> lstPayeeSummaryBatch_Criteria)
    {
        try
        {
            if(!lstPayeeSummaryBatch_Criteria.isEmpty())
            {
                PayeeSummaryBatch_Criteria__c objPayeeSummaryBatchCriteria = PayeeSummaryBatch_Criteria__c.getOrgDefaults();
                objPayeeSummaryBatchCriteria.Month__c = String.valueOf(Date.today().month());
                objPayeeSummaryBatchCriteria.Year__c = String.valueOf(Date.today().Year());
                objPayeeSummaryBatchCriteria.Quarter__c = String.valueOf(Integer.valueOf(date.today().Month()/3) + 1);
                update objPayeeSummaryBatchCriteria; 
            }
        }
        catch(Exception e)
        {
            
        }
    }
    
    
    global void finish(Database.BatchableContext BC)
    {

    }
         
}