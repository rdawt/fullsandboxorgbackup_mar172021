({
    getFunds : function(component, event, helper) {
        component.set("v.showSpinner",true);
        var action = component.get('c.getFundTickers');
        action.setParams({
            strFundVehicleType :  component.get('v.selectedFundType'),
            boolExcludeProcessed : component.get('v.excludeProcessed'),
            strYear : component.get('v.selectedYear'),
            intQuarter : component.get('v.selectedQuarter'),
            idChannel : component.get('v.selectedChannel')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.fundOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading page.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    getChannels : function(component, event, helper) {
        var action = component.get('c.getChannels');
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.channelOptions", response.getReturnValue());
                if(results.length > 0){
                    component.set("v.selectedChannel",results[0].value);
                    component.set('v.selectedChannelName',results[0].label);
                    helper.getFunds(component, event, helper);
                    helper.getSummaryTableData(component, event, helper);
                }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading page.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    calculateRollupData : function(cmp, event, helper) {
        var action = cmp.get('c.calculatePortfolioChannelRollup');
        action.setParams({
            lstPortfolio :  cmp.get('v.selectedFundValues'),
            idChannel : cmp.get('v.selectedChannel'),
            strYear : cmp.get('v.selectedYear'),
            intQuarter : cmp.get('v.selectedQuarter'),
            strVehicleType : cmp.get('v.selectedFundType')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
               if(result){
                    helper.showToast('Success', 'Records have been updated successfully.','success');
                    cmp.set('v.selectedFundValues',null);
                    helper.getFunds(cmp, event, helper);
                    helper.getSummaryTableData(cmp, event, helper);
               }else{
                helper.showToast('Error', 'Error has occurred while loading data.','error'); 
               }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            cmp.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },

    getSummaryTableData : function(cmp, event, helper) {
        /*if(cmp.get('v.selectedFundType') == 'ETF'){
            cmp.set('v.summaryData',[]);
            cmp.set('v.tableHeader' , 'No items to display.');
            return;
        }*/
        var action = cmp.get('c.getCalculatedDataFundWise');
        action.setParams({
            idChannel : cmp.get('v.selectedChannel'),
            strYear : cmp.get('v.selectedYear'),
            intQuarter : cmp.get('v.selectedQuarter'),
            strVehicleType : cmp.get('v.selectedFundType')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var rows = response.getReturnValue();
               if(rows){
                   if(rows.length > 0){
                        cmp.set('v.tableHeader' , rows.length + ' records showing');
                   }else{
                        cmp.set('v.tableHeader' , 'No items to display.');
                   }
                // for (var i = 0; i < rows.length; i++) {
                //     var row = rows[i];
                //     if (row.Fund__c){
                //         row.fundName = row.Fund__r.Name;
                //         row.fundLink = '/'+row.Fund__c;
                //         row.netAsset = (cmp.get('v.selectedQuarter') == 1 ? row.Q1_NetSales__c : 
                //                        (cmp.get('v.selectedQuarter') == 2 ? row.Q2_NetSales__c : 
                //                        (cmp.get('v.selectedQuarter') == 3 ? row.Q3_NetSales__c : 
                //                        (cmp.get('v.selectedQuarter') == 4 ? row.Q4_NetSales__c : '' ))));
                //     } 
                // }
                cmp.set('v.summaryData',rows);
               }else{
                helper.showToast('Error', 'Error has occurred while loading data.','error'); 
               }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            cmp.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    showDrillDownDetails : function(cmp, row, helper) {
        var action = cmp.get('c.getMFFirmwiseDrillDown');
        action.setParams({
            idChannel : cmp.get('v.selectedChannel'),
            strYear : cmp.get('v.selectedYear'),
            intQuarter : cmp.get('v.selectedQuarter'),
            strVehicleType : cmp.get('v.selectedFundType'),
            idFund : row.fundId
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var rows = response.getReturnValue();
               if(rows){
                // for (var i = 0; i < rows.length; i++) {
                //     var row = rows[i];
                //     if (row.accountId){
                //         row.accountLink = '/'+row.accountId;
                //     } 
                // }
                var tableHeader = (rows.length > 0 ?  rows.length + ' records showing' : 'No items to display.');
                $A.createComponent("c:PortfolioChannelRollupDrillDown_Cmp", { "data": rows, "selectedYear" : cmp.get('v.selectedYear'), "selectedFundType" : cmp.get('v.selectedFundType') , "selectedChannelName" : cmp.get('v.selectedChannelName'), "tableHeader" : tableHeader }, 
                function(content, status) {
                    if (status === "SUCCESS") {
                        var modalBody = content;
                        cmp.find('overlayLib').showCustomModal({
                            header: 'Firm level drill down', 
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "slds-modal_large",
                            closeCallback: function() {
                                // helper.showRowDetails(component,component.get('v.row'));
                            }
                        })
                    }
                });

               }else{
                helper.showToast('Error', 'Error has occurred while loading data.','error'); 
               }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            cmp.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                return primer(x[field]);
            }
            : function(x) {
                return x[field];
            };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

})