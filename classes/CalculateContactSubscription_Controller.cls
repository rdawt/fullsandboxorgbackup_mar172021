/*
    Class Name  :   CalculateContactSubscription_Controller
    JIRA Ticket :   
    Description :   Controller for ligthning component CalculateContactSubscription 
*/
public with sharing class CalculateContactSubscription_Controller{
    public static String strEm          {get;set;}
    public static Boolean hasEmailOpt   {get;set;}
    public static String subGrId        {get;set;}
    public static Contact con           {get;set;}
    
    /*
        Method Name :   getSubscriptionDetails
        Params      :   recordId -> Current Contact Id, for which subscription management is in progress
        Descritpion :   Logic similar to SubscriptionManagement (mentioned in ticket)
                        get contact details, get related subscription group subscription, get related susbscription members
                        and create wrapper list(RecordSet) , passed to lightning component.
    */
    @AuraEnabled
    public static conAndSubDetailWrapper getSubscriptionDetails(Id recordId){
        //Map<Contact,list<Recordset>> conNameToRSLstMap = new Map<Contact,list<Recordset>>();
        list<RecordSet> lstRSet = new list<RecordSet>();
        list<Subscription_Group_Subscription__c> lstSGS =new list<Subscription_Group_Subscription__c>();
        
        Contact objContact = [Select Id, MailingCountry, Auto_Subscription_Group__c,Auto_Subscription_Group__r.Name, DB_Source__c,
                                Service_Level__c, Channel_NEW__c,Subscription_Group__c, AccountId ,
                                OwnerId
                                from Contact 
                                where Id=:recordId limit 1];
        List<Contact> lstContact = new List<Contact>();
        lstContact.add(objContact);
        
        // Calling Dynamic Eligilibity to get Subscription Group Id Check
        Map<Id,Id> mapCurrentContact_CalculatedSubscription = new Map<Id, Id>();
        
        // calling Eligilibity --Added condition based on the Country.
        boolean subGrpCountry = SubscriptionGrpCountry(objContact.MailingCountry);
        
        if(subGrpCountry){
            mapCurrentContact_CalculatedSubscription = SubscriptionEligibility.getEligibility(lstContact);
        }
        else{
            mapCurrentContact_CalculatedSubscription = DynamicEligibility.getEligibility(lstContact);
        } 
        
        
        //get details from contact and assign to global variables
        con = getContactDetails(recordId);

        // if Contact is New Record and there is no Subscription link to it
        if(subGrId == NULL || subGrId == '')
            subGrId = mapCurrentContact_CalculatedSubscription.get(recordId);
        else if(subGrId != null && subGrId != '') // if Contact is existing one and there is already Subscription link to it
        {
            if(subGrId != mapCurrentContact_CalculatedSubscription.get(recordId))
            {
                //objContact.Auto_Subscription_Group__c = mapCurrentContact_CalculatedSubscription.get(recordId);
                //objContact.Subscription_Group__c = mapCurrentContact_CalculatedSubscription.get(recordId);
                //update objContact;
                
                subGrId = mapCurrentContact_CalculatedSubscription.get(recordId);
            }
        }

        Subscription_Group__c objSubscription_Group = new Subscription_Group__c();
        if(subGrId != NULL && subGrId != '')
            objSubscription_Group = [Select Id, Name from Subscription_Group__c where Id =: subGrId limit 1];
        
        lstSGS = [SELECT Subscription__r.Subscription_Type__r.Id, Sort_Order_For_Form__c,
                         Subscription__r.Name,Subscription__r.Subscription_Type__r.Name, 
                         Subscription__r.Subscription_Display_Name__c, Subscription__r.Id,
                         Subscription_Group__r.Id, Subscription_Group__r.Name,Subscription__r.IsActive__c
                    FROM Subscription_Group_Subscription__c 
                    WHERE Subscription_Group__c =: subGrId 
                        AND Subscription__r.IsActive__c=true 
                        ORDER BY Subscription__r.Name,Sort_Order_For_Form__c];
        
        list<Subscription_Member__c> lstSM = [SELECT Id, Subscription__c,Subscription_Unsubscribed_Date__c,Subscribed__c, Contact__c 
                                                FROM Subscription_Member__c WHERE Contact__c =: recordId ];
        
        if(hasEmailOpt!=true && strEm != null && !(strEm.equals('')) && !lstSGS.isEmpty())
        {
            for(Subscription_Group_Subscription__c sgs: lstSGS)
            {
                lstRSet.add(createWrapper(sgs,lstSM));
            }
            list<RecordSet> lstRSetBackup = new list<RecordSet>();
            lstRSetBackup = lstRSet.clone();
        }
        //conNameToRSLstMap.put(con,lstRSet);
        conAndSubDetailWrapper objWrapper = new conAndSubDetailWrapper();
        objWrapper.con = con;
        objWrapper.wrapperLst = lstRSet;
        objWrapper.subscriptionGroup = objSubscription_Group;
        System.debug('con-------->'+con);
        //System.debug('map returning-------->'+conNameToRSLstMap);
        return objWrapper;
    }
    
    private static boolean SubscriptionGrpCountry(string mailingCountry){
        return Eligibility_Subscription_Country__c.getValues(mailingCountry) == null ? false : true;
    }
    
    /*
        Method Name :   getContactDetails
        Params      :   recordId -> Current Contact Id, for which subscription management is in progress
        Descritpion :   called from above method to get contact details
    */
    public static Contact  getContactDetails(Id recordId){
        con=[Select Id,Email,FirstName, Name, 
                    LastName,HasOptedOutOfEmail, Channel_New__c,
                    Subscription_Group__c,Bounced_Email__c
                from Contact 
                Where Id=:recordId LIMIT 1];
        //subGrId = con.Subscription_Group__c;
        System.debug('con----11------->'+con);
        /*conName='' ;
        if(con.FirstName != NULL){
            conName = conName+con.FirstName+' ';
        }
        conName = conName+con.LastName;*/
        
        if(con.Email != null && !(con.Email.equals('')))
        {
            strEm=con.Email;
        }
        else
        {
            strEm=null;
        }
            
        if(con.HasOptedOutOfEmail!=true)
        {
            hasEmailOpt=false;
        }
        else
        {
            hasEmailOpt=true;
        }

        return con;
    }
    
    /*
        Method Name :   getContactDetails
        Params      :   sgs -> Susbscription group subscription
        Descritpion :   called from getSubscriptionDetails method to create wrapper list
    */
    public static RecordSet createWrapper(Subscription_Group_Subscription__c sgs, list<Subscription_Member__c> lstSM){
        //loop through every record in the subscription_groupd_subscription for the given set of subscription id in sub_grp 
        //and put them into record set which will be returned
        System.debug('called twice------------');
        RecordSet rs = new RecordSet();
        rs.SubType = sgs.Subscription__r.Subscription_Type__r.Name;
        rs.SubName = sgs.Subscription__r.Name;
        rs.SubId = sgs.Subscription__r.Id;
        for(Subscription_Member__c sm: lstSM)
        { 
            if(sm.Subscription__c == sgs.Subscription__r.Id)
            {
                rs.Subscribed = sm.Subscribed__c;
                rs.subUnsubDate=sm.Subscription_Unsubscribed_Date__c;
                rs.SubMemId = sm.Id;
            }
        }
        return rs;
    }
    
    /*
        Method Name :   setSubscription
        Params      :   recordId -> Current Contact Id, for which subscription management is in progress
                        lstOldRSet -> wrapperList without change (old wrapper list which is not modified in lightning component, used to compare with new one)                      
                        lstRSetChanged -> new wrapperList, which has beedn modified by user in lightning component.
        Description :   update the Subscription members(related to current contact) , and inserts Subscription members(not related to current contact),
                        based on the new wrapper list by comparing with old wrapper list.
    */
    /*
    @AuraEnabled
    public static String setSubscription(Id recordId, list<RecordSet> lstOldRSet, list<RecordSet> lstRSetChanged){
        String resultStr = 'Success!!';
        map<String, Subscription_Member__c> mapChanges = new map<String, Subscription_Member__c>();
        list<Subscription_Member__c> lstRec = new list<Subscription_Member__c>();
        list<Subscription_Member__c> lstRecInsert = new list<Subscription_Member__c>();
        list<Subscription_Member__c> lstRecUpdate = new list<Subscription_Member__c>();
        Map<String,Boolean> checkOldSubscribeVal = new Map<String,Boolean>();
        
        for(RecordSet rec:lstOldRSet)
        {
            checkOldSubscribeVal.put(rec.SubId,rec.Subscribed);
        }
                    
        lstRec = [Select Id,Subscription__c,Subscribed__c,Subscription_Unsubscribed_Date__c,Contact__c,Composite_Key_ExternalId_SId_CId_OR_LId__c from Subscription_Member__c where Contact__c=:recordId];
        for(Subscription_Member__c subMem : lstRec)
        {
            mapChanges.put(subMem.Subscription__c, subMem);
        }
        
        for(RecordSet sm : lstRSetChanged)
        {
            if(mapChanges.containsKey(sm.SubId))
            { // Update
                if(lstOldRSet.size()>0)
                {
                    lstRecUpdate.add(createSubMemLstToUpdate(mapChanges,sm,checkOldSubscribeVal));
                }
            }
            else
            { // Insert
                if(sm.Subscribed == true){
                    lstRecInsert.add(createSubMemLstToInsert(sm,recordId));
                }               
            }
        }
        try{
            if(!lstRecUpdate.isEmpty()){
                update lstRecUpdate;
            }
            if(!lstRecInsert.isEmpty()){
                insert lstRecInsert;
            }
        }catch(DMLException e){
            resultStr = 'Error Message : '+ e.getDmlMessage(0);
        }
        return resultStr;
    }
    */
    /*
    public static Subscription_Member__c createSubMemLstToUpdate(map<String, Subscription_Member__c> mapChanges,RecordSet sm,Map<String,Boolean> checkOldSubscribeVal){
        Subscription_Member__c smem = new Subscription_Member__c(Id = mapChanges.get(sm.SubId).Id);
        smem.Subscribed__c = sm.Subscribed;
        System.debug('checkOldSubscribeVal.get(sm.SubId)-------------->'+checkOldSubscribeVal.get(sm.SubId));
        System.debug('sm.Subscribed -------------->'+sm.Subscribed );
        if(checkOldSubscribeVal.get(sm.SubId) == false && sm.Subscribed == true)
        { // if previously un-subscribed & subscribing now
            smem.Subscription_Unsubscribed_Date__c = null;
        } 
        
        if(checkOldSubscribeVal.get(sm.SubId) == true && sm.Subscribed == false)
        { // if previously subscribed & un-subscribing now
            smem.Subscription_Unsubscribed_Date__c = system.now();
        }
        return smem;
    }
    
    public static Subscription_Member__c createSubMemLstToInsert(RecordSet sm,Id recordId){
        Subscription_Member__c smem = new Subscription_Member__c();
        smem.Contact__c = recordId;
        smem.Subscription__c = sm.SubId;
        smem.Subscribed__c = sm.Subscribed;
        
        return smem;
    }
    */
    
    /*
        Method Name :   getBaseUrl
        Description :   returning base URL of the org, 
                        Used in lightning component for redirect on cancelling and conversion
    */
    @AuraEnabled
    public static String getBaseUrl () {
        return system.URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    /*
        Class Name  :   Recordset
        Descrption  :   Used to store subscription member, subscription group subscription group info
    */
    public class Recordset {
        @AuraEnabled    public Boolean Subscribed {get; set;} //Sub Mem --> Subscribed
        @AuraEnabled    public String SubMemId {get; set;} //Sub Mem --> Sub Mem Id
        @AuraEnabled    public String SubId {get; set;} //Sub Mem --> Subscription --> Id
        @AuraEnabled    public String SubName {get; set;} //Sub Mem --> Subscription --> Name
        @AuraEnabled    public DateTime subUnsubDate{get;set;}
        @AuraEnabled    public String SubType{get; set;} //Sub Mem --> Subscription --> Subscription Type --> Name
    }
    
    public class conAndSubDetailWrapper {
        @AuraEnabled    public Contact con {get; set;}
        @AuraEnabled    public list<Recordset> wrapperLst {get; set;}
        @AuraEnabled    public Subscription_Group__c subscriptionGroup {get; set;}
    }
}