@isTest
private class batchContactUpdateTest  {

    @isTest  static void testbatchContactUpdate() {     
        TaskTriggerHandler eth = new TaskTriggerHandler();
        DMLException e = null;
        Set<ID> accIds = new Set<ID>();    
        List<Contact> conlist = new List<Contact>();
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    
        Account acc = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        
        Contact con = new Contact(LastName='Aalund',FirstName='Gail',AccountId=acc.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        //insert con;
        
        Contact con1 = new Contact(LastName='Aalund',FirstName='Gail2',AccountId=acc.Id,MailingStreet='xyz', Email='test-con1@abc.com',MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        //insert con1;
        
        Contact con2 = new Contact(LastName='Aalund-Con2',FirstName='Gail3-Con2',AccountId=acc.Id,Email='test-con2@abc.com',MailingStreet='xyz1', MailingCountry='United States',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null); //,OwnerId ='005A0000000Oqpm'
        //insert con2;
        
        Contact con3 = new Contact(LastName='Aalund',FirstName='Gail4',AccountId=acc.Id,Email='test-con3@abc.com',MailingStreet='999', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        //insert con3;
        
        Contact con4 = new Contact(LastName='Aalund-Contact4-Con4',FirstName='Gail-Contact4-Con4',AccountId=acc.Id, Email='test-con4@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        //insert con4;
    
        List<Contact> lstContact = new List<Contact>();    
        lstContact.add(con);
        lstContact.add(con1);
        lstContact.add(con2);
        lstContact.add(con3);
        lstContact.add(con4);
    
        insert lstContact;
    
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+con2.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
    
        //\'call\' AND Status = \'call completed\'
        Task t1 = new Task(Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId, Activity_Date_Derived__c = system.today());
        
        Comments = Comments+'2nd task for con with who id in task object';    
        Task t2 = new Task(Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap2',ActivityDate = System.today()+2, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId, Activity_Date_Derived__c = system.today());  
        
        Comments = Comments+'3rd task for con with who id in task object';
        Task t3 = new Task(Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',ActivityDate = System.today()+5, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId, Activity_Date_Derived__c = system.today());
                                        
        Task t4 = new Task(Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
        ActivityDate = System.today()+5, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con1.OwnerId, Activity_Date_Derived__c = system.today());
        
        //activity date y'day
        Task t5 = new Task(Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
        ActivityDate = System.today()+5, ReminderDateTime = System.now()-1, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId,Activity_Date_Derived__c = system.today());
                                        
        List<Task> lstTask = new List<Task>();
        lstTask.add(t1);
        lstTask.add(t2);
        lstTask.add(t3);
        lstTask.add(t4);
    
        insert lstTask;
    
   
    
        event e1 = new Event(Description = Comments,  Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-1, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()-1, Activity_Date_Derived__c = system.today());
        
        Comments = Comments+'2nd task for con with who id in task object';    
        event e2 = new Event(Description = Comments,  Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap2',
                                        ActivityDate = System.today()-2, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()-2, Activity_Date_Derived__c = system.today());  
        
        Comments = Comments+'3rd task for con with who id in task object';    
        event e3 = new Event(Description = Comments,   Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-5, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()-5, Activity_Date_Derived__c = system.today());
        
        event e4 = new Event(Description = Comments,   Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-5, ReminderDateTime = System.now()+1, WhoId = con2.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()-5, Activity_Date_Derived__c = system.today());
        
        //event date is 7 days + todays date
        event e5 = new Event(result__c='held',Description = Comments,   Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap3',ActivityDate = System.today()+7, ReminderDateTime = System.now()+7, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7, Activity_Date_Derived__c = system.today()); 
    
    
        List<Event> lstEvent = new List<Event>();
        lstEvent.add(e1);
        lstEvent.add(e2);
        lstEvent.add(e3);
        lstEvent.add(e4);
        lstEvent.add(e5);
    
        insert lstEvent;    
        //batchContactUpdate.fetchEventRecord(e5.Id);
        //batchContactUpdate.fetchTaskRecord(t4.Id);
        
        EventRelation er = new EventRelation(EventId = e5.Id,RelationId = con.Id,isParent = false, isInvitee = false);
        EventRelation er2 = new EventRelation(EventId = e5.Id,RelationId = con3.Id,isParent = false, isInvitee = false);
        EventRelation er3 = new EventRelation(EventId = e5.Id,RelationId = con4.Id,isParent = false, isInvitee = false);        
        insert new List<EventRelation>{er,er2,er3};
        
        TaskRelation tr = new TaskRelation(TaskId = t1.Id,RelationId = con2.Id);
        TaskRelation tr2 = new TaskRelation(TaskId = t1.Id,RelationId = con3.Id);
        TaskRelation tr3 = new TaskRelation(TaskId = t1.Id,RelationId = con4.Id);       
        insert new List<TaskRelation>{tr,tr2,tr3};
        
        string jobId = System.schedule('batchContactUpdateScheduleApex', '0 0 0 5 7 ? 2022',  new BatchScheduleContactUpdate());
        Test.stopTest(); 
    }
}