({
    handleInit : function(cmp, event, helper) {
        cmp.set("v.showSpinner",true);
        //helper.getFunds(cmp, event, helper);
        helper.getChannels(cmp, event, helper);

        cmp.set('v.summaryColumnsMF', [
            {label: 'Fund Ticker', fieldName: 'fundTicker', type: 'text' , sortable: true},
            {label: 'Fund Name', fieldName: 'fundLink', type: 'url', 
            typeAttributes: {label: { fieldName: 'fundName' }, target: '_blank'} , sortable: true },
            {label: 'Asset Amount', fieldName: 'netSales_sfdc', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Drill Down', type: 'button', typeAttributes: { label: 'Dril Down', name: 'firm_drill_down_mf', title: 'Click to View Firm Level Drill Down Details'},cellAttributes: { alignment: 'center' }}
        ]);   

        cmp.set('v.summaryColumnsETF', [
            // {label: 'Fund Ticker', fieldName: 'fundTicker', type: 'text' , sortable: true},
            {label: 'Fund Ticker', fieldName: 'fundLink', type: 'url', 
            typeAttributes: {label: { fieldName: 'fundTicker' }, target: '_blank'} , sortable: true },
            {label: 'AUM (BR)', fieldName: 'aum_br', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'AUM (SFDC)', fieldName: 'aum_sfdc', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Net Sales (BR)', fieldName: 'netSales_br', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Net Sales (SFDC)', fieldName: 'netSales_sfdc', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Net Sales Last Month (BR)', fieldName: 'netSalesLastMonth_br', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Net Sales Last Month (SFDC)', fieldName: 'netSalesLastMonth_sfdc', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
            {label: 'Drill Down', type: 'button', typeAttributes: { label: 'Dril Down', name: 'firm_drill_down_etf', title: 'Click to View Firm Level Drill Down Details'},cellAttributes: { alignment: 'center' }}
        ]);   

        //,cellAttributes: { alignment: 'left' }
        // {label: 'Fund Name', fieldName: 'Fund__r.Name', type: 'text'},
    },

    handleFundOptionChange : function(cmp, event, helper) {
        var selectedFunds = event.getParam("value");
        cmp.set('v.selectedFundValues',selectedFunds);
    },

    handleCalculate : function(cmp, event, helper) {
        cmp.set("v.showSpinner",true);
        helper.calculateRollupData(cmp, event, helper);
    },

    handleYearChange : function(cmp, event, helper) {
        helper.getFunds(cmp, event, helper);
        helper.getSummaryTableData(cmp, event, helper);
    },

    handleQuarterChange : function(cmp, event, helper) {
        helper.getFunds(cmp, event, helper);
        helper.getSummaryTableData(cmp, event, helper);
    },

    handleChannelChange : function(cmp, event, helper) {
        var value = event.getParam('value');
        var channels = cmp.get('v.channelOptions');
        for (var i = 0; i < channels.length; i++) {
            if(channels[i].value == value){
                cmp.set('v.selectedChannelName',channels[i].label);
                break;
            }
        }
        helper.getFunds(cmp, event, helper);
        helper.getSummaryTableData(cmp, event, helper);
    },

    hanleCheckboxChange : function(cmp, event, helper) {
        helper.getFunds(cmp, event, helper);
    },

    handleFundTypeChange : function(cmp, event, helper) {
        helper.getFunds(cmp, event, helper);
        helper.getSummaryTableData(cmp, event, helper);
    },

    handleDetailRowAction: function (cmp, event, helper) {
        cmp.set('v.showSpinner',true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'firm_drill_down_mf':
                helper.showDrillDownDetails(cmp,row,helper);
                break;
            case 'firm_drill_down_etf':
                helper.showDrillDownDetails(cmp,row,helper);
                break;
            default:
                helper.showDrillDownDetails(cmp,row,helper);
                break;
        }
    },

    handleSort : function(cmp, event,helper) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var data = cmp.get('v.summaryData');
        var cloneData = data.slice(0);
        cloneData.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.summaryData', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBy', sortedBy);
    }
})