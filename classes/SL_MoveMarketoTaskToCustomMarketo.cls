global class SL_MoveMarketoTaskToCustomMarketo implements Database.Batchable<sObject>{
    
    global Database.querylocator start(Database.BatchableContext BC){
        String strQuery = 'SELECT Id, ActivityDate, Description, WhoId, Priority, AccountId, Status, Subject, Processed_From_Batch__c FROM Task WHERE Owner.Name = \'Marketo API\' and Processed_From_Batch__c = false';
            return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<Task> lstTask){
        
        List<Marketo_Activitiy__c> lstMarketoActivityToInsert = new List<Marketo_Activitiy__c>();
        List<Task> lstTaskToUpdate = new List<Task>();
        
        for(Task objTask :lstTask){
             Marketo_Activitiy__c objMarketo = new Marketo_Activitiy__c();
            objMarketo.ActivityDate__c = objTask.ActivityDate;
            objMarketo.Comments__c = objTask.Description;
            objMarketo.Who__c = objTask.WhoId;
            objMarketo.Priority__c = objTask.Priority;
            objMarketo.What__c = objTask.AccountId;
            objMarketo.Status__c = objTask.Status;
            objMarketo.Subject__c = objTask.Subject;
            objMarketo.Task_Id__c = objTask.Id;
            lstMarketoActivityToInsert.add(objMarketo);
            
            objTask.Processed_From_Batch__c = true;
            lstTaskToUpdate.add(objTask);
        }
        
        if(!lstMarketoActivityToInsert.isEmpty()){
            insert lstMarketoActivityToInsert;
        }
        
        if(!lstTaskToUpdate.isEmpty()){
            update lstTaskToUpdate;
        }
       
        
    }
    
    global void finish(Database.BatchableContext BC){}    
}