global class Schedule_UpdatePayeeSummaryBatchCriteria implements Schedulable 
{
   global void execute(SchedulableContext SC) 
   {
      String jobId = Database.executeBatch(new Batch_UpdatePayeeSummaryBatchCriteria(), 1);
   }
}