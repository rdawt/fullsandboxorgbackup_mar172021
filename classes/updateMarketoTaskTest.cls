@isTest
private class updateMarketoTaskTest {

 static testMethod void testUpdateMarketoTest() {
        try{
            Test.startTest();
            list<Task> marketoTask = new list<Task>();
            string marketoAPIUserId = '005A00000032QAD';
            string jobId = System.schedule('testTaskScheduleApex', '0 0 0 5 7 ? 2022',  new updateMarketoTask());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('0 0 0 5 7 ? 2022', ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2022-07-05 00:00:00',String.valueOf(ct.NextFireTime));
            
            Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
            insert acc;
            Contact con = new Contact(FirstName = 'ABC', LastName = 'ABC', Email = 'abc@gmail.com', AccountId = acc.Id, MailingCountry = 'USA');
            insert con;
            Task marketoTask1 = new Task (ownerid = '005A00000032QAD', whatid = acc.id, activityDate = date.today(), status = 'Completed', whoid = con.id, Summary_Recap__c = 'Opened Email:Marketo Test', Description = 'abc');
            insert marketoTask1;
            Task marketoTask2 = new Task (ownerid = '005A00000032QAD', whatid = acc.id, activityDate = date.today(), status = 'Completed', whoid = con.id, Summary_Recap__c = 'Send Email:Marketo Test', Description = 'abc');
            insert marketoTask2;
            //AggregateResult[] delCount = [select count(id) from Task where ownerId = '005A00000032QAD' and Status != 'Clicked Link'];
            if(Test.isRunningTest()){
                marketoTask = [select id, Summary_Recap__c from Task where ownerId =:marketoAPIUserId and Status != 'Clicked Link'];
            }
            else{
                marketoTask = [select id, Summary_Recap__c from Task where ownerId =:marketoAPIUserId and Status != 'Clicked Link' limit 50000];
            }

            If (marketoTask.size() > 10000) {
                SendEmailNotification se = new SendEmailNotification('MKTO Send & Deletion has high delete count: ' + marketoTask.size());

            }

            
            Test.stopTest();
        }catch(Exception e){}
    }
 }