/**
    Author: Shankar Venkateswaran
    Created Date: 04/09/2018
    Description: Schedule Apex Code to check ETF count for email notification
                    
                    
Modified : 04/09/2018
  
*/
global class SalesConnectETFTranCount implements Schedulable {
    global void execute(SchedulableContext SC){
        Integer SCTranCountAggResult =0;
        string msg;
       
        try{
       
            List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject,HTMLValue,developername  from EmailTemplate where developername = 'DST_ETF_DataLoadAlert']; //id=00X2F000000MiumUAC
       
           // if (lstEmailTemplates[0].developername == 'DST_ETF_DataLoadAlert') { 
               /*
                // Execute this code from developer Console to schedule this apex class hourly
                String CRON_EXP_SCETFCount = '0 0 * * * ?';
                SalesConnectETFTranCount SCTranCountJob = new SalesConnectETFTranCount();
                system.schedule('Run Tran ETF Count Daily', CRON_EXP_SCETFCount, SCTranCountJob);
                
                // To monitor the progress use this code
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                    FROM CronTrigger WHERE Id = :ctx.getTriggerId()];
                System.assertEquals(CRON_EXP_SCETFCount, ct.CronExpression);
                System.assertEquals(0, ct.TimesTriggered);
                System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
                */
                SCTranCountAggResult = [SELECT count() FROM SalesConnect__NewTransaction__c WHERE Fund_Vehicle_Type__c = 'ETF' AND CreatedDate = Today AND SalesConnect__Source_Description__c = 'MSSB ETF Trades'];
           
               if (SCTranCountAggResult > 0 ) {
                   
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(lstEmailTemplates[0].Id);
                    mail.setSenderDisplayName('Bryan Paisley');
                    mail.setSubject('MS ETF Transaction Data Load Alert');
                    DailySalesAlertsList__c dlSalesDl = DailySalesAlertsList__c.getInstance('00eA0000000q4Ds');
                    mail.setToAddresses(new String[] {dlSalesDl.ToList__c});
                    mail.setccAddresses(new String[] {dlSalesDl.CCList__c});
                   // mail.setInReplyTo(dlSalesDl.CCList__c);
                   // mail.setToAddresses(new String[] {'svenkateswaran@vaneck.com'});
                    mail.setReplyTo(dlSalesDl.CCList__c);
                    mail.setHtmlBody(lstEmailTemplates[0].HTMLValue);
                   //mail.setToAddresses(new String[] {'svenkateswaran@vaneck.com','Help.Sales@vaneck.com'});
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
               }
               else{
                     
                  SendEmailNotification se = new SendEmailNotification('No ETF Data is loaded today and the Count in Transaction is: ' + SCTranCountAggResult );
                  DailySalesAlertsList__c dlSalesDl = DailySalesAlertsList__c.getInstance('00eA0000000q4Ds');
                  Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                  //message.toAddresses = new String[] {dlSalesDl.ToList__c};
                  message.toAddresses = new String[] {dlSalesDl.SalesTechOnly_ForTest__c};
                  message.subject = 'No ETF Data is Loaded by DST Data Feed';
                  message.setSenderDisplayName('Bryan Paisley');
                 // message.fromAddress = new String[] {dlSalesDl.SalesTechOnly_ForTest__c};
                  message.setSubject('MS ETF Transaction Data Load Alert');
                  message.setReplyTo(dlSalesDl.CCList__c);
                  //message.setReplyTo('svenkateswaran@vaneck.com');
                  message.setReplyTo(dlSalesDl.SalesTechOnly_ForTest__c);
                  message.plainTextBody = 'FYI - No ETF Data is loaded and the Count in Transaction is: \n ' + ' ' +SCTranCountAggResult;
                  Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                  Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                  if (results[0].success) {
                      System.debug('The email was sent successfully.');
                    
                  } else {
                      System.debug('The email failed to send: ' + results[0].errors[0].message);
                    
                  }

                 
               }
              
                   system.debug('Tran ETF Count+......'+SCTranCountAggResult);
                  
          //  }  
           // else{
            //     system.debug('Inside else in not found the right html +......'+lstEmailTemplates[0].developername);
            //     SendEmailNotification se = new SendEmailNotification('Email' + ' (' +'html with no header format' + ')'+ 'no longer exists' );
            //     msg = 'Please recreate a html Email Template without letterhead with developername as DST_ETF_DataLoadAlert as soon as possible to avoid this error';
            //     se.sendEmail(msg); 
            
           // }  
          
              //developername = 'SFDC_DST_DataLoad_Alert'
          
            }catch(Exception e){
                SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Counting ETF in SC Transaction Object Failed');
                msg = 'Exception Occurred in Apex for Counting ETF in SC Transaction Object for today Failed \n '+e.getMessage() + ' ' +SCTranCountAggResult;
                se.sendEmail(msg);
                }
           
            
        
    }
    

}