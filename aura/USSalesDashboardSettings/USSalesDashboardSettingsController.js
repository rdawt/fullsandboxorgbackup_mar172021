({
    handleOnSelect : function(component, event, helper) {
        var selectedItem = event.getParam("name");
        component.set('v.selectedTab',selectedItem);
    }
})