global class contactSalesPortfolioScheduled implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        contactSalesPortfolioBatchProcess b = new contactSalesPortfolioBatchProcess();
        ID myBatchJobID = database.executebatch(b);
    }
}