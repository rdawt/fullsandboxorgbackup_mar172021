<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Clearance</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Clearances</value>
    </caseValues>
    <fields>
        <label><!-- Age (Days) --></label>
        <name>Age__c</name>
    </fields>
    <fields>
        <help><!-- Lookup from firm record (Please ensure this field is filled out). --></help>
        <label><!-- Client Classification (EU &amp; CH) Only --></label>
        <name>Client_Classification_EU_CH_Only__c</name>
    </fields>
    <fields>
        <label><!-- Compliance Review Date --></label>
        <name>ComplianceReviewDate__c</name>
    </fields>
    <fields>
        <help><!-- Firm Type as used to determine what level of clearance a firm may need for specific funds. This value comes from the Firm Record. --></help>
        <label><!-- Compliance Firm Type --></label>
        <name>Compliance_Firm_Type__c</name>
    </fields>
    <fields>
        <label><!-- Compliance Review --></label>
        <name>Compliance_Review__c</name>
    </fields>
    <fields>
        <help><!-- To be set manually when the Clearance has been completed. To be left blank when the Clearance is still in process. --></help>
        <label><!-- Final Clearance Date --></label>
        <name>Date_Cleared__c</name>
    </fields>
    <fields>
        <help><!-- Clearances must be revisited 1 year after the final approval date. --></help>
        <label><!-- Due for review --></label>
        <name>Due_for_review__c</name>
    </fields>
    <fields>
        <label><!-- Evidence Comments --></label>
        <name>Evidence_Comments__c</name>
    </fields>
    <fields>
        <help><!-- For cases where the evidence to be captured is a link to a website --></help>
        <label><!-- Evidence Link --></label>
        <name>Evidence_Link__c</name>
    </fields>
    <fields>
        <label><!-- Evidence Notes --></label>
        <name>Evidence_Notes__c</name>
    </fields>
    <fields>
        <help><!-- Based on the firm type, the jurisdiction, whether it&apos;s a reverse enquiry or an existing client, what evidence is required to be saved as an attachment to this record. --></help>
        <label><!-- Evidence Requirements --></label>
        <name>Evidence_Requirements__c</name>
    </fields>
    <fields>
        <help><!-- Check this box is this is an Existing Investor --></help>
        <label><!-- Existing Investor --></label>
        <name>Existing_Investor__c</name>
    </fields>
    <fields>
        <label><!-- Firm/Branch --></label>
        <name>Firm_Branch__c</name>
        <relationshipLabel><!-- Clearances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Compliance Firm Type (set at Firm/Branch level) --></help>
        <label><!-- Firm Type --></label>
        <name>Firm_Type__c</name>
    </fields>
    <fields>
        <help><!-- Number of funds cleared. --></help>
        <label><!-- Funds Cleared --></label>
        <name>Funds_Cleared__c</name>
    </fields>
    <fields>
        <help><!-- Select Funds for this clearance --></help>
        <label><!-- Funds --></label>
        <name>Funds__c</name>
        <picklistValues>
            <masterLabel>-</masterLabel>
            <translation><!-- - --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>AFK-Africa Index ETF</masterLabel>
            <translation><!-- AFK-Africa Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ANGL-Fallen Angel HY Bond ETF</masterLabel>
            <translation><!-- ANGL-Fallen Angel HY Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>BBH-Biotech ETF</masterLabel>
            <translation><!-- BBH-Biotech ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>BJK-Gaming ETF</masterLabel>
            <translation><!-- BJK-Gaming ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>BONO-LatAm Aggregate Bond ETF</masterLabel>
            <translation><!-- BONO-LatAm Aggregate Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>BRF-Brazil Small-Cap ETF</masterLabel>
            <translation><!-- BRF-Brazil Small-Cap ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CHLC-Renminbi Bond ETF</masterLabel>
            <translation><!-- CHLC-Renminbi Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CNXT-China ETF</masterLabel>
            <translation><!-- CNXT-China ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CNY-Chinese Renminbi/USD ETN</masterLabel>
            <translation><!-- CNY-Chinese Renminbi/USD ETN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>COLX-Colombia ETF</masterLabel>
            <translation><!-- COLX-Colombia ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>DRR-Double Short Euro ETN</masterLabel>
            <translation><!-- DRR-Double Short Euro ETN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EGPT-Egypt Index ETF</masterLabel>
            <translation><!-- EGPT-Egypt Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EMLC-EM Local Curr Bond ETF</masterLabel>
            <translation><!-- EMLC-EM Local Curr Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EVX-Environmental Services ETF</masterLabel>
            <translation><!-- EVX-Environmental Services ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Emerging Markets Fund</masterLabel>
            <translation><!-- Emerging Markets Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>FLTR-Invest Grade Floating Rate ETF</masterLabel>
            <translation><!-- FLTR-Invest Grade Floating Rate ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>FRAK-Unconventional Oil &amp; Gas ETF</masterLabel>
            <translation><!-- FRAK-Unconventional Oil &amp; Gas ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Falcon Gold Equity</masterLabel>
            <translation><!-- Falcon Gold Equity --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Falcon Hard Assets Fund</masterLabel>
            <translation><!-- Falcon Hard Assets Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>G-175 Strategies Ltd. (offs)</masterLabel>
            <translation><!-- G-175 Strategies Ltd. (offs) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GDX-Gold Miners ETF</masterLabel>
            <translation><!-- GDX-Gold Miners ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GDXJ-Junior Gold Miners ETF</masterLabel>
            <translation><!-- GDXJ-Junior Gold Miners ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GERJ-Germany Small-Cap ETF</masterLabel>
            <translation><!-- GERJ-Germany Small-Cap ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GEX-Global Alternative Energy ETF</masterLabel>
            <translation><!-- GEX-Global Alternative Energy ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GGEM-GDP-Emerging Markets Equity</masterLabel>
            <translation><!-- GGEM-GDP-Emerging Markets Equity --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GINT-GDP-International Equity ETF</masterLabel>
            <translation><!-- GINT-GDP-International Equity ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GRNB - VanEck Vectors Green Bond ETF</masterLabel>
            <translation><!-- GRNB - VanEck Vectors Green Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Global Hard Assets Fund</masterLabel>
            <translation><!-- Global Hard Assets Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HA Opportunity Fund Ltd. (offs)</masterLabel>
            <translation><!-- HA Opportunity Fund Ltd. (offs) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HAP-Natural Resources ETF</masterLabel>
            <translation><!-- HAP-Natural Resources ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HYD-High-Yield Municipal Index ETF</masterLabel>
            <translation><!-- HYD-High-Yield Municipal Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HYE-European Curr High Yield ETF</masterLabel>
            <translation><!-- HYE-European Curr High Yield ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HYEM-EM High Yield Bond ETF</masterLabel>
            <translation><!-- HYEM-EM High Yield Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hard Assets 2X Fund Ltd. (offs)</masterLabel>
            <translation><!-- Hard Assets 2X Fund Ltd. (offs) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hard Assets Portfolio Ltd. (offs)</masterLabel>
            <translation><!-- Hard Assets Portfolio Ltd. (offs) --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IDX-Indonesia Index ETF</masterLabel>
            <translation><!-- IDX-Indonesia Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IDXJ-Indonesia Small-Cap ETF</masterLabel>
            <translation><!-- IDXJ-Indonesia Small-Cap ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IHY-Intl High Yield Bond ETF</masterLabel>
            <translation><!-- IHY-Intl High Yield Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>INR-Indian Rupee/USD ETN</masterLabel>
            <translation><!-- INR-Indian Rupee/USD ETN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ITM-Intermediate Muni Index ETF</masterLabel>
            <translation><!-- ITM-Intermediate Muni Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>International Investors Gold Fund</masterLabel>
            <translation><!-- International Investors Gold Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>KOL-Coal ETF</masterLabel>
            <translation><!-- KOL-Coal ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>KWT-Solar Energy ETF</masterLabel>
            <translation><!-- KWT-Solar Energy ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LATM-Lat America Small-Cap Index ETF</masterLabel>
            <translation><!-- LATM-Lat America Small-Cap Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LGOS-Nigeria-Focused W. Africa ETF</masterLabel>
            <translation><!-- LGOS-Nigeria-Focused W. Africa ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LGT Commodity Select Prods UCITS</masterLabel>
            <translation><!-- LGT Commodity Select Prods UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>LOF World Gold Expertise UCITS</masterLabel>
            <translation><!-- LOF World Gold Expertise UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MES-Gulf States Index ETF</masterLabel>
            <translation><!-- MES-Gulf States Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ML/VE Commodities L/S Equity UCITS</masterLabel>
            <translation><!-- ML/VE Commodities L/S Equity UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MLN-Long Municipal Index ETF</masterLabel>
            <translation><!-- MLN-Long Municipal Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MLPE-MLP Index ETF</masterLabel>
            <translation><!-- MLPE-MLP Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MOAT UCITS-Morningstar Wide Moat ETF</masterLabel>
            <translation><!-- MOAT UCITS-Morningstar Wide Moat ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MOAT-Morningstar Wide Moat Rsch ETF</masterLabel>
            <translation><!-- MOAT-Morningstar Wide Moat Rsch ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MOO-Agribusiness ETF</masterLabel>
            <translation><!-- MOO-Agribusiness ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MORT-Mortgage REIT Income ETF</masterLabel>
            <translation><!-- MORT-Mortgage REIT Income ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Multi-Manager Alternatives Fund</masterLabel>
            <translation><!-- Multi-Manager Alternatives Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>NLR-Uranium+Nuclear Energy ETF</masterLabel>
            <translation><!-- NLR-Uranium+Nuclear Energy ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>OIH-Oil Services ETF</masterLabel>
            <translation><!-- OIH-Oil Services ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PEK-China ETF</masterLabel>
            <translation><!-- PEK-China ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PFXF-Pref Securities exFins ETF</masterLabel>
            <translation><!-- PFXF-Pref Securities exFins ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PLND-Poland ETF</masterLabel>
            <translation><!-- PLND-Poland ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PPH-Pharmaceutical ETF</masterLabel>
            <translation><!-- PPH-Pharmaceutical ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PRB-Pre-Refunded Municipal ETF</masterLabel>
            <translation><!-- PRB-Pre-Refunded Municipal ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RAAX-VanEck Vectors Real Asset Allocation ETF</masterLabel>
            <translation><!-- RAAX-VanEck Vectors Real Asset Allocation ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>REMX-Rare EarthStrat. Metals ETF</masterLabel>
            <translation><!-- REMX-Rare EarthStrat. Metals ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RKH-Bank and Brokerage ETF</masterLabel>
            <translation><!-- RKH-Bank and Brokerage ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RSX-Russia ETF</masterLabel>
            <translation><!-- RSX-Russia ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RSXJ-Russia Small-Cap ETF</masterLabel>
            <translation><!-- RSXJ-Russia Small-Cap ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RTH-Retail ETF</masterLabel>
            <translation><!-- RTH-Retail ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SCIF-India Small-Cap Index ETF</masterLabel>
            <translation><!-- SCIF-India Small-Cap Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SLX-Steel ETF</masterLabel>
            <translation><!-- SLX-Steel ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SMB-Short Municipal Index ETF</masterLabel>
            <translation><!-- SMB-Short Municipal Index ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SMH-Semiconductor ETF</masterLabel>
            <translation><!-- SMH-Semiconductor ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTDTM Think Total Market UCITS ETF Defensief</masterLabel>
            <translation><!-- UCTDTM Think Total Market UCITS ETF Defensief --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTEMLC VanEck Vectors J.P. Morgan EM Local Currency Bond ETF</masterLabel>
            <translation><!-- UCTEMLC VanEck Vectors J.P. Morgan EM Local Currency Bond ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTGDIG VanEck Vectors Global Mining UCITS ETF</masterLabel>
            <translation><!-- UCTGDIG VanEck Vectors Global Mining UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTGDX VanEck Vectors Gold Miners UCITS ETF</masterLabel>
            <translation><!-- UCTGDX VanEck Vectors Gold Miners UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTGDXJ VanEck Vectors Junior Gold Miners UCITS ETF</masterLabel>
            <translation><!-- UCTGDXJ VanEck Vectors Junior Gold Miners UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTGFA Global Fallen Angels High Yield Bond UCITS ETF</masterLabel>
            <translation><!-- UCTGFA Global Fallen Angels High Yield Bond UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTHAP VanEck Vectors Natural Resources ETF</masterLabel>
            <translation><!-- UCTHAP VanEck Vectors Natural Resources ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTHYEM Emerging Market High Yield Bond UCITS ETF</masterLabel>
            <translation><!-- UCTHYEM Emerging Market High Yield Bond UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTMOAT VanEck Vectors Morningstar US Wide Moat UCITS ETF</masterLabel>
            <translation><!-- UCTMOAT VanEck Vectors Morningstar US Wide Moat UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTNTM Think Total Market UCITS ETF Neutraal</masterLabel>
            <translation><!-- UCTNTM Think Total Market UCITS ETF Neutraal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTPRF VanEck Vectors Preferred US Equity ETF</masterLabel>
            <translation><!-- UCTPRF VanEck Vectors Preferred US Equity ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTSMH-UCITS VanEck Vectors Semiconductor</masterLabel>
            <translation><!-- UCTSMH-UCITS VanEck Vectors Semiconductor --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTAT Think iBoxx AAA-AA Government Bond UCITS ETF</masterLabel>
            <translation><!-- UCTTAT Think iBoxx AAA-AA Government Bond UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTCBT Think iBoxx Corporate Bond UCITS ETF</masterLabel>
            <translation><!-- UCTTCBT Think iBoxx Corporate Bond UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTDIV Think Morningstar® High Dividend UCITS ETF</masterLabel>
            <translation><!-- UCTTDIV Think Morningstar® High Dividend UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTDT Think AEX UCITS ETF</masterLabel>
            <translation><!-- UCTTDT Think AEX UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTEET Think European Equity UCITS ETF</masterLabel>
            <translation><!-- UCTTEET Think European Equity UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTGBT Think iBoxx Government Bond UCITS ETF</masterLabel>
            <translation><!-- UCTTGBT Think iBoxx Government Bond UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTGET Think Global Equity UCITS ETF</masterLabel>
            <translation><!-- UCTTGET Think Global Equity UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTMX Think AMX UCITS ETF</masterLabel>
            <translation><!-- UCTTMX Think AMX UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTNAE Think Morningstar® North America Equity UCITS ETF</masterLabel>
            <translation><!-- UCTTNAE Think Morningstar® North America Equity UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTOF Think Total Market UCITS ETF Offensief</masterLabel>
            <translation><!-- UCTTOF Think Total Market UCITS ETF Offensief --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTRET Think Global Real Estate UCITS ETF</masterLabel>
            <translation><!-- UCTTRET Think Global Real Estate UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UCTTSWE Think Sustainable World UCITS ETF</masterLabel>
            <translation><!-- UCTTSWE Think Sustainable World UCITS ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>URR-Double Long Euro ETN</masterLabel>
            <translation><!-- URR-Double Long Euro ETN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UXR-Emerging Europe ETF</masterLabel>
            <translation><!-- UXR-Emerging Europe ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VBTC-ETN VanEck Vectors Bitcoin</masterLabel>
            <translation><!-- VBTC-ETN VanEck Vectors Bitcoin --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VNM-Vietnam ETF</masterLabel>
            <translation><!-- VNM-Vietnam ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Emerging Markets Equity UCITS</masterLabel>
            <translation><!-- VanEck Emerging Markets Equity UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Global Gold UCITS</masterLabel>
            <translation><!-- VanEck Global Gold UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Global Hard Assets UCITS</masterLabel>
            <translation><!-- VanEck Global Hard Assets UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Unconstrained EM Bond Fund</masterLabel>
            <translation><!-- VanEck Unconstrained EM Bond Fund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Unconstrained EM Bond UCITS</masterLabel>
            <translation><!-- VanEck Unconstrained EM Bond UCITS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VanEck Vectors Video Gaming and eSports (ESPO) ETF</masterLabel>
            <translation><!-- VanEck Vectors Video Gaming and eSports (ESPO) ETF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>XMPT-CEF Municipal Income ETF</masterLabel>
            <translation><!-- XMPT-CEF Municipal Income ETF --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Jurisdiction of the Firm/Branch (set at the Firm/Branch level) --></help>
        <label><!-- Jurisdiction --></label>
        <name>Jurisdiction__c</name>
    </fields>
    <fields>
        <label><!-- Legal &amp; Compliance Comments --></label>
        <name>Legal_Compliance_Comments__c</name>
    </fields>
    <fields>
        <label><!-- Number of Funds --></label>
        <name>Number_of_Funds__c</name>
    </fields>
    <fields>
        <help><!-- Check if initial contact was by Board of Director of Fund --></help>
        <label><!-- Public Dist. Cleared by Board of Dir --></label>
        <name>Public_Dist_Cleared_by_Board_of_Dir__c</name>
    </fields>
    <fields>
        <help><!-- Check this box if this is a Reverse Enquiry --></help>
        <label><!-- Reverse Enquiry? --></label>
        <name>Reverse_Enquiry__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Cleared by Legal &amp; Compliance</masterLabel>
            <translation><!-- Cleared by Legal &amp; Compliance --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cleared by Sales</masterLabel>
            <translation><!-- Cleared by Sales --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cleared for Email Only</masterLabel>
            <translation><!-- Cleared for Email Only --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed</masterLabel>
            <translation><!-- Closed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Applicable</masterLabel>
            <translation><!-- Not Applicable --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Hold</masterLabel>
            <translation><!-- On Hold --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pending by Legal &amp; Compliance</masterLabel>
            <translation><!-- Pending by Legal &amp; Compliance --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pending by Sales</masterLabel>
            <translation><!-- Pending by Sales --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation><!-- Rejected --></translation>
        </picklistValues>
    </fields>
    <layouts>
        <layout>Admin Clearance Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Evidence --></label>
            <section>Evidence</section>
        </sections>
        <sections>
            <label><!-- Evidence Requirements (Save as Attachments) --></label>
            <section>Evidence Requirements (Save as Attachments)</section>
        </sections>
        <sections>
            <label><!-- Funds --></label>
            <section>Funds</section>
        </sections>
        <sections>
            <label><!-- Legal &amp; Compliance --></label>
            <section>Legal &amp; Compliance</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Clearance Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Evidence --></label>
            <section>Evidence</section>
        </sections>
        <sections>
            <label><!-- Funds --></label>
            <section>Funds</section>
        </sections>
        <sections>
            <label><!-- Legal &amp; Compliance --></label>
            <section>Legal &amp; Compliance</section>
        </sections>
    </layouts>
    <startsWith>Consonant</startsWith>
    <validationRules>
        <errorMessage><!-- At least one fund must be added before setting the status to &apos;Cleared&apos; --></errorMessage>
        <name>At_least_one_fund_must_be_added</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Jurisdiction and Compliance Firm Type must be set (at the Firm/Branch level) before a clearance can be set to &quot;cleared&quot; --></errorMessage>
        <name>Jurisdiction_and_Firm_Type_must_be_set</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Reverse Enquiry clearances must have at least one fund associated. --></errorMessage>
        <name>Reverse_Enquiry_Must_Have_One_Fund</name>
    </validationRules>
</CustomObjectTranslation>
