/**
    Author: Vidhya Krishnan
    Created Date: 1/26/2012
    Documents available in: \\FS1A\Shares\Projects DOCS\CRM\SubscriptionManagement\Dynamic Eligibility
    Description: Class to handle Dynamic Eligibility Concept
        1. When there is a change in Account id, Owner Id, KASL or DB Source - fires the trigger 
        2. Determine the new eligibility for all the contacts - based on the rules defined in sub_grp object
        3. For every contact Check if the eligibility has changed or it is same as the old.
        4. If changed, Automate the eligibility - assign new sub_grp 
*/
public with sharing class DynamicEligibility {
    public static List<Contact> con = new List<Contact>();
    public static SendEmailNotification se = new SendEmailNotification('Error: Dynamic Eligibility Class Failed');

    /*  Main Method that calls the other methods    */
    public static void handleEligibility(Contact[] newCon, string s, boolean callNewLogic)
    {
        con = newCon;
        Map<Id, Id> conSubGrpMap = new Map<Id, Id>();
        if(!con.isEmpty()){
            if(callNewLogic)
                conSubGrpMap = SubscriptionEligibility.getEligibility(con);
            else
                conSubGrpMap = DynamicEligibility.getEligibility(con);
                
            if(!conSubGrpMap.isEmpty()){
                if(s=='account')
                    updateAutoSubGrp(conSubGrpMap);
                else
                    updateEligibility(conSubGrpMap,s);
                  if(callNewLogic){
                    updateEligibilitySubscription(conSubGrpMap, callNewLogic); //08-Dec-2020 passed the country flag i.e. if the country exist in custom setting
                }
                       }
        }
    }
    
    /*  For all the contacts determines the eligibility.    */
    public static Map<Id,Id> getEligibility(List<Contact> con)
    {
        
        Map<Id, Id> conSubGrpMap = new Map<Id, Id>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        Map<Id, User> userMap = new Map<Id, User>();
        List<Subscription_Group__c> subGrpList = new List<Subscription_Group__c>();
        Set<Id> accIds = new Set<Id>();
        Set<Id> profileIds = new Set<Id>();
        boolean subCountryFlag, conCountryFlag;
        String conProfileId = '';
        
        // Unassigned Eligibility group - hard coded ***** provide appropriate id in each sand box.
        // String unAssignedGroup = 'a0cZ0000000bjwS'; // for CMS
        // String unAssignedGroup = 'a0lS0000001M9D2'; // for Full
        //String unAssignedGroup = 'a0cA0000004NqWy'; // for Production
        
        String unAssignedGroup = [SELECT Id,Name FROM Subscription_Group__c WHERE Name='Unassigned Eligibility'].Id;
        
        for (Contact c : con){
           accIds.add(c.accountId);
           profileIds.add(c.OwnerId);
        }
        accMap.putAll([select id, Name, Channel__c from Account where id in: accIds]);
        System.debug('>>>>>>>>>>>>accMap>>>>>>>>>>>>>>>' + accMap);
        userMap.putAll([select id, ProfileId from User where id in: profileIds]);
        
        subGrpList = [Select Name, Channel_Filter__c, Firm_Name_Filter__c, KASL_Filter__c, DB_Source_Inc_Filter__c, DB_Source_Exc_Filter__c, Owner_Profile_Filter__c, Profile_Channel_Filter__c, 
                        Country_Filter__c from Subscription_Group__c where IsActive__c = true AND Enable__c = false order by Eligibility_Sort_Order__c];
        
    
        System.debug('???????????con????????????' + con);
        for(Contact c : con){

            subCountryFlag = false;conCountryFlag = false;
            if(c.MailingCountry == null || c.MailingCountry == '')
                conCountryFlag = false;
            else
                conCountryFlag = true;
            
            Account a = accMap.get(c.accountId);
            User u = userMap.get(c.OwnerId);
            if(u != null)
               conProfileId = u.ProfileId;
            c.Auto_Subscription_Group__c = null;
            
            system.debug('for testing'+ subGrpList.size());

            for(Subscription_Group__c sg : subGrpList)
            {
                /*  define the sub_grp logic here which has to be determined for every single contact
                    once the sub_grp is determined, create map of contact with its sub_grp_id           */
                List<Boolean> dbFlagList = new List<Boolean>();
                Boolean dbFlag = false;
                system.debug('Subscription Group inside loop.......'+sg);
                if(sg.Country_Filter__c == null || sg.Country_Filter__c == '')
                    subCountryFlag = false;
                else{
                    subCountryFlag = true;
                    // For UnassignedGroup Eligibility alone assign if the Country matches
                    // there are no any sub group containing country filter as not null
                    if(sg.Id == unAssignedGroup)
                    {
                        List<String> s = (sg.Country_Filter__c).split(',',0);     
                        system.debug('\n--s--'+s);
                        for(String st: s)
                        {                            
                            if(c.MailingCountry != null && c.MailingCountry.contains(st))
                            {
                                conSubGrpMap.put(c.Id, sg.Id);     
                                system.debug('\n--conSubGrpMap1--'+conSubGrpMap);
                                break;
                            }

                        }
                    }
                }
                system.debug('contact value'+ c.DB_Source__c);
                if(c.DB_Source__c == null)
                    dbFlag = false;
                // for MVIS & zzHolders group, DB Source Inclusion Filter    
                else if((c.DB_Source__c != null || !(c.DB_Source__c.equals(''))) && sg.DB_Source_Inc_Filter__c != null){
                    if(sg.DB_Source_Inc_Filter__c == c.DB_Source__c || (c.DB_Source__c.contains(sg.DB_Source_Inc_Filter__c))){
                        conSubGrpMap.put(c.Id, sg.Id);
                        system.debug('\n--conSubGrpMap2--'+conSubGrpMap);
                        dbFlag = true;
                        break;
                    }
                    else{
                        List<String> s = (sg.DB_Source_Inc_Filter__c).split(',',0);
                        for(String st : s)
                            if(c.DB_Source__c.contains(st)){
                                conSubGrpMap.put(c.Id, sg.Id);
                                system.debug('\n--conSubGrpMap3--'+conSubGrpMap);
                                dbFlag = true; 
                                break;
                            }
                    }
                }
                // for other group, DB Source Exc Filter    
                else if((c.DB_Source__c != null || !(c.DB_Source__c.equals(''))) && sg.DB_Source_Exc_Filter__c != null){
                        List<String> s = (sg.DB_Source_Exc_Filter__c).split(',',0);
                        for(String st : s)
                            if(!c.DB_Source__c.contains(st))
                                dbFlagList.add(false);
                }
                if(dbFlag == false){
                    for(Boolean b : dbFlagList)
                        if(b == true)                            { dbFlag = true; break; }
                        else if(b == false)
                            dbFlag = false;
                }
                system.debug('\n---dbFlag--'+dbFlag);
                if(dbFlag == false) { // for all other group
                    if(c.Service_Level__c == null){
                        // KASL should be blank for groups - Wirehouse, FA, Insurance, Institutional
                        Boolean whFlag = false;

                        // Firm Name check for Wirehouse & Van Eck
                        if(sg.Firm_Name_Filter__c != null){
                            List<String> s = new List<String>();
                            s = (sg.Firm_Name_Filter__c).split(',',0);
                            for(String st : s){
                                // Wire house, Van Eck - Firm Name match *****(WITHOUT space)*****
                                system.debug('\n--a--'+a+'\n--start--'+(a.Name).startsWith(st));
                                if((a != null) && (a.Name).startsWith(st)){
                                    if(subCountryFlag){
                                        List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                        for(String stCountry : sCountry){
                                            if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                // blank or International Countries cannot be eligible
                                                conSubGrpMap.put(c.Id, sg.Id);
                                                system.debug('\n--conSubGrpMap4--'+conSubGrpMap);
                                                break;
                                            }
                                            else{
                                                conSubGrpMap.put(c.Id, unAssignedGroup);
                                                system.debug('\n--conSubGrpMap5--'+conSubGrpMap);
                                            }
                                        }
                                    }else{  
                                        conSubGrpMap.put(c.Id, sg.Id); 
                                        system.debug('\n--conSubGrpMap6--'+conSubGrpMap);
                                    }
                                    whFlag = true; break;
                                }
                                if(conSubGrpMap.get(c.id) != null) break;
                            }
                            if(conSubGrpMap.get(c.id) != null) break;
                        } // end of Firm Name Filter

                        // No Wire houses & No Key Accounts & No Holders/MVIS - FA groups, Insurance, Instituational groups
                        else if(whFlag==false && sg.Firm_Name_Filter__c == null){
                            //If Profile = 'ETF-US Sales' & channel = 'Institutional', then groutp = US Inst
                            if(sg.Profile_Channel_Filter__c != null){
                                system.debug('>>>>>>>>>>>>>>>>>>>>>>' + c.Channel_NEW__c);
                                system.debug('>>>>>>>>>>>>>>>>>>>>>>' + sg.Channel_Filter__c);
                                system.debug('>>>>>>>>>>>>>>>>>>>>>>' + sg.KASL_Filter__c);
                                system.debug('>>>>>>>>>>>>>>>>>>>>>>' + (sg.Channel_Filter__c == c.Channel_NEW__c));

                                if(c.Channel_NEW__c != null && sg.Channel_Filter__c != null && sg.KASL_Filter__c == null && sg.Channel_Filter__c == c.Channel_NEW__c){
                                    List<String> s = new List<String>();
                                    s = (sg.Profile_Channel_Filter__c).split(',',0);
                                    system.debug('>>>>>>>>>>>>>>>>>>>>>>' + s);
                                    system.debug('>>>>>>>>>>>>>>>>>>>>>>' + conProfileId);
                                    for(String st : s)
                                    {
                                        system.debug('>>>>>>>>>>>>>>>>>>>>>>' + s);
                                        system.debug('>>>>>>>>>>>>>>>>>>>>>>' + conProfileId);
                                        system.debug('>>>>>>>>>>>>>>>>>>>>>>' + st);
                                    
                                        if(conProfileId.startsWith(st)) {
                                            if(subCountryFlag){                                                List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                                for(String stCountry : sCountry)
                                                    if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                        // blank or International Countries cannot be eligible
                                                        conSubGrpMap.put(c.Id, sg.Id);
                                                        system.debug('\n--conSubGrpMap7--'+conSubGrpMap);
                                                        break;
                                                    }
                                            }else
                                                conSubGrpMap.put(c.Id, sg.Id);  
                                                system.debug('\n--conSubGrpMap8--'+conSubGrpMap);
                                                break;
                                            }
                                        }
                                    }
                                }

                            // Institutional - US, Europe, Switz
                            if(sg.Owner_Profile_Filter__c != null){
                                List<String> s = new List<String>();
                                s = (sg.Owner_Profile_Filter__c).split(',',0);
                                for(String st : s)
                                    if(conProfileId.startsWith(st)){ // starts with is used to pass through the 18 char id got from select stmt (vs) 15 char from UI.
                                        // For all other profiles specified other than 'ETF-US Sales'
                                        if(subCountryFlag){                                            List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                            for(String stCountry : sCountry)                                                if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                    // blank or International Countries cannot be eligible
                                                    conSubGrpMap.put(c.Id, sg.Id);
                                                    system.debug('\n--conSubGrpMap9--'+conSubGrpMap);
                                                    break;
                                                }
                                        }else{
                                            conSubGrpMap.put(c.Id, sg.Id); 
                                            system.debug('\n--conSubGrpMap10--'+conSubGrpMap);
                                        }                                           
                                        break;
                                    }
                            } // end of Inst group
                            
                            if(conSubGrpMap.get(c.id) != null) break;
                            if(c.Channel_NEW__c != null && sg.Channel_Filter__c != null && sg.KASL_Filter__c == null){
                                if(sg.Channel_Filter__c == c.Channel_NEW__c) {
                                    //  FA 401K NW, Insurance group
                                    if((sg.Owner_Profile_Filter__c == null)){
                                        if(subCountryFlag){                                            List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                            for(String stCountry : sCountry)                                                if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                    // blank or International Countries cannot be eligible
                                                    conSubGrpMap.put(c.Id, sg.Id);
                                                    system.debug('\n--conSubGrpMap11--'+conSubGrpMap);
                                                    break;
                                                }
                                        }else{
                                            conSubGrpMap.put(c.Id, sg.Id);      
                                            system.debug('\n--conSubGrpMap12--'+conSubGrpMap);
                                        }
                                            break;
                                    }
                                }
                                else { // FA NW - channel = Financial Advisor,RIA,Asset Manager,Insurance,401K
                                    List<string> s = (sg.Channel_Filter__c).split(',',0);
                                    system.debug('\n--s--'+s+'\n--channel--'+c.Channel_NEW__c);
                                    for(string st : s)
                                        if(c.Channel_NEW__c == st) {
                                            if(subCountryFlag){
                                                List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                                for(String stCountry : sCountry)                                                    if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                        // blank or International Countries cannot be eligible
                                                        conSubGrpMap.put(c.Id, sg.Id);
                                                        system.debug('\n--conSubGrpMap--'+conSubGrpMap);
                                                        break;
                                                    }
                                            }else{
                                                conSubGrpMap.put(c.Id, sg.Id);
                                                system.debug('\n--conSubGrpMap13--'+conSubGrpMap);
                                            }
                                            break;
                                        }
                                }
                            } // end of channel != null check
                            if(conSubGrpMap.get(c.id) != null) break;
                            
                            /****** for contact channel = professional, there wont be any KASL value tagged - still we need those contacts under Key Accounts Group */
                            if(c.Channel_NEW__c != null && sg.Channel_Filter__c != null && sg.KASL_Filter__c != null && c.Service_Level__c == null){
                                if(c.Channel_NEW__c != null && sg.Channel_Filter__c != null){
                                    List<string> s = (sg.Channel_Filter__c).split(',',0);
                                    for(string st : s)
                                        if(c.Channel_NEW__c == st) {
                                            if(subCountryFlag){
                                                List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                                for(String stCountry : sCountry)                                                    if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                                        // blank or International Countries cannot be eligible
                                                        conSubGrpMap.put(c.Id, sg.Id);
                                                        system.debug('\n--conSubGrpMap14--'+conSubGrpMap);
                                                        break;
                                                    }
                                            }else{
                                                conSubGrpMap.put(c.Id, sg.Id); 
                                                system.debug('\n--conSubGrpMap15--'+conSubGrpMap);
                                            }
                                                break;
                                        }
                                }
                            } // end of ELST - Professional Channel check
                            if(conSubGrpMap.get(c.id) != null) break;
                            
                        } // end of no wire house firm filter
                        if(conSubGrpMap.get(c.id) != null) break;   

                    } // end of KASL=null
                                        
                    else if(sg.KASL_Filter__c != null){
                        // for ETF Group - KASL = ETF Trader
                        if(c.Service_Level__c == sg.KASL_Filter__c){
                            if(subCountryFlag){
                                List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                for(String stCountry : sCountry)                                    if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                        // blank or International Countries cannot be eligible
                                        conSubGrpMap.put(c.Id, sg.Id);      
                                        system.debug('\n--conSubGrpMap16--'+conSubGrpMap);
                                }
                                        break;
                            }else{  
                                conSubGrpMap.put(c.Id, sg.Id);
                                system.debug('\n--conSubGrpMap17--'+conSubGrpMap);
                            }
                            break;
                        }
                        else if(sg.KASL_Filter__c.contains(c.Service_Level__c)){
                            // for other Key Account - KASL = Super Senior,Fund Analyst,Wholesaler,Operations
                            if(subCountryFlag){
                                List<String> sCountry = (sg.Country_Filter__c).split(',',0);
                                for(String stCountry : sCountry)                                    if(conCountryFlag && c.MailingCountry.equalsIgnoreCase(stCountry)){
                                        // blank or International Countries cannot be eligible
                                        conSubGrpMap.put(c.Id, sg.Id);     
                                        system.debug('\n--conSubGrpMap18--'+conSubGrpMap);
                                }
                                        break;
                            }else{
                                system.debug('\n--conSubGrpMap19--'+conSubGrpMap);
                                conSubGrpMap.put(c.Id, sg.Id);
                            }
                            break;
                        }
                       if(conSubGrpMap.get(c.id) != null) break;
                    } // end of KASL check

                } // end of DB flag = true check
            } // end of SG for loop
            
            // Unassigned Eligibility group
            if(conSubGrpMap.get(c.id) == null){
                conSubGrpMap.put(c.Id, unAssignedGroup);
                system.debug('\n--conSubGrpMap20--'+conSubGrpMap);
            }
                    
        } // end of Contact for loop
        
        system.debug('\n--conSubGrpMap21--'+conSubGrpMap);
        if(conSubGrpMap.size() > 0)
            return conSubGrpMap;
        else
            return null;
    }
    
    /*  For every contact - Checks if the eligibility has changed & updates the sub_grp accordingly.    */
    public static void updateEligibility(Map<Id,Id> conSubGrpMap, string s)
    {
        List<Contact> conUpdList = new List<Contact>();
        for(Contact c : con){
            //for now - update the value in new field and test how it works with live data.
            c.Auto_Subscription_Group__c = conSubGrpMap.get(c.id);
            try{
                if(s.equals('insert'))
                    c.Subscription_Group__c = c.Auto_Subscription_Group__c;
                else if (s.equals('update')){
                    if(c.Subscription_Group__c != c.Auto_Subscription_Group__c){
                        c.Subscription_Group__c = c.Auto_Subscription_Group__c;
                        //c.Dynamic_Subscription_Date__c = system.today();  -   No need to use Apex schedule when sub_mem is child of contact object
                        /*  Update Registrant record's subscription group   */
                        if(c.Community_Registrant__c != null){
                            Community_Registrant__c cr = new Community_Registrant__c(Id = c.Community_Registrant__c);
                            cr.Subscription_Group__c = c.Auto_Subscription_Group__c;
                            update cr;
                        }
                        conUpdList.add(c);
                    }
                }
            }catch(Exception e){                se.sendEmail('Exception Occurred in updateEligibility method : \n'+e.getMessage()+conSubGrpMap);
            }
        }
        update conUpdList;
    }
    
   
     //  code added on 9-Dec-2020 For every contact if country is present in Eligibility Subscription Custom Settings then we're updaing 
     //  We're not using insert and update parameter because contact trigger logic is disabled  /
    public static void updateEligibilitySubscription(Map<Id,Id> conSubGrpMap, boolean countryExistinSetting) //08-Dec-2020 (added the parameter of country)
    {
        List<Contact> conUpdList = new List<Contact>();
        for(Contact c : con){
            system.debug('\n--countryExistinSetting--'+countryExistinSetting);
            //c.Subscription_Verify__c = countryExistinSetting; //08-Dec-2020
            
            //update the contact field if the country is not present in custom setting
        try{            
             if(countryExistinSetting){
                c.Subscription_Eligibility__c  = c.Auto_Subscription_Group__c;
              }
            }catch(Exception e){                se.sendEmail('Exception Occurred in updateEligibility method : \n'+e.getMessage()+conSubGrpMap);
            }
        }
       update conUpdList;
    }

    /*  This is called from Account Trigger when channel of account is changed  */
    public static void updateAutoSubGrp(Map<Id,Id>conSubGrpMap){
        try{
            system.debug('Account trigger con sub grp map....'+conSubGrpMap);
            List<Contact> conList = new List<Contact>();
            List<Contact> cons = new List<Contact>();
            Set<Id> conIds = conSubGrpMap.keySet();
            cons = [select Subscription_Group__c from Contact where Id IN: conIds];
            for(Contact c : cons){
                c.Auto_Subscription_Group__c = conSubGrpMap.get(c.Id);
                if(c.Subscription_Group__c != c.Auto_Subscription_Group__c){
                    c.Subscription_Group__c = c.Auto_Subscription_Group__c;
                    conList.add(c);
                }
            }
            if(conList.size() > 0){
                update conList;
                DynamicSubscription ds = new DynamicSubscription(conList,'account');
                ds.handleSubscription();
            }
        }
        catch(Exception e){            se.sendEmail('Exception Occurred in updateAutoSubGrp method : \n'+e.getMessage()+conSubGrpMap);
        }
    }

}