/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-14-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-14-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private class SL_UpdatePayeeSummaryBatch_CO_Test {

	@testSetup
	static void createTestUser()
	{
		// User 1 Created for testPayeeSummaryBatch_User5
		Profile pf= [Select Id from profile where Name='ETF-US Sales']; 
        UserRole objUserRole = [Select Id from UserRole where Name = 'Global Head of Sales/Exec' limit 1];
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'Test Parker', 
                         lastName = 'User Johnson', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id,
                         UserRoleId = objUserRole.Id,
                         Internal_Sales_Score_Model__c = 'FA – ETF 1',
                         User_Internal_External__c = 'External'
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        
        // User 2 Created fro testPayeeSummaryBatch_User6
        Profile pf2= [Select Id from profile where Name='ETF-US Sales']; 
        UserRole objUserRole2 = [Select Id from UserRole where Name = 'Global Head of Sales/Exec' limit 1];
        
        String orgId2=UserInfo.getOrganizationId(); 
        String dateString2=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId2=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName2=orgId2+dateString2+RandomId2; 
        User uu2=new User(firstname = 'Test Parker 2', 
                         lastName = 'User Johnson 2', 
                         email = uniqueName2 + '@test' + orgId2 + '.org', 
                         Username = uniqueName2 + '@test' + orgId2 + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName2.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf2.Id,
                         UserRoleId = objUserRole2.Id,
                         Internal_Sales_Score_Model__c = 'FA – ETF'
                        ); 
        
        
        User objUser2 = (User)SL_TestDataFactory.createSObject(uu2,true);
        
        
        
	}
	
    //Testing from User 1 for 
    @isTest 
    static void testPayeeSummaryBatch_User1() {
        
		SL_TestDataFactory.createTestDataForCustomSetting();
				
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
        user objUser1 = [Select Id from User where Id = '005A0000005DhiSIAS'];
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        Set<ID> accIds = new Set<ID>();
        List<Contact> conlist = new List<Contact>();
        
        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc', 
        							IS_User__c=false, IsTestContact__c=false, Email='test-con@abc.com', MailingCountry='United States',
        							Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        //Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',LastHumanContactActivityType__c ='',LastHumanContactDate__c =null);
        //sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
        system.runAs(objUser1){
        
	     	newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', Subject = 'Subject test added by sv', 
	     								IsReminderSet = true, Summary_Recap__c = 'summary recap', IsVEContact__c = false , IsTestContact__c = false ,
	                                 	ActivityDate = date.today()+1, ReminderDateTime = datetime.now()+1, whoid=conxConverted.id,OwnerId = '005A0000005DhiSIAS'));
	       
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Subject = 'Subject test added by sv', IsReminderSet = true, 
	        							Summary_Recap__c = 'summary recap', IsVEContact__c = false , IsTestContact__c = false ,
	                                 	ActivityDate = date.today()+1, ReminderDateTime = datetime.now()+1, WhoId = conxConverted.Id, WhatId = conxConverted.AccountId, 
	                                 	OwnerId = '005A0000005DhiSIAS',Glance__c=true,TimeTrade__c=true,status = 'Call Completed'));
        	
        	insert newTaskList;
        }
        
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        system.assert(lstPayee.size()>0);
        
    }
    
    //Testing from User 2 for 
    @isTest 
    static void testPayeeSummaryBatch_User2() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
        user objUser2 = [Select Id from User where Id = '005A0000000Oqpm'];
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    system.runAs(objUser2){    
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                 ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, OwnerId = conxConverted.OwnerId));
	    	insert newTaskList;
	    	
	    }
	    
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        //system.assert(lstPayee.size()>0);
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_User3() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
        user objUser3 = [Select Id from User where Id = '005A00000032QAD'];
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    system.runAs(objUser3){     
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = 'Subject test added by sv', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                 ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, OwnerId = '005A00000032QAD'));
	      	insert newTaskList;  
	    }  
	    
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        //system.assert(lstPayee.size()>0);
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_User4() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
        user objUser4 = [Select Id from User where User_Internal_External__c = 'External' and isActive = true limit 1];
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    system.runAs(objUser4){     
	    	Account objAccUser4 = new Account(Name = 'WFA-Unbranched 4', BillingCountry='United States',Channel__c='Insurance');
	        sObject  objAccUser4Ins = SL_TestDataFactory.createSObject(objAccUser4,true);
	        Account objAccUser4Convert = (Account) objAccUser4Ins;
	        
	        Contact objConUser4 = new Contact(LastName='Aalund 4',FirstName='Gail 4',AccountId=objAccUser4Convert.Id,MailingStreet='abc',
	        									Email='test-con4@abc.com', MailingCountry='United States',
	        									Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',//LastHumanContactActivityType__c ='',
	        									LastHumanContactDate__c =null);
	        sObject objConUser4Ins = SL_TestDataFactory.createSObject(objConUser4,true);
	        
	        Contact objConUser4Convert = (Contact) objConUser4Ins;
        
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed',
	        							Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                 	ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, 
	                                 	WhoId=objConUser4Convert.Id, OwnerId = objUser4.Id));
	       
	       
	      	insert newTaskList;  
	    }
	    
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        //system.assert(lstPayee.size()>0);
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_User5() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
		Test.startTest();
									
        user objUser5 = [Select Id from User 
        					where User_Internal_External__c = 'External' AND 
        							Internal_Sales_Score_Model__c != NULL AND Internal_Sales_Score_Model__c != 'FA – ETF' AND 
        							Profile.Name = 'ETF-US Sales' AND
        							isActive = true limit 1];
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    //Internal_Sales_Score_Model__c != 'FA – ETF'
	    system.assert(objUser5.ID != NULL);
	    
	    system.runAs(objUser5){  
	    	Account objAccUser5 = new Account(Name = 'WFA-Unbranched 5', BillingCountry='United States',Channel__c='Insurance');
	        sObject  objAccUser5Ins = SL_TestDataFactory.createSObject(objAccUser5,true);
	        Account objAccUser5Convert = (Account) objAccUser5Ins;
	        
	        Contact objConUser5 = new Contact(LastName='Aalund 5',FirstName='Gail 5',AccountId=objAccUser5Convert.Id,MailingStreet='abc',
	        									Email='test-con5@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',
	        									OwnerId =objUser5.Id,//LastHumanContactActivityType__c ='',
	        									LastHumanContactDate__c =null);
	        sObject objConUser5Ins = SL_TestDataFactory.createSObject(objConUser5,true);
	        
	        Contact objConUser5Convert = (Contact) objConUser5Ins;
	           
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', 
	        								Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                		ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, 
	                                		WhoId=objConUser5Convert.Id, OwnerId = objUser5.Id));
	       
	       
	      	insert newTaskList;
	      	
	      	system.debug('>>>>>>>>>>>>>>>Check What is missing>>>>>>' + [Select Id, createddate, ProfileIdCheck__c, createdby.Internal_Sales_Score_Model__c, createdbyid from Task where Id IN: newTaskList]);
	      	  
	    }
	    
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        //system.assert(lstPayee.size()>0);
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_User6() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
									
        user objUser6 = [Select Id from User where Internal_Sales_Score_Model__c = 'FA – ETF' and isActive = true limit 1];
        
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm', /*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    system.runAs(objUser6){   
	    	Account objAccUser6 = new Account(Name = 'WFA-Unbranched 6', BillingCountry='United States',Channel__c='Insurance');
	        sObject  objAccUser6Ins = SL_TestDataFactory.createSObject(objAccUser6,true);
	        Account objAccUser6Convert = (Account) objAccUser6Ins;
	        
	        Contact objConUser6 = new Contact(LastName='Aalund 6',FirstName='Gail 6',AccountId=objAccUser6Convert.Id,MailingStreet='abc',
	        									Email='test-con5@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',
	        									OwnerId =objUser6.Id, //LastHumanContactActivityType__c ='',
	        									LastHumanContactDate__c =null);
	        sObject objConUser6Ins = SL_TestDataFactory.createSObject(objConUser6,true);
	        
	        Contact objConUser6Convert = (Contact) objConUser6Ins;
	    	
	    	  
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', 
	        							Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                 	ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, 
	                                 	WhoId=objConUser6Convert.Id, OwnerId = objUser6.Id));
	       
	       
	      	insert newTaskList;  
	    }    
	     
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        date currentDate = Date.today();
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        system.assert(lstPayee.size()>0);
        
    }
    
    @isTest 
    static void testPayeeSummaryBatch_User5_UpdateCase() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
		
		system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
							'Expecting this Custom Setting value as true before doing any insert on Task');
		Test.startTest();
									
        user objUser5 = [Select Id from User 
        					where User_Internal_External__c = 'External' AND 
        							Internal_Sales_Score_Model__c != NULL AND Internal_Sales_Score_Model__c != 'FA – ETF' AND 
        							Profile.Name = 'ETF-US Sales' AND
        							isActive = true limit 1];
        							
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);

        Account accx = new Account(Name = 'WFA-Unbranched', BillingCountry='United States',Channel__c='Insurance');
        sObject accxInserted = SL_TestDataFactory.createSObject(accx,true);
        Contact conx = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conxInserted = SL_TestDataFactory.createSObject(conx,true);
        
        Contact conxConverted = (Contact) conxInserted;
        
        Contact conx1 = new Contact(LastName='Aalund',FirstName='Gail',AccountId=accx.Id,MailingStreet='abc',Email='test-con2@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm',/*LastHumanContactActivityType__c ='',*/LastHumanContactDate__c =null);
        sObject conx1Inserted = SL_TestDataFactory.createSObject(conx1,true);
        //005A00000032QAD
        
        List<Task> newTaskList = new List<Task>();
        string Comments;
        Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
        Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
        Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conxConverted.Id+'\n\n';
        Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
	        
	    //Internal_Sales_Score_Model__c != 'FA – ETF'
	    system.assert(objUser5.ID != NULL);
	    Date currentDate = Date.Today();
	    
	    system.runAs(objUser5){ 
	    	
	        PayeeSummaryBatch__c payeeSummary = new PayeeSummaryBatch__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),
	                                                    	User__c=objUser5.Id,TaskFAScore__c =0,TaskFAQScore__c =0 ,
	                                                    	Total_Days_for_this_Calendar_Month__c = 30,
		                									No_Of_Days_in_Office__c = 25,
	                                                        TaskRIAScore__c =0,TaskRIAQScore__c = 0 , timeTradeQCount__c = 0,GlaceQCount__c =  0, 
	                                                        IWSQTasksCount__c = 0, IWSToExternalTasksQCount__c = 0, ETF_Task_Referral_Q_Count__c = 0, 
	                                                        ETF_Referral_Q_Count_4_Xternals_ETF_Spl__c = 0, ETF_Referral_Q_Count_4_Xternals_RIA_Spl__c = 0);
	        insert payeeSummary;	
	        system.assert(payeeSummary.Id != NULL);
         
	    	Account objAccUser5 = new Account(Name = 'WFA-Unbranched 5', BillingCountry='United States',Channel__c='Insurance');
	        sObject  objAccUser5Ins = SL_TestDataFactory.createSObject(objAccUser5,true);
	        Account objAccUser5Convert = (Account) objAccUser5Ins;
	        
	        Contact objConUser5 = new Contact(LastName='Aalund 5',FirstName='Gail 5',AccountId=objAccUser5Convert.Id,MailingStreet='abc',
	        									Email='test-con5@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',
	        									OwnerId =objUser5.Id,//LastHumanContactActivityType__c ='',
	        									LastHumanContactDate__c =null);
	        sObject objConUser5Ins = SL_TestDataFactory.createSObject(objConUser5,true);
	        
	        Contact objConUser5Convert = (Contact) objConUser5Ins;
	           
	        newTaskList = new List<Task>();
	        newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Call Completed', 
	        								Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap',
	                                		ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, 
	                                		WhoId=objConUser5Convert.Id, OwnerId = objUser5.Id));
	       
	       
	      	insert newTaskList;
	      	
	      	system.debug('>>>>>>>>>>>>>>>Check What is missing>>>>>>' + [Select Id, createddate, ProfileIdCheck__c, createdby.Internal_Sales_Score_Model__c, createdbyid from Task where Id IN: newTaskList]);
	      	  
	    }
	    
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        
        Test.stopTest();
        
        List<PayeeSummaryBatch__c> lstPayee = [SELECT Id FROM PayeeSummaryBatch__c WHERE Year__c =: String.valueOf(currentDate.year())];
        system.assert(lstPayee.size()>0);
        
    }
    
     @isTest 
    static void testPayeeSummaryBatch_User5_ExceptionQuery() 
    {
    	SL_TestDataFactory.createTestDataForCustomSettingSentEmail();
		Test.startTest();
	   	String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch_CO(), 100);
        Test.stopTest();
    }
    
}