/**
    Author: Vidhya Krishnan
    Date: 4/10/12
    Desc: Test class for Registrant To Contact Class & trigger
 */
@isTest
private class RegistrantToContactTest {
    static testMethod void testRegToCon() {
        PageReference pageRef = Page.RegistrantToContact;
        Test.setCurrentPageReference(pageRef);
        Account acc = new Account(Name='ghdjyuygdk',Channel__c='RIA',BillingCountry='US');
        insert acc;
        Contact con1 = new Contact(LastName='T Con1',AccountId=acc.Id,MailingCountry='US');
        insert con1;
        Community_Registrant__c cr = new Community_Registrant__c(First_Name__c='test',Last_Name__c='test',Email__c='testReg@test.com',Phone__c='123456789', Company__c='Test Firm',
                                User_Name__c='testReg@test.com' ,Password__c='test',Address1__c='test st',City__c='test city',State__c='NY',Country__c='USA',Zip_Code__c='10017');
        insert cr;
        Community_Registrant__c cr1 = new Community_Registrant__c(First_Name__c='test',Last_Name__c='test',Email__c='testReg1@test.com',Phone__c='123456789', Company__c='Test Firm',
                                User_Name__c='testReg1@test.com' ,Password__c='test',Address1__c='test st',City__c='test city',State__c='NY',Country__c='USA',Zip_Code__c='10017');
        insert cr1;
        Community__c c = new Community__c(Name='Test Community',is_Active__c=true);
        insert c;
        Community_Member__c cm = new Community_Member__c(Community__c=c.Id, Community_Registrant__c=cr.Id);
        insert cm;
        cr.Contact__c = con1.Id; // this fires the trigger
        update cr;

        ApexPages.currentPage().getParameters().put('cId',cr1.Id);
        Contact con = new Contact(LastName='T Con',AccountId=acc.Id,MailingCountry='US'); // do not insert contact here
        ApexPages.StandardController controller = new ApexPages.standardController(con);
        Contact con2 = new Contact(AccountId=acc.Id,MailingCountry='US');
        ApexPages.StandardController controller1 = new ApexPages.standardController(con2);
        Contact con3 = new Contact(LastName='T Con3',MailingCountry='US');
        ApexPages.StandardController controller2 = new ApexPages.standardController(con3);
        Contact con4 = new Contact(LastName='T Con4',AccountId=acc.Id);
        ApexPages.StandardController controller3 = new ApexPages.standardController(con4);

        RegistrantToContact rtc = new RegistrantToContact(controller);
        RegistrantToContact rtc1 = new RegistrantToContact(controller1);
        RegistrantToContact rtc2 = new RegistrantToContact(controller2);
        RegistrantToContact rtc3 = new RegistrantToContact(controller3);
        
        rtc.addContact(); // contact is inserted here in the VF page by class
        rtc1.addContact();
        rtc2.addContact();
        rtc3.addContact();
        
        rtc.cancelRecord();
        cr.Contact__c = con.Id;
        update cr;
        cr.Contact__c = null;
        update cr;
        delete cr;
    }
}