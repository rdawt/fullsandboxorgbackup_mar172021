trigger EmailMessageTrigger on EmailMessage (before insert, before update, after insert, after update) {

  if(Trigger.isAfter && Trigger.isInsert){
    System.debug('--------');
       Map<Id, EmailTemplate> mapName = new Map<Id, EmailTemplate>();   
    List<EmailTemplate> lstTemplate = [SELECT FolderId,FolderName,Id,Name FROM EmailTemplate WHERE Id IN ('00X19000000jtsUEAQ','00X19000000jtsPEAQ','00X19000000jtrlEAA','00X19000000jtrqEAA')];
    System.debug(lstTemplate);
    for(EmailTemplate temp : lstTemplate){
      mapName.put(temp.Id, temp);
    }
    System.debug(mapName);
            List<EmailMessage> lstEmailMessage = (List<EmailMessage>)Trigger.New;
            List<Template_Usage_Test__c> listUsage = new List<Template_Usage_Test__c>();
            for(EmailMessage email: lstEmailMessage){
              EmailTemplate template = mapName.get(email.EmailTemplateId);
                Template_Usage_Test__c obj = new Template_Usage_Test__c();
                obj.Is_Tracked__c = email.IsTracked;
                obj.Sent_By__c = email.CreatedById;
                obj.Sent_Date__c = email.CreatedDate;
                obj.Template_Id__c = email.EmailTemplateId;
                obj.To_Address__c = email.ToAddress;
                obj.Open_Count__c = 0;
                obj.Email_Template_Folder__c = template.FolderName;
                obj.Template_Name__c = template.Name;
                listUsage.add(obj);
                System.debug(obj);
            }
            
            insert listUsage;
        }
        
}