<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Action_Password_is_expiring_soon_APP_API_User</fullName>
        <ccEmails>help.salesforce@vaneck.com</ccEmails>
        <description>Action - Password is expiring soon - APP/API User.</description>
        <protected>false</protected>
        <recipients>
            <recipient>ADMAPIS</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Password_is_expiring_soon_APP_API_User</template>
    </alerts>
    <alerts>
        <fullName>Check_and_see_if_DST_Sync_is_comepleted</fullName>
        <description>Alert about DST Sync (incomplete)</description>
        <protected>false</protected>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/DST_Sync</template>
    </alerts>
    <rules>
        <fullName>DST Login %28BETA%29</fullName>
        <actions>
            <name>Check_and_see_if_DST_Sync_is_comepleted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.LastLoginDate</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>equals</operation>
            <value>sconn</value>
        </criteriaItems>
        <description>Flag that DST have not performed the Daily Sync</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Password Expiration Notification %28API%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>User.API_User__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastPasswordChangeDate</field>
            <operation>greaterThan</operation>
            <value>LAST 135 DAYS</value>
        </criteriaItems>
        <description>Coordinate password change - PW is expiring in 45 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Action_Password_is_expiring_soon_APP_API_User</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>User.LastPasswordChangeDate</offsetFromField>
            <timeLength>134</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Prevent edit on %22Division%22</fullName>
        <active>true</active>
        <description>You do not have permission to update this field.</description>
        <formula>ISCHANGED(Division)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
