@isTest
private class SL_Batch_MoveMarketoJSONActivityTaskTest 
{
	public static User objTestUser = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1];
	
	//Get Record Types
    public static Map<String, Schema.RecordTypeInfo> accountRecordTypeNameToIdMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Id SalesConnectFirm = accountRecordTypeNameToIdMap.get('SalesConnect Firm').getRecordTypeId();
        
	public static Subscription_Group__c objSubscription_Group = (Subscription_Group__c)SL_TestDataFactory.createSObject(new Subscription_Group__c
            																			(Name='Subscription', IsActive__c = true, 
            																			Description__c = 'Test Description',
            																			Eligibility_Sort_Order__c=1, KASL_Filter__c = 'ETF Trader'), True);
	public static Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Bank', OwnerId=objTestUser.Id,
            																			RecordTypeId = SalesConnectFirm,
            																			BillingCountry='United States'), True);
	public static Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(AccountId = objAccount.Id, 
            																			Unsubscribe_Public__c = 'Unsubscribe me from future emails',
        																				FirstName='Test', LastName='Contact', OwnerId=objTestUser.Id,
        																				Email='test@testvaneck.com.test',
        																				MailingCountry = 'United States',
        																				Subscription_Group__c = objSubscription_Group.id
        																				), True);
	public static String json = '[{'+
		'	\"id\": 175013197,'+
		'	\"marketoGUID\": \"175013197\",'+
		'	\"leadId\": 6399842,'+
		'	\"activityDate\": \"2019-09-22T23:16:02Z\",'+
		'	\"activityTypeId\": 10,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"matthew.larocca@mortgagechoice.com.au\",'+
		'	\"sfdcContactId\": "'+ objContact.Id + '",'+
		'	\"sfdcAccountId\": "' + objAccount.Id + '",'+
		'	\"sfdcLeadOwnerId\": "' + objAccount.OwnerId + '",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}, {'+
		'	\"id\": 175371996,'+
		'	\"marketoGUID\": \"175371996\",'+
		'	\"leadId\": 8423567,'+
		'	\"activityDate\": \"2019-09-24T13:55:51Z\",'+
		'	\"activityTypeId\": 10,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"imelda.alexopoulos@au.pwc.com\",'+
		'	\"sfdcContactId\": "'+ objContact.Id + '",'+
		'	\"sfdcAccountId\": "' + objAccount.Id + '",'+
		'	\"sfdcLeadOwnerId\": "' + objAccount.OwnerId + '",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}]';
		
	@isTest
	public static void testBatchMoveMarketoJSONActivityToTask()
	{
		system.runAs(objTestUser)
		{
			Test.startTest();
				SL_MarketoRecords_Service.createMarketoJSONActivityRecords();
				system.assertEquals(2, [Select Id from Marketo_JSON_Activity__c].size(), 'Expecting 2 records in Marketo_JSON_Activity__c as in JSON file we only 2 records');
				
				String jobId = Database.executeBatch(new SL_Batch_MoveMarketoJSONActivityToTask(), 100);
			Test.stopTest();	
			
			system.assertEquals(2, [Select Id from Task where OwnerId =: objTestUser.Id].size(), 'Expecting 2 records in Task as in JSON file we only 2 records');
			
		}
	} 	
}