@isTest
public class SL_MarketoRecords_Wrapper_Test {
	
	// This test method should give 100% coverage
	public static String json = '[{'+
		'	\"id\": 175013197,'+
		'	\"marketoGUID\": \"175013197\",'+
		'	\"leadId\": 6399842,'+
		'	\"activityDate\": \"2019-09-22T23:16:02Z\",'+
		'	\"activityTypeId\": 10,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"matthew.larocca@mortgagechoice.com.au\",'+
		'	\"sfdcContactId\": \"003A000001wz35wIAA\",'+
		'	\"sfdcAccountId\": \"001A000001TPrSQIA1\",'+
		'	\"sfdcLeadOwnerId\": \"005A0000004wbcUIAQ\",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}, {'+
		'	\"id\": 175013198,'+
		'	\"marketoGUID\": \"175013198\",'+
		'	\"leadId\": 4056181,'+
		'	\"activityDate\": \"2019-09-22T23:16:02Z\",'+
		'	\"activityTypeId\": 11,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"tristan@purposeadvisory.com.au\",'+
		'	\"sfdcContactId\": \"003A000001fvpfgIAA\",'+
		'	\"sfdcAccountId\": \"001A00000112TBXIA2\",'+
		'	\"sfdcLeadOwnerId\": \"005A0000003mSNtIAM\",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}, {'+
		'	\"id\": 175013199,'+
		'	\"marketoGUID\": \"175013199\",'+
		'	\"leadId\": 9717729,'+
		'	\"activityDate\": \"2019-09-22T23:16:02Z\",'+
		'	\"activityTypeId\": 10,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"garry@wealthenhancers.community\",'+
		'	\"sfdcContactId\": \"0032K00002KRYw4QAH\",'+
		'	\"sfdcAccountId\": \"001A000000uajdbIAA\",'+
		'	\"sfdcLeadOwnerId\": \"005A0000005Agz9IAC\",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}, {'+
		'	\"id\": 175371996,'+
		'	\"marketoGUID\": \"175371996\",'+
		'	\"leadId\": 8423567,'+
		'	\"activityDate\": \"2019-09-24T13:55:51Z\",'+
		'	\"activityTypeId\": 10,'+
		'	\"campaignId\": 14933,'+
		'	\"primaryAttributeValueId\": 39208,'+
		'	\"primaryAttributeValue\": \"AU 2019-09-23 Weekly Trade Idea.QUAL – Starbucks, not a quality bean\",'+
		'	\"email\": \"imelda.alexopoulos@au.pwc.com\",'+
		'	\"sfdcContactId\": \"0032K00002CXUtRQAX\",'+
		'	\"sfdcAccountId\": \"0012K00001XlUTJQA3\",'+
		'	\"sfdcLeadOwnerId\": \"005A0000003lJgAIAU\",'+
		'	\"attributes\": [{'+
		'		\"name\": \"Campaign Run ID\",'+
		'		\"value\": \"31557\"'+
		'	}, {'+
		'		\"name\": \"Choice Number\",'+
		'		\"value\": \"0\"'+
		'	}, {'+
		'		\"name\": \"Device\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Is Mobile Device\",'+
		'		\"value\": false'+
		'	}, {'+
		'		\"name\": \"Platform\",'+
		'		\"value\": \"unknown\"'+
		'	}, {'+
		'		\"name\": \"Step ID\",'+
		'		\"value\": \"45775\"'+
		'	}, {'+
		'		\"name\": \"User Agent\",'+
		'		\"value\": \"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)\"'+
		'	}, {'+
		'		\"name\": \"Campaign\",'+
		'		\"value\": \"Email Batch Program-6416-send-email-campaign\"'+
		'	}]'+
		'}]';
	
	@isTest 	
	static void testParse() {
		
		List<SL_MarketoRecords_Wrapper> r = SL_MarketoRecords_Wrapper.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		SL_MarketoRecords_Wrapper.Attributes objAttributes = new SL_MarketoRecords_Wrapper.Attributes(System.JSON.createParser(json));
		System.assert(objAttributes != null);
		System.assert(objAttributes.name == null);
		System.assert(objAttributes.value == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		SL_MarketoRecords_Wrapper objSL_MarketoRecords = new SL_MarketoRecords_Wrapper(System.JSON.createParser(json));
		System.assert(objSL_MarketoRecords != null);
		System.assert(objSL_MarketoRecords.id == null);
		System.assert(objSL_MarketoRecords.marketoGUID == null);
		System.assert(objSL_MarketoRecords.leadId == null);
		System.assert(objSL_MarketoRecords.activityDate == null);
		System.assert(objSL_MarketoRecords.activityTypeId == null);
		System.assert(objSL_MarketoRecords.campaignId == null);
		System.assert(objSL_MarketoRecords.primaryAttributeValueId == null);
		System.assert(objSL_MarketoRecords.primaryAttributeValue == null);
		System.assert(objSL_MarketoRecords.email == null);
		System.assert(objSL_MarketoRecords.sfdcContactId == null);
		System.assert(objSL_MarketoRecords.sfdcAccountId == null);
		System.assert(objSL_MarketoRecords.sfdcLeadOwnerId == null);
		System.assert(objSL_MarketoRecords.attributes == null);
	}
}