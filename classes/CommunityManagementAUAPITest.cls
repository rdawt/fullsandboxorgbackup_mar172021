/**
    Author  :       Vidhya Krishnan
    Date Created:   10/18/12
    Description:    Test class for Community Management AU API Class
 */
@isTest
private class CommunityManagementAUAPITest {

    static testMethod void testCommunityManagementAUAPI() {
        CommunityManagementAUAPI cmAUAPI= new CommunityManagementAUAPI();
        Subscription__c s = new Subscription__c();
        s.Name = 'Test Subscription'; s.IsActive__c = true; s.Subscription_Indicator_Marker__c = 'test';
        s.Subscription_Display_Name__c = 'Test Subscription';
        insert s;
        Subscription_Group__c sg = new Subscription_Group__c();
        sg.Name = 'Test Group'; insert sg;
        Account acc = new Account(Name = 'Code Test1', Channel__c = 'RIA', BillingCountry = 'USA');
        insert acc;
        Contact con=new Contact(FirstName='Test',LastName='User', AccountId = acc.Id, MailingCountry = 'USA');
        insert con;
        Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true, Default_Subscription_Group__c = sg.Id);
        insert c;
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
        insert cr1;
        Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id,Community_Registrant__c = cr1.Id);
        insert cm1;
        CommunityManagementAU.AUMemberRecord mr = new CommunityManagementAU.AUMemberRecord();
        mr.communityId = String.valueOf(c.Id); mr.leadId = '00000'; mr.contactId = con.Id;
        mr.email = 'test@test.com'; mr.cUserName  = 'vaneck'; mr.passWord  = 'global1955'; mr.isActive  = true;
        mr.firstName  = 'test'; mr.lastName  = 'test'; mr.companyName  = 'test'; mr.jobTitle  = 'test';
        mr.address1  = 'test'; mr.address2  = 'test'; mr.city  = 'test'; mr.state  = 'test';
        mr.country  = 'test'; mr.zipcode  = 'test'; mr.phone  = 'test'; mr.loginCount = 1; mr.sourceId = 'test';
        mr.investorType  = 'test'; mr.aum  = 'test'; mr.isFP  = 'true'; mr.subscriptionRequested  = 'test,test';
        mr.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr.communityId =c.Id; mr.cMemberId=cm1.Id; 
        mr.dbSourceCreateDate = system.today(); mr.subscriptionGroupId = sg.Id;
        CommunityManagementAUAPI.getCMemberId(c.Id,'test1');
        CommunityManagementAUAPI.isEmailTaken(c.Id,'test1@test.com');
        CommunityManagementAUAPI.getCMemberType(cm1.Id);
        CommunityManagementAUAPI.getPassword(cm1.Id);
        CommunityManagementAUAPI.setPassword(cr1.Id, 'test1');
        CommunityManagementAUAPI.isActive(cm1.Id);
        CommunityManagementAUAPI.setActive(cr1.Id,true);
        CommunityManagementAUAPI.getCommunityDetails(c.Id);
        CommunityManagementAUAPI.getCMember(cm1.Id);
        CommunityManagementAUAPI.setLastLogin(cr1.Id, system.today());
        list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
        SubscriptionManagementAPI.SubscriptionDetails sub = new SubscriptionManagementAPI.SubscriptionDetails();
        sub.contactId = c.Id; sub.subscriptionId = s.Id; sub.newValue = '1'; sub.oldValue = '0';
        subList.add(sub);
        CommunityManagementAUAPI.addCMemberAndSubscriptions(mr, subList);
        CommunityManagementAUAPI.getErrorPrefix();
        CommunityManagementAUAPI.getWarningPrefix();
        CommunityManagementAUAPI.getSubGroupId(mr.email, mr.communityId);
        CommunityManagementAUAPI.setCMemberAndSubscriptions(mr.cMemberId, mr, subList);
    }
}