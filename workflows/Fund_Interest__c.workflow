<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_CompositeKey_On_Fund_Interest</fullName>
        <field>Composite_Key__c</field>
        <formula>Fund__c&amp;&quot;_&quot;&amp;Contact__c</formula>
        <name>Set CompositeKey On Fund Interest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Fund Interest Composite Key</fullName>
        <actions>
            <name>Set_CompositeKey_On_Fund_Interest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(isnull( Fund__c )) || not(isnull( Contact__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
