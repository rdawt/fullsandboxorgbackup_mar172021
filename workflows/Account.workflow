<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AUM_update</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>Asset_Under_Management__c</formula>
        <name>AUM update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Action_for_AddressTools_Validate_USA</fullName>
        <description>Converts account billing country from &quot;United States of America&quot; to &quot;United States&quot;.</description>
        <field>BillingCountry</field>
        <formula>&apos;United States&apos;</formula>
        <name>Action for AddressTools Validate &quot;USA&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Call_Goal_0</fullName>
        <field>Analyst_Calls_Year_Goal__c</field>
        <formula>0</formula>
        <name>Analyst Call Goal 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Call_Goal_1</fullName>
        <field>Analyst_Calls_Year_Goal__c</field>
        <formula>1</formula>
        <name>Analyst Call Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Call_Goal_2</fullName>
        <field>Analyst_Calls_Year_Goal__c</field>
        <formula>2</formula>
        <name>Analyst Call Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Meeting_Goal_0</fullName>
        <field>Analyst_Meetings_Year_Goal__c</field>
        <formula>0</formula>
        <name>Analyst Meeting Goal 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Meeting_Goal_1</fullName>
        <field>Analyst_Meetings_Year_Goal__c</field>
        <formula>1</formula>
        <name>Analyst Meeting Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analyst_Meeting_Goal_2</fullName>
        <field>Analyst_Meetings_Year_Goal__c</field>
        <formula>2</formula>
        <name>Analyst Meeting Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approve</fullName>
        <field>Asset_Under_Management__c</field>
        <name>Approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approve1</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>DDB_Firm_Total_AUM__c</formula>
        <name>Approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calls_Year_Goal_1</fullName>
        <field>Calls_Year_No_Analyst_PM_Goal__c</field>
        <formula>1</formula>
        <name>Calls Year Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calls_Year_Goal_2</fullName>
        <field>Calls_Year_No_Analyst_PM_Goal__c</field>
        <formula>2</formula>
        <name>Calls Year Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Decline</fullName>
        <field>Asset_Under_Management__c</field>
        <name>Decline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_MVIS_Team_update_Account_DB_Source</fullName>
        <field>DB_Source__c</field>
        <formula>&apos;MVIS BlueStar&apos;</formula>
        <name>FU MVIS Team update Account DB Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Update_01</fullName>
        <field>Zone__c</field>
        <literalValue>1</literalValue>
        <name>Firm Update 01</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Update_02</fullName>
        <field>Zone__c</field>
        <literalValue>2</literalValue>
        <name>Firm Update 02</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Update_03</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>Asset_Under_Management__c + 500</formula>
        <name>Firm Update 03</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Update_04</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>PRIORVALUE( Asset_Under_Management__c )</formula>
        <name>Firm Update 04</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MVIS_Account_Update</fullName>
        <description>Updated DB Source of Accounts created from MVIS Site</description>
        <field>DB_Source__c</field>
        <formula>&apos;MVIS Registrant&apos;</formula>
        <name>MVIS Account DB Source Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MVIS_Account_Updates</fullName>
        <description>Updates Channel value of Accounts created from MVIS site</description>
        <field>Channel__c</field>
        <literalValue>Professional</literalValue>
        <name>MVIS Account Channel Updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meetings_Year_Goal_1</fullName>
        <field>Meetings_Year_No_Analyst_PM_Goal__c</field>
        <formula>1</formula>
        <name>Meetings Year Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meetings_Year_Goal_2</fullName>
        <field>Meetings_Year_No_Analyst_PM_Goal__c</field>
        <formula>2</formula>
        <name>Meetings Year Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MifidEUNSwissValidation_Action</fullName>
        <field>MiFIDClassification__c</field>
        <name>MifidEUNSwissValidation Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MifidEUNonSwiss_Validation_Action</fullName>
        <field>MiFIDClassification__c</field>
        <literalValue>Professional Investor</literalValue>
        <name>MifidEUNonSwiss Validation Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mifid_Swiss_Validation_Action</fullName>
        <field>MiFIDClassification__c</field>
        <literalValue>Qualified Investor</literalValue>
        <name>Mifid Swiss Validation Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Calls_Goal_0</fullName>
        <field>PM_Calls_Year_Goal__c</field>
        <formula>0</formula>
        <name>PM Calls Goal 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Calls_Goal_1</fullName>
        <field>PM_Calls_Year_Goal__c</field>
        <formula>1</formula>
        <name>PM Calls Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Calls_Goal_2</fullName>
        <field>PM_Calls_Year_Goal__c</field>
        <formula>2</formula>
        <name>PM Calls Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Meetings_Goal_0</fullName>
        <field>PM_Meetings_Year_Goal__c</field>
        <formula>0</formula>
        <name>PM Meetings Goal 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Meetings_Goal_1</fullName>
        <field>PM_Meetings_Year_Goal__c</field>
        <formula>1</formula>
        <name>PM Meetings Goal 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Meetings_Goal_2</fullName>
        <field>PM_Meetings_Year_Goal__c</field>
        <formula>2</formula>
        <name>PM Meetings Goal 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending</fullName>
        <field>Asset_Under_Management__c</field>
        <name>Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_AUM_Field</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>Assets_Under_Management_Backup__c</formula>
        <name>Revert AUM Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetBillingCountry</fullName>
        <description>Assigning a default value (United States) to Billing Country field</description>
        <field>BillingCountry</field>
        <formula>&quot;United States&quot;</formula>
        <name>SetBillingCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AUM_Field</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>Asset_Under_Management__c</formula>
        <name>Update AUM Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AUM_Field_V1_0</fullName>
        <field>Asset_Under_Management__c</field>
        <formula>Assets_Under_Management_Backup__c</formula>
        <name>Update AUM Field V1.0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AddressTools Validate %22United States Of America%22</fullName>
        <actions>
            <name>Action_for_AddressTools_Validate_USA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>United States Of America</value>
        </criteriaItems>
        <description>AddressTools Validate &quot;United States Of America&quot; - Billing Country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %2410 to %2425m LO%3B %242%2E5m to %245m L%2FS</fullName>
        <actions>
            <name>Analyst_Call_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>$10 to $25m LO; $2.5m to $5m L/S</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %24100m LO%3B %2425m L%2FS %2F %2425m L%2FS%2C%2425b%2B %28Wealth Management Client%29</fullName>
        <actions>
            <name>Analyst_Call_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>$250b+ (Client),$100m LO; $25m L/S,$25b+ (Wealth Management Client)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %2425b to %24250b %28Client%29 %2F %24500b%2B AUA %28Prospect%29 %2F %2450b%2B %28Wealth Management Prospect%29</fullName>
        <actions>
            <name>Analyst_Call_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>$500b+ AUA (Prospect),$50b+ (Wealth Management Prospect),$25b to $250b (Client)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %2425m to %24100m LO%3B %245m to %2425m L%2FS</fullName>
        <actions>
            <name>Analyst_Call_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>$25m to $100m LO; $5m to $25m L/S</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %2450b to %24500b %28Prospect%29 %2F %3C%2425b %28Wealth Management Client%29 %2F %3C%2425b %28Client%29</fullName>
        <actions>
            <name>Analyst_Call_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>$50b to $500b (Prospect),&lt;$25b (Wealth Management Client),&lt;$25b (Client)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - %3C%2450b %28Prospect%29 %2F %3C%2450b %28Wealth Management Prospect%29</fullName>
        <actions>
            <name>Analyst_Call_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>&lt;$50b (Wealth Management Prospect),&lt;$50b (Prospect)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IS - Endowment %26 Foundation - Contact %2F Pensions - Contact %2F Fund of Hedge Funds - Contact</fullName>
        <actions>
            <name>Analyst_Call_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analyst_Meeting_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Calls_Year_Goal_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meetings_Year_Goal_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Calls_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Meetings_Goal_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Institutional_Segment__c</field>
            <operation>equals</operation>
            <value>Pensions - Contact,Endowment &amp; Foundation - Contact,Fund of Hedge Funds - Contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MVIS Account Updates</fullName>
        <actions>
            <name>MVIS_Account_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MVIS_Account_Updates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>app user MVIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>app user MVIS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MVIS Team update Account DB Source</fullName>
        <actions>
            <name>FU_MVIS_Team_update_Account_DB_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Emre Camlilar,Thomas Kettner,Josh Kaplan,Tzlil Keren-Blum,Joe Levin,Steven Schoenfeld,Joy Yang</value>
        </criteriaItems>
        <description>For now, contacts owned Emre Camlilar https://na7.salesforce.com/005A0000008kvNv
and Thomas Kettner - https://na7.salesforce.com/005A00000030LP1</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MiFid Validation-EU-NonSwiss</fullName>
        <actions>
            <name>MifidEUNonSwiss_Validation_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.MiFIDClassification__c</field>
            <operation>equals</operation>
            <value>Others,Qualified Investor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>United Kingdom,Spain,Sweden,Slovenia,Slovakia,Romania,Portugal,Poland,Netherlands,Malta,Luxembourg,Lithuania,Latvia,Italy,Hungary,Ireland,Germany,Greece,France,Finland,Estonia,Denmark,Cyprus,Czech Republic,Croatia,Bulgaria,Belgium,Austria</value>
        </criteriaItems>
        <description>Update Client Classification based on Account-Billing Country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MiFid Validation-EUNSwiss</fullName>
        <actions>
            <name>MifidEUNSwissValidation_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.MiFIDClassification__c</field>
            <operation>equals</operation>
            <value>Others,Qualified Investor,Professional Investor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>notContain</operation>
            <value>Switzerland,United Kingdom,Spain,Sweden,Slovenia,Slovakia,Romania,Portugal,Poland,Netherlands,Malta,Luxembourg,Lithuania,Latvia,Italy,Hungary,Ireland,Germany,Greece,France,Finland,Estonia,Denmark,Cyprus,Czech Republic,Croatia,Bulgaria,Belgium,Austria</value>
        </criteriaItems>
        <description>Update Client Classification based on Account-Billing Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MiFid Validation-Swiss</fullName>
        <actions>
            <name>Mifid_Swiss_Validation_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.MiFIDClassification__c</field>
            <operation>equals</operation>
            <value>Others,Professional Investor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>Switzerland</value>
        </criteriaItems>
        <description>Update Client Classification based on Account-Billing Country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateBillingCountry</fullName>
        <actions>
            <name>SetBillingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>Sales Connect</value>
        </criteriaItems>
        <description>This workflow sets the Billing Country value to &quot;United States&quot; when SalesConnect creates the record and billing value is empty.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
