@isTest
public with sharing class DB_InternalWholeSaler_Cont_Clone_Test {
    public static List<Id> getUserIdList()
    {
        List<Id> lstUserId = new List<Id>();
        Set<String> setUsers = new Set<String>();

        for(DB_InternalWholeSaler_Users__mdt objDB_InternalWholeSaler_Users : [SELECT Id, MasterLabel, DeveloperName FROM DB_InternalWholeSaler_Users__mdt])
        {
            setUsers.add(objDB_InternalWholeSaler_Users.MasterLabel);
        }

        for(User objUser : [Select Id, Name from User where Name IN: setUsers and isActive = true])
        {
            lstUserId.add(objUser.Id);
        }

        return lstUserId;
    }

    @TestSetup
    static void makeData()
    {
        Integer intCurrentMonth = Date.today().month();
        String strName = intCurrentMonth + '_' + Date.today().year();
        DB_Matt_InterwholeSeller__c objDB_Matt_InterwholeSeller = new DB_Matt_InterwholeSeller__c();
        objDB_Matt_InterwholeSeller.Name = strName;
        objDB_Matt_InterwholeSeller.BradPope__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.ChristopherGagnier__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.IanAmberson__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JacobWilson__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JamesCooper__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JarrettGatling__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.MadisonMorelli__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.MichaelHofbauer__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.NicholasPhillips__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.RyanHenshaw__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.VladKasyanov__c = intCurrentMonth;
        insert objDB_Matt_InterwholeSeller;

        List<Id> lstUserId = new List<Id>();        
        lstUserId = getUserIdList();

        List<Task> lstTask = new List<Task>();
        List<Event> lstEvent = new List<Event>();
        List<PayeeUser_Attendance__c> lstInsertPayeeUserAttendance = new List<PayeeUser_Attendance__c>();
        List<PayeeSummary__c> lstPayeeSummary = new List<PayeeSummary__c>();

        for(Id objUserId : lstUserId)
        {
            Task objTask = new Task();
            objTask.Subject = objUserId;
            objTask.ActivityDate = Date.today();
            objTask.OwnerId = objUserId;
            lstTask.add(objTask);

            Event objEvent = new Event();
			objEvent.ActivityDate=Date.today();
			objEvent.StartDateTime=Date.today();
			objEvent.EndDateTime=Date.today();
			objEvent.OwnerId = objUserId;
			objEvent.Subject = 'Activity for ' + objUserId + ' - ' + Date.today();
            lstEvent.add(objEvent);
            
            PayeeUser_Attendance__c objPayeeUser_Attendance = new PayeeUser_Attendance__c();
            objPayeeUser_Attendance.Payee_User__c = objUserId;
            objPayeeUser_Attendance.No_of_Activity_Records__c = 2;
            objPayeeUser_Attendance.Attendance_Date__c = Date.today();
            lstInsertPayeeUserAttendance.add(objPayeeUser_Attendance);

            PayeeSummary__c objPayeeSummary = new PayeeSummary__c();
            objPayeeSummary.Year__c=String.ValueOf(Date.today().Year());
            objPayeeSummary.Month__c=String.ValueOf(Date.today().Month());
            objPayeeSummary.User__c=objUserId;
            objPayeeSummary.TaskFAScore__c =1;
            objPayeeSummary.TaskFAQScore__c =1;
            objPayeeSummary.Approved_Monthly_Bonus__c  = 2000;
            objPayeeSummary.RIA_Activity_Score__c = 2000;
            objPayeeSummary.Total_Days_for_this_Calendar_Month__c = 22;
            objPayeeSummary.No_Of_Days_in_Office__c = 22;
            objPayeeSummary.TaskRIAScore__c =1;
            objPayeeSummary.TaskRIAQScore__c = 1; 
            objPayeeSummary.timeTradeQCount__c = 1;
            objPayeeSummary.GlaceQCount__c =  1;
            objPayeeSummary.IWSQTasksCount__c = 1; 
            objPayeeSummary.IWSToExternalTasksQCount__c = 1;
            objPayeeSummary.ETF_Task_Referral_Q_Count__c = 1;
            objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_ETF_Spl__c = 1;
            objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_RIA_Spl__c = 1;
            lstPayeeSummary.add(objPayeeSummary);
        }

        if(!lstTask.isEmpty())
            insert lstTask;
            
        if(!lstEvent.isEmpty())
            insert lstEvent;

        if(!lstInsertPayeeUserAttendance.isEmpty())
            insert lstInsertPayeeUserAttendance;
        
        if(!lstPayeeSummary.isEmpty())
            insert lstPayeeSummary;
    }

    @isTest
    public static void testGetOnLoadRecords() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String, Object> objGetOnLoadData = new Map<String, Object>();
        List<Map<String, String>> lstDrillDownOptions =DB_InternalWholeSaler_Controller_Clone.getDrillDownUserOptions();
        List<String> lstDefaultUser = new List<String>();
        lstDefaultUser.add(UserInfo.getUserId());

        String strUserId = '';
        if(!lstDrillDownOptions.isEmpty())
            strUserId = lstDrillDownOptions[0].values()[0];
        objGetOnLoadData =DB_InternalWholeSaler_Controller_Clone.getOnLoadRecords(intCurrentMonth, intCurrentYear, strUserId, lstDefaultUser);
    }

    @isTest
    public static void testGetAllDatesFromSelectedMonth() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        List<Date> lstDate = new List<Date>();
        lstDate =DB_InternalWholeSaler_Controller_Clone.getAllDatesFromSelectedMonth(intCurrentMonth,intCurrentYear);
    }

    @isTest
    public static void testGetCurrentMonthPayeeAttendanceRecords() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String, List<String>> mapUserRecords = new Map<String, List<String>>();
        mapUserRecords =DB_InternalWholeSaler_Controller_Clone.getCurrentMonthPayeeAttendanceRecords(intCurrentMonth,intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartCurrentMonth() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getUserWiseChartCurrentMonth(lstUserId, 
                           DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                           DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartQuarterly() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getUserWiseChartQuarterly(lstUserId, 
                           DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                           DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testSaveActivityTypeAndUserDays() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

       DB_InternalWholeSaler_Controller_Clone.UserDays objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);
       DB_InternalWholeSaler_Controller_Clone.ActivityType objActivityType =DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear);

       DB_InternalWholeSaler_Controller_Clone.saveActivityTypeAndUserDays(objActivityType, objUserDays);


        User objUser = [Select Id from User where Name = 'Matt Bartlett' limit 1];

        System.runAs(objUser)
        {
            objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);
            objActivityType =DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear);

           DB_InternalWholeSaler_Controller_Clone.saveActivityTypeAndUserDays(objActivityType, objUserDays);
        }

    }

    @isTest
    public static void testGetUserDaysApprovedStatus_MattBartlett() 
    {
       DB_InternalWholeSaler_Controller_Clone.UserDays objUserDaysReturn;

        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

       DB_InternalWholeSaler_Controller_Clone.UserDays objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);

        User objUser = [Select Id, Name, Username, Email from User where Email = 'mbartlett@vaneck.com' limit 1];

        system.debug('????????????objUser???????????' + objUser);
        System.runAs(objUser)
        {
            List<PayeeSummary__c> lstPayeeSummary = new List<PayeeSummary__c>();
            for(PayeeSummary__c objPayeeSummary : [Select Id, Status__c, Approved_Monthly_Bonus__c , 
                                                    RIA_Activity_Score__c, RIA_Monthly_Bonus__c , LastDayOfMonth__c from PayeeSummary__c])
            {
                Date today = date.today().addDays(-30); 
                Test.setCreatedDate(objPayeeSummary.Id, DateTime.newInstance(today.year(), today.month(), today.day()));

                if(objPayeeSummary.Approved_Monthly_Bonus__c == objPayeeSummary.RIA_Activity_Score__c)
                {
                    objPayeeSummary.Status__c = 'Approved';
                    objPayeeSummary.No_Of_Days_in_Office__c = 22;
                    objPayeeSummary.Total_Days_for_this_Calendar_Month__c  = 22;
                    lstPayeeSummary.add(objPayeeSummary);
                }
            }

            update lstPayeeSummary;

            objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);

            objUserDaysReturn =DB_InternalWholeSaler_Controller_Clone.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
        }
        

        objUserDaysReturn =DB_InternalWholeSaler_Controller_Clone.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);

    }

    @isTest
    public static void testGetUserDaysApprovedStatus_BradPope() 
    {
       DB_InternalWholeSaler_Controller_Clone.UserDays objUserDaysReturn;

        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

       DB_InternalWholeSaler_Controller_Clone.UserDays objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);

        User objUser = [Select Id, Name, Username, Email from User where Name = 'Brad Pope' limit 1];

        system.debug('????????????objUser???????????' + objUser);
        System.runAs(objUser)
        {
            objUserDays =DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth, intCurrentYear);
            objUserDaysReturn =DB_InternalWholeSaler_Controller_Clone.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
        }

        objUserDaysReturn =DB_InternalWholeSaler_Controller_Clone.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
    }

    

    @isTest
    public static void testGetUserWiseChartMeetingAndZoomMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getUserWiseChartMeetingAndZoomMetrics(lstUserId, 
                           DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                           DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartProfileMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getUserWiseChartProfileMetrics(lstUserId, 
                           DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                           DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartETFLeadsMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getUserWiseChartETFLeadsMetrics(lstUserId, 
                           DB_InternalWholeSaler_Controller_Clone.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                           DB_InternalWholeSaler_Controller_Clone.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetSelectedYearQuarter() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarter(3, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarter(6, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarter(9, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarter(12, 2021);
    }

    @isTest
    public static void testGetSelectedYearOnlyQuarter() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearOnlyQuarter(3, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearOnlyQuarter(6, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearOnlyQuarter(9, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearOnlyQuarter(12, 2021);
    }

    
    @isTest
    public static void testGetSelectedYearQuarterDates() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String,DB_InternalWholeSaler_Controller_Clone.QuarterDates> jsonUserRecords = new Map<String,DB_InternalWholeSaler_Controller_Clone.QuarterDates>();
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarterDates(3, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarterDates(6, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarterDates(9, 2021);
        jsonUserRecords =DB_InternalWholeSaler_Controller_Clone.getSelectedYearQuarterDates(12, 2021);
    }
    
}