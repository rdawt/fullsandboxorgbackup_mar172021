/**
      Author: Vidhya Krishnan
      Date Created: 12/13/2011
      Description: Test Class for Add Members To Campaign that is called for batch processing in Populate Campaign from CSG
 */
@isTest
private class AddMembersToCampaignTest
{
    static testMethod void testAddMembersToCampaign() {
        test.startTest();
        PCnotifyemail__c po = new PCnotifyemail__c(Name='Suresh Anugu1',Email_Ids_of_Campaign_Admins__c='test@aol.in');
        insert po;
        Campaign_Subscription_Group__c campSubGrp = new Campaign_Subscription_Group__c(Name = 'Test1');
        insert campSubGrp;
        String selectedSubscription = campSubGrp.Id;
        Campaign camp = new Campaign(Name = 'Test1', IsActive = true);
        insert camp;
        String selectedCampaign = camp.Id;
        Subscription__c subs = new Subscription__c(Name='abcd',Subscription_Display_Name__c='ABC', IsActive__c=true);
        insert subs;        
        Subscription_Group__c subGrp = new Subscription_Group__c(Name='test',IsActive__c=true);
        insert subGrp;
        Account acc = new Account(Name = 'Test Account 101', Channel__c = 'RIA', BillingCountry='US');
        insert acc;
        Contact con = new Contact(AccountId=acc.Id, FirstName='ABC', LastName='ABC', MailingCountry='US',Email='abc@gmail.com',Subscription_Group__c=subGrp.Id);
        insert con;
        Subscription_Member__c subMem = new Subscription_Member__c(Subscription__c =subs.id, Contact__c =con.Id, Email__c ='abc@gmail.com', subscribed__c=true);
        insert subMem;
        Campaign_Subscription_Group_Member__c csgm = new Campaign_Subscription_Group_Member__c(Campaign_Subscription_Group__c = campSubGrp.Id, Subscription__c = subs.id);
        insert csgm ;
        List<Subscription_Member__c> scope = new List<Subscription_Member__c> () ;
        scope.add(subMem);
        CampaignMember campMem = new CampaignMember(ContactId = con.Id,CampaignId = camp.Id);
        insert campMem;
        
        AddMembersToCampaign addCmp=new AddMembersToCampaign(selectedSubscription,selectedCampaign);
        addCmp.strLimit=200;
        Database.executeBatch(addCmp);
        Test.StopTest();
        
        
    }
}