<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_SFDC_Channel_Id</fullName>
        <field>SFDC_Channel_Id__c</field>
        <formula>IF(SFDC_Channel_Derived__c == &apos;RIA&apos; , &apos;a3P19000000OHvaEAG&apos;, IF(SFDC_Channel_Derived__c == &apos;Institutional&apos;, &apos;a3P19000000OHveEAG&apos;, IF(SFDC_Channel_Derived__c == &apos;Asset Manager&apos;, &apos;a3P19000000OHvZEAW&apos;, IF(SFDC_Channel_Derived__c == &apos;Financial Advisor&apos;, &apos;a3P19000000OGZKEA4&apos;, IF(SFDC_Channel_Derived__c == &apos;Insurance&apos;, &apos;a3P19000000OI3mEAG&apos;, IF(SFDC_Channel_Derived__c == &apos;Retirement&apos;, &apos;a3P19000000OI3hEAG&apos;, &apos;&apos;) ) ) ) ) )</formula>
        <name>Set SFDC Channel Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SFDC_Fund_Id</fullName>
        <field>SFDC_Fund_Id__c</field>
        <formula>IF ( Name = &quot;VanEck Cm Commodity Index Fund&quot;, &quot;a0uA0000001OOehIAG&quot;,IF(Name = &quot;VanEck Emerging Markets Fund&quot;, &quot;a0uA0000001OOemIAG&quot;,IF(Name = &quot;VanEck Global Hard Assets Fund&quot;, &quot;a0uA0000001OOerIAG&quot;,IF(Name =&quot;VanEck International Investors Gold&quot;,&quot;a0uA0000001OOewIAG&quot;,IF(Name =&quot;VanEck Morningstar Wide Moat Fund&quot;,&quot;a0uA000000EnuDsIAJ&quot;,IF(Name =&quot;VanEck NDR Managed Allocation Fund&quot;,&quot;a0uA0000009poGbIAI&quot;,IF(Name =&quot;VanEck Emerging Markets Bond Fund&quot;,&quot;a0uA0000001OOeOIAW&quot;,If(Name=&quot;VanEck Vip Global Gold Fund&quot;,&quot;a0uA0000001yppgIAA&quot;,If((Name=&quot;VanEck VIP Emerging Markets Bond Fund&quot; || Name=&quot;VanEck Vip Emerging Markets Bond Fund&quot;),&quot;a0u19000002T9KzAAK&quot;,If(Name=&quot;VanEck Vip Unconstrained Emerging Markets Bond Fund&quot;,&quot;a0uA0000001Nt7jIAC&quot;,IF(Name=&quot;VanEck Vip Emerging Markets&quot;,&quot;a0uA0000001Nt7iIAC&quot;,IF(Name=&quot;VanEck VIP Emerging Markets Fund&quot;,&quot;a0u19000002TafyAAC&quot;,IF(Name=&quot;VanEck Vip Global Hard Assets Fund&quot;,&quot;a0uA0000001Nt7kIAC&quot;,If(Name=&quot;VanEck Unconstrained Emerg Mkt Bond&quot;,&quot;a0uA0000001OOeOIAW&quot;,&quot;&quot;))))))))))))))</formula>
        <name>Set SFDC Fund Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SFDC_Fund_Ticker</fullName>
        <field>SFDC_Fund_Ticker__c</field>
        <formula>IF ( Name = &quot;VanEck Cm Commodity Index Fund&quot;, &quot;CMCI&quot;,IF(Name = &quot;VanEck Emerging Markets Fund&quot;, &quot;EME&quot;,IF(Name = &quot;VanEck Global Hard Assets Fund&quot;, &quot;GHA&quot;,IF(Name =&quot;VanEck International Investors Gold&quot;,&quot;IIG&quot;,IF(Name =&quot;VanEck Morningstar Wide Moat Fund&quot;,&quot;MWM&quot;,IF(Name =&quot;VanEck NDR Managed Allocation Fund&quot;,&quot;NDR&quot;,IF(Name =&quot;VanEck Emerging Markets Bond Fund&quot;,&quot;EMB&quot;,If(Name=&quot;VanEck Vip Global Gold Fund&quot;,&quot;VGOLDS&quot;,If((Name=&quot;VanEck VIP Emerging Markets Bond Fund&quot; || Name=&quot;VanEck Vip Emerging Markets Bond Fund&quot;),&quot;WWBD&quot;,If(Name=&quot;VanEck Vip Unconstrained Emerging Markets Bond Fund&quot;,&quot;WWBD_1&quot;,IF(Name=&quot;VanEck Vip Emerging Markets&quot;,&quot;WWEM&quot;,IF(Name=&quot;VanEck VIP Emerging Markets Fund&quot;,&quot;WWEMS&quot;,IF(Name=&quot;VanEck Vip Global Hard Assets Fund&quot;,&quot;WWHA&quot;,If(Name=&quot;VanEck Unconstrained Emerg Mkt Bond&quot;,&quot;EMB&quot;,&quot;&quot;))))))))))))))</formula>
        <name>Set SFDC Fund Ticker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Account Sales by Fund History SFDC Fields</fullName>
        <actions>
            <name>Set_SFDC_Channel_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SFDC_Fund_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SFDC_Fund_Ticker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>History_Firm_Portfolio_Breakdown__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>History_Firm_Portfolio_Breakdown__c.Fund_Vehicle_Type__c</field>
            <operation>equals</operation>
            <value>Mutual Fund</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
