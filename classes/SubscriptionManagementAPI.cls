global class SubscriptionManagementAPI //extends DataStructures
{
  public static SendEmailNotification se;
 
  public SubscriptionManagementAPI(){
    se = new SendEmailNotification('Error: Subscription Management API class failed');
  }
 
    //Method to get subscription group Id
    Webservice static string GetSubscriptionGroupID(String contactID)
    {
      try{
           list<Contact> con = new list<Contact>();
           con = [Select Id,Subscription_Group_Derived_Id__c from Contact Where Id =: contactID];
           if (con.isEmpty())
              return '0'; // As per Al's advice returning a string with value 0
           else
              return con[0].Subscription_Group_Derived_Id__c;
      }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetSubscriptionGroupID method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return '0';
      }
    }
 
    //Method added by vidhya - to check if the Privacy Policy has been sent or not
    WebService static boolean showPrivacyAck(String contactID)
    {
      try{
          list<Contact> con = new list<Contact>();
          con = [Select Id,Privacy_Policy_Sent_Date__c,Privacy_Policy_Web_Ack_Date__c from Contact Where Id =: contactID];
          if(!con.isEmpty() && con[0].Privacy_Policy_Sent_Date__c == null && con[0].Privacy_Policy_Web_Ack_Date__c == null)
              return true; // Privacy policy tab has to be shown in the API.
          else
              return false; // Privacy policy should not be sent from SFDC or the link should bot be shown from VanEck site
      }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in showPrivacyAck method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return false;
      }
    }
 
    //Method added by Geeta - to return Privacy Policy Ack Date if it has been sent and
    // NULL if it has not been sent
    global static DateTime getPrivacyAckDate(String contactID)
    { 
    	DateTime privacyDate = null; 
        try{
            list<Contact> con = new list<Contact>();
            con = [Select Id,Privacy_Policy_Sent_Date__c,Privacy_Policy_Web_Ack_Date__c from Contact Where Id =: contactID];
            system.debug('getPrivacyAckDate Contact.......:'+con);
            if(!con.isEmpty() && con[0].Privacy_Policy_Web_Ack_Date__c != null)
                privacyDate = con[0].Privacy_Policy_Web_Ack_Date__c;
            else if (!con.isEmpty() && con[0].Privacy_Policy_Sent_Date__c != null)
            	privacyDate = con[0].Privacy_Policy_Sent_Date__c;
            return privacyDate;
        }catch(Exception e){
            if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in getPrivacyAckDate method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return null;
        }
    }
 
    //Method to update the Privacy Policy Web Ack Date in contact object
    WebService static boolean UpdateContactPrivacy(String contactID, DateTime updated)
    {
      try{
          list<Contact> con = new list<Contact>();
          con = [Select Id,Privacy_Policy_Web_Ack_Date__c from Contact Where Id =: contactID];
           if (con.isEmpty())
              return false;
           else{
              Contact conRec = new Contact(Id=contactID);
              conRec.Privacy_Policy_Web_Ack_Date__c = updated;
              update conRec;
              return true;
           }
      }catch(Exception e){ if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in UpdateContactPrivacy method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return false;
      }
    }
 
    //Method to Get contact name
    WebService static string GetContactName(String contactID)
    {
      try{
           list<Contact> con = new list<Contact>();
           con = [Select FirstName,LastName from Contact Where Id =: contactID];
           if (con.isEmpty())
              return '0';  // As per Al's advice returning a string with value 0, assuming null value is not handled in c# coding
           else
              return con[0].LastName + ',' + con[0].FirstName ;
      }catch(Exception e){if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetContactName method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return '0';
      }
    }

    //Method to insert/update the list of subscription member records at a time
    WebService static boolean setSubscriptions(list<SubscriptionManagementAPI.SubscriptionDetails> subList){
        map<Id,Subscription_Member__c> subMemMap = new map<Id,Subscription_Member__c>();// key -> Sub Mem Id; value -> Sub Mem Record
        map<Id,Subscription_Member__c> subValueMap = new map<Id,Subscription_Member__c>();// key -> Subscription Id; value -> Sub Mem Record
        list<Subscription_Member__c> lstRecInsert = new list<Subscription_Member__c>();
        list<Subscription_Member__c> lstRecUpdate = new list<Subscription_Member__c>();
        list<String> subIdList = new List<string>();
        Subscription_Member__c sm = new Subscription_Member__c();
        string conId = null;
  
        try{
          conId = subList[0].contactId;
          if(conId == null) return false;
    
          for(SubscriptionManagementAPI.SubscriptionDetails subMem : subList)
              subIdList.add(subMem.subscriptionId);
    
          subMemMap.putAll([SELECT Id, Subscription__c, Subscribed__c FROM Subscription_Member__c where Subscription__c IN: subIdList and Contact__c =: conId]);
          for(Subscription_Member__c sm1 : subMemMap.Values())
        subValueMap.put(sm1.Subscription__c, sm1);
    
          // If given sub list is null and there is no Externally managed subscription available for the contact, then return TRUE
          if(subValueMap.isEmpty()){
            list<Contact> c = new list<Contact>();
            c = [select id, Subscription_Group__c from Contact where id =: conId limit 1];
            list<Subscription_Group_Subscription__c> sgsList = new list<Subscription_Group_Subscription__c>();
            sgsList = [select id from Subscription_Group_Subscription__c s where s.Subscription_Group__c =: c[0].Subscription_Group__c and s.Subscription__r.Internally_Managed__c != true and s.Subscription__r.IsActive__c =: true];
            if(sgsList.isEmpty())
              return true;
          }
    
            for(SubscriptionManagementAPI.SubscriptionDetails subDetail : subList){
                if(subDetail.oldValue == subDetail.newValue) {}
                    //No change required - for both true & false values
                else{
                    sm.clear();
                    if(subValueMap.containsKey(subDetail.subscriptionId)){    // Update
                        sm = subValueMap.get(subDetail.subscriptionId);
                        if(Boolean.valueOf(subDetail.newValue) != sm.Subscribed__c){
                            Subscription_Member__c smem = new Subscription_Member__c(Id=sm.Id);
                            //smem.Contact__c = conId; // Master-Detail Field is not writable in update
                            smem.Subscribed__c = Boolean.valueOf(subDetail.newValue);
                            if(subDetail.newValue.equalsIgnoreCase('false'))
                                smem.Subscription_Unsubscribed_Date__c = system.now();
                            if(subDetail.newValue.equalsIgnoreCase('true'))
                                smem.Subscription_Unsubscribed_Date__c = null;
                            lstRecUpdate.add(smem);
                        }
                    }
                    else{   //Insert
                        if(subDetail.newValue.equalsIgnoreCase('true')){
                            Subscription_Member__c smem = new Subscription_Member__c();
                            smem.Subscription__c = subDetail.subscriptionId;
                            smem.Contact__c = conId;
                            smem.Subscribed__c = Boolean.valueOf(subDetail.newValue);
                            lstRecInsert.add(smem);
                        }
                    }
                }
            }
      
            if(!lstRecUpdate.isEmpty())
                update lstRecUpdate;
            if(!lstRecInsert.isEmpty())
                insert lstRecInsert;
        }
        catch(Exception e){
          if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed');
          se.sendEmail('Exception Occurred in setSubscriptions method : \n'+e.getMessage()+' ; Sub List passed......'+subList+' ; '+e.getStackTraceString());
            return false;
        }
        return true;
    }
 
    //Method to insert/update the subscription member record one at a time
    WebService static boolean setsubscription(string SubId,string Conid,boolean subscribedNewValue,boolean subscribedOldValue)
    {
      try{
            //List to collect the new subscription member records
            list<Subscription_Member__c> lstRecInsert = new list<Subscription_Member__c>();
            //List to collect the existing subscription member records
            list<Subscription_Member__c> lstRecUpdate = new list<Subscription_Member__c>();
            list<Subscription_Member__c> lstSM = [SELECT s.Id, s.Subscription__c, s.Subscribed__c, s.Contact__c FROM Subscription_Member__c s where s.Subscription__c=:SubId and s.Contact__c=:Conid LIMIT 1];
            list<Contact> con = new list<Contact>();
            con = [Select Id,Email from Contact Where Id=: Conid LIMIT 1];
            if(con.isEmpty()) return false;
            if(subscribedNewValue == subscribedOldValue)
            {
               list<Subscription_Member__c> lstSMCheck = [SELECT s.Id, s.Subscription__c, s.Subscribed__c, s.Contact__c FROM Subscription_Member__c s where s.Subscription__c=:SubId and s.Contact__c=:Conid and s.Subscribed__c=:subscribedOldValue LIMIT 1];
               if(lstSMCheck.size()>0)
                 return true;
               else
                 return false;
            }
            else
            {
                if(lstSM.size()>0)
                {
                       //Assign the new subscribe value to existing subscription member if old value is not equal to new value
                       if(lstSM[0].Subscribed__c != subscribedNewValue)
                       {
                           Subscription_Member__c smem = new Subscription_Member__c(Id=lstSM[0].Id);
                           if(subscribedNewValue == false)
                             smem.Subscription_Unsubscribed_Date__c=system.now();
                           if(subscribedNewValue == true)
                             smem.Subscription_Unsubscribed_Date__c=null;
                           smem.Subscribed__c = subscribedNewValue;
                           lstRecUpdate.add(smem);
                       }
                }
                else
                {
                       //Assign the values to new subscription member
                       Subscription_Member__c smem = new Subscription_Member__c();
                       smem.Contact__c = Conid;
                       smem.Subscription__c = SubId;
                       if(subscribedNewValue==false)
                           smem.Subscription_Unsubscribed_Date__c=system.now();
                       if(subscribedNewValue==true)
                          smem.Subscription_Unsubscribed_Date__c=null;
                       smem.Subscribed__c = subscribedNewValue;
                       lstRecInsert.add(smem);
                }
                //Update the existing subscription member records with new subscribe value
                if(!lstRecUpdate.isEmpty())
                   update lstRecUpdate;
                //Insert the new subscription member records with new subscribe value
                if(!lstRecInsert.isEmpty())
                   insert lstRecInsert;
            }
            return true;
      }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in setsubscription method : \n'+e.getMessage()+' ; Contact Id passed......'+Conid+' ; Sub Id passed.....'+SubId+' ; '+e.getStackTraceString()); return false;
      }
    }

    //Used in Van Eck FA Registration Site - Filters Subscriptions that is tied to FA Community.
    WebService static SubscriptionManagementAPI.RecordSet [] GetSubscriptions(String contactID, String subGroupID)
    {
        list<Subscription_Group_Subscription__c> lstSGS = new list<Subscription_Group_Subscription__c>();
        list<SubscriptionManagementAPI.RecordSet> lstRSet = new list<SubscriptionManagementAPI.RecordSet>();
        try{
          system.debug('value passed.....'+contactID+ ' ; '+subGroupID);
          // RL - Updated Subscription__r.Name >> to >>Subscription__r.Subscription_Display_Name__c
          // SA - Added one more filter(Internally Managed) to the where clause - 01/05/12
          lstSGS = [SELECT s.Subscription__r.Subscription_Type__r.Id, s.Subscription__r.Subscription_Type__r.Name, s.Subscription__r.Subscription_Display_Name__c, s.Subscription__r.Id,
                s.Subscription__r.Description__c, s.Subscription__r.Subscription_Type__r.Display_Name__c ,s.Subscription_Group__r.Id, s.Subscription_Group__r.Name, s.Sort_Order_For_Form__c
                FROM Subscription_Group_Subscription__c s
                WHERE s.Subscription_Group__c =: subGroupID and s.Subscription__r.Internally_Managed__c != true and s.Subscription__r.IsActive__c =: true

                /*  Hard Coded Community Id value - Provide appropriate ids in each sand box  */
                and s.Subscription__r.Community__c =: 'a0qZ0000001X6bE' // CMS FA Community Id
                // and s.Subscription__r.Community__c =: 'a0qA0000001VEbp' // Prod FA Community Id
                order by s.Sort_Order_For_Form__c];

          /*  To Test the MV AU Overloaded Method
          system.debug('List of SGS.....'+lstSGS);
          lstRSet = SubscriptionManagementAPI.GetSubscriptionsWithCommunity(contactID, subGroupID, 'a0gT0000002iQw4');
          system.debug('Back to FA method....');  */
          list<Subscription_Member__c> lstSM = [SELECT s.Id, s.Subscription__c,s.Subscription_Unsubscribed_Date__c,s.Subscribed__c, s.Contact__c FROM Subscription_Member__c s
                              WHERE s.Contact__c =: contactID];
          if(!lstSGS.isEmpty())
          {
                for(Subscription_Group_Subscription__c sgs: lstSGS)
                {            
                      //loop through every record in the subscription_groupd_subscription for the given set of subscription id in sub_grp
                      //and put them into record set which will be returned
                      SubscriptionManagementAPI.RecordSet rs = new SubscriptionManagementAPI.RecordSet();
                      rs.SubGroup = sgs.Subscription_Group__r.Name;
                      rs.SubGroupId = sgs.Subscription_Group__r.Id;
                      rs.SubType = sgs.Subscription__r.Subscription_Type__r.Display_Name__c != null ? sgs.Subscription__r.Subscription_Type__r.Display_Name__c : sgs.Subscription__r.Subscription_Type__r.Name;
                      rs.SubTypeId = sgs.Subscription__r.Subscription_Type__r.Id;
                      rs.SubName = sgs.Subscription__r.Subscription_Display_Name__c;
                      rs.SubDesc = sgs.Subscription__r.Description__c;
                      rs.SubId = sgs.Subscription__r.Id;
                      for(Subscription_Member__c sm: lstSM)
                      {
                           if(sm.Subscription__c == sgs.Subscription__r.Id)
                           {
                                rs.Subscribed = sm.Subscribed__c;
                                rs.subUnsubDate=sm.Subscription_Unsubscribed_Date__c;
                                rs.SubMemId = sm.Id;
                            }
                      }
                      lstRSet.add(rs);
                 }
          }
      }catch(Exception e){ if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetSubscriptions method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; Sub Grp Id passed......'+subGroupID+' ; '+e.getStackTraceString());
      }
        System.Debug('Returned FA RecordSet....'+lstRSet);
        return lstRSet;
    }
 
    WebService static SubscriptionManagementAPI.RecordSet [] GetSubscriptionsWithCommunity(String contactID, String subGroupID, String communityId)
    {
        list<Subscription_Group_Subscription__c> lstSGS = new list<Subscription_Group_Subscription__c>();
        list<SubscriptionManagementAPI.RecordSet> lstRSet = new list<SubscriptionManagementAPI.RecordSet>();
        system.debug('Values passed.....'+contactID+';'+subGroupID+';'+communityId);
        try{
          lstSGS = [SELECT s.Subscription__r.Subscription_Type__r.Id, s.Subscription__r.Subscription_Type__r.Name, s.Subscription__r.Subscription_Display_Name__c, s.Subscription__r.Id,
                s.Subscription__r.Subscription_Type__r.Display_Name__c, s.Subscription__r.Description__c, s.Subscription_Group__r.Id, s.Subscription_Group__r.Name, s.Sort_Order_For_Form__c
                FROM Subscription_Group_Subscription__c s
                WHERE s.Subscription_Group__c =: subGroupID and s.Subscription__r.Internally_Managed__c!=true and s.Subscription__r.IsActive__c =: true
                and s.Subscription__r.Community__c =: communityId order by s.Sort_Order_For_Form__c];
          list<Subscription_Member__c> lstSM = [SELECT s.Id, s.Subscription__c,s.Subscription_Unsubscribed_Date__c,s.Subscribed__c, s.Contact__c FROM Subscription_Member__c s
                              WHERE s.Contact__c =: contactID];
          if(!lstSGS.isEmpty())
          {
                for(Subscription_Group_Subscription__c sgs: lstSGS)
                {
                      //loop through every record in the subscription_groupd_subscription for the given set of subscription id in sub_grp
                      //and put them into record set which will be returned
                      SubscriptionManagementAPI.RecordSet rs = new SubscriptionManagementAPI.RecordSet();
                      rs.SubGroup = sgs.Subscription_Group__r.Name;
                      rs.SubGroupId = sgs.Subscription_Group__r.Id;
                      rs.SubType = sgs.Subscription__r.Subscription_Type__r.Display_Name__c != null ? sgs.Subscription__r.Subscription_Type__r.Display_Name__c : sgs.Subscription__r.Subscription_Type__r.Name;
                      rs.SubTypeId = sgs.Subscription__r.Subscription_Type__r.Id;
                      rs.SubName = sgs.Subscription__r.Subscription_Display_Name__c;
                      rs.SubDesc = sgs.Subscription__r.Description__c;
                      rs.SubId = sgs.Subscription__r.Id;
                      for(Subscription_Member__c sm: lstSM)
                      {
                           if(sm.Subscription__c == sgs.Subscription__r.Id)
                           {
                                rs.Subscribed = sm.Subscribed__c;
                                rs.subUnsubDate=sm.Subscription_Unsubscribed_Date__c;
                                rs.SubMemId = sm.Id;
                            }
                      }
                      lstRSet.add(rs);
                 }
          }
      }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetSubscriptionsWithCommunity method : Values passed.....'+contactID+' ; '+subGroupID+';'+communityId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
      }
        System.Debug('Returned RecordSet....'+lstRSet);
        return lstRSet;
    }
 
    //Used in Community Registration Create Membership page
  WebService static SubscriptionManagementAPI.RecordSet [] GetSubscriptionsWithSubGrp(String subGroupID, String communityId)
    {
        list<Subscription_Group_Subscription__c> lstSGS =new list<Subscription_Group_Subscription__c>();
        list<SubscriptionManagementAPI.RecordSet> lstRSet = new list<SubscriptionManagementAPI.RecordSet>();
        system.debug('Values passed.....'+subGroupID+';'+communityId);
        try{
          lstSGS = [SELECT s.Subscription__r.Subscription_Type__r.Id, s.Subscription__r.Subscription_Type__r.Name, s.Subscription__r.Subscription_Display_Name__c, s.Subscription__r.Id,
                s.Subscription__r.Subscription_Type__r.Display_Name__c, s.Subscription__r.Description__c, s.Subscription_Group__r.Id, s.Subscription_Group__r.Name, s.Sort_Order_For_Form__c
                FROM Subscription_Group_Subscription__c s
                WHERE s.Subscription_Group__c =: subGroupID and s.Subscription__r.Internally_Managed__c!=true and s.Subscription__r.IsActive__c =: true
                and s.Subscription__r.Community__c =: communityId order by s.Sort_Order_For_Form__c];
          if(!lstSGS.isEmpty())
          {
                for(Subscription_Group_Subscription__c sgs: lstSGS)
                {
                      //loop through every record in the subscription_groupd_subscription for the given set of subscription id in sub_grp
                      //and put them into record set which will be returned
                      SubscriptionManagementAPI.RecordSet rs = new SubscriptionManagementAPI.RecordSet();
                      rs.SubGroup = sgs.Subscription_Group__r.Name;
                      rs.SubGroupId = sgs.Subscription_Group__r.Id;
                      rs.SubType = sgs.Subscription__r.Subscription_Type__r.Display_Name__c != null ? sgs.Subscription__r.Subscription_Type__r.Display_Name__c : sgs.Subscription__r.Subscription_Type__r.Name;
                      rs.SubTypeId = sgs.Subscription__r.Subscription_Type__r.Id;
                      rs.SubName = sgs.Subscription__r.Subscription_Display_Name__c;
                      rs.SubDesc = sgs.Subscription__r.Description__c;
                      rs.SubId = sgs.Subscription__r.Id;
                      lstRSet.add(rs);
                 }
          }
      }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetSubscriptionsWithSubGrp method : Values passed.....'+subGroupID+';'+communityId+' ; '+e.getMessage()+' ; '+e.getStackTraceString());
      }
        System.Debug('Returned RecordSet....'+lstRSet);
        return lstRSet;
    }
    /*
    This method is created for Snap Shot Processing ONLY
    Should not be used for regular Subscription Process.
    */
  Webservice static string GetInvestorType(String contactID){
    try{
      system.debug('Given Contact Id....'+contactID);
      list<Contact> con = new list<Contact>();
      Community_Registrant__c cReg = new Community_Registrant__c();
      con = [Select Id, Community_Registrant__c,InvestorTpSD__c from Contact Where Id =: contactID];
      if (con.isEmpty())
        return '0';  // As per Al's advice returning a string with value 0, assuming null value is not handled in c# coding
      if(con[0].Community_Registrant__c == null){
        if(con[0].InvestorTpSD__c == null)
          return '0';
        else{
          system.debug('Returning Contact value.....'+con[0].InvestorTpSD__c);
          return con[0].InvestorTpSD__c;
        }
      }
      else{
        cReg = [select Investor_Type__c from Community_Registrant__c where id =: con[0].Community_Registrant__c];
        system.debug('Registrant....'+cReg);
        if(cReg != null && cReg.Investor_Type__c != null){
          system.debug('Returning Registrant value.....'+cReg.Investor_Type__c);
          return cReg.Investor_Type__c ;
        }
        else
          return '0';
      }
    }catch(Exception e){
      if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetInvestorType method : Contact Id passed..... '+contactID+' ; '+e.getMessage()+' ; '+e.getStackTraceString()); return '0';
    }
  }
 
     /*  Used in Onc Click Subscription  */
     Webservice static string GetSubscriptionId(String contactID, string subName, String communityId){
       try{
         list<Subscription__c> subNameList = new list<Subscription__c>();
         string subGrpId, investorType, subId;
         boolean b = false;
         subGrpId = GetSubscriptionGroupID(contactID);
   
         system.debug('Given Contact Id....'+contactID+' ; sub_name_enum.......'+subName+' ; Community Id.......'+communityId+' ; Sub Grp Id.......'+subGrpId);
            
         // check for Invalid Contact Id
         if(subGrpId == '0') {
           system.debug('Returning.......Invalid Contact Id');
           return 'SF_Error : Invalid Contact Id';
         }

         // check for Invalid Sub Name
         subNameList = [select SubName_Enum__c from Subscription__c where IsActive__c = true];
         if(!subNameList.isEmpty()){
           for(Subscription__c s : subNameList)
             if(s.SubName_Enum__c == subName)
               { b = true; break; }
             else b = false;
         }
         if(b == false) {
           system.debug('Returning.......Invalid Sub Name');
           return 'SF_Error : Invalid Sub Name';
         }
   
         b = false; subId = null;
         list<Subscription_Group_Subscription__c> lstSGS = new list<Subscription_Group_Subscription__c>();
         lstSGS = [SELECT s.Subscription__r.Id, s.Subscription__r.SubName_Enum__c FROM Subscription_Group_Subscription__c s
              WHERE s.Subscription_Group__c =: subGrpId and s.Subscription__r.Internally_Managed__c != true and s.Subscription__r.IsActive__c = true
              and s.Subscription__r.Community__c =: communityId];
          system.debug('list......'+lstSGS);
    
          if(!lstSGS.isEmpty()){
            for(Subscription_Group_Subscription__c sgs: lstSGS){
              if(sgs.Subscription__r.SubName_Enum__c != null && sgs.Subscription__r.SubName_Enum__c == subName){
                b = true;
                subId = sgs.Subscription__r.Id;
                system.debug('Subscription Id..........'+sgs.Subscription__r.Id);
              }
            }
          }
    
      // check if Not eligible
        if(b == false){
          system.debug('Returning..........Not eligible');
          return 'SF_Warning : Not Eligible';
        }
  
      // check if already subscribed
         list<SubscriptionManagementAPI.RecordSet> lstRecordSet = new list<SubscriptionManagementAPI.RecordSet>();
         lstRecordSet = GetSubscriptionsWithCommunity(contactID, subGrpId, communityId);
         system.debug('Record Set list......'+lstRecordSet);
      if(!lstSGS.isEmpty()){
          for(SubscriptionManagementAPI.RecordSet rs : lstRecordSet){
            if(rs.SubId == subId && rs.Subscribed == true){
              system.debug('Returning..........Already Subscribed');
              return 'SF_Warning : Already Subscribed';
            }
          }
         }
   
         //if not return the sub id to be subscribed
         if(subId != null){
           system.debug('Returning Subscription Id....'+subId);
             return subId;
         }
          return null;
       }catch(Exception e){ if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in GetSubscriptionId method : \n'+e.getMessage()+' ; '+contactID+' ; '+subName+' ; '+e.getStackTraceString()); return null;
       }
     }
 
    //Method added by Geeta to get the OptoutValue of the contact
    WebService static boolean getOptout(String ContactId){
       boolean result=false;
       try{
          list<contact> con= [Select Id, HasOptedOutOfEmail from Contact where Id =: contactID ];
          System.debug('Contact List......' +con);
          if(con.isEmpty()==false && con[0].HasOptedOutOfEmail==true){
          result=true;
          }
            return result;
       }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in getOptout method : \n'+e.getMessage()+' ; '+contactID + ' ; '+e.getStackTraceString()); return false;
       }
    }

    //Method added by Geeta to set the OptoutValue of the contact
  webService static boolean setOptout(String ContactId, Boolean requestedStatus){
  try{
    System.debug('setOptout values passed....' +ContactId+':'+requestedStatus );
    Contact conRec = new Contact(Id=contactID);
      conRec.HasOptedOutOfEmail= requestedStatus;
      update conRec;
      return true;
  }catch(Exception e){if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in setOptout method : \n'+e.getMessage()+' ; '+contactID +  ' ; '+e.getStackTraceString()); return false;
        }
    }

    //Method added by Geeta to get the email of the contact
    WebService static String getEmail(String ContactId){
       String emailId='';
       try{
          list<contact> con= [Select Id, Email from Contact where Id =: contactID ];
          System.debug('contact.......' +con);
          if(con.isEmpty()==false){
           emailId=con[0].Email;
          }
            return emailId;
       }catch(Exception e){
        if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed');
        se.sendEmail('Exception Occurred in getEmail method : \n'+e.getMessage()+' ; '+contactID + ' ; '+e.getStackTraceString());
            return emailId;
       }
    }
 
    //Added by Geeta to GET ALL THE REQUIRED FIELDS FOR CONTACT RECORD
    Webservice static SubscriptionManagementAPI.ContactRecordDetails getContactRecordDetails(String contactID){
		SubscriptionManagementAPI.ContactRecordDetails conRecord;
		try{
			String privacyDate = null; 
			String OptOut = 'false';
			conRecord = new SubscriptionManagementAPI.ContactRecordDetails();
			
			list<Contact> con = new list<Contact>();
			con = [Select Id, FirstName, LastName, Subscription_Group_Derived_Id__c, Email,
					Privacy_Policy_Sent_Date__c, Privacy_Policy_Web_Ack_Date__c, HasOptedOutOfEmail
					from Contact Where Id =: contactID];

			if(!con.isEmpty() && con[0].Privacy_Policy_Web_Ack_Date__c != null)
			    privacyDate = (con[0].Privacy_Policy_Web_Ack_Date__c).format();
			else if (!con.isEmpty() && con[0].Privacy_Policy_Sent_Date__c != null)
				privacyDate = (con[0].Privacy_Policy_Sent_Date__c).format();
			
			if(!con.isEmpty() && con[0].HasOptedOutOfEmail == true)
				OptOut = 'True';
			if(!con.isEmpty() && con[0].Email == null)
				conRecord.errorMsg = 'SF_ERROR: Email Id of the contact is deleted by a Salesforce User';
			
			if(!con.isEmpty()){
				conRecord.contactID = con[0].Id;
				conRecord.contactName = con[0].LastName + ',' + con[0].FirstName;
				conRecord.firstName = con[0].FirstName;
				conRecord.lastName = con[0].LastName;
				conRecord.emailId = con[0].Email;
				conRecord.hasOptedOut = OptOut;
				conRecord.privacyPolicyAck = privacyDate;
				conRecord.subscriptionGroupID = con[0].Subscription_Group_Derived_Id__c;
				conRecord.investorType = GetInvestorType(contactID);
			}

      
      /***** VK - Commented Geeta's code as it is using multiple DB Quries to get value of same record
      
      conRecord.contactID = contactID;//This is not needed
      conRecord.subscriptionGroupID = GetSubscriptionGroupID(contactID);
 
      //if the privacy policy was acknowledged, returns acknowledged date
      //if the privacy policy was not acknowledged, returns null
      DateTime tempDateTime = getPrivacyAckDate(contactID);
      String tempDate = Null;
  
      if(tempDateTime != null) {
       tempDate = tempDateTime.format();
      }
      system.debug('getContactRecordDetails.....tempDate.......'+tempDate);
      conRecord.privacyPolicyAck = tempDate;
 
      conRecord.contactName = GetContactName(contactID); 
      
      if(GetOptout(contactID) == true) {
        conRecord.hasOptedOut = 'true';
      } else {
        conRecord.hasOptedOut = 'false';
      }
      conRecord.emailId = GetEmail(contactID);*/
      
      system.debug('getContactRecordDetails return value.......'+conRecord);
      }
      catch(Exception e){
            if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in getContactRecordDetails method : \n'+e.getMessage()+' ; Contact Id passed......'+contactID+' ; '+e.getStackTraceString()); return null;
      }
      return conRecord;
    }
 
    //Added by Geeta to SET ALL THE REQUIRED FIELDS FOR CONTACT RECORD
    Webservice static Boolean setContactRecordDetails(
      SubscriptionManagementAPI.ContactRecordDetails conRecord){
      system.debug('setContactRecordDetails passed values.....'+conRecord);
      Boolean result=false;
      Boolean isUpdateContactPrivacyCalled=false,resultForUpdateContactPrivacy=false;
      Boolean isSetOptOutCalled=false,resultForSetOptOut=false;
      DateTime policyAckedDate;
      Boolean isDateRecvd;
      policyAckedDate = getPrivacyAckDate(conRecord.contactID);
      isDateRecvd = (String.isNotBlank(conRecord.privacyPolicyAck) && String.isNotEmpty(conRecord.privacyPolicyAck));
      system.debug('setContactRecordDetails:isDateRecvd........'+isDateRecvd);
      try{
      if(isDateRecvd) {
          if(policyAckedDate == null) {
           DateTime policyAckRecvdDate;
           isUpdateContactPrivacyCalled = true;
           DateTime tempDateTime =  DateTime.parse(conRecord.privacyPolicyAck);
           system.debug('setContactRecordDetails:tempDateTime:'+tempDateTime);
           resultForUpdateContactPrivacy = UpdateContactPrivacy(conRecord.contactID, tempDateTime);
          }
      }

      Boolean currStatus,newStatus;
      if(String.isNotBlank(conRecord.hasOptedOut) && String.isNotEmpty(conRecord.hasOptedOut)) {
       currStatus = GetOptout(conRecord.contactID);
       if(conRecord.hasOptedOut.equalsIgnoreCase('true')) {
         newStatus = True; 
       } else {
         newStatus = False; 
       }
       if(currStatus != newStatus) {
        isSetOptOutCalled = true;
        resultForSetOptOut = setOptOut(conRecord.contactID,newStatus);
       }
      } 
 
      String currEmail,newEmail;
      Boolean isSetEmailCalled=false,resultForSetEmail=false;
 
      system.debug('setContactRecordDetails:record:'+conRecord);
 
      if(isUpdateContactPrivacyCalled==true) {
        result=resultForUpdateContactPrivacy;
      }
 
      if(isSetOptOutCalled==true) {
        result=resultForSetOptOut;
      }
 
      }
      catch(Exception e){
            if(se == null) se = new SendEmailNotification('Error: Subscription Management API class failed'); se.sendEmail('Exception Occurred in setContactRecordDetails method : \n'+e.getMessage()+' ; '+conRecord.contactID+' ; '+e.getStackTraceString()); return false;
      }
      return result;
    }

    // WRAPPER CLASS
    global class Recordset
    {
      WebService Boolean Subscribed {get; set;} //Sub Mem --> Subscribed
      WebService String SubMemId {get; set;} //Sub Mem --> Sub Mem Id
      WebService String SubId {get; set;} //Subscription --> Id
      WebService String SubName {get; set;} //Subscription --> Name
      WebService String SubDesc {get; set;} //Subscription --> Description
      WebService DateTime subUnsubDate{get;set;}
      WebService String SubTypeId{get; set;} //Subscription Type --> Id
      WebService String SubType{get; set;} //Subscription Type --> Name
      WebService String SubGroupId{get; set;} //SG --> Id
      WebService String SubGroup{get; set;} //SG --> Name
    }//End Recordset
 
    // SUB SET WRAPPER CLASS
    global class SubscriptionDetails{
        Webservice String contactId {get; set;}
        Webservice String subscriptionId {get; set;}
        Webservice string oldValue {get; set;} // string value -> 'true' or 'false'
        Webservice string newValue {get; set;} // string value -> 'true' or 'false'
    }


 // Contact WRAPPER CLASS
    global class ContactRecordDetails{
        Webservice String contactId {get; set;}
        Webservice String subscriptionGroupID {get; set;} // can be null for SC House accounts; should never receive a email
        Webservice String privacyPolicyAck {get; set;} // can be null - which is equal to false
        Webservice String contactName {get;set;}
        Webservice string lastName {get; set;} // last name is mandatory
        Webservice string firstName {get; set;}
        Webservice string investorType {get; set;} // can be null, not critical to manage subscription; critical in community management
        Webservice string errorMsg {get;set;} // blank for now until we find business reason  
        Webservice String hasOptedOut {get; set;} //True => 'true', False =>  'false', Null => 'null'
        Webservice String emailId {get; set;} // can be null if sales person deletes; 'errorMsg' = 'SF_Error : No Email' & send internal email alert
    }
}