/***
*   This class is for updating the contact subscription
This is new version of DynamicEligibility apex class
**/
public with sharing class SubscriptionEligibility {
    
    // this is used if any exception comes in
    public static SendEmailNotification se = new SendEmailNotification('Error: Subscription Eligibility Class Failed');
    
    // called from the button 
    @AuraEnabled 
    public static void updateSubsGrp(string conId){
        try{
            Contact conRec = [select id,
                              accountId, 
                              OwnerId,
                              Auto_Subscription_Group__c,
                              MailingCountry,                          
                              Channel_NEW__c,
                              DB_Source__c 
                              FROM contact where id =: conId];
            handleEligibility(new List<contact>{conRec}, null);
        }
        catch(Exception ex){
            se.sendEmail('Exception Occurred in updateSubsGrp method : \n'+ex.getMessage());
        }
    }
    
    // this method will call other method to get the appropriate subscription for the contact passed
    public static void handleEligibility(Contact[] newCon, string objectName){      
        
        // this will save the contact id and the subscription group id which will be updated on the contact
        Map<Id, Id> conSubGrpMap = new Map<Id, Id>();
        
        // if contact is passed
        if(!newCon.isEmpty()){
            
            // calling method to get subscription group
            conSubGrpMap = getEligibility(newCon);
            
            // update contact
            List<Contact> lstcon = new List<Contact>();
            for(id conId : conSubGrpMap.keySet()){
                Contact tempCon = new Contact(id = conId, Subscription_Group__c = conSubGrpMap.get(conId));
                lstcon.add(tempCon);
            }
            system.debug('\n--lstcon--'+lstcon);
            if(!lstcon.isEmpty()){
                update lstcon;
            }
        }
    }
    
    //For all the contacts- determine the eligibility.
    public static Map<Id,Id> getEligibility(List<Contact> con){
        
        // this will be used save contact and subscription
        Map<Id, Id> conSubGrpMap = new Map<Id, Id>();
        
        try{
        
            Map<Id, Account> accMap = new Map<Id, Account>();       
            List<Subscription_Group__c> subGrpList = new List<Subscription_Group__c>();        
            String conProfileName = ''; 
            String conProfileId = ''; 
            Set<Id> profileIds = new Set<Id>();
            Map<Id, User> userMap = new Map<Id, User>();        
               
            // getting unassigned Subscription grp 
            String unAssignedGroup = [SELECT Id,
                                      Name 
                                      FROM Subscription_Group__c 
                                      WHERE Name = 'Unassigned Eligibility'].Id;
            
            // saving profile id of the contact owner
            for(Contact c : con){            
                profileIds.add(c.OwnerId);         
            }
            
            // getting profile details
            userMap.putAll([select id, ProfileId, profile.Name from User where id in: profileIds]);
            System.debug('\n--userMap--'+userMap);
            
            // getting all subscription group which are active and Enable = true
            subGrpList = [Select Name,                       
                          Firm_Branch__c,                     
                          DB_Source__c,
                          Profile_Names__c,
                          Channel_Filters__c,
                          Country_Filter__c
                          FROM Subscription_Group__c 
                          WHERE IsActive__c = true 
                          AND Enable__c = true];    
            System.debug('\n--Contacts--' + con+'\n---subGrpList--'+subGrpList.size());
            
            // for each contact passed
            for(Contact c : con){    
                
                // getting user details
                User u = userMap.get(c.OwnerId);
                
                // getting contact owner profile
                if(u != null){
                    conProfileName = u.Profile.Name;
                    conProfileId = u.Profile.Id;
                }
                
                // flag which is used to set if subscription is found
                boolean isSubsFound = false;
                boolean firmFilterFound = false;
                boolean dbSource = false;
                boolean profileOnwer = false;  
                boolean channelFilter = false;         
                
                // for each subscription
                for(Subscription_Group__c sg : subGrpList){
                    system.debug('\n--sg--'+sg);             
                    
                    /************* Firm Filter Name Code  *************/                
                    if(sg.Firm_Branch__c != null
                       && c.accountId == sg.Firm_Branch__c
                       && c.MailingCountry == sg.Country_Filter__c){                                   
                           system.debug('\n--acc--'+c.accountId+'\n--Firm Branch--'+sg.Firm_Branch__c);
                           conSubGrpMap.put(c.Id, sg.Id); 
                           isSubsFound = true;
                           firmFilterFound = true; 
                           Continue;                  
                       }
                    
                    /************* DB_Source Code  *************/
                    if(!string.isBlank(c.DB_Source__c) && !string.isBlank(sg.DB_Source__c) && !firmFilterFound && (sg.DB_Source__c == c.DB_Source__c || (c.DB_Source__c.contains(sg.DB_Source__c)))){
                        system.debug('\n--DB-Source--');
                        conSubGrpMap.put(c.Id, sg.Id);
                        isSubsFound = true;
                        dbSource = true;                      
                        Continue;
                    }
                    
                    /************* Contact Owner/Profile Code  *************/
                    if(sg.Profile_Names__c != null && !firmFilterFound && !dbSource && conProfileName == sg.Profile_Names__c){ 
                        system.debug('\n--ProfileName--');
                        conSubGrpMap.put(c.Id, sg.Id); 
                        isSubsFound = true;                         
                        profileOnwer = true;                   
                        Continue;                                        
                    }
                    
                    /*********** Channel Filter / Country Code  *************/                             
                    system.debug('\n--sg country--'+sg.Country_Filter__c+'\n--country--'+c.MailingCountry+'\n--chan--'+(c.Channel_NEW__c == sg.Channel_Filters__c));
                    if(!string.isBlank(sg.Country_Filter__c) && !profileOnwer && !firmFilterFound && !dbSource && c.MailingCountry != null && c.MailingCountry.contains(sg.Country_Filter__c) && !string.isBlank(sg.Channel_Filters__c) && c.Channel_NEW__c == sg.Channel_Filters__c){      
                        system.debug('\n--sg.Country_Filter__c-Channel-'+sg.Country_Filter__c);
                        conSubGrpMap.put(c.Id, sg.Id);
                        isSubsFound = true;
                        channelFilter = true;
                        Continue;
                    }  
                    
                    /*** matching Channel ***/               
                    if(c.Channel_NEW__c != null && string.isBlank(sg.Country_Filter__c) && sg.Channel_Filters__c != null && !channelFilter && !profileOnwer && !firmFilterFound && !dbSource && c.Channel_NEW__c != null && sg.Channel_Filters__c != null && c.Channel_NEW__c == sg.Channel_Filters__c){
                        system.debug('\n--Channel Filter--');
                        conSubGrpMap.put(c.Id, sg.Id); 
                        isSubsFound = true;
                        Continue;
                    }
                    /*********** Channel Filter Code  *************/
                    
                } // end of Subscription grp
                
                // un assigned Subscription
                if(!isSubsFound){
                    conSubGrpMap.put(c.Id, unAssignedGroup);
                }
                
            } // end of Contact for loop
            system.debug('\n--conSubGrpMap--'+conSubGrpMap);
            if(conSubGrpMap.size() > 0)
                return conSubGrpMap;
            else
                return null;
        }
        catch(Exception ex){
            
            // code to sent the email when exception occurs
            se.sendEmail('Exception Occurred in getEligibility method : \n'+ex.getMessage());
        }
        return conSubGrpMap;
    }
    
}