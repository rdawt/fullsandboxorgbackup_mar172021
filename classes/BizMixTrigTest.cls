/**
      Author: Shankar Venkateswaran
      Date Created: 03/21/2018
      Description: Test Class for Contact Trigger that manages all the sub triggers on Contact and Biz Mix
 */
@isTest
private class BizMixTrigTest {

    static testMethod void testBizMixTrigger() {
      // To check if the No. of contact field in BizMix is updated, inserted and deleted
      
      //ContactTriggerHandler cth = new ContactTriggerHandler();
//      DynamicEligibility de = new DynamicEligibility();
        DMLException e = null;
    Set<ID> accIds = new Set<ID>();
    List<Contact> conlist = new List<Contact>();
    
    Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    
    Account acc = new Account(Name = 'qwerpoiuadsf', BillingCountry='United States');
         insert acc;
    Account acc1 = new Account(Name = 'lkjhgfda 987896', BillingCountry='United States');
         insert acc1;
        Contact con = new Contact(LastName='xyz',FirstName='test',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States');
        insert con;
        Contact con1 = new Contact(LastName='xyz11',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz', MailingCountry='United States');
        insert con1;
        //Contact con2 = new Contact(LastName='xyz21',FirstName='test2',AccountId=acc1.Id,MailingStreet='xyz1', MailingCountry='United States');
        //insert con2;
        //Contact con3 = new Contact(LastName='xyz1',FirstName='test1',AccountId=acc.Id,MailingStreet='yyy', MailingCountry='United States',
        //  ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC');
        //insert con3;
        //Contact con4 = new Contact(LastName='xyz41',AccountId=acc.Id, Email='test@abc.com', MailingCountry='United States');
       // insert con4;
        //accIds.add(con.AccountId);
        accIds.add(con1.AccountId);
        //accIds.add(con2.AccountId);
      Business_Mix__c bz = new Business_Mix__c(Business_Mix__c='MF', Comments__c='test1',Contact__c=con.id);
        insert bz;
        Business_Mix__c bz1 = new Business_Mix__c(Business_Mix__c='ETF', Comments__c='test2',Contact__c=con1.id);
        insert bz1;
        Business_Mix__c bz2 = new Business_Mix__c(Business_Mix__c='SMA', Comments__c='test3',Contact__c=con1.id);
        insert bz2;
        bz.Comments__c = 'test1totest1again';
        bz.Business_Mix__c = 'ETF';
        update bz;
        bz1.Comments__c = 'test1totest1again';
    bz1.Business_Mix__c = 'MF';
        update bz1;
       // delete con3;
        conlist.add(con);
      //  conlist.add(con1);
     //   conlist.add(con2);
      //conlist.add(con3);
       Business_Mix__c bz3 = new Business_Mix__c(Business_Mix__c='401k', Comments__c='test3',Contact__c=con1.id);
        insert bz3;
        bz3.Comments__c = 'test3tobz3totest1again';
        update bz3;
        delete bz3;
        //ContactTriggerHandler.updateContactCounts(accIds);
        //cth.contactRemoveLineBreak(conlist);
        //cth.sequoiaCalcualtions(conlist);
//       //   de.updateEligibility(conList);
        System.assert(e==null);
    }
}