<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Cvent_Event_OwnerId</description>
    <label>Cvent_Event_OwnerId</label>
    <protected>false</protected>
    <values>
        <field>CVENT__Field__c</field>
        <value xsi:type="xsd:string">OwnerId</value>
    </values>
    <values>
        <field>CVENT__Object__c</field>
        <value xsi:type="xsd:string">Cvent_Event__c</value>
    </values>
    <values>
        <field>CVENT__isTest__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
