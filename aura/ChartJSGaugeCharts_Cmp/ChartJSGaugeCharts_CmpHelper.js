({
    setTableColumns : function(component,event,helper){
        if(component.get('v.contentType') == 'main'){
            component.set('v.columns', [
                {label: 'Title', fieldName: 'title', type: 'text', initialWidth: 175},
                {label: 'Portfolio Name', fieldName: 'portfolioNameFull', type: 'text' , initialWidth: 300},
                {label: 'Fund Ticker', fieldName: 'portfolio', type: 'text' , initialWidth: 125},
                {label: 'Channel', fieldName: 'channel', type: 'text'},
                {label: 'Goal Amount', fieldName: 'goalAmount', type: 'currency', typeAttributes: { currencyCode: 'USD'}  , initialWidth: 160},
                {label: 'Current Net Sales', fieldName: 'netSalesCalculated', type: 'currency', typeAttributes: { currencyCode: 'USD'},cellAttributes: { class: { fieldName: 'colourClass' }} , initialWidth: 160},
                {label: 'Drill Down', type: 'button',  typeAttributes:
                    { label: { fieldName: 'drillDownLabel'}, title: 'Click to drill down', name: 'drill_down', iconName: 'utility:search', class: { fieldName: 'drillDownVisibility' }},cellAttributes: { alignment: 'center' }},
            ]);
        }else if(component.get('v.contentType') == 'sub'){
            if(component.get('v.isOrgType')){
                component.set('v.columns', [
                    {label: 'Title', fieldName: 'title', type: 'text'},
                    {label: 'Portfolio Name', fieldName: 'portfolioNameFull', type: 'text'},
                    {label: 'Fund Ticker', fieldName: 'portfolio', type: 'text'},
                    {label: 'Channel', fieldName: 'channel', type: 'text'},
                    {label: 'Organization Type', fieldName: 'organizationType', type: 'text'},
                    {label: 'Goal Amount', fieldName: 'goalAmount', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Current Net Sales', fieldName: 'netSalesCalculated', type: 'currency', typeAttributes: { currencyCode: 'USD'},cellAttributes: { class: { fieldName: 'colourClass' }}}
                ]);
            }else if(component.get('v.isTerritory')){
                component.set('v.columns', [
                    {label: 'Title', fieldName: 'title', type: 'text'},
                    {label: 'Portfolio Name', fieldName: 'portfolioNameFull', type: 'text'},
                    {label: 'Fund Ticker', fieldName: 'portfolio', type: 'text'},
                    {label: 'Channel', fieldName: 'channel', type: 'text'},
                    {label: 'Territory', fieldName: 'territoryName', type: 'text'},
                    {label: 'Goal Amount', fieldName: 'goalAmount', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Current Net Sales', fieldName: 'netSalesCalculated', type: 'currency', typeAttributes: { currencyCode: 'USD'},cellAttributes: { class: { fieldName: 'colourClass' }}}
                ]);
            }
        }
    },
   
    getChartData : function(component,event,helper) {
        var portfolio = component.get('v.portfolio');
        // if($A.util.isEmpty(portfolio)){
        //     helper.showToast('Error', 'Can not find portfolio for this chart.','error');
        //     component.set("v.showSpinner",false);
        //     return;
        // }
        var action = component.get('c.getUSSalesGoalsByPortfolio');
        action.setParams({
            type : component.get('v.type'),
            portfolio :  component.get('v.portfolio'),
            channel :  component.get('v.channel'),
            isOrgType :  component.get('v.isOrgType'),
            isTerritory : component.get('v.isTerritory'),
            isForceCalculate : false,
            allocationETF : false,
            year : component.get('v.selectedYear')
		  });

        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.charts",response.getReturnValue());
                if(component.get('v.portfolio') == 'EME-IIG'){
                    helper.calculateHiddenMFData(component,'EME');
                    helper.calculateHiddenMFData(component,'IIG');
                }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);

    },

    handleShowModal: function (component, evt, helper, chartId) {
        debugger;
        var modalBody;
        var res = chartId.split("__");
        var portfolio = res[0];
        var channel = res[1];
        var isOrgType = false;
        var isTerritory = false;
        var header = '';
        //isTerritory = true;
        //header = 'Territory Details - ';
        // if(channel == 'FINANCIAL ADVISOR'){
        //     channel = 'Financial Advisor';
        // }
        if(channel == 'Financial Advisor' || channel == 'RIA' || channel == 'FINANCIAL ADVISOR'){
            isTerritory = true;
            header = 'Territory Details - ';
        }
        else{
            isOrgType = true;
            header = 'Organization Type Detail -';
        }
        $A.createComponent("c:ChartJSGaugeCharts_Cmp", { "portfolio": portfolio ,"channel" : channel ,"isOrgType": isOrgType , "isTerritory" : isTerritory , "selectedContent" : component.get('v.selectedContent') , "contentType" : 'sub',"selectedYear" : component.get('v.selectedYear'),"showButtons":component.get('v.showButtons')},
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       header: header+portfolio, 
                       body: modalBody,
                       showCloseButton: true,
                       cssClass: "slds-modal_medium",
                       closeCallback: function() {
                       }
                   })
               }
           });
    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },

    calculateHiddenMFData : function(component , portfolio){
        var action = component.get('c.getUSSalesGoalsByPortfolio');
        action.setParams({
            type : component.get('v.type'),
            portfolio :  portfolio,
            channel :  '',
            isOrgType :  false,
            isTerritory : false,
            isForceCalculate : false,
            allocationETF : false,
            year : component.get('v.selectedYear')
		  });

        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                console.log('Completed sccuessfully');
                //component.set("v.charts",response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                //helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            //component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    setVisualType : function(component,event,helper,type) {
        
        var action = component.get('c.setDataVisualType');
        action.setParams({
            type : type
		  });

        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                console.log('Completed sccuessfully');
                var appEvent = $A.get("e.c:DBVisualTypeChangeEvent"); 
                appEvent.setParams({"visualType" : type}); 
                appEvent.fire(); 
            } 
        });
        $A.enqueueAction(action);
    },

})