<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TEST_NOT_UCITS_GOLD</fullName>
        <ccEmails>ddossantos@vaneck.com</ccEmails>
        <description>TEST_NOT UCITS GOLD</description>
        <protected>false</protected>
        <recipients>
            <recipient>ddossantos@vaneck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_UCITS_Subscription_Alert</template>
    </alerts>
    <alerts>
        <fullName>UCIT_Gold_Subscription_Alert</fullName>
        <description>UCIT Gold Subscription Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>VEG_UCITS_Gold_Alert</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/New_UCIT_Gold_Subscription_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_CompositeKey_On_Subscription_Member</fullName>
        <description>******* ENHANCE FOR USE WITH LEADS Sets Composite_Key_ExternalId_SId_CId_OR_LId</description>
        <field>Composite_Key_ExternalId_SId_CId_OR_LId__c</field>
        <formula>Subscription__c   &amp;&quot;_&quot;&amp;Contact__c</formula>
        <name>Set CompositeKey On Subscription Member</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Email_Address_On_Subscription_Member</fullName>
        <description>Set Email Address On Subscription Member
*add functionality for lead**</description>
        <field>Email__c</field>
        <formula>Contact__r.Email</formula>
        <name>Set Email Address On Subscription Member</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Subscription Member -Contact Or Lead Assigned</fullName>
        <actions>
            <name>Set_CompositeKey_On_Subscription_Member</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Email_Address_On_Subscription_Member</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>**Enhance to factor in lead options</description>
        <formula>not(isnull( Contact__c )) || not(isnull(  Lead__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WFR-NewUCITGoldAlert</fullName>
        <actions>
            <name>UCIT_Gold_Subscription_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Subscription_Member__c.Subscription_Display_Name__c</field>
            <operation>equals</operation>
            <value>UCITS Gold Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.IsTestContact__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.IS_User__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notContain</operation>
            <value>@vanecktest.com</value>
        </criteriaItems>
        <description>Catches new Subs.Ind.48(UCIT Gold)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WFR-NewUCITS %28NOT GoldAlert%29</fullName>
        <actions>
            <name>TEST_NOT_UCITS_GOLD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Subscription_Member__c.Subscription_Display_Name__c</field>
            <operation>equals</operation>
            <value>UCITS Natural Resources,UCITS Moat,UCITS Emerging Markets Equity,UCITS Emerging Markets Bond</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.IsTestContact__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.IS_User__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notContain</operation>
            <value>@vanecktest.com</value>
        </criteriaItems>
        <description>UCITS Natural Resources,UCITS Moat,UCITS Emerging Markets Equity,UCITS Emerging Markets Bond</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
