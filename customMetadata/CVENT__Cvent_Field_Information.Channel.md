<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Channel</description>
    <label>Channel</label>
    <protected>false</protected>
    <values>
        <field>CVENT__Cvent_Account_Name__c</field>
        <value xsi:type="xsd:string">Van Eck Global</value>
    </values>
    <values>
        <field>CVENT__Cvent_Custom_Field_Id__c</field>
        <value xsi:type="xsd:string">9d54174d-84fe-47f9-81b4-42e79bcf3a39</value>
    </values>
    <values>
        <field>CVENT__Field_Name__c</field>
        <value xsi:type="xsd:string">Channel</value>
    </values>
    <values>
        <field>CVENT__Field_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>CVENT__HTTP_POST_Param__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CVENT__IsCustom__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CVENT__IsRequired__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CVENT__Sql_Data_Type__c</field>
        <value xsi:type="xsd:string">OpenEndedText</value>
    </values>
    <values>
        <field>CVENT__isTest__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
