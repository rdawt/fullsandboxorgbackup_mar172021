trigger activityCount on Task(after insert, after update, after delete) {
   
    set<id> ids= new set<id>();
   //buggy!! datetime currentDate = Date.today();
  //  date currentDate = system.today() + 35; to test the future month date entry in payee detail or summary object
  date currentDate = system.today();
    system.debug('what is !!Today!-1! '+ currentDate );
    set<id> userIds= new set<id>();
    
    if ((trigger.isDelete) && trigger.isAfter) {
      for(task t: trigger.old){
       // ids.addAll(trigger.newMap.accountid);
       //exclude mkto api and nulls
        if (((t.whoid != null) && (t.ownerid <> '005A00000032QAD')) || ((t.whoid != null) && (t.Marketing_Automation__c == false))){
            system.debug('inside delete trigger condition - contact id'+t.whoid);
              
            ids.add(t.whoid);
            userIds.add(t.createdbyid);
        }

     } 

    }
    else{
        for(task t: trigger.new){
        //ids.addAll(trigger.newMap.accountid);
         if (((t.whoid != null) && (t.ownerid <> '005A00000032QAD')) || ((t.whoid != null) && (t.Marketing_Automation__c == false))) {
            ids.add(t.whoid);
            userIds.add(t.createdbyid);
        }

      } 
       

    } 
    AggregateResult[] tCounts;  
    if (ids.size() != 0){   
        tCounts  =[select  whoid, count(id) taskCount
                                  from task
                                      where whoid in : ids and Marketing_Automation__c = false 
                                          group by whoid];//and CALENDAR_MONTH(CreatedDate) = 1 //ActivityDate
        system.debug('after agg. count- count is'+ tCounts);

     
     //AggregateResult[] eCounts  =[select  whoid, count(id) eventCount
       //                           from event task
         //                         where
           //                        (whoid in :ids and Marketing_Automation__c = false)
             //                      group by whoid,whoid];//   
                                                          
         list<Contact> conList= new list<Contact>();                             
         for(AggregateResult forLoopCount : tCounts){
           contact parentTaskContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),TaskCounter__c =  integer.valueof(forLoopCount.get('taskCount')));
           conList.add(parentTaskContact);
              
 
        }

    

        update conList;
    }


//this is for user object count

AggregateResult[] taskUsersCount; 
AggregateResult[] taskUsersQCount; 
AggregateResult[] timeTradeUsersCount;
AggregateResult[] glanceUsersCount;

list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();

//Get Current Date


 
    if (userIds.size() != 0){
        taskUsersCount  =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityScore, sum(RIA_Activity_Score__c ) RIAActivityScore
                                  from Task
                                      where createdbyid in : userIds  and  Marketing_Automation__c = false and createddate = THIS_MONTH and createddate = THIS_YEAR
                                           group by createdbyid];//
         
         taskUsersQCount =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityQScore, sum(RIA_Activity_Score__c ) RIAActivityQScore
                                  from Task
                                      where createdbyid in : userIds  and  Marketing_Automation__c = false and createddate = THIS_QUARTER and createddate = THIS_YEAR
                                           group by createdbyid];//
 
        //Timetrade count query
        timeTradeUsersCount = [SELECT count(OwnerId) TimeTradeCount,createdbyid FROM Task where (NOT (Subject like '%Email: Van Eck Global’s Online Privacy Policy')) 
                                    and whoid IN  :ids
                                        and timetrade__c = true and (status = 'Call Completed' or status = 'Call Attempted' or status = 'call left voicemail') 
                                            and activitydate = this_quarter and ownerid in : userIds group by createdbyid];
        
       // if (timeTradeUsersCount.size() == 0){
      //       return;
      //  }
          
         //Glace count query
        glanceUsersCount = [SELECT count(OwnerId) GlanceCount,createdbyid FROM Task where  (NOT (Subject like '%Email: Van Eck Global’s Online Privacy Policy')) and 
                                 whoid IN : Ids
                                     and (status = 'Call Completed' or status = 'Call Attempted' or status ='call left voicemail' or status like '%call%')  and glance__c = true 
                                         and activitydate = this_quarter and ownerid in : userIds group by  createdbyid];
        
        if ((timeTradeUsersCount.size() == 0) && (glanceUsersCount.size() == 0) && (taskUsersCount.size() == 0) && (taskUsersQCount.size() == 0) ){
             return;
        }                               
                                            
         system.debug('**********TASKUserCount!!*******************'+ taskUsersCount.size());
         system.debug('**********TASKUserQCount!!*******************'+ taskUsersQCount.size());
         system.debug('**********timeTradeUsersCount!!*******************'+ timeTradeUsersCount.size());
         system.debug('**********timeTradeUsersCount!!*******************'+ glanceUsersCount.size());
         
         
         //for Q count of Score
         
          Map<Id, PayeeSummary__c> payeeListQScoreCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult scoreQLoopUsersCount  : taskUsersQCount ) //
         {
           
           string payeeId = string.valueof(scoreQLoopUsersCount.get('createdbyid'));
           integer scoreQFACounter = integer.valueof(scoreQLoopUsersCount.get('FAActivityQScore'));
           integer scoreQRIACounter = integer.valueof(scoreQLoopUsersCount.get('RIAActivityQScore'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,TaskFAQScore__c= scoreQFACounter , TaskRIAQScore__c = scoreQRIACounter  );
           payeeListQScoreCountMap.put(payeeId,pSummary);
                                                     
         }
         
         //end Q score 
         Map<Id, PayeeSummary__c> payeeListQTimeTradeCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult timeTradeLoopUsersCount  : timeTradeUsersCount ) //
         {
           
           string payeeId = string.valueof(timeTradeLoopUsersCount.get('createdbyid'));
           integer timeTradeQCounter = integer.valueof(timeTradeLoopUsersCount.get('TimeTradeCount'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,timeTradeQCount__c = timeTradeQCounter );
           payeeListQTimeTradeCountMap.put(payeeId,pSummary);
                                                     
         }
         
         Map<Id, PayeeSummary__c> payeeListQGlanceCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult glanceLoopUsersCount  : glanceUsersCount ) //
         {
           
           string payeeId = string.valueof(glanceLoopUsersCount.get('createdbyid'));
           integer glanceQCounter = integer.valueof(glanceLoopUsersCount.get('GlanceCount'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,GlaceQCount__c= glanceQCounter );
           payeeListQGlanceCountMap.put(payeeId,pSummary);
                                                     
         }
          
        // Map<String,Integer> payeeListMap = new Map<String,Integer>();
         Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
         Integer j =0;
         
         system.debug('what is !!Today!! '+ currentDate );
         system.debug('what is this ***month*** '+ currentDate.Month());
         system.debug('what is this ***calendar month*** '+ (system.today().month()));
         system.debug('what is this ***year***'+ currentDate.Year());
         system.debug('current month after using string of  :'+String.ValueOf(currentDate.Month()));
         system.debug('current year after using string of  :'+String.ValueOf(currentDate.Year()));
         
         
         for(PayeeSummary__c payeeSummary: [Select id, user__c from PayeeSummary__c  Where user__c in : userIds and Month__c = :String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ]){
            modPayeeSummaryList.add(payeeSummary);
            payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
            system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
            //payeeListMap.put(payeeSummary.User__c,j++);
         }
         

          //addPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c !=: String.ValueOf(currentDate.Month()) and Year__c !=: String.ValueOf(currentDate.Year()) ];
          //modPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ];
          
          system.debug('after agg. User-Payee FA Activity Score Sum- Payee List is'+ taskUsersCount  );
        
          //system.debug('after agg. User-Payee RIA Activity Score Sum- Sum is'+ RIAActivityScore);
                            
         list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
         list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
         
         
                                 
         for(AggregateResult forLoopCount : taskUsersCount){
         
            system.debug('entering forloop to chase the matching payee ids'+ taskUsersCount  );

         
           if((payeeListMap != null) && payeeListMap.containsKey(string.valueof(forLoopCount.get('createdbyid'))))
           {
             //get the index
             integer timeTradeCount = 0;
             integer glanceCount = 0;
             integer fAScoreQCount = 0;
             integer rIAScoreQCount = 0;
              
             
              PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('createdbyid')));
              // If (index != null){
                   //do something if Payee  exists in CO for the same month and year already so use update instead of insert in the PayeeSummary CO
                   system.debug('inside else of **MODIFYING** new payee summary for the current month'+ taskUsersCount  );
                 //  payeeListMap.get(createdbyid);
                 
                 if((payeeListQTimeTradeCountMap!= null) && (  payeeListQTimeTradeCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c timeTradeIndex = payeeListQTimeTradeCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   timeTradeCount = integer.valueOf(timeTradeIndex.timeTradeQCount__c );
              
                 }
                 if((payeeListQGlanceCountMap!= null) && (  payeeListQGlanceCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c glanceIndex = payeeListQGlanceCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   glanceCount = integer.valueOf(glanceIndex.GlaceQCount__c );
              
                 }
                  if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                   rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
              
                 }
                   PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof( String.ValueOf(index.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')), TaskFAScore__c =integer.valueof(forLoopCount.get('FAActivityScore')),TaskFAQScore__c = fAScoreQCount  ,TaskRIAScore__c =integer.valueof(forLoopCount.get('RIAActivityScore')),TaskRIAQScore__c = rIAScoreQCount , timeTradeQCount__c = timeTradeCount, GlaceQCount__c =  glanceCount );
                   updatePayeeList.add(payeeSummary);                   
           //    }
            }
            else{
            
                integer timeTradeCount = 0;
                integer glanceCount = 0;
                integer fAScoreQCount = 0;
                integer rIAScoreQCount = 0;
                
                system.debug('inside else of **ADDING** new payee summary for the current month'+ taskUsersCount  );
                
                if((payeeListQTimeTradeCountMap!= null) && (  payeeListQTimeTradeCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c timeTradeIndex = payeeListQTimeTradeCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   timeTradeCount = integer.valueOf(timeTradeIndex.timeTradeQCount__c );
              
                 }
                 if((payeeListQGlanceCountMap!= null) && (  payeeListQGlanceCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c glanceIndex = payeeListQGlanceCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   glanceCount = integer.valueOf(glanceIndex.GlaceQCount__c );
              
                 }
                 if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                   rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
              
                 }
                  
                PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),TaskFAScore__c =integer.valueof(forLoopCount.get('FAActivityScore')),TaskFAQScore__c =fAScoreQCount ,TaskRIAScore__c =integer.valueof(forLoopCount.get('RIAActivityScore')),TaskRIAQScore__c = rIAScoreQCount , timeTradeQCount__c = timeTradeCount,GlaceQCount__c =  glanceCount);
                addPayeeList.add(payeeSummary);
            
            }
         
              

                
        }
            
         
 
        
    
        if (updatePayeeList.size() != 0) {
            update updatePayeeList;
        }
        if (addPayeeList.size() != 0) {
            insert addPayeeList;
        }
        
        //start
        
        
        //end
        
        
        //start
        
        
        //end
        
    }


}