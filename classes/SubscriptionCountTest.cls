/**
      Author: Vidhya Krishnan
      Date Created: 12/09/2011
      Description: Test Class for Subscription Trigger that handles bulk operations
 */
@isTest
private class SubscriptionCountTest {
    static testMethod void testSubscriptionCount() {
        Boolean error = false;
        Subscription__c s1 = new Subscription__c(Name='Test Subscription1', Subscription_Display_Name__c='Test Subscription1', Subscription_Indicator_Marker__c='abc1');
   		insert s1;
        Subscription__c s2 = new Subscription__c(Name='Test Subscription2', Subscription_Display_Name__c='Test Subscription2', Subscription_Indicator_Marker__c='abc2');
   		insert s2;
   		List<Contact> c = new List<Contact>();
   		Account acc = new Account(Name = 'ektron', Channel__c = 'RIA', BillingCountry='US');
   		insert acc;
   		List<Subscription_Member__c> sm = new List<Subscription_Member__c>();
   		Contact con = new Contact(AccountId=acc.Id,LastName='xyz1',FirstName='test',ACT_ACCOUNTID__c='ektron', Email='abc1@xyc.com', MailingCountry='US');
   		insert con;
   		Subscription_Member__c sub1 = new Subscription_Member__c(Email__c = 'abc1@xyc.com', Contact__c = con.ID, Subscribed__c = true, Subscription__c = s1.ID);
   		Subscription_Member__c sub2 = new Subscription_Member__c(Email__c = 'abc2@xyc.com', Contact__c = con.ID, Subscribed__c = true, Subscription__c = s2.ID);
   		sm.add(sub1);
   		sm.add(sub2);
        try{
	    	Test.startTest();
	    	insert sm;
	    	sm[0].Subscribed__c = false;
	    	update sm[0];
	    	delete sm[0];
	    	Test.stopTest();
        }
        catch(System.DMLException e1){
        	error = true;
        }
       	System.assert(error == false);
    }
}