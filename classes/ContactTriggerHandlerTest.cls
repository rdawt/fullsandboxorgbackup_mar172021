/**
      Author: Vidhya Krishnan
      Date Created: 12/08/2011
      Description: Test Class for Contact Trigger that manages all the sub triggers on Contact
        1. To check if the No. of contact field in Account is updated
        2. To check if the line break in Mailing Address is removed before storing in custom field
        3. To check the Sequoia Qualification Field Updates.
        4. To check the Deletion of sub_mem records when contact is deleted.
 */
@isTest
private class ContactTriggerHandlerTest {

    static testMethod void TestContactTriggerHandler() {
        DMLException e=null;
        Set<ID> accIds = new Set<ID>();
        Account acc = new Account(Name = 'dfg54sdfg', BillingCountry='US');
        Account acc1 = new Account(Name = 'asdf5645sadf', Channel__c = 'RIA', BillingCountry='US');
        insert acc;
        insert acc1;
        Subscription__c sub = new Subscription__c(Name='abcd',Subscription_Display_Name__c='ABC', IsActive__c=true);
        insert sub;
        Subscription_Type__c subType = new Subscription_Type__c(Name='abcd');
        insert subType;
        Subscription_Group__c subGrp = new Subscription_Group__c(Name='test',IsActive__c=true);
        insert subGrp;
        Contact con = new Contact(LastName='xyz',FirstName='test',AccountId=acc1.Id,MailingStreet='', MailingCountry='US',
            ETF_PU__c='Yes',QT__c='Yes',COI__c='Yes',RepPM__c='Yes',YoYG__c='Yes',NTA__c='Yes',MVEP__c='Yes',VE_AtB__c='Yes',FB__c='Yes',AUM__c=150,YEB__c='1985',OR__c='N/A');
        insert con;
        Contact con1 = new Contact(LastName='xyz',FirstName='test1',AccountId=acc.Id,MailingStreet='abc\n345\nxyz', MailingCountry='US',
            ETF_PU__c='No',QT__c='No',COI__c='No',RepPM__c='No',YoYG__c='No',NTA__c='No',MVEP__c='No',VE_AtB__c='No',FB__c='No',AUM__c=150,YEB__c='2000',OR__c='ABC');
        insert con1;
        Contact con2 = new Contact(LastName='xyz',FirstName='test1',AccountId=acc.Id,MailingStreet='xyz\n123', MailingCountry='US',
            ETF_PU__c='Yes',QT__c='No',COI__c='Yes',RepPM__c='No',YoYG__c='Yes',NTA__c='No',MVEP__c='Yes',VE_AtB__c='No',FB__c='Yes',AUM__c=10,YEB__c='2010',OR__c='NA');
        insert con2;
        Contact con3 = new Contact(LastName='xyz',FirstName='test1',AccountId=acc.Id,MailingStreet='abc', MailingCountry='US');
        insert con3;
        Subscription_Member__c sm = new Subscription_Member__c(Contact__c = con.Id, Subscribed__c = true, Subscription__c = sub.Id, Email__c = con2.Email);
        insert sm;
        accIds.add(con.AccountId);
        accIds.add(con1.AccountId);
        accIds.add(con2.AccountId);
        accIds.add(con3.AccountId);
        if ( !accIds.isEmpty() ){
            ContactTriggerHandler.updateContactCounts(accIds);
            //ContactTriggerHandler.countContactsAsync(accIds);
        }
        delete con;
        con2.AccountId = acc1.Id;
        con2.RepPM__c = UserInfo.getUserId();
        update con2;

        con2.AccountId = acc.Id;
        con2.Model_Constructor_User__c = 'User';
        con2.Investment_Perspective__c = 'Tactical';
        con2.Outside_Research__c = 'NDR';
        update con2;
        System.assert(e==null);
    }
}