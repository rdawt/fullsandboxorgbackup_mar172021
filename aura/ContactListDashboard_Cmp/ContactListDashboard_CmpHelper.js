({
    getOnLoadRecords : function(component,event) {
        component.set("v.showSpinner",true);
        var action = component.get("c.getOnLoadRecords");        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                component.set("v.currentSelectedContactId",rtnVal.currentSelectedContactId);
                component.set("v.currentSelectedContactName",rtnVal.currentSelectedContactName);            
                component.set("v.lstContact",rtnVal.lstContact);
                component.set("v.hasContactRecords",rtnVal.hasContactRecords);
                component.set("v.lstMarketoActivity",rtnVal.lstMarketoActivity);
                component.set("v.hasMarketoActivityRecords",rtnVal.hasMarketoActivityRecords);
                component.set("v.lstActivity",rtnVal.lstActivity);
                component.set("v.hasActivityRecords",rtnVal.hasActivityRecords);
                component.set("v.OwnersOptions",rtnVal.OwnersOptions);
                component.set("v.SelectedOwnersOption",rtnVal.SelectedOwnersOption.Id);
                component.set("v.FirmTypeOptions",rtnVal.FirmTypeOptions);
                component.set("v.showSpinner",false);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    getMarketoActivityForEachContactHelper : function(component,event) {
        component.set("v.showSpinner",true);
        var contactId= '';
        var selectedContactId = component.get("v.currentSelectedContactId");
        if(!$A.util.isEmpty(event.currentTarget.dataset))
            contactId = event.currentTarget.dataset.myid;
        else
            contactId = selectedContactId;

        var activityDateMin = component.get("v.activityDateMin");
        var activityDateMax = component.get("v.activityDateMax");
        var action = component.get("c.getMarketoActivityForEachContact");
        action.setParams({
            "contactId" : contactId,
            "activityDateMin" : activityDateMin,
            "activityDateMax" : activityDateMax
        });        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();   
            if (state === 'SUCCESS') 
            {               
                component.set("v.currentSelectedContactId",rtnVal.currentSelectedContactId);
                component.set("v.currentSelectedContactName",rtnVal.currentSelectedContactName);  
                component.set("v.lstMarketoActivity",rtnVal.lstMarketoActivity);
                component.set("v.hasMarketoActivityRecords",rtnVal.hasMarketoActivityRecords);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
        
    },
    
    getActivityForEachContactHelper : function(component,event) {
        component.set("v.showSpinner",true);
        var contactId= '';
        var selectedContactId = component.get("v.currentSelectedContactId");
        if(!$A.util.isEmpty(event.currentTarget.dataset))
            contactId = event.currentTarget.dataset.myid;
        else
            contactId = selectedContactId;

        var activityDateMin = component.get("v.activityDateMin");
        var activityDateMax = component.get("v.activityDateMax");
        var action = component.get("c.getActivityForEachContact");
        action.setParams({
            "contactId" : contactId,
            "activityDateMin" : activityDateMin,
            "activityDateMax" : activityDateMax
        });        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();   
            if (state === 'SUCCESS') 
            {               
                component.set("v.currentSelectedContactId",rtnVal.currentSelectedContactId);
                component.set("v.currentSelectedContactName",rtnVal.currentSelectedContactName);  
                component.set("v.lstActivity",rtnVal.lstActivity);
                component.set("v.hasActivityRecords",rtnVal.hasActivityRecords);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
        
    },

    getContactRecordsHelper : function(component,event) {
        component.set("v.showSpinner",true);
        var contactOwner = component.get("v.SelectedOwnersOption");
        var lastHumanContactDateMin = component.get("v.lastHumanContactDateMin");
        var lastHumanContactDateMax = component.get("v.lastHumanContactDateMax");

        var action = component.get("c.getFilterContactRecords");
        action.setParams({
            "contactOwner" : contactOwner,
            "lastHContDateMin" : lastHumanContactDateMin,
            "lastHContDateMax" : lastHumanContactDateMax
        });        
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();   
            if (state === 'SUCCESS') 
            {               
                component.set("v.lstContact",rtnVal.lstContact);
                component.set("v.hasContactRecords",rtnVal.hasContactRecords);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
        
    },

    loadCharts : function(component, event, helper) 
    {
        var action = component.get("c.getOpportunityJSON"); 
        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            //alert(state);
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                //jsonData = dataObj;
                console.log('===='+dataObj);
                component.set("v.data",dataObj);
                helper.piechart(component,event,helper);
                helper.Linechart(component,event,helper);
                helper.donutchart(component,event,helper);
                helper.Columnchart(component,event,helper);
            } 
        });
        $A.enqueueAction(action);
    },
    piechart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);
        
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find("chart").getElement(),
                type: 'pie'
            },
            title: {
                text: component.get("v.chartTitle")+' (Pie Chart)'
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: 
                {
                    text: component.get("v.yAxisParameter")
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name:'StageName',
                data:dataObj
            }]
            
        });
        
    },

    Columnchart : function(component,event,helper) {
        var jsonData = '';
        var dataObj = [];
        dataObj[0] = {
            name: 'MTD 2016',
            data: [15,25,35]
        };
        dataObj[1] = {
            name: 'Prior Year',
            data: [10,35,45]
        };
        dataObj[2] = {
            name: 'Next Year',
            data: [15,20,32]
        };
        jsonData = JSON.stringify(dataObj);
        //var dataObj = JSON.parse(jsonData);
        console.log('jsonData===',jsonData);
        console.log('dataObj===',dataObj);
        new Highcharts.Chart({
            chart: {
                renderTo: component.find("columnchart").getElement(),
                type: component.get("v.chartType")
            },
            title: {
                text: component.get("v.chartTitle")
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: 
                {
                    text: component.get("v.yAxisParameter")
                }
            },
            tooltip: 
            {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: dataObj
        });
        
    },
    
    Linechart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);
        
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find("linechart").getElement(),
                type: 'line'
            },
            title: {
                text: component.get("v.chartTitle")+' (Line Chart)'
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: 
                {
                    text: component.get("v.yAxisParameter")
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name:'StageName',
                data:dataObj
            }]
            
        });
        
    },
    donutchart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);
        
        new Highcharts.Chart({
            chart: {
                renderTo: component.find("donutchart").getElement(),
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: component.get("v.chartTitle")+' (Donut Chart)'
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45
                }
            },
            series: [{
                type: 'pie',
                name:'StageName',
                data:dataObj
            }]
            
        });
        
    }
})