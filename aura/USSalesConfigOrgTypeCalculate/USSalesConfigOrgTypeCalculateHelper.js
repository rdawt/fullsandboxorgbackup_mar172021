({
    getHistoryAndAssetDate : function(component, event, helper) {
        var action = component.get('c.getOrgTypeConfig');
        action.setParams({
            fundType :  component.get('v.selectedFund')
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                component.set('v.assetDate',result.assetDate);
                component.set('v.data',result.historyDate);
                // component.set("v.fundOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
        helper.setDataTableHeaders(component);
    },

    calculateOrgTypeNetFlow : function(component, event, helper,fundType) {
        var action = component.get('c.calculateOrgTypeNetFlowFF'); 
        var dateFrom =  '';
        var dateTo =  '';
        if(component.get('v.selectedFund') == 'Mutual Fund'){
            dateFrom = component.find("fromDate").get("v.value");
            dateTo = component.find("toDate").get("v.value");
            // if(dateFrom.substring(0,7) != dateTo.substring(0,7)){
            //     helper.showToast('Error', 'You can not calculate more than one month.','error');
            //     component.set('v.showSpinner',false);
            //     return;
            // }
        }
        action.setParams({
            fundType :  component.get('v.selectedFund'),
            channel :  component.get('v.selectedChannel'),
            fromDate :  dateFrom,
            toDate :  dateTo,
            parentFirmId : component.get('v.parentFirmId'),
            firmId :  component.get('v.firmId')
		  });
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            if (response.getState() === 'SUCCESS') {
                if(response.getReturnValue()){
                    helper.showToast('Success', 'Operation has been completed successfully','success');
                    helper.getHistoryAndAssetDate(component, event, helper);
                }else if(!response.getReturnValue()){
                    helper.showToast('Warning', component.get('v.selectedFund') +' net amount already calculated for today.','warning');
                }
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while processing data.','error');
            }
        });
        $A.enqueueAction(action);
    },

    getFirmName : function(component, event, helper) {
        var action = component.get('c.getFirmNames'); 
        var dateFrom = component.find("fromDate").get("v.value");
        var dateTo =  component.find("toDate").get("v.value");
        action.setParams({
            channel :  component.get('v.selectedChannel'),
            dateFrom :  dateFrom,
            dateTo :  dateTo
		  });
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            if (response.getState() === 'SUCCESS') {
               var result = response.getReturnValue();
               component.set('v.firmOptions',result.firm);
               component.set('v.parentFirmOptions',result.parentFirmName);
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while processing data.','error');
            }
        });
        $A.enqueueAction(action);
    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },

    setDataTableHeaders : function(component)
    {
        var actions = [
            { label: 'Show details', name: 'show_details' }
        ],
        fetchData = {
            postingDate : 'postingDate',
            fromPostingDate: 'fromPostingDate',
            toPostingDate : 'toPostingDate'
        };

        component.set('v.columns', [
            {label: 'Record Count', fieldName: 'recordCount', type: 'number' ,cellAttributes: { alignment: 'left' }},
            {label: 'Channel', fieldName: 'channel', type: 'text'},
            {label: 'Posting Date (As Of)', fieldName: 'postingDate', type: 'date-local'}, 
            {label: 'Created Date', fieldName: 'createdDate', type: 'date-local'},
            {label: 'Posting Date From', fieldName: 'fromPostingDate', type: 'date-local'},
            {label: 'Posting Date To', fieldName: 'toPostingDate', type: 'date-local'},
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);   

        component.set('v.columnsEtf', [
            {label: 'Record Count', fieldName: 'recordCount', type: 'number' ,cellAttributes: { alignment: 'left' }},
            {label: 'Channel', fieldName: 'channel', type: 'text'},
            {label: 'Asset Date', fieldName: 'postingDate', type: 'date-local'}, 
            {label: 'Created Date', fieldName: 'createdDate', type: 'date-local'},
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);   

        component.set('v.configColumns', [
            {label: 'Fund Vehicle Type', fieldName: 'Fund_Vehicle_Type__c', type: 'text'},
            {label: 'Fund', fieldName: 'Fund_Ticker_Symbol__c', type: 'text' ,sortable: true},
            {label: 'Channel', fieldName: 'Channel__c', type: 'text'},
            {label: 'Organization Type', fieldName: 'Organization_Type__c', type: 'text'},
            {label: 'Transaction Type ', fieldName: 'Portfolio__c', type: 'text'},
            {label: 'NetSales Calculated', fieldName: 'NetSales_Calculated__c', type: 'currency' , typeAttributes: { currencyCode: 'USD'}, sortable: true},
            {label: 'Drill Down', type: 'button', initialWidth: 200, typeAttributes: { label: { fieldName: 'actionLabel'}, name: 'drill_down', title: 'Click to View Drill Down Details'},cellAttributes: { alignment: 'center' }},
        ]);  

        component.set('v.configColumnsEtf', [
            // {label: 'Fund Vehicle Type', fieldName: 'Fund_Vehicle_Type__c', type: 'text'},
            {label: 'Fund', fieldName: 'Fund_Ticker_Symbol__c', type: 'text' ,sortable: true},
            // {label: 'Channel', fieldName: 'ETF_Channel__c', type: 'text'},
            // {label: 'Asset Date', fieldName: 'PostingDate_AssetDate__c', type: 'date-local'},
            {label: 'ETF AUM', fieldName: 'Fund_Asset_Balance__c', type: 'currency' , typeAttributes: { currencyCode: 'USD'}, sortable: true},
            {label: 'ETF Net Flow : YTD', fieldName: 'NetSales_Calculated__c', type: 'currency' , typeAttributes: { currencyCode: 'USD'}, sortable: true},
            {label: 'ETF Net Flow : Month', fieldName: 'Net_Flows_Last_Month__c', type: 'currency' , typeAttributes: { currencyCode: 'USD'}, sortable: true},
            {label: 'Drill Down', type: 'button', initialWidth: 100, typeAttributes: { label: 'Firm', name: 'firm_drill_down', title: 'Click to View Firm Level Drill Down Details'},cellAttributes: { alignment: 'center' }},
            {label: 'Drill Down', type: 'button', initialWidth: 100, typeAttributes: { label: 'Territory', name: 'territory_drill_down', title: 'Click to View Territory Level Drill Down Details'},cellAttributes: { alignment: 'center' }},
        ]);  
    },

    showRowDetails : function(component,row) {
        component.set('v.disableInputs',true);
        var helper = this;
        component.set('v.selectedChannelBackUp', component.get('v.selectedChannel'));
        component.set('v.assetDateBackUp', component.get('v.assetDate'));
        component.set('v.dateFromBackUp', component.get('v.dateFrom'));
        component.set('v.dateToBackUp', component.get('v.dateTo'));
        component.set('v.selectedChannel', row.channel);
        component.set('v.assetDate', row.postingDate);
        component.set('v.dateFrom', row.fromPostingDate);
        component.set('v.dateTo', row.toPostingDate);
        component.set("v.showSpinner",true);

        component.set('v.row',row);
        var action = component.get('c.getConfigRecordForDrillDown');
        action.setParams({
            fundType :  component.get('v.selectedFund'),
            channel : row.channel,
            postingDate : row.postingDate,
            fromDate : row.fromPostingDate,
            toDate : row.toPostingDate
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.dataTableType','Details');
                component.set('v.subHeader','Details for selected history record :');
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if(row.Organization_Type_Drill_Down_Count__c > 0){
                        row.actionLabel = 'View Drill Down Details';
                    }else{
                        row.actionLabel = 'Drill Down';
                    }
                }


                component.set('v.configData',rows);
                // console.log(result.assetDate);
                // console.log(result.historyDate);
                // component.set("v.fundOptions", response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    showDrillDownDetails : function(component,row,helper,drillDownType) {
        debugger;
        var modalBody;
        var action = component.get('c.getDrillDownRecords');
        action.setParams({
            configId :  row.Id,
            drillDownType : drillDownType
		  });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                console.log(response.getReturnValue());
                //var data = response.getReturnValue();
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Firm_Branch_Lookup__c){
                        row.AccountName = row.Firm_Branch_Lookup__r.Name;
                        row.AccountOwner = row.Firm_Branch_Lookup__r.Owner.Name;
                        row.linkName = '/'+row.Firm_Branch_Lookup__c;
                        row.fundTicker =  ($A.util.isEmpty(row.Fund_Lookup__c) ? '' : row.Fund_Lookup__r.Fund_Ticker_Symbol__c );
                        row.transactionLink = '/'+row.SFDC_Transaction_Id__c;
                        row.broadridgeFirmName = row.Broadridge_Firm_Name__c;
                        row.broadridgeFirmName = row.Broadridge_Firm_Name__c;
                        row.etfFlowsLink = '/'+row.ETFFlowsByFundWithFlowDateLookup__c;
                        row.firmChannel = row.Firm_Branch_Lookup__r.Channel__c;
                    } 
                }
            // cmp.set('v.mydata', rows);
                $A.createComponent("c:USSalesConfigOrgTypeDrillDown", { "data": rows, "fundType" : component.get('v.selectedFund'), "drillDownType" : drillDownType },
                function(content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        component.find('overlayLib').showCustomModal({
                            header: drillDownType +' level drill down', 
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "slds-modal_large",
                            closeCallback: function() {
                                helper.showRowDetails(component,component.get('v.row'));
                            }
                        })
                    }
                });

            } else if (response.getState() === "ERROR") {
                helper.showToast('Error', 'Error has occurred while loading data.','error');
            }
            component.set("v.showSpinner",false);
        });
        $A.enqueueAction(action);
    },

    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                return primer(x[field]);
            }
            : function(x) {
                return x[field];
            };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    handleSort: function(cmp, event) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var data = cmp.get('v.configData');
        var cloneData = data.slice(0);
        cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.configData', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBy', sortedBy);
    }
})