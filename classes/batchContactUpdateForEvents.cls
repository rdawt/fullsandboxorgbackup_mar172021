global class batchContactUpdateForEvents implements Database.Batchable<sObject>
{
    
    ICMDataFeedCriteria__c ICMDataFeedCriteria = ICMDataFeedCriteria__c.getInstance('00eA0000000q4Ds');
   integer last_N_Days = Integer.Valueof(ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c);
  //  Date lastModDate2  = Date.valueof(Date.Today().addDays(-5));
    DateTime lastModDate2  =Date.Today().addDays(last_N_Days);
    string criteria = 'last_N_days:'+ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
            system.debug('Criteria value from Custom Settings--**************' +criteria);
         
      //   ICMDataFeedCriteria__c ICMDataFeedCriteria = ICMDataFeedCriteria__c.getInstance('00eA0000000q4Ds');
      //   integer last_N_Days = Integer.Valueof(ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c);
      //Date lastModDate2  = Date.valueof(Date.Today().addDays(-5));
         //Date lastModDate2  = Date.valueof(Date.Today().addDays(last_N_Days));
         //lastModDate2 =  Date.valueof(Date.Today().addDays(-5));
        //Set<Id> EventIdsFromEventRelation = new Set<Id>();
        //for (EventWhoRelation EventRel : [Select EventId,RelationId from EventWhoRelation where RelationId in (Select Id from Contact Where Contact_Status__c != 'DELETE' AND MailingCountry = 'United States' )]) //and AccountId ='001A000000i8CAs'
        
       // {
       //     EventIdsFromEventRelation.add(EventRel.EventId);
      //  }
        //Tasks
        //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id IN :TaskIdsFromTaskRelation AND Subject = \'call\' AND Status = \'call completed\' ORDER BY whoid,activitydate desc';
        
        //Events
          //String query = 'SELECT Id,ActivityDate,whoid FROM Event WHERE Id IN :EventIdsFromEventRelation AND Subject = \'meeting\' AND Result__c = \'held\' and whoid != null ORDER BY whoid,activitydate desc';
          
          //Event ID must be passed only once so reactived as of NOV 06 2018 11:30am ET
          String query = 'SELECT Id,ActivityDate,whoid FROM Event WHERE whoid != null  and lastmodifieddate >= ' +criteria +' ORDER BY whoid,activitydate desc'; //and lastmodifieddate = Today
          
          //String query = 'SELECT Id,ActivityDate,whoid FROM Event WHERE Subject = \'meeting\' AND Result__c = \'held\' and whoid != null ORDER BY whoid,activitydate desc';
          //commented on 11/1/18 for large vol test
          //String query = 'Select EventId,RelationId from EventWhoRelation where RelationId in (Select Id from Contact Where Contact_Status__c != \'DELETE\' AND MailingCountry = \'United States\' ) and lastmodifieddate = last_N_days:500';
          
          //use it for large vol test and comment it later
          // String query = 'Select EventId,RelationId from EventWhoRelation where RelationId in (Select Id from Contact Where Contact_Status__c != \'DELETE\' AND (NOT(DB_Source__c LIKE \'%MVIS%\')))';
          
         
         
          //Oct. 17,2018
          
        
                                                
          //String query = 'Select Event.id, Event.activitydate , Event.whoid,(Select id,RelationId,eventid from Event.EventWhoRelations) from event where Subject = \'meeting\' AND Result__c = \'held\' and whoid != null ORDER BY whoid,activitydate desc';
          
    
          
          //Select Event.id, Event.activitydate , Event.whoid,(Select id,RelationId,eventid from Event.EventWhoRelations) from event where
    
            //String query = 'SELECT Id, FirstName,LastName FROM Contact';
            //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact)) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
            //'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact where AccountId =\'001A000000i8CAs\')) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
            system.debug('Batch Query String--**************' +query );
         return Database.getQueryLocator(query);
        }
 
    //global void execute(Database.BatchableContext BC, List<EventWhoRelation> scope)
    global void execute(Database.BatchableContext BC, List<Event> scope)
     {
         //Create a new list of Contacts for updating;
         list<Contact> conList= new list<Contact>();
         list<Contact> conList2= new list<Contact>();
         list<Event> eventTempList= new list<Event>();
         list<ICMFlatExtractForActivity__c> ICMActivityFlatList = new list<ICMFlatExtractForActivity__c> ();
         date loopLastHumanTouchDate;
         string loopWhoId;
         string loopRelationId;
         Contact parentTaskContact;
         Contact parentEvtContact;
         Event tempEvent;
         ICMFlatExtractForActivity__c ICMFlatActivity;
         //added 10/10/18
         string loopEventId;
         //List < sObject > ContIdWithMAxDateList;
         ICMDataFeedCriteria__c ICMDataFeedCriteria = ICMDataFeedCriteria__c.getInstance('00eA0000000q4Ds');
         //integer last_N_Days = Integer.Valueof(ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c);
        //Date lastModDate2  = Date.valueof(Date.Today().addDays(-5));
        // Date lastModDate2  = Date.valueof(Date.Today().addDays(last_N_Days));
         string criteria = 'last_N_days:'+ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c;
         
         try{
         
             for(Event evt : scope)
             //for(EventWhoRelation evt : scope)
             
             {
                 
                 
                //added 10/16/18
                //loopEventId = string.valueof(evt.get('EventId'));
                
                //added on 10/22/2018 if we use EVENT object as loop then this is variable is populated for ID of EVENT- on 11/6/2018 11:232am ET
                
                loopEventId = string.valueof(evt.get('Id'));
                
                //added on 10/22/2018 if we use EVENT object as loop then this is is not available so commented for test on 11/6/2018 11:232am ET
                //loopRelationId = string.valueof(evt.get('RelationId'));
                
                //start new test - 10/18/2018
                //before
               // Set<Id> EventIdsFromEventRelation = new Set<Id>();
               // for (EventWhoRelation EventRel : [Select EventId,RelationId from EventWhoRelation where RelationId in (Select Id from Contact Where Contact_Status__c != 'DELETE' AND MailingCountry = 'United States' )]) //and AccountId ='001A000000i8CAs'
                
              //  {
              //      EventIdsFromEventRelation.add(EventRel.EventId);
              //  }
                  //      parentEvtContact = new contact(Id = '003A000001qL4T3',SourceValue_EventsLastTouch__c = 'Jonathan Gross-GOOD-!!!!!!!!!!!!!!!!!!'); 
                //if(!conList2.contains(parentEvtContact)){
                //    conList2.add(parentEvtContact);
               // }
                //ContIdWithMAXDateList.Clear();
             
                //List < sObject > ContIdWithMAxDateList = [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid From EventWhoRelation Where Event.Subject = 'meeting' AND Event.Result__c = 'held' and Event.whoid != null and eventid = :loopEventId ORDER BY event.whoid,event.activitydate desc limit 1];  
                //remove below line!!!!!!!!!!!!!!!!
                List<EventWhoRelation> ContIdWithMAxDateList;
               //**re uncomment after test--commented temp 10/29** List<EventWhoRelation> ContIdWithMAxDateList = [Select Id, Event.ID,FORMAT(Event.ActivityDate),Event.whoid,relationid,eventid From EventWhoRelation Where Event.Subject = 'meeting' AND Event.Result__c = 'held' and Event.whoid != null and relationid = :loopRelationId ORDER BY event.whoid,event.activitydate desc limit 1]; 
                //List<EventWhoRelation> ContIdWithMAxDateList = [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid From EventWhoRelation Where Event.Subject = 'meeting' AND Event.Result__c = 'held' and Event.whoid != null and relationid = :loopRelationId ORDER BY event.whoid,event.activitydate desc ]; 
                //10/26/18 add for catching icm flattend activities
                //List<EventWhoRelation> TempICMActivityFlatList =  [Select Event.ID,FORMAT(Event.ActivityDate),FORMAT(Event.ActivityDateTime),Event.Subject,Event.Summary_Recap__c,relationid,Event.result__c,Event.OwnerId,Event.CreatedById,FORMAT(Event.CreatedDate),Event.LastModifiedById,FORMAT(Event.LastModifiedDate),Event.ETF_Team__c From EventWhoRelation Where Event.whoid != null and relationid = :loopRelationId and lastmodifieddate = last_N_days:500 and event.lastmodifieddate = last_N_days:500 ORDER BY event.whoid,event.activitydate desc ];
               
               // DateTime lastModDateTime = DateTime.valueof(lastModDate);
               // ICMDataFeedCriteria__c ICMDataFeedCriteria = ICMDataFeedCriteria__c.getInstance('00eA0000000q4Ds');
               // DateTime lastModDateTime = DateTime.valueof(ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c);
               // string lastModDateTime = string.valueof(ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c);
                List<EventWhoRelation> TempICMActivityFlatList =  [Select Event.FA_Activity_Score__c,Event.RIA_Activity_Score__c,Event.WhoId,Event.ID,FORMAT(Event.ActivityDate),FORMAT(Event.ActivityDateTime),Event.Subject,Event.Summary_Recap__c,relationid,Event.result__c,Event.OwnerId,Event.CreatedById,FORMAT(Event.CreatedDate),Event.LastModifiedById,FORMAT(Event.LastModifiedDate),Event.ETF_Team__c From EventWhoRelation Where Event.whoid != null and eventid = :loopeventId   ORDER BY event.whoid,event.activitydate desc];
              //SELECT CreatedById,CreatedDate,LastModifiedById,LastModifiedDate,Subject FROM Event
              // list <ICMFlatExtractForActivity__c> groupedResults = [SELECT Id FROM ICMFlatExtractForActivity__c where ActivityEventID__c = :loopeventid limit 1 ];
             
                  If (TempICMActivityFlatList != null ) {
                      system.debug('TempICMActivityFlatList ListValues ------**************' +TempICMActivityFlatList  );
                    //Iterate over the account with.
                        for(EventWhoRelation ewr : TempICMActivityFlatList ){
                          loopRelationId = ewr.relationid;
                        //For each account object, get the child records using tests__r and iterate over each child records.
                       // for(eventwhorelation rwr: e.events__r){
                      //add job runn date time and lastmodifiedby and lastmodifieddate values from event or task
                            System.debug('**inside-icmparsing**-The event id is:::::' + ewr.eventid);
                             System.debug('**inside-icmparsing**The eventwhorelation=>relationid is:::::' + ewr.relationid);
                            System.debug('**inside-icmparsing**The Contact ID from Child-EWR is:::::' + ewr.relationid);
                           System.debug('**inside-icmparsing**The Parent=>activity date is:::::' + ewr.event.activitydate);
                            //TempEvent = new Event(Id = ewr.eventid);
                           
                          //  if ((groupedResults.size() <= 0) || (groupedResults == null)) {
                                 ICMFlatActivity = new ICMFlatExtractForActivity__c(FA_Activity_Score__c=String.valueOf(ewr.Event.FA_Activity_Score__c),RIA_Activity_Score__c=String.valueOf(ewr.Event.RIA_Activity_Score__c),WhoId__c=ewr.Event.WhoId,ICMActivityContact__c = ewr.relationid,Name = string.valueof(date.Today().format()),ActivityDate__c=ewr.event.activitydate,ActivityDateTime__c=ewr.event.activitydatetime,ActivitySubject__c=ewr.event.Subject,ActivitySummaryRecap__c=ewr.event.Summary_Recap__c,ActivityAssigned__c=ewr.event.OwnerId,ActivityEventID__c=ewr.eventid,Result__c=ewr.event.result__c, ActivityCreatedById__c = ewr.event.CreatedById,ActivityCreatedDateTime__c = ewr.event.CreatedDate, ActivityLastModifiedById__c = ewr.event.LastModifiedById, ActivityLastModifiedDate__c = ewr.event.LastModifiedDate, ActivityETFTeam__c = ewr.event.ETF_Team__c, ICMExportJobLastRunDateTime__c = datetime.NOW() ); //,ActivityRepAsPM__c= ,ActivityCallResult__c = ewr. ); //date.today().format())
                                // system.debug('**inside-icmparsing-ICMFlatExtractForActivity__c**-flatrow creation in list for add later ------**************' +ICMFlatActivity);
                           //     if(!eventTempList.contains(TempEvent)){
                       //         eventTempList.add(TempEvent);
                                  ICMActivityFlatList.add(ICMFlatActivity);
                                // system.debug('ICMFlatActivity -list values added  ------**************' +ICMActivityFlatList);
                            
                        //   }
                       //     }
                       
                      }
                  }
                  
                
               
                
               // If (ContIdWithMAxDateList.size() != 0 || (ContIdWithMAxDateList.size() != null ))
           //     If (ContIdWithMAxDateList != null )
           //     {    system.debug('ConIdWithMAXDateListValues ------**************' +ContIdWithMAxDateList );
                    //Iterate over the account with.
           //             for(EventWhoRelation ewr : ContIdWithMAxDateList){
                         
                        //For each account object, get the child records using tests__r and iterate over each child records.
                       // for(eventwhorelation rwr: e.events__r){
                         
            //                System.debug('The event id is:::::' + ewr.eventid);
           //                  System.debug('The eventwhorelation=>relationid is:::::' + ewr.relationid);
           //                 System.debug('The Contact ID from Child-EWR is:::::' + ewr.relationid);
           //                 System.debug('The Parent=>activity date is:::::' + ewr.event.activitydate);
            //                parentEvtContact = new contact(Id = ewr.relationid,SourceValue_EventsLastTouch__c = string.valueof(ewr.event.activitydate));
            //                system.debug('parentEvtContact ------**************' +parentEvtContact);
             //               if(!conList2.contains(parentEvtContact)){
             //                   conList2.add(parentEvtContact);
             //                   system.debug('parentEvtContact ------**************' +conList2);
                         
              //             }
                       // }
             //       }

                
                    //SObject obj;
                    //obj = (SObject) ContIdWithMAxDateList.get(0);
                   // parentEvtContact = new contact(Id = ewr.relationid,SourceValue_EventsLastTouch__c = ewr.event.activitydate);
                    //parentEvtContact = new contact(Id = string.valueof(obj.get('relationid')),SourceValue_EventsLastTouch__c = string.valueof(date.valueof(obj.get('ActivityDate'))));
                    //parentEvtContact = new contact(Id = string.valueof(ContIdWithMAxDateList.get('relationid')),SourceValue_EventsLastTouch__c = string.valueof(date.valueof(ContIdWithMAxDateList.get('Event.ActivityDate'))));
                   // system.debug('parentEvtContact ------**************' +parentEvtContact);
                    //parentTaskContact = new contact(Id=string.valueof(tsk.get('whoId')),LastHumanTouchDate_Events__c =  date.valueof(evt.get('ActivityDate')));
                    //if(!conList2.contains(parentEvtContact)){
                     //   conList2.add(parentEvtContact);
                     //   system.debug('parentEvtContact ------**************' +conList2);
                         
                    //   }
                    //break;
          //      }
                        
                    //List < sObject > ContIdWithMAxDateList = Database.query(query);
                   // for(sObject obj : ContIdWithMAxDateList)
                    {
                      // parentEvtContact = new contact(Id = string.valueof(obj.get('relationid')),SourceValue_EventsLastTouch__c = string.valueof(obj.get('Event.ActivityDate'))); //003A000001Sw8pQ
                      // parentEvtContact = new contact(Id = '003A000001Sw8pQ',SourceValue_EventsLastTouch__c = 'John Ryan GOOD-**************'); 
                      // if(!conList2.contains(parentEvtContact)){
                      //   conList2.add(parentEvtContact);
                         
                  //      }
                   }
               
                //end new test - 10/18/2018
                 //first time loop entry-how do we know? since the loopactivitydate variable is blank or null I assume we are starting at the first item of the loop
                 
                 //if ((string.valueof(loopLastHumanTouchDate) == null) || string.valueof(loopLastHumanTouchDate) == '') {
                     //parentTaskContact = new contact(Id=string.valueof(tsk.get('whoId')),LastHumanTouchDate_Events__c =  date.valueof(evt.get('ActivityDate')));
                    // loopLastHumanTouchDate = date.valueof(evt.get('ActivityDate'));
                    // loopWhoId = string.valueof(evt.get('whoId'));
                     //added 10/10/18
                    // loopEventId = string.valueof(evt.get('Id'));
                    
                     
                 //}
                 
                 //2nd time or further loop entry
                // else{
                     //do the check of latest activity date extract from loop only when the task is for the same Contact id
                   //  if(loopWhoId == string.valueof(evt.get('whoId'))) {
                     
                    //     If (loopLastHumanTouchDate < date.valueof(evt.get('ActivityDate'))) {
                         
                     //        loopLastHumanTouchDate  = date.valueof(evt.get('ActivityDate'));
                             
                             //added 10/10/18
                       //      loopEventId = string.valueof(evt.get('Id'));
                       //  }
                 
                         
                    // }
                     
                     //task belongs to another contact or who id so no need to check the latest activity date comparison with the loopactivitydate variable
                    // else{
                     
                         //before disturbing the loop variables add to the processing list -add that who id into the list that would update the contact with the latest activity date
                         //added 10/10/08-we may not need the below line as we have done it in the loop 
                         //parentTaskContact = new contact(Id = loopWhoId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate  );
                         //this I am not sure where place
                         //added on 10/10/18
                        // for (EventWhoRelation contactLoopToAddToUpdateList : [Select RelationId from EventWhoRelation where EventId =:string.valueof(evt.get('Id'))]) {
                                  //if (ContactLoopToAddToUpdateList != null ) {
                                  //string loopSourceValue_EventsLastTouch = contactLoopToAddToUpdateList.RelationId + '-- ' + loopEventId ;
                                  //string loopSourceValue_EventsLastTouch = 'goood';
                                  //parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate, SourceValue_EventsLastTouch__c = 'good');
                                  //parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate);
                                 // parentTaskContact = new contact(Id = loopWhoId,LastHumanTouchDate_Events__c= loopLastHumanTouchDate  );
                                 // if(!conList.contains(parentTaskContact)){
                                  //   conList.add(parentTaskContact);
                                 // }
                                //}
                                //else{
                                    //parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate, SourceValue_EventsLastTouch__c = 'good' );
                               // }
                        // }
                         
                         //populate the loop varibles with activity date from the next whoid or contact
                        // loopWhoId = string.valueof(evt.get('whoId'));
                        // loopLastHumanTouchDate  = date.valueof(evt.get('ActivityDate'));
                         //added 10/10/18
                        // loopEventId = string.valueof(evt.get('Id'));
                     
                   //  }
                
                 //}
                 
             }
             //before disturbing the loop variables add to the processing list -add that who id into the list that would update the contact with the latest activity date
             //parentTaskContact = new contact(Id = loopWhoId,LastHumanTouchDate_Events__c= loopLastHumanTouchDate  );
             //this I am not sure where place????
             //conList.add(parentTaskContact);
             //added on 10/10/18
           
          //   for (EventWhoRelation contactLoopToAddToUpdateList : [Select RelationId from EventWhoRelation where EventId = :loopEventId]) {
                // if (ContactLoopToAddToUpdateList != null ) {
                      //string loopSourceValue_EventsLastTouch = contactLoopToAddToUpdateList.RelationId+ '-- ' + loopEventId ;
                      //string loopSourceValue_EventsLastTouch = 'goood';
                      //parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate, SourceValue_EventsLastTouch__c = 'good');
                      // parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate);
                    //   parentTaskContact = new contact(Id = loopWhoId,LastHumanTouchDate_Events__c= loopLastHumanTouchDate  );
                  //   if(!conList.contains(parentTaskContact)){  
                   //      conList.add(parentTaskContact);
                   //  }
                    
                 //}
                 //else{
                      //parentTaskContact = new contact(Id = contactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate, SourceValue_EventsLastTouch__c = 'good' );
                // }
        //     }
             // parentEvtContact = new contact(Id = '003A000001Sw8pQ',SourceValue_EventsLastTouch__c = 'John Ryan GOOD-@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'); 
              //       if(!conList2.contains(parentEvtContact)){
               //          conList2.add(parentEvtContact);
                //     }
             //added 10/11/18
             // for (EventWhoRelation SecondaryContactLoopToAddToUpdateList : [Select RelationId from EventWhoRelation where EventId =:loopEventId and RelationId != :loopWhoId]) {
             //    if ((SecondaryContactLoopToAddToUpdateList) != null) {
                      //string loopSourceValue_EventsLastTouch = SecondaryContactLoopToAddToUpdateList.RelationId+ '-- ' + loopEventId ;
                      //parentTaskContact = new contact(Id = SecondaryContactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate, SourceValue_EventsLastTouch__c = loopSourceValue_EventsLastTouch);
               //       parentTaskContact = new contact(Id = SecondarycontactLoopToAddToUpdateList.RelationId,LastHumanTouchDate_Events__c = loopLastHumanTouchDate);
               //      conList.add(parentTaskContact);
               //  }
            // }
             //update happens after completing the for loop
             //update conList;
             
             //parentEvtContact = new contact(Id = '003A000001Sw8pQ',SourceValue_EventsLastTouch__c = 'John Ryan GOOD-@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'); 
             //        if(!conList2.contains(parentEvtContact)){
             //            conList2.add(parentEvtContact);
            // parentEvtContact = new contact(Id = '003A000001qL4T3',SourceValue_EventsLastTouch__c = 'Jonathan Gross-GOOD-!!!!!!!!!!!!!!!!!!');  
             //if(!conList2.contains(parentEvtContact)){
                         //conList2.add(parentEvtContact);
                  //   }
             // }
             insert ICMActivityFlatList;
            // update conList2;
        }catch (Exception e) {
            // exeption
             
            string error = 'Exception occured in  Batch Apex failed while processing the loop in Events or sObjects..... '+e.getMessage()+conList;
            system.debug('error catch at the exeption level-error catch bloc ------' +error);
            SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Event Data in Flat Object');
            String    msg = 'Exception Occurred in Apex while writing to Flat object from Events and EventWhoRelation \n '+e.getMessage() + ' ' +ICMActivityFlatList;
            se.sendEmail(msg);
            //system.debug('List of Record Types........'+'rt');
         }
    }
    global void finish(Database.BatchableContext BC)
    {
    }
}