public with sharing class PortfolioChannelRollupService {
   
    public Boolean upsertRollupRecords(List<Id> lstPortfolio,Id idChannel,String strYear,Integer intQuarter,String strVehicleType)
    {
        try{
            Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUpExisting = new Map<String,Portfolio_Channel_Rollup_Quarterly__c>();
            Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUpExisting = new Map<String,Portfolio_Territory_Rollup_Quarterly__c>();
            Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp = new Map<String,Portfolio_Channel_Rollup_Quarterly__c>();
            Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp = new Map<String,Portfolio_Territory_Rollup_Quarterly__c>();
            Map<String, Id> mapTickers = getFundTickers(lstPortfolio,strVehicleType);
            Map<String, Id> mapTerritory = getTerritoryMap(idChannel);
            Channel__c channel = getChannelObj(idChannel);
            String strChannel = channel.Channel_Name__c; 

            for(Portfolio_Channel_Rollup_Quarterly__c rollupResults : [SELECT Fund__r.Fund_Ticker_Symbol__c,Id,Channel__c,Fund__c,Year__c,Q1_AUM__c,Q1_NetSales_Last_Month__c,Q1_NetSales__c,Q1_AUM_BR__c,Q1_NetSales_Last_Month_BR__c,Q1_NetSales_BR__c,Q2_AUM__c,Q2_NetSales_Last_Month__c,Q2_NetSales__c,Q2_AUM_BR__c,Q2_NetSales_Last_Month_BR__c,Q2_NetSales_BR__c,Q3_AUM__c,Q3_NetSales_Last_Month__c,Q3_NetSales__c,Q3_AUM_BR__c,Q3_NetSales_Last_Month_BR__c,Q3_NetSales_BR__c,Q4_AUM__c,Q4_NetSales_Last_Month__c,Q4_NetSales__c,Q4_AUM_BR__c,Q4_NetSales_Last_Month_BR__c,Q4_NetSales_BR__c FROM Portfolio_Channel_Rollup_Quarterly__c WHERE Fund__c IN :lstPortfolio AND Channel__c =:idChannel AND Year__c =:strYear AND Fund_Vehicle_Type__c = :strVehicleType AND Territory__c = null]){
                mapChannelRollUpExisting.put(rollupResults.Fund__r.Fund_Ticker_Symbol__c,rollupResults);
            }
            for(Portfolio_Territory_Rollup_Quarterly__c rollupResults : [SELECT Fund__r.Fund_Ticker_Symbol__c,Id,Channel__c,Fund__c,Year__c,Q1_AUM__c,Q1_NetSales_Last_Month__c,Q1_NetSales__c,Q2_AUM__c,Q2_NetSales_Last_Month__c,Q2_NetSales__c,Q3_AUM__c,Q3_NetSales_Last_Month__c,Q3_NetSales__c,Q4_AUM__c,Q4_NetSales_Last_Month__c,Q4_NetSales__c,Territory__c,Territory__r.Territory_Name__c,Portfolio_Channel_Rollup_Id__c FROM Portfolio_Territory_Rollup_Quarterly__c WHERE Fund__c IN :lstPortfolio AND Channel__c =:idChannel AND Year__c =:strYear AND Fund_Vehicle_Type__c = :strVehicleType AND Territory__c != null]){
                mapTerritoryRollUpExisting.put(rollupResults.Fund__r.Fund_Ticker_Symbol__c+''+rollupResults.Territory__r.Territory_Name__c,rollupResults);
            }
            System.debug('mapChannelRollUpExisting -->'+mapChannelRollUpExisting);
            System.debug('mapTerritoryRollUpExisting -->'+mapTerritoryRollUpExisting);
            Portfolio_Channel_Rollup_Quarterly__c rollUpChannel;
            for(String key : mapTickers.keySet()){
                if(mapChannelRollUpExisting.containsKey(key)){
                    mapChannelRollUp.put(key,mapChannelRollUpExisting.get(key));
                }else{
                    rollUpChannel = new Portfolio_Channel_Rollup_Quarterly__c();
                    rollUpChannel.Name = key;
                    rollUpChannel.Fund__c = mapTickers.get(key);
                    rollUpChannel.Channel__c = idChannel;
                    rollUpChannel.Year__c = strYear;
                    rollUpChannel.Fund_Vehicle_Type__c = strVehicleType;
                    mapChannelRollUp.put(key,rollUpChannel);
                }
                System.debug('mapChannelRollUp -->'+mapChannelRollUp);
               
                for(String territoryKey : mapTerritory.keySet()){
                    System.debug('mapTerritoryRollUpExisting -->'+territoryKey);
                    String strFullTerritoryKey = key + '' +territoryKey;
                    System.debug('Else -->'+strFullTerritoryKey);
                    if(mapTerritoryRollUpExisting.containsKey(strFullTerritoryKey)){
                        mapTerritoryRollUp.put(strFullTerritoryKey,mapTerritoryRollUpExisting.get(strFullTerritoryKey));
                    }else{
                        System.debug('Else -->'+strFullTerritoryKey);
                        Portfolio_Territory_Rollup_Quarterly__c rollUpTerritory = new Portfolio_Territory_Rollup_Quarterly__c();
                        rollUpTerritory.Name = key;
                        rollUpTerritory.Fund__c = mapTickers.get(key);
                        rollUpTerritory.Channel__c = idChannel;
                        rollUpTerritory.Year__c = strYear;
                        rollUpTerritory.Fund_Vehicle_Type__c = strVehicleType;
                        rollUpTerritory.Territory__c = mapTerritory.get(territoryKey);
                        System.debug('rollUpTerritory -1->'+rollUpTerritory);
                        //rollUpTerritory.Portfolio_Channel_Rollup_Id__c = rollUpChannel.Id;
                        System.debug('rollUpTerritory -2->'+rollUpTerritory);
                        mapTerritoryRollUp.put(strFullTerritoryKey,rollUpTerritory);
                    }
                }
            }
            Date dStartDate = getQuarterDates(strYear,intQuarter,'startDate','Quarter');
            Date dEndDate =  getQuarterDates(strYear,intQuarter,'endDate','Quarter');

            if(strVehicleType == 'Mutual Fund'){
                if(channel.Channel_Type__c == 'SFDC'){
                    System.debug('mapChannelRollUp -->'+mapChannelRollUp);
                    System.debug('mapTerritoryRollUp -->'+mapTerritoryRollUp);
                    calculateMFAllChannels(mapChannelRollUp,mapTerritoryRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,channel.Channel_Description__c,intQuarter,strVehicleType,strYear);
                }
            }else if(strVehicleType == 'ETF'){
                if(channel.Channel_Type__c == 'SFDC'){
                    if(strChannel == 'VE - Financial Advisor'){
                        calculateSFDCFinancialAdvisorETF(mapChannelRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,channel.Channel_Description__c,intQuarter,strVehicleType);
                    }else if(strChannel == 'VE - Asset Manager'){
                        calculateSFDCAssetManagerETF(mapChannelRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,channel.Channel_Description__c,intQuarter,strVehicleType);
                    }else if(strChannel == 'VE - Institutional'){
                        calculateSFDCInstitutionalETF(mapChannelRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,channel.Channel_Description__c,intQuarter,strVehicleType);
                    }else if(strChannel == 'VE - RIA'){
                        calculateSFDCRIAETF(mapChannelRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,channel.Channel_Description__c,intQuarter,strVehicleType);
                    }
                }else if(channel.Channel_Type__c == 'Broadridge'){
                    if(strChannel == 'FINANCIAL ADVISOR'){
                        calculateBRFinancialAdvisorETF(mapChannelRollUp,mapTerritoryRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,strChannel,intQuarter,strVehicleType);
                    }else if(strChannel == 'ASSET MANAGER'){
                        calculateBRAssetManagerETF(mapChannelRollUp,mapTerritoryRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,strChannel,intQuarter,strVehicleType);
                    }else if(strChannel == 'INSTITUTIONAL'){
                        calculateBRInstitutionalETF(mapChannelRollUp,mapTerritoryRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,strChannel,intQuarter,strVehicleType);
                    }else if(strChannel == 'RIA'){
                        calculateBRRIAETF(mapChannelRollUp,mapTerritoryRollUp,lstPortfolio,mapTickers.keySet(),dStartDate,dEndDate,strChannel,intQuarter,strVehicleType);
                    }
                }
            }
        }catch(Exception ex){
            System.debug('Error : '+ex);
            throw ex;
        }
        return true;
    }

    private Map<String, Id> getFundTickers(List<Id> lstFunds,String strFundVehicleType){
        Map<String, Id> mapTickers = new Map<String, Id>();
        for(Fund__c fund : [SELECT Id,Fund_Ticker_Symbol__c FROM Fund__c WHERE Id =: lstFunds]){
            //mapTickers.put(strFundVehicleType == 'ETF' ? fund.Fund_Ticker_Symbol__c : convertFundTicker(fund.Fund_Ticker_Symbol__c), fund.Id);
            mapTickers.put(fund.Fund_Ticker_Symbol__c, fund.Id);
        }
        return mapTickers;
    }

    public String getChannelInfo(Id idChannel,String strType){
        List<Channel__c> lstChannels = [SELECT Id,Channel_Name__c,Channel_Description__c FROM Channel__c WHERE Id =: idChannel];
        if(lstChannels.size() > 0){
            if(strType == 'Name'){
                return lstChannels[0].Channel_Name__c;
            }else if(strType == 'Discription'){
                return lstChannels[0].Channel_Description__c;
            }
        }
        return null;
    }

    public Channel__c getChannelObj(Id idChannel){
        List<Channel__c> lstChannels = [SELECT Id,Channel_Name__c,Channel_Description__c,Channel_Type__c FROM Channel__c WHERE Id =: idChannel];
        if(lstChannels.size() > 0){
            return lstChannels[0];
        }
        return null;
    }

    public Date getQuarterDates(String strYear ,Integer iQuarter, String strMode,String strType)
    {
        Date returnDate;
        if(strMode == 'startDate'){
            returnDate = [Select startdate From Period Where type = :strType and FiscalYearSettings.Name = :strYear and Number = :iQuarter].startdate;
        }else if(strMode == 'enddate'){
            returnDate = [Select enddate From Period Where type = :strType and FiscalYearSettings.Name = :strYear and Number = :iQuarter].enddate;
        }
        return returnDate;
    }

    public Map<String,Id> getTerritoryMap(Id idChannel){
        Map<String, Id> mapResults = new Map<String, Id>();
        for(Territory__c territory: [SELECT Id,Territory_Name__c FROM Territory__c WHERE Channel__c = :idChannel AND Status__c = 'Active']){
            mapResults.put(territory.Territory_Name__c,territory.Id);
        }
        return mapResults;
    }

    private void calculateMFAllChannels(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setFunds,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType,String strYear){
        try{
            AggregateResult[] resultSfdc;
            AggregateResult[] resultTerritory;
            Integer iYear = Integer.valueof(strYear);
            if(iYear < 2021){
                resultSfdc = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets FROM History_Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c];
                resultTerritory = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets,SalesConnect_Account__r.Owner.User_Territory__c territory FROM History_Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c,SalesConnect_Account__r.Owner.User_Territory__c];
            }else{
                resultSfdc = [SELECT SFDC_Fund_Ticker__c fund,sum(salesconnect__current_assets__c) currentAssets FROM SalesConnect__Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND LastModifiedDate >= :dStartDate AND LastModifiedDate <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c];
                resultTerritory = [SELECT SFDC_Fund_Ticker__c fund,sum(salesconnect__current_assets__c) currentAssets,SalesConnect__Account__r.Owner.User_Territory__c territory FROM SalesConnect__Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND LastModifiedDate >= :dStartDate AND LastModifiedDate <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c,SalesConnect__Account__r.Owner.User_Territory__c];
            }           
            mapChannelRollUp = fillAggregateResultsMF(resultSfdc,mapChannelRollUp,iQuarter);
            mapTerritoryRollUp = fillAggregateResultsMFTerritory(resultTerritory,mapTerritoryRollUp,iQuarter);

            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateSFDCFinancialAdvisorETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //FinancialAdvisor query for SFDC
            AggregateResult[] resultSfdc = [SELECT SFDCFundID__r.Fund_Ticker_Symbol__c fund,sum(Fund_NNA__c) netSales, sum(Fund_YTM_NNA__c) netSalesLastMonth, sum(Monthly_Asset__c) aum 
                                            FROM ETF_Assets_By_Firm__c 
                                            WHERE Account__r.Channel__c = 'Financial Advisor' and SFDCFundID__r.Fund_Ticker_Symbol__c IN :setTickers AND Asset_Date__c >= :dStartDate AND  Asset_Date__c <= :dEndDate  group by SFDCFundID__r.Fund_Ticker_Symbol__c];
            mapChannelRollUp = fillAggregateResultsETF(resultSfdc,mapChannelRollUp,'sfdc',iQuarter,strVehicleType);
            //FinancialAdvisor query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Fund__r.Fund_Ticker_Symbol__c fund 
                                        FROM Firm_Focus_Fund_Channel_Territory__c 
                                        WHERE  Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate and Asset_Date__c = null and Line_of_Business_Description__c='Financial Advisor' and OfficeType__c='actual' and country__c='united states' and ((NOT SFDC_Parent_Firm_Name__c  like '%Bank Parent%') AND SFDC_Firm_Org_Type__c NOT in ('bank','private bank','Private Bank/Wealth Management')) and name IN :setTickers and territory__c<> null  AND DataView__c = 'Salesforce' group by Fund__r.Fund_Ticker_Symbol__c];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            upsert mapChannelRollUp.values();
        }catch(Exception e){
            System.debug(e);
            throw e;
        }
    }

    private void calculateBRFinancialAdvisorETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //FinancialAdvisor query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Fund__r.Fund_Ticker_Symbol__c fund 
                                        FROM Firm_Focus_Fund_Channel_Territory__c 
                                        WHERE  Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate and Asset_Date__c = null and Line_of_Business_Description__c='Financial Advisor' and OfficeType__c='actual' and country__c='united states' and ((NOT SFDC_Parent_Firm_Name__c  like '%Bank Parent%') AND SFDC_Firm_Org_Type__c NOT in ('bank','private bank','Private Bank/Wealth Management')) and name IN :setTickers and territory__c<> null  AND DataView__c = 'Salesforce' group by Fund__r.Fund_Ticker_Symbol__c];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            System.debug(resultBD);
            //FinancialAdvisor query for BD Territory
            AggregateResult[] resultTerritory = [SELECT Territory__c territory, Sum(Net_Flows_YTD__c) netSales,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Fund__r.Fund_Ticker_Symbol__c fund
                                            FROM Firm_Focus_Fund_Channel_Territory__c 
                                            WHERE  Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate and Asset_Date__c = null and Line_of_Business_Description__c='Financial Advisor' and OfficeType__c='actual' and country__c='united states' and ((NOT SFDC_Parent_Firm_Name__c  like '%Bank Parent%') AND SFDC_Firm_Org_Type__c NOT in ('bank','private bank','Private Bank/Wealth Management')) and name IN :setTickers and territory__c<> null  AND DataView__c = 'Salesforce' group by Fund__r.Fund_Ticker_Symbol__c,Territory__c];
            System.debug(resultTerritory);
            mapTerritoryRollUp = fillAggregateResultsTerritory(resultTerritory,mapTerritoryRollUp,'territory',iQuarter,strVehicleType);
            
            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            System.debug(e);
            throw e;
        }
    }

    private void calculateSFDCInstitutionalETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //Institutional query for SFDC
            AggregateResult[] resultSfdc = [SELECT Name fund, Sum(Net_Flows_YTD__c) netSales,Sum(Net_Flows_Last_Month__c) netSalesLastMonth,Sum(Net_Flow_Last_Month_Fidelity__c) aum 
                                            FROM Net_Sales_Data_For_Focus_Funds__c 
                                            WHERE  (Channel__c IN ('INSTITUTIONAL') OR (Channel__c IN ('ASSET MANAGER') and Territory__c = 'ASSET MANAGER INSTITUTIONAL')) AND Name IN :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate group by Name];
            mapChannelRollUp = fillAggregateResultsETF(resultSfdc,mapChannelRollUp,'sfdc',iQuarter,strVehicleType);
            //Institutional query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales ,Name fund, SUM(FundAssetBalance__c) aum ,SUM(Net_Flows_Last_Month__c) netSalesLastMonth
                                            FROM Firm_Focus_Fund_Channel_Territory__c 
                                            WHERE ((Channel__c = 'INSTITUTIONAL' AND Territory__c ='INSTITUTIONAL') OR (Channel__c ='ASSET MANAGER' AND Territory__c = 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            upsert mapChannelRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateBRInstitutionalETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //Institutional query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales ,Name fund, SUM(FundAssetBalance__c) aum ,SUM(Net_Flows_Last_Month__c) netSalesLastMonth
                                            FROM Firm_Focus_Fund_Channel_Territory__c 
                                            WHERE ((Channel__c = 'INSTITUTIONAL' AND Territory__c ='INSTITUTIONAL') OR (Channel__c ='ASSET MANAGER' AND Territory__c = 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            //Institutional query for BD Territory
            AggregateResult[] resultTerritory = [SELECT Sum(Net_Flows_YTD__c) netSales ,Name fund, SUM(FundAssetBalance__c) aum ,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Territory__c territory
                                                FROM Firm_Focus_Fund_Channel_Territory__c 
                                                WHERE ((Channel__c = 'INSTITUTIONAL' AND Territory__c ='INSTITUTIONAL') OR (Channel__c ='ASSET MANAGER' AND Territory__c = 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name,Territory__c];
            mapTerritoryRollUp = fillAggregateResultsTerritory(resultTerritory,mapTerritoryRollUp,'territory',iQuarter,strVehicleType);

            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateSFDCRIAETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //RIA query for SFDC
            AggregateResult[] resultSfdc = [SELECT Name fund, Sum(Net_Flows_YTD__c) netSales,Sum(Net_Flows_Last_Month__c) netSalesLastMonth,Sum(Net_Flow_Last_Month_Fidelity__c) aum 
                                            FROM Net_Sales_Data_For_Focus_Funds__c 
                                            WHERE  (Channel__c IN ('RIA') OR (Channel__c IN ('ASSET MANAGER') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL')) AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate group by Name];
            mapChannelRollUp = fillAggregateResultsETF(resultSfdc,mapChannelRollUp,'sfdc',iQuarter,strVehicleType);
            //RIA query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth  
                                        FROM Firm_Focus_Fund_Channel_Territory__c  
                                        WHERE Name  IN :setTickers AND Territory__c IN ('RIA West', 'RIA South','Asset Manager West','Asset Manager East','RIA North') AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate  AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            upsert mapChannelRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateBRRIAETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //RIA query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth  
                                        FROM Firm_Focus_Fund_Channel_Territory__c  
                                        WHERE Name  IN :setTickers AND Territory__c IN ('RIA West', 'RIA South','Asset Manager West','Asset Manager East','RIA North') AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate  AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            //RIA query for BD Territory
            AggregateResult[] resultTerritory = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Territory__c territory
                                                FROM Firm_Focus_Fund_Channel_Territory__c  
                                                WHERE Name  IN :setTickers AND Territory__c IN ('RIA West', 'RIA South','Asset Manager West','Asset Manager East','RIA North') AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate  AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name,Territory__c];
            mapTerritoryRollUp = fillAggregateResultsTerritory(resultTerritory,mapTerritoryRollUp,'territory',iQuarter,strVehicleType);

            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateSFDCAssetManagerETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //Asset Manager  query for SFDC
            AggregateResult[] resultSfdc = [SELECT Name fund, Sum(Net_Flows_YTD__c) netSales,Sum(Net_Flows_Last_Month__c) netSalesLastMonth,Sum(Net_Flow_Last_Month_Fidelity__c) aum  
                                            FROM Net_Sales_Data_For_Focus_Funds__c 
                                            WHERE  (Channel__c IN ('ASSET MANAGER') AND (Channel__c NOT IN ('INSTITUTIONAL') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate group by Name];
            mapChannelRollUp = fillAggregateResultsETF(resultSfdc,mapChannelRollUp,'sfdc',iQuarter,strVehicleType);
            //Asset Manager  query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth 
                                        FROM Firm_Focus_Fund_Channel_Territory__c 
                                        WHERE (Channel__c IN ('ASSET MANAGER') AND (Channel__c NOT IN ('INSTITUTIONAL') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            upsert mapChannelRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private void calculateBRAssetManagerETF(Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setTickers,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType){
        try{
            //Asset Manager  query for BD
            AggregateResult[] resultBD = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth 
                                        FROM Firm_Focus_Fund_Channel_Territory__c 
                                        WHERE (Channel__c IN ('ASSET MANAGER') AND (Channel__c NOT IN ('INSTITUTIONAL') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name];
            mapChannelRollUp = fillAggregateResultsETF(resultBD,mapChannelRollUp,'bd',iQuarter,strVehicleType);
            //Asset Manager  query for BD Territory
            AggregateResult[] resultTerritory = [SELECT Sum(Net_Flows_YTD__c) netSales,Name fund,SUM(FundAssetBalance__c) aum,SUM(Net_Flows_Last_Month__c) netSalesLastMonth,Territory__c territory
                                                FROM Firm_Focus_Fund_Channel_Territory__c 
                                                WHERE (Channel__c IN ('ASSET MANAGER') AND (Channel__c NOT IN ('INSTITUTIONAL') and Territory__c <> 'ASSET MANAGER INSTITUTIONAL'))  AND Name IN  :setTickers AND Asset_As_Of_Date__c >= :dStartDate AND  Asset_As_Of_Date__c <= :dEndDate AND Asset_Date__c = null AND DataView__c = 'Salesforce' GROUP BY Name,Territory__c];
            mapTerritoryRollUp = fillAggregateResultsTerritory(resultTerritory,mapTerritoryRollUp,'territory',iQuarter,strVehicleType);

            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            throw e;
        }
    }

    private Map<String,Portfolio_Channel_Rollup_Quarterly__c> fillAggregateResultsETF(AggregateResult[] results,Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,String strDataView,Integer iQuarter,String strVehicleType){
        for(AggregateResult result: results){
            String strKey = (String)result.get('fund');
            if(mapChannelRollUp.containsKey(strKey))
            {
                Portfolio_Channel_Rollup_Quarterly__c rollUp = mapChannelRollUp.get(strKey);
                if(strDataView == 'sfdc'){
                    if(iQuarter == 1){
                        rollUp.Q1_AUM__c = (decimal)result.get('aum');
                        rollUp.Q1_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q1_NetSales__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 2){
                        rollUp.Q2_AUM__c = (decimal)result.get('aum');
                        rollUp.Q2_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q2_NetSales__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 3){
                        rollUp.Q3_AUM__c = (decimal)result.get('aum');
                        rollUp.Q3_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q3_NetSales__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 4){
                        rollUp.Q4_AUM__c = (decimal)result.get('aum');
                        rollUp.Q4_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q4_NetSales__c = (decimal)result.get('netSales');
                    }
                }else if(strDataView == 'bd'){
                    if(iQuarter == 1){
                        rollUp.Q1_AUM_BR__c = (decimal)result.get('aum');
                        rollUp.Q1_NetSales_Last_Month_BR__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q1_NetSales_BR__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 2){
                        rollUp.Q2_AUM_BR__c = (decimal)result.get('aum');
                        rollUp.Q2_NetSales_Last_Month_BR__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q2_NetSales_BR__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 3){
                        rollUp.Q3_AUM_BR__c = (decimal)result.get('aum');
                        rollUp.Q3_NetSales_Last_Month_BR__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q3_NetSales_BR__c = (decimal)result.get('netSales');
                    }else if(iQuarter == 4){
                        rollUp.Q4_AUM_BR__c = (decimal)result.get('aum');
                        rollUp.Q4_NetSales_Last_Month_BR__c = (decimal)result.get('netSalesLastMonth');
                        rollUp.Q4_NetSales_BR__c = (decimal)result.get('netSales');
                    }
                }
                mapChannelRollUp.put(strKey,rollUp);
            }
        }
        return mapChannelRollUp;
    }

    private Map<String,Portfolio_Territory_Rollup_Quarterly__c> fillAggregateResultsTerritory(AggregateResult[] results,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,String strDataView,Integer iQuarter,String strVehicleType){
        String strFund;
        String strTerritory;
        String strKey;
        for(AggregateResult result: results){
            strFund = (String)result.get('fund');
            strTerritory = (String)result.get('territory');
            strKey = strFund + '' + strTerritory;
            if(mapTerritoryRollUp.containsKey(strKey))
            {
                Portfolio_Territory_Rollup_Quarterly__c rollUp = mapTerritoryRollUp.get(strKey);
                if(iQuarter == 1){
                    rollUp.Q1_AUM__c = (decimal)result.get('aum');
                    rollUp.Q1_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                    rollUp.Q1_NetSales__c = (decimal)result.get('netSales');
                }else if(iQuarter == 2){
                    rollUp.Q2_AUM__c = (decimal)result.get('aum');
                    rollUp.Q2_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                    rollUp.Q2_NetSales__c = (decimal)result.get('netSales');
                }else if(iQuarter == 3){
                    rollUp.Q3_AUM__c = (decimal)result.get('aum');
                    rollUp.Q3_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                    rollUp.Q3_NetSales__c = (decimal)result.get('netSales');
                }else if(iQuarter == 4){
                    rollUp.Q4_AUM__c = (decimal)result.get('aum');
                    rollUp.Q4_NetSales_Last_Month__c = (decimal)result.get('netSalesLastMonth');
                    rollUp.Q4_NetSales__c = (decimal)result.get('netSales');
                }
                mapTerritoryRollUp.put(strKey,rollUp);
            }
        }
        return mapTerritoryRollUp;
    }

    private Map<String,Portfolio_Channel_Rollup_Quarterly__c> fillAggregateResultsMF(AggregateResult[] results,Map<String,Portfolio_Channel_Rollup_Quarterly__c> mapChannelRollUp,Integer iQuarter){
        System.debug('Terr result '+results);
        System.debug('Terr mapTerritoryRollUp '+mapChannelRollUp);
        for(AggregateResult result: results){
            String strKey = (String)result.get('fund');
            if(mapChannelRollUp.containsKey(strKey))
            {
                Portfolio_Channel_Rollup_Quarterly__c rollUp = mapChannelRollUp.get(strKey);
                if(iQuarter == 1){
                    rollUp.Q1_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 2){
                    rollUp.Q2_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 3){
                    rollUp.Q3_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 4){
                    rollUp.Q4_Total_Assets__c = (decimal)result.get('currentAssets');
                }
                mapChannelRollUp.put(strKey,rollUp);
            }
        }
        return mapChannelRollUp;
    }

    private Map<String,Portfolio_Territory_Rollup_Quarterly__c> fillAggregateResultsMFTerritory(AggregateResult[] results,Map<String,Portfolio_Territory_Rollup_Quarterly__c> mapTerritoryRollUp,Integer iQuarter){
        String strFund;
        String strTerritory;
        String strKey;
        System.debug('Terr result '+results);
        System.debug('Terr mapTerritoryRollUp '+mapTerritoryRollUp);
        for(AggregateResult result: results){
            strFund = (String)result.get('fund');
            strTerritory = (String)result.get('territory');
            strKey = strFund + '' + strTerritory;
            if(mapTerritoryRollUp.containsKey(strKey))
            {
                Portfolio_Territory_Rollup_Quarterly__c rollUp = mapTerritoryRollUp.get(strKey);
                if(iQuarter == 1){
                    rollUp.Q1_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 2){
                    rollUp.Q2_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 3){
                    rollUp.Q3_Total_Assets__c = (decimal)result.get('currentAssets');
                }else if(iQuarter == 4){
                    rollUp.Q4_Total_Assets__c = (decimal)result.get('currentAssets');
                }
                mapTerritoryRollUp.put(strKey,rollUp);
            }
        }
        return mapTerritoryRollUp;
    }
    
}