<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_tasks_to_Henry_Mortlock</fullName>
        <description>Action for WFR called &quot;Assign tasks to Henry Mortlock&quot;
Update field and change assignment on task.</description>
        <field>OwnerId</field>
        <lookupValue>sfdcadmin2@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign tasks to Henry Mortlock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Task</fullName>
        <field>Status</field>
        <literalValue>Email Sent</literalValue>
        <name>Close Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Activity_Type</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Email</literalValue>
        <name>Email Activity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Assign_tasks_to_SFDC_Placeholder</fullName>
        <description>Action for WF Move to Status for &quot;Skip Status&quot;</description>
        <field>OwnerId</field>
        <lookupValue>sfdcadmin2@vaneck.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>FU Assign tasks to SFDC Placeholder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_NOTES_Populate_Task_Summary_recap</fullName>
        <description>For Tasks</description>
        <field>Description</field>
        <formula>IF ((Summary_Recap__c &lt;&gt; NULL &amp;&amp; Description &lt;&gt; NULL) , &apos; Summary Recap: &apos;+Summary_Recap__c  +BR() ,&apos;&apos;)</formula>
        <name>FieldU NOTES Populate Task Summary recap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_RIA_Activity_Score</fullName>
        <field>RIA_Activity_Score__c</field>
        <formula>IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; NOT(CONTAINS(Subject,&apos;Email:&apos;)) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp;

(
 ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;) || ISPICKVAL(US_Advisory_Call_Type__c, &apos;Follow Up Sales Call&apos;)
)
,20, 


IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; NOT(CONTAINS(Subject,&apos;Email:&apos;)) &amp;&amp; IsTask = true &amp;&amp;

(
 ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;) || ISPICKVAL(US_Advisory_Call_Type__c, &apos;Follow Up Sales Call&apos;)
)
,10, 

IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CONTAINS(Subject,&apos;Email:&apos;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;),2, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CONTAINS(Subject,&apos;Email:&apos;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Sales Call&quot;),3, 


IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Service Call&apos;),6, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Service Call&quot;),3, 

IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Email&apos;),2, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Sales Email&quot;),2, 

IF(ISPICKVAL(Status, &quot;Email Sent&quot;) &amp;&amp; IsTask = true &amp;&amp; NOT(ISPICKVAL(US_Advisory_Call_Type__c, &apos;&apos;)),2

,0 
 
)))))))))</formula>
        <name>FU RIA Task Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_RIA_Task_Score_NEW</fullName>
        <field>RIA_Activity_Score__c</field>
        <formula>IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; NOT(CONTAINS(Subject,&apos;Email:&apos;)) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; 

( 
 ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;) || ISPICKVAL(US_Advisory_Call_Type__c, &apos;Follow Up Sales Call&apos;) 
) 
,20, 


IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; NOT(CONTAINS(Subject,&apos;Email:&apos;)) &amp;&amp; IsTask = true &amp;&amp; 

( 
 ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;) || ISPICKVAL(US_Advisory_Call_Type__c, &apos;Follow Up Sales Call&apos;) 
) 
,10, 

IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CONTAINS(Subject,&apos;Email:&apos;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Call&apos;),2, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CONTAINS(Subject,&apos;Email:&apos;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Sales Call&quot;),3, 


IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Service Call&apos;),6, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Service Call&quot;),3, 

IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; CreatedBy.Alias = &apos;iambe&apos; &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &apos;Sales Email&apos;),2, 
IF(ISPICKVAL(Status, &quot;Call Completed&quot;) &amp;&amp; IsTask = true &amp;&amp; ISPICKVAL(US_Advisory_Call_Type__c, &quot;Sales Email&quot;),2, 

IF(ISPICKVAL(Status, &quot;Email Sent&quot;) &amp;&amp; IsTask = true &amp;&amp; NOT(ISPICKVAL(US_Advisory_Call_Type__c, &apos;&apos;)),2 

,0 

)))))))))</formula>
        <name>FU RIA Task Score NEW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Status_to_Skip_Status</fullName>
        <description>Action for WF Move to Status for &quot;Skip Status&quot;</description>
        <field>Status</field>
        <literalValue>Skip (Do not use).</literalValue>
        <name>FU Update Status to &quot;Skip Status&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FieldUpdate_CommentPreview</fullName>
        <field>Comments_Preview__c</field>
        <formula>Trim(Left(Description,255))</formula>
        <name>FieldUpdate-Task-CommentPreview</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FieldUpdate_Task_SumRecapCommentPreview</fullName>
        <field>Comments_Preview__c</field>
        <formula>Trim(Left(Description,255))</formula>
        <name>FieldUpdate-Task-SumRecapCommentPreview</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Purpose</fullName>
        <field>Purpose__c</field>
        <literalValue>Client Event</literalValue>
        <name>Purpose</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Report_Opt_Activity_Type_CF</fullName>
        <description>Update Report Opt Activity Type CF field from Report_Optimization_Activity_Type__c value</description>
        <field>Report_Opt_Activity_Type_CF__c</field>
        <formula>Report_Optimization_Activity_Type__c</formula>
        <name>Report Opt Activity Type CF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject</fullName>
        <field>Subject</field>
        <formula>Text(Activity_Type__c)</formula>
        <name>Update Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Summ_recap</fullName>
        <field>Summary_Recap__c</field>
        <formula>Subject</formula>
        <name>Update Summ_recap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Summary_Recap</fullName>
        <field>Summary_Recap__c</field>
        <formula>Subject</formula>
        <name>Update Summary Recap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Status_Clicked_Link</fullName>
        <field>Status</field>
        <literalValue>Clicked Link</literalValue>
        <name>Update Task Status Clicked Link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Status_Opened_Email</fullName>
        <field>Status</field>
        <literalValue>Opened Email</literalValue>
        <name>Update Task Status Opened Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_for_Log_a_Call</fullName>
        <description>Action for WFR called &quot;Update status when using &quot;Log a Call&quot;
Will update status to &quot;Call Completed&quot;.</description>
        <field>Status</field>
        <literalValue>Call Completed</literalValue>
        <name>Update status for &quot;Log a Call&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>%22Opens and Clicks%22 assigned to Jan</fullName>
        <actions>
            <name>FU_Assign_tasks_to_SFDC_Placeholder</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Status_to_Skip_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Jan van Eck</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Clicked Link in Email:,Opened Email:</value>
        </criteriaItems>
        <description>Handler for Marketo Activities assigned to Jan Van Eck - Transfer ownership to placeholder</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign tasks to Henry Mortlock</fullName>
        <actions>
            <name>Assign_tasks_to_Henry_Mortlock</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Summary_Recap__c</field>
            <operation>contains</operation>
            <value>Clicked Link in Email: ETF - AU Vector Insights</value>
        </criteriaItems>
        <description>Tasks related to &quot;Clicked Link in Email: ETF - AU Vector Insights&quot;
Created by Marketo API and assigned to Arian Neiron
Assign to Henry Mortlock</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Marketo MVIS Open Activities Clicked Link</fullName>
        <actions>
            <name>Update_Task_Status_Clicked_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Clicked Link</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Marketo MVIS Open Activities Opened Email</fullName>
        <actions>
            <name>Update_Task_Status_Opened_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Opened Email</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Marketo Open Activities Clicked Link</fullName>
        <actions>
            <name>Update_Task_Status_Clicked_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Clicked Link</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Marketo Open Activities Opened Email</fullName>
        <actions>
            <name>Update_Task_Status_Opened_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Opened Email</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Marketo and Marketo MVIS Open Activities Other</fullName>
        <actions>
            <name>Close_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo API,Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API,Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notContain</operation>
            <value>Clicked Link</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notContain</operation>
            <value>Opened Email</value>
        </criteriaItems>
        <description>Was Sent Email: update the status and close the task.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Activity Type</fullName>
        <actions>
            <name>Email_Activity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Purpose</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Email</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Task Summary recap-Task</fullName>
        <actions>
            <name>FieldUpdate_CommentPreview</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Description &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RIA Task Score</fullName>
        <actions>
            <name>FU_RIA_Task_Score_NEW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Internal_Sales_Score_Model__c</field>
            <operation>equals</operation>
            <value>RIA – ETF,RIA – Active</value>
        </criteriaItems>
        <description>Recalculate the score for emails sent.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Report Opt Activity Type CF Task</fullName>
        <actions>
            <name>Report_Opt_Activity_Type_CF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>notEqual</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Summary Recap</fullName>
        <actions>
            <name>Update_Summ_recap</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API,Marketo MVIS API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo API,Marketo MVIS API</value>
        </criteriaItems>
        <description>This workflow updates summary recap from the value sent by Marketo and Markeot MVIS to the Subject field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Summary Recap for Marketo Activities</fullName>
        <actions>
            <name>Close_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Summary_Recap</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Marketo API</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Subject</fullName>
        <actions>
            <name>Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Activity_Type__c</field>
            <operation>equals</operation>
            <value>Office Visits,Conference Call,Email,Call,Webinar,Other,Meeting,Conference,Regional Meeting</value>
        </criteriaItems>
        <description>Updates task&apos;s Subject according to the Activity Type

6/5/14 VK : BD,BP,RL dint know the reason behind this workflow rule creation in 2010. Since its causing issue with Marketo Task sync, I am deactivating the same.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update status when using %22Log a Call%22</fullName>
        <actions>
            <name>Update_status_for_Log_a_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Email Sent</value>
        </criteriaItems>
        <description>To avoid e-mails to be synched as calls. This rule will update the Task Status to &quot;Call Completed&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Update Obsolete Open Tasks %28Weekly Trade Idea%29</fullName>
        <active>false</active>
        <booleanFilter>((1 OR 2) AND 3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Opened Email:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Clicked Link in Email:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Weekly Trade Idea</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ActivityDate</field>
            <operation>notEqual</operation>
            <value>LAST 90 DAYS</value>
        </criteriaItems>
        <description>AU - Open Tasks (Weekly Trade Idea) older than 90 days will be moved to skip status: 
Assign to SFDC Placeholder &amp; close as &quot;Skip (Do not use)&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FU_Assign_tasks_to_SFDC_Placeholder</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>FU_Update_Status_to_Skip_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>89</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WFR-TaskCreation</fullName>
        <active>false</active>
        <formula>WhoId &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WorkFlowRule-Task-SummRecapUpdateForPreview</fullName>
        <actions>
            <name>FieldUpdate_Task_SumRecapCommentPreview</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Description &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
