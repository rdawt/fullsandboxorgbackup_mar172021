@isTest
//(SeeAllData=true)
private class batchContactUpdateForTasksTest  {

     @isTest  static void testbatchContactUpdateForTasks() {
     
      TaskTriggerHandler eth = new TaskTriggerHandler();
       DMLException e = null;
    Set<ID> accIds = new Set<ID>();
    List<Contact> conlist = new List<Contact>();
    
    Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    
    Account acc = new Account(Name = 'WFA-Unbranched-onlyforTest**', BillingCountry='United States',Channel__c='Insurance');
    insert acc;
    Contact con = new Contact(LastName='Aalund',FirstName='Gail',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
    insert con;
   // Contact con1 = new Contact(LastName='Aalund',FirstName='Gail2',AccountId=acc.Id,MailingStreet='xyz', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
  //  insert con1;
 //   Contact con2 = new Contact(LastName='Aalund',FirstName='Gail3',AccountId=acc.Id,MailingStreet='xyz1', MailingCountry='United States',OwnerId ='005A0000000Oqpm'); //,OwnerId ='005A0000000Oqpm'
  //  insert con2;
 //   Contact con3 = new Contact(LastName='Aalund',FirstName='Gail4',AccountId=acc.Id,MailingStreet='999', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
 //   insert con3;
  //  Contact con4 = new Contact(LastName='Aalund',FirstName='Gail3',AccountId=acc.Id, Email='test@abc.com', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
 //   insert con4;
    
   // List<Task> newTaskList = new List<Task>();
    string Comments;
 //   Comments = 'The Community Registrant '+'my first name'+ ' '+'mylast name'+ ' (Id: '+'0000'+')';
 //   Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
 //   Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+con2.Id+'\n\n';
 //   Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
    
    //Subject = \'meeting\' AND Result__c = \'held\'
    Task e1 = new Task(Description = Comments, Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today(), ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
    Comments = Comments+'2nd task for con with who id in task object';
    
  //  Event e2 = new Event(Description = Comments,Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap2',
  //                                      ActivityDate = System.today()+5, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today());  
  //  Comments = Comments+'3rd task for con with who id in task object';
  //  
  //  Event e3 = new Event(Description = Comments, Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
  //                                      ActivityDate = System.today()-5, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=20,ActivityDateTime=System.today());
                                        
    insert e1;
   // insert e2;
   // insert e3;
    
  //  List<Event> evt = new List<Event>();
    
    //List<EventWhoRelation> newEventWhoRelList = new List<EventWhoRelation>();

    //newEventWhoRelList.add(new EventWhoRelation(EventId=t1.Id));
    //newTaskRelList.add(new TaskRelation(RelationId=con1.Id,TaskId=t1.Id));
   // newTaskRelList.add(new TaskRelation(RelationId=con2.Id,TaskId=t1.Id));
    //newTaskRelList.add(new TaskRelation(RelationId=con3.Id,TaskId=t1.Id));
    //newTaskRelList.add(new TaskRelation(RelationId=con4.Id,TaskId=t2.Id));
   // newTaskRelList.add(new TaskRelation(RelationId=con2.Id,TaskId=t3.Id));
    
    //insert  newTaskRelList;
    
    ICMDataFeedCriteria__c ICMDataFeedCriteria = new  ICMDataFeedCriteria__c ();
        ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c ='5';
            insert ICMDataFeedCriteria;
         string criteria = 'last_N_days:'+ICMDataFeedCriteria.ICMDailyDataExtractCriteria__c;
    
   // **start teset**
    Test.startTest();
        try{
               // string jobId = System.schedule('batchContactUpdateScheduleApex', '0 0 0 5 7 ? 2022',  new BatchScheduleContactUpdateForEvents() ); //BatchScheduleContactUpdateForEvents
                
                batchContactUpdateForTasks batchJobForXtractingTasks = new batchContactUpdateForTasks();
                dataBase.executeBatch(batchJobForXtractingtasks,200 );
              //  string jobId = System.schedule('batchContactUpdateScheduleApex', '0 0 0 5 7 ? 2022',  new BatchScheduleContactUpdate());
               
             //   CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
               // database.executebatch(b,200);
             //   System.assertEquals('0 0 0 5 7 ? 2022', ct.CronExpression);
             //   System.assertEquals(0, ct.TimesTriggered);
             //   System.assertEquals('2022-07-05 00:00:00',String.valueOf(ct.NextFireTime));
             // string loopEventId;
            //  for(Event loopEvt : evt) {
            //      try{
                  
             //         loopEventId = string.valueof(loopEvt.get('Id'));
             //         List<EventWhoRelation> TempICMActivityFlatList =  [Select Event.ID,FORMAT(Event.ActivityDate),FORMAT(Event.ActivityDateTime),Event.Subject,Event.Summary_Recap__c,relationid,Event.result__c,Event.OwnerId,Event.CreatedById,FORMAT(Event.CreatedDate),Event.LastModifiedById,FORMAT(Event.LastModifiedDate),Event.ETF_Team__c From EventWhoRelation Where Event.whoid != null and eventid = :loopeventId   ORDER BY event.whoid,event.activitydate desc];
              
                  
              //    }catch(Exception except0){}
              
              
             // }
             
             
    
       
    }catch(Exception except1){
    System.assert(except1!=null);
     }
    Test.stopTest();
   // **end test**
    
    System.assert(e==null);
    
    }
}